#!/bin/bash
#============================================================================
#title          :Unlock Interface Project
#description    :Project web and gui interface combined with a library
#author         :Jasper Koehorst
#date           :2021
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# Ensure that node is not available and the version provided by vaadin will be used
sudo apt-get remove nodejs

# ////////////////////////////////////////////////////////////////////////////////////
$DIR/gradlew build -Dvaadin.productionMode=true -x test
