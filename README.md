## Interface for the unlock metadata infrastructure

This interface has been developed to collect and validate metadata. When doing research it is crucial that information about the project, what is being investigated, who is involved, which samples are taken and what data has been collected is recorded in an consistent manner. With this interface users will be able to generate and validate metadatasets regarding research projects.

## Running the application

The application can be obtained from http://download.systemsbiology.nl/unlock. After download the program can be started by `java -jar -Dserver.port=8082 fairds-<version>.jar` in which `-Dserver.port=8082` sets the port that will be used by the application.

## Development

To continue development with this package you need several programs. 
We currently use Intellij in combination with SDKMAN for Java 11

- IntelliJ: https://www.jetbrains.com/idea/
- SDKMAN: https://sdkman.io

## Java dependencies:

Currently there are two packages that are not available through the maven repository. A function has been implemented in gradle which will download the JAR files. (There is also a `build.sh` script which will be able to download the jar files from our repository and install them in your local maven repository but should not be needed in the long run.)

The two packages are:

- iRODS available via https://github.com/DICE-UNC/jargon/releases/download/4.3.0.2-RELEASE/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar
- UNLOCK Ontology available via http://download.systemsbiology.nl/unlock/UnlockOntology.jar

The iRODS package is required for the authentication to an iRODS instance where data and metadata can be processed.
The UNLOCK ontology package contains the ISA ontology in combination with UNLOCK extensions allowing metadata files to be processed, validated and transformed into RDF statements. The ontology can be found at http://git.wur.nl/unlock/ontology

## Starting the project
In intellij you can open this folder and it should recognise it as a gradle project. It will then start loading, building and indexing all the files that are available. Once it is completed you should be able to see the different gradle options on the right. Make sure that the two jar (iRODS and UNLOCK ontology) are already present in the root of this folder. 

## Running the code in development mode
During development you need to open the code base in IntelliJ and load it as a gradle project. Once it is successfully loaded and compiled there should be an elephant / Gradle button available on the right top side of the window. This contains all possible tasks available for the unlock-interface project. Under tasks there should be Application with the bootRun option. By clicking the bootRun option it will start the web interface. Once it is successfully started you can access the application at http://localhost:8081. Further configuration can be done via the *application.properties* file.

## Running the code in production mode
To start the program in production mode you need to compile the code into a jar package. There is a startProduction.sh script available that will pull in the latest version available and build with production mode enabled through `./gradlew build -Dvaadin.productionMode=true`. Once it is completed you can start the program using `java -jar -Dserver.port=8082 web/build/libs/web-1.0.1.jar` in which in this case it will be available under http://localhost:8082.

## Metadata
An important part of the metadata registration form is the metadata.xlsx file that is incorporated into the resource (src/main/resources/metadata.xlsx). This excel file contains the elements that are optional or required for the different levels (Project, Investigation, etc...). Each level contains some obligatory elements such as the identifier or description of an object and can easily be modified as indicated by the `Requirement` column. 

Each sheet contains the following columns: 

```
# Package: makes a distinction between different metadata packages available (Currently only supported for observation unit, sample and assay types)
Structured comment name: The identifier of the property (e.g., carb_dioxide)
Item (rdfs:label): A human readable label which is used in the excel headers and in the ontology as rdfs:label for the properties
Requirement: Mandatory (M) or Optional (X)
Value syntax: The format used (numeric, data, string, unit) validated through regular expressions (e.g., {float} {unit})
Example: An example of the value and how it is defined (e.g., 410 parts per million)
URL: RDF property URL when defined otherwise will switch to default URL space + /<structured comment name>
Preferred unit: The unit of measurement that is preferred but not obligatory
Definition: The definition of the structured comment name
```

The sheets, Project, Investigation, Study, ObservationUnit, Sample and Assay are obligatory sheets the content of the optional properties can be freely adjusted. New ObservationUnit, Sample and Assay types can be created by creating a new row with a different `# Package name`. These packages are an extension on the core package which contains elements that are shared among the packages defined.

## Ontology
The metadata infrastructure is supported by an ontology that is based on the ISA schema from the JERM Ontology. It has been further expanded with project information, users and observation units. The latest version of the ontology is available at http://git.wur.nl/unlock/ontology and we are looking for collaborators to imporve this further!
