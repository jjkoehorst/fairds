package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class EntityImpl extends OWLThingImpl implements Entity {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Entity";

  protected EntityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Entity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new EntityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Entity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Entity.class,false);
          if(toRet == null) {
            toRet = new EntityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Entity)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.EntityImpl expected");
        }
      }
      return (Entity)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }
}
