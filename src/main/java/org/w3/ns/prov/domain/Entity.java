package org.w3.ns.prov.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Entity extends OWLThing {
  String getIdentifier();

  void setIdentifier(String val);
}
