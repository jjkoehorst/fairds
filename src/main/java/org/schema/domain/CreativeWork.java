package org.schema.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://schema.org/ ontology
 */
public interface CreativeWork extends OWLThing {
}
