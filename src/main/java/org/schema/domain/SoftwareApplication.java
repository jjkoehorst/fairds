package org.schema.domain;

import java.lang.String;

/**
 * Code generated from http://schema.org/ ontology
 */
public interface SoftwareApplication extends CreativeWork {
  String getName();

  void setName(String val);

  String getSoftwareVersion();

  void setSoftwareVersion(String val);

  String getDownloadUrl();

  void setDownloadUrl(String val);
}
