package org.schema.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.schema.domain.Organization;

/**
 * Code generated from http://schema.org/ ontology
 */
public class OrganizationImpl extends OWLThingImpl implements Organization {
  public static final String TypeIRI = "http://schema.org/Organization";

  protected OrganizationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Organization make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new OrganizationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Organization.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Organization.class,false);
          if(toRet == null) {
            toRet = new OrganizationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Organization)) {
          throw new RuntimeException("Instance of org.schema.domain.impl.OrganizationImpl expected");
        }
      }
      return (Organization)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getSameAs() {
    return this.getExternalRef("http://schema.org/sameAs",true);
  }

  public void setSameAs(String val) {
    this.setExternalRef("http://schema.org/sameAs",val);
  }

  public String getLegalName() {
    return this.getStringLit("http://schema.org/legalName",true);
  }

  public void setLegalName(String val) {
    this.setStringLit("http://schema.org/legalName",val);
  }
}
