package org.schema.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.schema.domain.Organization;
import org.schema.domain.Person;

/**
 * Code generated from http://schema.org/ ontology
 */
public class PersonImpl extends OWLThingImpl implements Person {
  public static final String TypeIRI = "http://schema.org/Person";

  protected PersonImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Person make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PersonImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Person.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Person.class,false);
          if(toRet == null) {
            toRet = new PersonImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Person)) {
          throw new RuntimeException("Instance of org.schema.domain.impl.PersonImpl expected");
        }
      }
      return (Person)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/email");
    this.checkCardMin1("http://schema.org/familyName");
    this.checkCardMin1("http://schema.org/givenName");
  }

  public void remMemberOf(Organization val) {
    this.remRef("http://schema.org/memberOf",val,true);
  }

  public List<? extends Organization> getAllMemberOf() {
    return this.getRefSet("http://schema.org/memberOf",true,Organization.class);
  }

  public void addMemberOf(Organization val) {
    this.addRef("http://schema.org/memberOf",val);
  }

  public String getJobTitle() {
    return this.getStringLit("http://schema.org/jobTitle",true);
  }

  public void setJobTitle(String val) {
    this.setStringLit("http://schema.org/jobTitle",val);
  }

  public String getEmail() {
    return this.getExternalRef("http://schema.org/email",false);
  }

  public void setEmail(String val) {
    this.setExternalRef("http://schema.org/email",val);
  }

  public String getFamilyName() {
    return this.getStringLit("http://schema.org/familyName",false);
  }

  public void setFamilyName(String val) {
    this.setStringLit("http://schema.org/familyName",val);
  }

  public String getDepartment() {
    return this.getStringLit("http://schema.org/department",true);
  }

  public void setDepartment(String val) {
    this.setStringLit("http://schema.org/department",val);
  }

  public String getOrcid() {
    return this.getExternalRef("http://fairbydesign.nl/ontology/orcid",true);
  }

  public void setOrcid(String val) {
    this.setExternalRef("http://fairbydesign.nl/ontology/orcid",val);
  }

  public String getGivenName() {
    return this.getStringLit("http://schema.org/givenName",false);
  }

  public void setGivenName(String val) {
    this.setStringLit("http://schema.org/givenName",val);
  }
}
