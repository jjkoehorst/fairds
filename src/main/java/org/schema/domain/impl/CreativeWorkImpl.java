package org.schema.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.schema.domain.CreativeWork;

/**
 * Code generated from http://schema.org/ ontology
 */
public class CreativeWorkImpl extends OWLThingImpl implements CreativeWork {
  public static final String TypeIRI = "http://schema.org/CreativeWork";

  protected CreativeWorkImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static CreativeWork make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CreativeWorkImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,CreativeWork.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,CreativeWork.class,false);
          if(toRet == null) {
            toRet = new CreativeWorkImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof CreativeWork)) {
          throw new RuntimeException("Instance of org.schema.domain.impl.CreativeWorkImpl expected");
        }
      }
      return (CreativeWork)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
