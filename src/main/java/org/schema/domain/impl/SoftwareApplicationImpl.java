package org.schema.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.schema.domain.SoftwareApplication;

/**
 * Code generated from http://schema.org/ ontology
 */
public class SoftwareApplicationImpl extends CreativeWorkImpl implements SoftwareApplication {
  public static final String TypeIRI = "http://schema.org/SoftwareApplication";

  protected SoftwareApplicationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SoftwareApplication make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SoftwareApplicationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SoftwareApplication.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SoftwareApplication.class,false);
          if(toRet == null) {
            toRet = new SoftwareApplicationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SoftwareApplication)) {
          throw new RuntimeException("Instance of org.schema.domain.impl.SoftwareApplicationImpl expected");
        }
      }
      return (SoftwareApplication)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/name");
    this.checkCardMin1("http://schema.org/softwareVersion");
  }

  public String getName() {
    return this.getStringLit("http://schema.org/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://schema.org/name",val);
  }

  public String getSoftwareVersion() {
    return this.getStringLit("http://schema.org/softwareVersion",false);
  }

  public void setSoftwareVersion(String val) {
    this.setStringLit("http://schema.org/softwareVersion",val);
  }

  public String getDownloadUrl() {
    return this.getExternalRef("http://schema.org/downloadUrl",true);
  }

  public void setDownloadUrl(String val) {
    this.setExternalRef("http://schema.org/downloadUrl",val);
  }
}
