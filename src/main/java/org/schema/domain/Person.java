package org.schema.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://schema.org/ ontology
 */
public interface Person extends OWLThing {
  void remMemberOf(Organization val);

  List<? extends Organization> getAllMemberOf();

  void addMemberOf(Organization val);

  String getJobTitle();

  void setJobTitle(String val);

  String getEmail();

  void setEmail(String val);

  String getFamilyName();

  void setFamilyName(String val);

  String getDepartment();

  void setDepartment(String val);

  String getOrcid();

  void setOrcid(String val);

  String getGivenName();

  void setGivenName(String val);
}
