package org.purl.ppeo.PPEO.owl.domain;

import java.lang.String;
import java.util.List;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.process;

/**
 * Code generated from http://purl.org/ppeo/PPEO.owl# ontology
 */
public interface observation_unit extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remHasPart(Sample val);

  List<? extends Sample> getAllHasPart();

  void addHasPart(Sample val);
}
