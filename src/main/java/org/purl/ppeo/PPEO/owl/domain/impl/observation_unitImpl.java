package org.purl.ppeo.PPEO.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.impl.processImpl;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

/**
 * Code generated from http://purl.org/ppeo/PPEO.owl# ontology
 */
public class observation_unitImpl extends processImpl implements observation_unit {
  public static final String TypeIRI = "http://purl.org/ppeo/PPEO.owl#observation_unit";

  protected observation_unitImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static observation_unit make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new observation_unitImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,observation_unit.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,observation_unit.class,false);
          if(toRet == null) {
            toRet = new observation_unitImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof observation_unit)) {
          throw new RuntimeException("Instance of org.purl.ppeo.PPEO.owl.domain.impl.observation_unitImpl expected");
        }
      }
      return (observation_unit)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remHasPart(Sample val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Sample> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Sample.class);
  }

  public void addHasPart(Sample val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
