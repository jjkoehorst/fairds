package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Data_sample extends Asset {
  void remHasPart(Data_sample val);

  List<? extends Data_sample> getAllHasPart();

  void addHasPart(Data_sample val);

  String getIdentifier();

  void setIdentifier(String val);
}
