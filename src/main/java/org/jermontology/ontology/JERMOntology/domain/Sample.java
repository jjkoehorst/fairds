package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Sample extends Material_entity {
  String getIdentifier();

  void setIdentifier(String val);

  String getDescription();

  void setDescription(String val);

  String getName();

  void setName(String val);

  Person getAccountablePerson();

  void setAccountablePerson(Person val);

  void remHasContributor(Person val);

  List<? extends Person> getAllHasContributor();

  void addHasContributor(Person val);

  void remHasPart(Assay val);

  List<? extends Assay> getAllHasPart();

  void addHasPart(Assay val);
}
