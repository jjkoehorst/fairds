package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Project extends Information_entity {
  String getIdentifier();

  void setIdentifier(String val);

  void remHasPart(Investigation val);

  List<? extends Investigation> getAllHasPart();

  void addHasPart(Investigation val);
}
