package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Information_entity extends Entity {
  String getIdentifier();

  void setIdentifier(String val);
}
