package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Investigation extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remHasPart(Study val);

  List<? extends Study> getAllHasPart();

  void addHasPart(Study val);
}
