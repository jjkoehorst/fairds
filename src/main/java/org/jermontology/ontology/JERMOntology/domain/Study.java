package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Study extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remHasPart(observation_unit val);

  List<? extends observation_unit> getAllHasPart();

  void addHasPart(observation_unit val);
}
