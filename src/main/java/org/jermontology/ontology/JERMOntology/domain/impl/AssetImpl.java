package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Asset;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class AssetImpl extends Information_entityImpl implements Asset {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Asset";

  protected AssetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Asset make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AssetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Asset.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Asset.class,false);
          if(toRet == null) {
            toRet = new AssetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Asset)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.AssetImpl expected");
        }
      }
      return (Asset)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }
}
