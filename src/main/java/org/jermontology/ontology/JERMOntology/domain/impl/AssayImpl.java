package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class AssayImpl extends processImpl implements Assay {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Assay";

  protected AssayImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Assay make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AssayImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Assay.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Assay.class,false);
          if(toRet == null) {
            toRet = new AssayImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Assay)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.AssayImpl expected");
        }
      }
      return (Assay)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remHasPart(Data_sample val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Data_sample> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Data_sample.class);
  }

  public void addHasPart(Data_sample val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
