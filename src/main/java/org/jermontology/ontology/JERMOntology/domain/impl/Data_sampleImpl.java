package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class Data_sampleImpl extends AssetImpl implements Data_sample {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Data_sample";

  protected Data_sampleImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Data_sample make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new Data_sampleImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Data_sample.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Data_sample.class,false);
          if(toRet == null) {
            toRet = new Data_sampleImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Data_sample)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.Data_sampleImpl expected");
        }
      }
      return (Data_sample)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public void remHasPart(Data_sample val) {
    this.remRef("http://schema.org/hasPart",val,true);
  }

  public List<? extends Data_sample> getAllHasPart() {
    return this.getRefSet("http://schema.org/hasPart",true,Data_sample.class);
  }

  public void addHasPart(Data_sample val) {
    this.addRef("http://schema.org/hasPart",val);
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }
}
