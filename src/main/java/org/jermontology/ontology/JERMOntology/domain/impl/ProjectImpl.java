package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Project;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class ProjectImpl extends Information_entityImpl implements Project {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Project";

  protected ProjectImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Project make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProjectImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Project.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Project.class,false);
          if(toRet == null) {
            toRet = new ProjectImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Project)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.ProjectImpl expected");
        }
      }
      return (Project)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remHasPart(Investigation val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Investigation> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Investigation.class);
  }

  public void addHasPart(Investigation val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
