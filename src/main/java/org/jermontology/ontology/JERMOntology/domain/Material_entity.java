package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Material_entity extends OWLThing {
  String getIdentifier();

  void setIdentifier(String val);

  String getDescription();

  void setDescription(String val);

  String getName();

  void setName(String val);

  Person getAccountablePerson();

  void setAccountablePerson(Person val);

  void remHasContributor(Person val);

  List<? extends Person> getAllHasContributor();

  void addHasContributor(Person val);
}
