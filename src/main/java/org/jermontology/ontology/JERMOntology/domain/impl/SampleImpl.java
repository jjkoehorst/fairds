package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class SampleImpl extends Material_entityImpl implements Sample {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Sample";

  protected SampleImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Sample make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SampleImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Sample.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Sample.class,false);
          if(toRet == null) {
            toRet = new SampleImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Sample)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.SampleImpl expected");
        }
      }
      return (Sample)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/description");
    this.checkCardMin1("http://schema.org/name");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public String getName() {
    return this.getStringLit("http://schema.org/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://schema.org/name",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }

  public void remHasContributor(Person val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasContributor",val,true);
  }

  public List<? extends Person> getAllHasContributor() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasContributor",true,Person.class);
  }

  public void addHasContributor(Person val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasContributor",val);
  }

  public void remHasPart(Assay val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Assay> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Assay.class);
  }

  public void addHasPart(Assay val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
