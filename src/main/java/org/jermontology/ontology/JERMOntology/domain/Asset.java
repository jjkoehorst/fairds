package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Asset extends Information_entity {
  String getIdentifier();

  void setIdentifier(String val);
}
