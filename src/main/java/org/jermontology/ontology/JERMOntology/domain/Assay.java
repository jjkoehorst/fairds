package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Assay extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remHasPart(Data_sample val);

  List<? extends Data_sample> getAllHasPart();

  void addHasPart(Data_sample val);
}
