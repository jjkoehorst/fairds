package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface process extends OWLThing {
  String getIdentifier();

  void setIdentifier(String val);
}
