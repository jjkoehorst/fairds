package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Material_entity;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class Material_entityImpl extends OWLThingImpl implements Material_entity {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Material_entity";

  protected Material_entityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Material_entity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new Material_entityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Material_entity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Material_entity.class,false);
          if(toRet == null) {
            toRet = new Material_entityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Material_entity)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.Material_entityImpl expected");
        }
      }
      return (Material_entity)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/description");
    this.checkCardMin1("http://schema.org/name");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public String getName() {
    return this.getStringLit("http://schema.org/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://schema.org/name",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }

  public void remHasContributor(Person val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasContributor",val,true);
  }

  public List<? extends Person> getAllHasContributor() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasContributor",true,Person.class);
  }

  public void addHasContributor(Person val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasContributor",val);
  }
}
