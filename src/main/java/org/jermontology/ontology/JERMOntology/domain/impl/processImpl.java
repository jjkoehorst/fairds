package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.process;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class processImpl extends OWLThingImpl implements process {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#process";

  protected processImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static process make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new processImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,process.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,process.class,false);
          if(toRet == null) {
            toRet = new processImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof process)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.processImpl expected");
        }
      }
      return (process)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }
}
