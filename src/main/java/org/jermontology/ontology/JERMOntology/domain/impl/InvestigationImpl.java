package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Study;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class InvestigationImpl extends processImpl implements Investigation {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Investigation";

  protected InvestigationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Investigation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InvestigationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Investigation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Investigation.class,false);
          if(toRet == null) {
            toRet = new InvestigationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Investigation)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.InvestigationImpl expected");
        }
      }
      return (Investigation)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remHasPart(Study val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Study> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Study.class);
  }

  public void addHasPart(Study val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
