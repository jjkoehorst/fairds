package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Information_entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class Information_entityImpl extends EntityImpl implements Information_entity {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Information_entity";

  protected Information_entityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Information_entity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new Information_entityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Information_entity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Information_entity.class,false);
          if(toRet == null) {
            toRet = new Information_entityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Information_entity)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.Information_entityImpl expected");
        }
      }
      return (Information_entity)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }
}
