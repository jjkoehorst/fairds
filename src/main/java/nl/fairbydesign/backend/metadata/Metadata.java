package nl.fairbydesign.backend.metadata;

import java.io.File;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Metadata object based on MIXS (excel) file adapted to ISA structure.
 */
public class Metadata implements Serializable {
    private static final long serialVersionUID = 7208928741154500357L;

    private String sheetName;
    private String packageName;
    private String packageIdentifier;
    private String requirement;
    private String syntax;
    private String example;
    private String URL;
    private String definition;
    private Pattern regex;
    private String preferredUnit;
    private String label;
    private boolean file;
    private String sessionID = "no_session";
    private File ontology;


//    @Override
//    public boolean equals(Object o) {
//        if (o == this)
//            return true;
//        if (!(o instanceof Metadata))
//            return false;
//        Metadata other = (Metadata) o;
//        return (this.structuredCommentName == null && other.structuredCommentName == null)
//                || (this.structuredCommentName != null && this.structuredCommentName.equals(other.structuredCommentName));
//    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String stringCellValue) {
        this.packageName = stringCellValue;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }

    public String getSyntax() {
        return syntax;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public String getExample() {
        return example;
    }

    public void setURL(String url) {
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getDefinition() {
        return definition;
    }

    public void setRegex(Pattern regex) {
        this.regex = regex;
    }

    public Pattern getRegex() {
        return regex;
    }

    public void setPreferredUnit(String preferredUnit) {
        this.preferredUnit = preferredUnit;
    }

    public String getPreferredUnit() {
        return preferredUnit;
    }

    public void setLabel(String label) {
        this.label = label.toLowerCase();
    }

    public String getLabel() {
        return label;
    }

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public void setPackageIdentifier(String packageIdentifier) {
        this.packageIdentifier = packageIdentifier;
    }

    public String getPackageIdentifier() {
        return packageIdentifier;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public void setOntology(File ontology) {
        this.ontology = ontology;
    }

    public File getOntology() {
        return ontology;
    }
}
