package nl.fairbydesign.backend.metadata;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static nl.fairbydesign.backend.ApplicationServiceInitListener.storage;
import static nl.fairbydesign.backend.WebGeneric.checkNotEmpty;
import static nl.fairbydesign.backend.WebGeneric.getStringValueCell;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

/**
 * Parser to transform the excel file into objects including regex information based on metadata types.
 */
public class MetadataParser extends Metadata {
    public static final Logger logger = Logger.getLogger(MetadataParser.class);
    static File xlsxFile = new File("./fairds_storage/metadata.xlsx");

    public static void setXlsxFile(File xlsxFile2) {
        xlsxFile = xlsxFile2;
    }

    /**
     * @param sheet Excel sheet containing metadata information
     * @return metadata object
     */
    public static ArrayList<Metadata> parser(Sheet sheet) {
        ArrayList<Metadata> metadataSet = new ArrayList<>();

        logger.debug("Parsing excel sheet: " + sheet.getSheetName());
        Row headerRow = sheet.getRow(0);
        HashMap<String, Integer> index = new HashMap<>();
        headerRow.cellIterator().forEachRemaining(h -> {
            String value = h.getStringCellValue().replace("#", "").trim().toLowerCase();
            index.put(value, h.getColumnIndex());
        });

        for (Row row : sheet) {
            if (checkRow(row)) {
                // Create metadata object
                // # Package name
                // Package identifier
                // Item (rdfs:label)
                // Requirement
                // Value syntax
                // Example
                // Preferred unit
                // URL
                // Definition

                Metadata metadata = new Metadata();
                String aPackage = getStringValueCell(row.getCell(index.get("package name"))).trim();
                // Excel sheetname restrictions
                aPackage = aPackage.replace("/", "-");
                aPackage = aPackage.replace("\\", "-");
                aPackage = aPackage.replace("?", "-");
                aPackage = aPackage.replace("*", "-");

                // Set the new package name
                metadata.setPackageName(aPackage);
                metadata.setPackageIdentifier(getStringValueCell(row.getCell(index.get("package identifier"))).trim());
                metadata.setLabel(getStringValueCell(row.getCell(index.get("item (rdfs:label)"))).trim());
                metadata.setSheetName(sheet.getSheetName());
                // System.err.println("METADATA GET LABEL: " + metadata.getLabel());
                metadata.setRequirement(getStringValueCell(row.getCell(index.get("requirement"))).trim());

                // H requirements are hidden. Useful when working on new standards while using the document
                if (metadata.getRequirement().toLowerCase().contains("ignore")) continue;

                // Get value syntax
                if (row.getCell(index.get("value syntax")) == null) {
                    metadata.setSyntax(".*");
                } else {
                    metadata.setSyntax(getStringValueCell(row.getCell(index.get("value syntax"))).trim());
                }
                // Check example if blank
                if (row.getCell(index.get("example")) != null) {
                    // Set the value but allow to overwrite for date
                    metadata.setExample(getStringValueCell(row.getCell(index.get("example"))));

                    // Improve parsing if it is a timestamp element
                    if (row.getCell(index.get("value syntax")) != null) {
                        String syntax = getStringValueCell(row.getCell(index.get("value syntax")));
                        if (syntax.equalsIgnoreCase("{timestamp}")) {
                            CellType cellType = row.getCell(index.get("example")).getCellType();
                            if (cellType.equals(CellType.NUMERIC)) {
                                // Set date object
                                Date date = row.getCell(index.get("example")).getDateCellValue();
                                LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                                metadata.setExample(localDate.toString());
                            }
                        }
                    }
                }

                // Set regex
                try {
                    java.net.URL urlString = new URL(metadata.getSyntax());

                    // Download ontology file
                    URL url = new URL(urlString.toString());
                    File destination = new File("./fairds_storage/" + url.getFile().toLowerCase());
                    destination.getParentFile().mkdirs();
                    if (!destination.exists()) {
                        HttpURLConnection http = (HttpURLConnection) url.openConnection();
                        http.setRequestProperty("Accept", "*/*");
                        InputStream inputStream = http.getInputStream();
                        Files.copy(inputStream, destination.toPath());
                        inputStream.close();
                        new File(destination + ".dir").mkdirs();
                        Domain domain = new Domain("file://" + destination + ".dir");
                        // Load file in memory and add to disk store
                        Domain tempDomain = new Domain("file://" + destination);
                        domain.getRDFSimpleCon().getModel().add(tempDomain.getRDFSimpleCon().getModel());
                        tempDomain.close();
                    }
                    metadata.setOntology(new File(destination + ".dir"));
                    System.err.println("ADDED!" + metadata.getLabel());
                } catch (MalformedURLException e) {
                    metadata.setRegex(generateRegex(metadata, metadata.getSyntax(), metadata.getExample()));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                // Preferred unit
                if (index.containsKey("preferred unit") && row.getCell(index.get("preferred unit")) != null) {
                    metadata.setPreferredUnit(getStringValueCell(row.getCell(index.get("preferred unit"))));
                    if (metadata.getPreferredUnit().length() > 0) {
                        // When there is no regex
                        String syntax;
                        // System.err.println(metadata.getRegex().pattern());
                        if (metadata.getRegex() == null) {
                            syntax = ".+ (" + metadata.getPreferredUnit() + ")";
                        } else {
                            syntax = metadata.getSyntax() + " ?(" + metadata.getPreferredUnit() + ")";
                        }
                        metadata.setRegex(generateRegex(metadata, syntax, metadata.getExample()));
                    }
                }

                // Check if url is blank
                if (row.getCell(index.get("url")) != null) {
                    metadata.setURL(getStringValueCell(row.getCell(index.get("url"))));
                }

                metadata.setDefinition(getStringValueCell(row.getCell(index.get("definition"))));

                // Check if metadata structured comment name is already used
                boolean exists = false;
                for (Metadata metadata1 : metadataSet) {
                    if (metadata.getPackageName().equals(metadata1.getPackageName()) && metadata.getLabel().equals(metadata1.getLabel())) {
                        exists = true;
                        logger.warn("A name has already been used, ignoring next entry with name " + metadata1.getLabel());
                    }
                }

                if (!exists) {
                    metadataSet.add(metadata);
                }
            }
        }
        return metadataSet;
    }

//    /**
//     * @param sheetName Sheet name of the template
//     * @return metadata object with parental identifier
//     */
//    private static Metadata addParentIdentifier(String sheetName) {
//        if (sheetName.equalsIgnoreCase("project"))
//            return null;
//        Metadata metadata = new Metadata();
//        metadata.setRequirement("mandatory");
//        metadata.setPackageName("default");
//        // Investigation
//        if (sheetName.equalsIgnoreCase("investigation")) {
//            metadata.setLabel("project identifier");
//        }
//
//        // Study
//        if (sheetName.equalsIgnoreCase("study")) {
//            metadata.setLabel("investigation identifier");
//        }
//
//        // ObservationUnit
//        if (sheetName.equalsIgnoreCase("observationunit")) {
//            metadata.setLabel("study identifier");
//        }
//
//        // Sample
//        if (sheetName.equalsIgnoreCase("sample")) {
//            metadata.setLabel("observation unit identifier");
//        }
//
//        // Assay
//        if (sheetName.equalsIgnoreCase("assay")) {
//            metadata.setLabel("sample identifier");
//        }
//        return metadata;
//    }

    /**
     *
     * @param value used in the excel file
     * @param example value to show to the user
     * @return the regex object or null
     */
    public static Pattern generateRegex(Metadata metadata, String value, String example) {
        // To preserve the original value
        String syntax = value;
        // Escape illegal characters
        // syntax = syntax.replace("-", "\\-");
        // Free text at least one character
        String baseRegex = ".+";
        // ids are alpha numeric and _
        syntax = syntax.replace("{id}", "^[a-zA-Z0-9_\\.-]*$");
        // text value only
        if (containsIgnoreCase(value, "{file}")) {
            syntax = syntax.replace("{file}", baseRegex);
            metadata.setFile(true);
        }
        // text value only
        syntax = syntax.replace("{text}", baseRegex);
        // email value from https://www.emailregex.com
        syntax = syntax.replace("{email}", "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        // orcid regex
        syntax = syntax.replace("{orcid}", "^(\\d|X{4}-){3}\\d|X{4}$");
        // Day(s) unit
        syntax = syntax.replace("{day}", "day[s]?");
        // Units are to broad so it becomes free text
        syntax = syntax.replace("{unit}", baseRegex);
        // one or more numbers and with excel it might append .0 to it
        syntax = syntax.replace("{integer}", "\\d+\\.?0?");
        // NCBI taxonomy value
        syntax = syntax.replace("{NCBI taxid}", "^\\d+\\.?0?(?:;\\d+\\.?0?)*$");
        // digit with optional dot and digits and negative values
        syntax = syntax.replace("{float}","-?\\d+\\.?\\d*");
        // Percentage
        syntax = syntax.replace("{percentage}", "-?\\d+\\.?\\d*");

        // timestamps are parsed as floats by excel right?
        String date = ".*";
        String duration = ".*";

        // 55.55555 or 2010-11-01T00:00:00
        syntax = syntax.replace("{timestamp}", "(\\d+-\\d+-\\d+[T ]\\d+:\\d+:\\d+|\\d+\\.*\\d*)");
        syntax = syntax.replace("{duration}", duration);
        syntax = syntax.replace("{Rn/start_time/end_time/duration}", "R[0-9]+/" + date + "/" + date + "/" + duration);

        // terms are just strings
        syntax = syntax.replace("{term}", baseRegex);
        syntax = syntax.replace("{termLabel}", baseRegex);
        syntax = syntax.replace("{TermLabel}", baseRegex);
        syntax = syntax.replace("{term label}", baseRegex);
        syntax = syntax.replace("{[termID]}", "\\[.+\\]");
        syntax = syntax.replace("{term ID}", baseRegex);
        // Measurements values
        syntax = syntax.replace("{measurement value}", baseRegex);
        // rank name TODO to be defined by select set of words?
        syntax = syntax.replace("{rank name}", baseRegex);

        // URL validation
        String url = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        syntax = syntax.replace("{PMID}", url);
        syntax = syntax.replace("{DOI}", url);
        syntax = syntax.replace("{URL}", url);
        // Taxonomy
//        syntax = syntax.replace("{NCBI taxid}", "\\d+");
        //boolean
        syntax = syntax.replace("{boolean}", "[true|false|yes|no]");
        // dna
        syntax = syntax.replace("{dna}", "[\\[\\]ACGTUWSMKRYBDHVN]+");

        if (syntax.matches(".*\\{[a-zA-Z].*")) {
            logger.error("Entity with original value of " + value + " with generated regex " + syntax + " with example " + example);
        }

        // Correction with length settings
        if (syntax.contains(".+{")) {
            syntax = syntax.replace(".+{", ".{");
        }

        // Validate
        try {
            Pattern r = Pattern.compile(syntax);
            if (example != null) {
                Matcher m = r.matcher(example);
                m.matches();
                return r;
            } else {
                return r;
            }

        } catch (NullPointerException | PatternSyntaxException e) {
            logger.error("Entity not parsable yet " + syntax + " with example " + example);
            System.err.println(e.getMessage());
        }
        return null;
    }

    private static boolean checkRow(Row row) {
        return row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row);
    }

    /**
     * @return objects per sheet in the metadata excel file
     */
    public static HashMap<String, ArrayList<Metadata>> main() {
        // If metadata file exists locally
        InputStream is;
        logger.info("Checking if " + new File("./fairds_storage/metadata.xlsx").getAbsolutePath() + " exists");
        xlsxFile = new File("./fairds_storage/metadata.xlsx");
        // Check if it exists and remove if a newer version is available
        if (xlsxFile.exists()) {
            // Date check of metadata.xlsx if newer internal then remove xlsx
            try {
                java.net.URL url = MetadataParser.class.getResource("metadata.xlsx");
                if (url != null) {
                    if (new Date(url.openConnection().getLastModified()).after(new Date(xlsxFile.lastModified()))) {
                        logger.warn("A newer excel file has been detected inside the JAR, but using the existing excel file");
                    } else {
                        logger.info("Using the excel file outside the jar");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Check if it still exists and otherwise ensure the file is placed there
        if (xlsxFile.exists()) {
            // Check the data
            logger.info("Using local excel file");
            try {
                is = new FileInputStream(xlsxFile);
            } catch (FileNotFoundException e) {
                logger.info("Using internal excel file");
                is = MetadataParser.class.getClassLoader().getResourceAsStream(xlsxFile.getName());
            }
        } else {
            // else load xlsx from jar file and copy to local
            logger.info("Using internal excel file " + xlsxFile.getName());
            is = MetadataParser.class.getClassLoader().getResourceAsStream(xlsxFile.getName());
            try {
                if (is != null) {
                    xlsxFile.getParentFile().mkdirs();
                    Files.copy(is, xlsxFile.toPath());
                    is.close();
                    is = MetadataParser.class.getClassLoader().getResourceAsStream(xlsxFile.getName());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Workbook workbook = null;
        try {
            if (is != null) {
                workbook = WorkbookFactory.create(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        HashMap<String, ArrayList<Metadata>> sheetObjects = new HashMap<>();
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            logger.info("Processing " + sheet.getSheetName() + " with " + sheet.getLastRowNum() + " entries");
            String name = sheet.getSheetName();
            ArrayList<Metadata> metadataHashSet = MetadataParser.parser(sheet);
            sheetObjects.put(name, metadataHashSet);
        }
        return sheetObjects;
    }
}
