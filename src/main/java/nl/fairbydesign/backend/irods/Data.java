package nl.fairbydesign.backend.irods;

import nl.fairbydesign.backend.data.objects.Biom;
import nl.fairbydesign.backend.data.objects.DiskUsage;
import nl.fairbydesign.backend.data.objects.Process;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.jni.Proc;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.*;
import org.jboss.logging.Logger;
import org.jermontology.ontology.JERMOntology.domain.process;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.irods.jargon.core.query.QueryConditionOperators.LIKE;
import static org.irods.jargon.core.query.QueryConditionOperators.NOT_LIKE;
import static org.irods.jargon.core.query.RodsGenQueryEnum.*;

public class Data {
    public static final Logger logger = Logger.getLogger(Data.class);


    /**
     * Obtains Project, investigation and study information combined with yaml status and workflow
     * @param irodsAccount the irods account for the query
     * @return
     */
    public static ArrayList<Process> getPIS(IRODSAccount irodsAccount) {
        try {
            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            // iquest --no-page "%s %s %s" "SELECT COLL_NAME, META_DATA_ATTR_VALUE, META_DATA_ATTR_UNITS WHERE
            // COLL_NAME NOT LIKE '/unlock/references%'
            // AND
            // COLL_NAME NOT LIKE '/unlock/trash%' AND
            // DATA_NAME LIKE '%.yaml'"
            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, "/unlock/home%");
            queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.yaml");

            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_VALUE);
            queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_UNITS);

            // Set limit?
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);

            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

            // unlock [0]
            // projects [1]
            // project [2]
            // investigation [3]
            // study [4]

            HashMap<String, Process> processHashMap = new HashMap<>();

            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                String workflow = irodsQueryResultRow.getColumn(1);
                String unit = irodsQueryResultRow.getColumn(2);
                String first = irodsQueryResultRow.getColumn(0).split("/")[1];
                String second = irodsQueryResultRow.getColumn(0).split("/")[2];
                String project = irodsQueryResultRow.getColumn(0).split("/")[3];
                String investigation = irodsQueryResultRow.getColumn(0).split("/")[4];
                String study = irodsQueryResultRow.getColumn(0).split("/")[5];
                String path = "/" + first + "/" + second + "/" + project + "/" + investigation +"/"+study;

                // For this path with this workflow
                String id = path + " " + workflow;
                Process process;

                if (processHashMap.containsKey(id)) {
                    process = processHashMap.get(id);
                } else {
                    process = new Process();
                    process.setProjectIdentifier(project);
                    process.setInvestigationIdentifier(investigation);
                    process.setStudyIdentifier(study);
                    process.setPath(path);
                    process.setWorkflow(workflow);
                    processHashMap.put(id, process);
                }

                // Status
                if (unit.equals("waiting")) {
                    process.setWaiting(process.getWaiting() + 1);
                } else if (unit.equals("running")) {
                    process.setRunning(process.getRunning() + 1);
                } else if (unit.equals("finished")) {
                    process.setFinished(process.getFinished() +1);
                } else if (unit.equals("failed")) {
                    logger.error(process.getPath() +" " + workflow +" " + unit);
                    process.setFailed(process.getFailed() + 1);
                } else if (unit.equals("queue")) {
                    process.setQueue(process.getQueue() + 1);
                } else {
                    logger.error("Status unknown" + unit);
                }
            }
            return new ArrayList(processHashMap.values());

        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static Collection<Process> getJobInformation(IRODSAccount irodsAccount, Process process) throws GenQueryBuilderException, JargonException, JargonQueryException {

        // Obtain all projects via metadata
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, process.getPath() + "%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.yaml");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.EQUAL, "cwl");

        // Column name as default does a distinct?
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_VALUE);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS);
        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_D_DATA_ID, GenQueryField.SelectFieldTypes.COUNT);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(1000);

        IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
        IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);

        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashMap<String, Process> processHashMap = new HashMap<>();

        for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
            String workflow = irodsQueryResultRow.getColumn(0).replaceAll(".*/", "");
            if (!processHashMap.containsKey(workflow)) {
                Process newProcess = new Process();
                newProcess.setProjectIdentifier(process.getProjectIdentifier());
                newProcess.setInvestigationIdentifier(process.getInvestigationIdentifier());
                newProcess.setStudyIdentifier(process.getStudyIdentifier());
                newProcess.setPath(process.getPath());
                newProcess.setIdentifier(process.getIdentifier());
                newProcess.setWorkflow(workflow);
                processHashMap.put(workflow, newProcess);
            }
            // Get copy of process from specific workflow
            Process processHash = processHashMap.get(workflow);
            processHash.setWorkflow(workflow);

            String unit = irodsQueryResultRow.getColumn(1);
            int count = Integer.parseInt(irodsQueryResultRow.getColumn(2));

            if (unit.equals("waiting")) {
                processHash.setWaiting(count);
            } else if (unit.equals("running")) {
                processHash.setRunning(count);
            } else if (unit.equals("finished")) {
                processHash.setFinished(count);
            } else if (unit.equals("failed")) {
                logger.error(process.getPath() +" " + workflow +" " + unit + " " + count);
                processHash.setFailed(count);
            } else if (unit.equals("queue")) {
                processHash.setQueue(count);
            } else {
                logger.error("Status unknown" + unit);
            }
        }
        logger.info("Processes " + processHashMap.size());
        return processHashMap.values();
    }

    public static DiskUsage getDiskUsage(IRODSAccount irodsAccount, Process process) throws GenQueryBuilderException, JargonException, JargonQueryException {

        // Obtain all disk usage for each project
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, "/" + irodsAccount.getZone() + "%" + process.getProjectIdentifier() + "/" + process.getInvestigationIdentifier() + "%");

        // Column name as default does a distinct?
        queryBuilder.addSelectAsGenQueryValue(COL_R_RESC_NAME);
        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE, GenQueryField.SelectFieldTypes.SUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(1000);

        IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
        IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);

        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        DiskUsage diskUsage = new DiskUsage();
        diskUsage.setProject(process.getProjectIdentifier());
        diskUsage.setInvestigation(process.getInvestigationIdentifier());
        diskUsage.setTapeSize(0);
        diskUsage.setDiskSize(0);
        for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
            String resource = irodsQueryResultRow.getColumn(0);
            String size = irodsQueryResultRow.getColumn(1).trim().strip();
            if (resource.contains("archiveResc") && StringUtils.isNumeric(size) && size.length() > 0)
                    diskUsage.setTapeSize(Long.parseLong(size) / (1024 * 1024 * 1024));
            if (resource.contains("dataResc") && StringUtils.isNumeric(size) && size.length() > 0)
                diskUsage.setDiskSize(Long.parseLong(size) / (1024 * 1024 * 1024));
        }
        return diskUsage;
    }


    /**
     * Uploading a local file to iRODS
     *
     * @param irodsAccount the authentication object to access irods
     * @param localFile  a local file that will be uploaded to the irods location
     * @param remoteFile the path to the destination
     * @throws JargonException
     * @throws FileNotFoundException
     */
    public static void uploadIrodsFile(IRODSAccount irodsAccount, File localFile, File remoteFile) throws JargonException, FileNotFoundException {
        uploadIrodsFile(irodsAccount, localFile, remoteFile, false);
    }

    public static void uploadIrodsFile(IRODSAccount irodsAccount, File localFile, File remoteFile, Boolean force) throws JargonException, FileNotFoundException {
        // Check if file is not an irods location
        IRODSFileFactory irodsFileFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getIRODSFileFactory(irodsAccount);
        IRODSFile irodsFileFinalDestination = irodsFileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());

        ChecksumValue remoteChecksumValue;
        ChecksumValue localChecksumValue;

        if (irodsFileFinalDestination.exists()) {
            if (force) {
                logger.info("Remove remote file location " + remoteFile);
                irodsFileFinalDestination.delete();
            } else {
                // Get local HASH
                LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                // Get remote hash
                DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(irodsAccount);
                remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFileFinalDestination);

                if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                    logger.info("Same checksum, not going to overwrite " + localFile.getName());
                    return;
                } else {
                    logger.info("Remove remote file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                    logger.info("Does not match checksum of " + irodsFileFinalDestination.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                    irodsFileFinalDestination.delete();
                }
            }
        }

        if (!irodsFileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).exists()) {
            // Create collection in iRODS
            logger.info("Creating directory: " + remoteFile.getParentFile().getAbsolutePath());
            irodsFileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).mkdirs();
        }

        // Logger.getLogger("org.irods.jargon.core.transfer").setLevel(Level.OFF);
        DataTransferOperations dataTransferOperationsAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataTransferOperations(irodsAccount);

        IRODSFile destFile = irodsFileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());
//        dataTransferOperationsAO.putOperation(localFile, destFile, null, tcb);
        logger.info("Uploading " + localFile + " to " + remoteFile);
        dataTransferOperationsAO.putOperation(localFile, destFile, null, null);
    }

    public static void downloadFile(IRODSFileFactory fileFactory, IRODSAccount irodsAccount,  String file) throws JargonException, IOException {
        logger.debug("Checking " + file + " for download");

        IRODSFile irodsFile = fileFactory.instanceIRODSFile(file);
        File localFile = new File("./" + file);
        if (!irodsFile.exists()) {
            return;
        }

        if (irodsFile.isDirectory())
            return;

        if (localFile.exists()) {
            // Check file size
            long localSize = Files.size(Path.of(localFile.getPath()));
            long irodsSize = irodsFile.length();
            if (localSize == irodsSize) {
                logger.debug("File size the same...");
                return;
            } else {
                while (localFile.exists()) {
                    logger.debug("Deleting " + localFile);
                    localFile.delete();
                }
            }
        }
        logger.debug("Downloading " + irodsFile);

        DataTransferOperations dataTransferOperationsAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataTransferOperations(irodsAccount);
        new File("." + file).getParentFile().mkdirs();
        dataTransferOperationsAO.getOperation(irodsFile, new File("." + file), null, null);
    }

    public static long getTrashInfo(IRODSAccount irodsAccount) throws JargonException, GenQueryBuilderException, JargonQueryException {
        // Obtain list in trash folder

        IRODSFileFactory irodsFileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);

            // Get size and whatever is under
        long size = getFolderSize(new File("/" + irodsAccount.getZone() + "/trash"), irodsAccount);

        return size;
    }

    private static long getFolderSize(File trashFolder, IRODSAccount irodsAccount) throws GenQueryBuilderException, JargonException, JargonQueryException {
        logger.debug("Checking " + trashFolder.getAbsolutePath());

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, trashFolder.getAbsolutePath() + "%");
        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE, GenQueryField.SelectFieldTypes.SUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        if (irodsQueryResultSetResults.size() > 1)
            throw new JargonException("To many items?");

        if (irodsQueryResultSetResults.get(0).getColumn(0).length() == 0) {
            return 0L;
        } else {
            return Long.parseLong(irodsQueryResultSetResults.get(0).getColumn(0));
        }
    }

    public static void deleteFile(String filePath, IRODSAccount irodsAccount) throws JargonException {
        IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);
        IRODSFile irodsFile = fileFactory.instanceIRODSFile(filePath);
        irodsFile.delete();
    }

    public static ArrayList<Biom> getJobOutputs(IRODSAccount irodsAccount, String search) {
        logger.info("Searching with " + search + " with " + irodsAccount.getUserName());
        try {
            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, search);

            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);

            // Set limit?
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);
            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            ArrayList<Biom> bioms = new ArrayList<>();
            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                String assayPath = irodsQueryResultRow.getColumn(0);
                Biom biom = new Biom();
                biom.setObservationUnit(new File(irodsQueryResultRow.getColumn(0)).getName());

                String root = "/" + irodsAccount.getZone()  + "/home/";
                if (assayPath.startsWith(root)) {
                    // assayPath = assayPath.replaceAll(root, "");
                    String[] pathSplit = assayPath.split("/");
                    if (pathSplit.length == 11) {
                        biom.setProject(pathSplit[3].split("_", 1)[0]);
                        biom.setInvestigation(pathSplit[4].split("_", 1)[0]);
                        biom.setStudy(pathSplit[5].split("_", 1)[0]);
                        biom.setObservationUnit(pathSplit[6].split("_", 1)[0]);
                        // 7 = assay type
                        biom.setAssay(pathSplit[8].split("_", 1)[0]);
                        biom.setJob(pathSplit[9]);
                        biom.setFolder(assayPath);
                        bioms.add(biom);
                    } else if (pathSplit.length == 12) {
                        // New formatting with /SampleID/ folder added
                        biom.setProject(pathSplit[3].replaceFirst("PRJ_",""));
                        biom.setInvestigation(pathSplit[4].replaceFirst("INV_",""));
                        biom.setStudy(pathSplit[5].replaceFirst("STU_",""));
                        biom.setObservationUnit(pathSplit[6].replaceFirst("OBS_",""));
                        biom.setSample(pathSplit[7].replaceFirst("SAM_", ""));
                        // 8 = assay type
                        biom.setAssay(pathSplit[9].replaceFirst("ASY_",""));
                        biom.setJob(pathSplit[10]);
                        biom.setFolder(assayPath);
                        bioms.add(biom);
                    }
                }
            }
            return bioms;
        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static ArrayList<nl.fairbydesign.backend.data.objects.File> getFilesBySize(IRODSAccount irodsAccount, String megabytes) {

        try {
            // Get all files without the RESOURCE value
            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);

            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_SIZE, QueryConditionOperators.NUMERIC_GREATER_THAN_OR_EQUAL_TO, Long.parseLong(megabytes) * 1024 * 1024);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_MODIFY_TIME);

            // Set limit?
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);

            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            HashMap<String, nl.fairbydesign.backend.data.objects.File> files = new HashMap<>();
            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                nl.fairbydesign.backend.data.objects.File file = new nl.fairbydesign.backend.data.objects.File();
                String path = irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1);
                file.setPath(path);
                file.setSize(Long.parseLong(irodsQueryResultRow.getColumn(2)) / (1024 * 1024));
                file.setModificationDate(irodsQueryResultRow.getColumn(3));
                file.setResource("none");
                files.put(path, file);
            }

            // Get all files with the resource value to be added to the object
            // Obtain all projects via metadata
            queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_SIZE, QueryConditionOperators.NUMERIC_GREATER_THAN_OR_EQUAL_TO, Long.parseLong(megabytes) * 1024 * 1024);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.EQUAL, "RESOURCE");
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_VALUE);

            // Set limit?
            query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            irodsFileSystem = new IRODSFileSystem();
            irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);

            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            irodsQueryResultSetResults = irodsQueryResultSet.getResults();

            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                String path = irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1);
                nl.fairbydesign.backend.data.objects.File file = files.get(path);
                file.setResource(irodsQueryResultRow.getColumn(3));
                files.put(path, file);
            }
            return new ArrayList<>(files.values());
        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}