package nl.fairbydesign.backend.irods;

import nl.fairbydesign.backend.data.objects.AVU;
import nl.fairbydesign.backend.data.objects.Item;
import nl.fairbydesign.views.browser.BrowserView;
import org.jboss.logging.Logger;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.query.MetaDataAndDomainData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static nl.fairbydesign.backend.WebGeneric.humanReadableByteCountBin;

/**
 * Functions used for browsing data files and collections in irods
 */
public class Browser {
    public static final Logger logger = Logger.getLogger(Browser.class);

    /**
     * @param irodsAccount object with credential information
     * @param path         on irods to obtain files and folders from
     */


    /**
     * @param irodsAccount object with credential information
     * @param item object containing info of which AVU (metadata) information should be obtained from
     * @return the item with AVU information
     */
    public static Item getAVUs(IRODSAccount irodsAccount, Item item) {
        try {
            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();
            // Make a hashmap for each tab?

            IRODSFile irodsFile = fileFactory.instanceIRODSFile(item.getPath());

            List<MetaDataAndDomainData> metaDataAndDomainDataList = new ArrayList<>();
            if (irodsFile.isDirectory()) {
                metaDataAndDomainDataList = accessObjectFactory.getCollectionAO(irodsAccount).findMetadataValuesForCollection(item.getPath());
            } else if (irodsFile.isFile()) {
                metaDataAndDomainDataList = accessObjectFactory.getDataObjectAO(irodsAccount).findMetadataValuesForDataObject(item.getPath());
            }
            item.removeAllAvus();
            for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
                AvuData avu = metaDataAndDomainData.asAvu();
                AVU localAVU = new AVU(avu.getAttribute(), avu.getValue(), avu.getUnit());
                logger.debug("AVU: " + avu.getAttribute() + " " + avu.getValue() + " " + avu.getUnit());
                item.addAvus(localAVU);
            }
            return item;
        } catch (JargonException | JargonQueryException e) {
            e.printStackTrace();
        }
        return item;
    }

    /**
     * @param irodsAccount object with credential information
     * @param item object containing file / folder informaiton
     * @param attribute the metadata key
     * @param value the metadata value
     * @param unit the metadata unit
     * @return the item with AVU update info
     */
    public static Item addMetadata(IRODSAccount irodsAccount, Item item, String attribute, String value, String unit) {
        try {
            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();

            IRODSFile irodsFile = fileFactory.instanceIRODSFile(item.getPath());
            AvuData avuData = new AvuData();
            avuData.setAttribute(attribute);
            avuData.setValue(value);
            avuData.setUnit(unit);
            if (irodsFile.isDirectory()) {
                CollectionAO collectionAO = accessObjectFactory.getCollectionAO(irodsAccount);
                collectionAO.addAVUMetadata(item.getPath(), avuData);
            } else {
                DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(irodsAccount);
                dataObjectAO.addAVUMetadata(item.getPath(), avuData);
            }
            return Browser.getAVUs(irodsAccount, item);
        } catch (JargonException e) {
            e.printStackTrace();
        }
        return item;
    }

    /**
     * @param irodsAccount object with credential information
     * @param item object containing file / folder informaiton
     * @param avu object to be removed
     * @return update of the item
     */
    public static Item deleteMetadata(IRODSAccount irodsAccount, Item item, AVU avu) {
        try {
            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();

            IRODSFile irodsFile = fileFactory.instanceIRODSFile(item.getPath());
            AvuData avuData = new AvuData();
            avuData.setAttribute(avu.getAttribute());
            avuData.setValue(avu.getValue());
            avuData.setUnit(avu.getUnit());
            if (irodsFile.isDirectory()) {
                CollectionAO collectionAO = accessObjectFactory.getCollectionAO(irodsAccount);
                collectionAO.deleteAVUMetadata(item.getPath(), avuData);
            } else {
                DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(irodsAccount);
                dataObjectAO.deleteAVUMetadata(item.getPath(), avuData);
            }
            return Browser.getAVUs(irodsAccount, item);
        } catch (JargonException e) {
            e.printStackTrace();
        }
        return item;
    }
}
