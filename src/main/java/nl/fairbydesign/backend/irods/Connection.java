package nl.fairbydesign.backend.irods;

import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

/**
 * Connection using system variables for bot operation access when needed
 */

public class Connection {
    private IRODSFileSystem irodsFileSystem;
    private IRODSAccount irodsAccount;
    private IRODSAccessObjectFactory accessObjectFactory;
    private IRODSFileFactory fileFactory;
    private static final Logger log = LoggerFactory.getLogger(Connection.class);

    public Connection() throws JargonException {
        log.info("Authenticating as " + System.getenv("irodsUserName"));
        // Initialize account object
        IRODSAccount irodsAccount = IRODSAccount.instance(
                System.getenv("irodsHost"),
                Integer.parseInt(System.getenv("irodsPort")),
                System.getenv("irodsUserName"),
                System.getenv("irodsPassword"),
                "",
                System.getenv("irodsZone"),
                "");

        // Perform the authentication?
        // SSL Settings
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
        if (System.getenv("irodsSSL").matches("CS_NEG_REQUIRE")) {
            sslPolicy = CS_NEG_REQUIRE;
        } else if (System.getenv("irodsSSL").matches("CS_NEG_REFUSE")) {
            sslPolicy = CS_NEG_REFUSE;
        } else {
            sslPolicy = CS_NEG_REQUIRE;
        }

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        setIrodsAccount(irodsAccount);

        setIrodsFileSystem(IRODSFileSystem.instance());

        setAccessObjectFactory(irodsFileSystem.getIRODSAccessObjectFactory());

        setFileFactory(accessObjectFactory.getIRODSFileFactory(irodsAccount));
    }

    public IRODSFileSystem getIrodsFileSystem() {
        return irodsFileSystem;
    }

    public void setIrodsFileSystem(IRODSFileSystem irodsFileSystem) {
        this.irodsFileSystem = irodsFileSystem;
    }

    public IRODSAccount getIrodsAccount() {
        return irodsAccount;
    }

    public void setIrodsAccount(IRODSAccount irodsAccount) {
        this.irodsAccount = irodsAccount;
    }

    public IRODSAccessObjectFactory getAccessObjectFactory() {
        return accessObjectFactory;
    }

    public void setAccessObjectFactory(IRODSAccessObjectFactory accessObjectFactory) {
        this.accessObjectFactory = accessObjectFactory;
    }

    public IRODSFileFactory getFileFactory() {
        return fileFactory;
    }

    public void setFileFactory(IRODSFileFactory fileFactory) {
        this.fileFactory = fileFactory;
    }
}
