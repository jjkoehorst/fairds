package nl.fairbydesign.backend.parsers;

import com.vaadin.flow.component.textfield.TextArea;
import nl.fairbydesign.backend.ApplicationServiceInitListener;
import nl.fairbydesign.backend.Generic;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.irods.Connection;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.DuplicateDataException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.UserTypeEnum;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.UserAO;
import org.irods.jargon.core.pub.UserGroupAO;
import org.irods.jargon.core.pub.domain.User;
import org.irods.jargon.core.pub.domain.UserGroup;
import org.jboss.logging.Logger;
import org.jermontology.ontology.JERMOntology.domain.*;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.rdfhdt.hdt.exceptions.IllegalFormatException;
import org.schema.domain.Organization;
import org.schema.domain.Person;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.fairbydesign.backend.WebGeneric.getStringValueCell;
import static org.apache.poi.hssf.usermodel.HSSFDateUtil.isCellDateFormatted;

/**
 * Main class for validating the excel file and transforming it into RDF
 */
public class ExcelValidator {
    // List of constant variables for easy modifications
//    public static final String FORWARD_FILENAME = "forward filename";
    public static final String OBSERVATION_UNIT_IDENTIFIER = "observation unit identifier";
    public static final String SAMPLE_IDENTIFIER = "sample identifier";
    public static final String ASSAY_IDENTIFIER = "assay identifier";
    public static final String SAME_AS = "same as";
//    public static final String REVERSE_FILENAME = "reverse filename";
//    public static final String REFERENCE = "reference";
    public static final String STUDY_IDENTIFIER = "study identifier";
    public static final String PROJECT_IDENTIFIER = "project identifier";
    public static final String INVESTIGATION_IDENTIFIER = "investigation identifier";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email address";
    public static final String ORCID = "orcid";
    public static final String DEPARTMENT = "department";
    public static final String ORGANIZATION = "organization";

    // RDF triple store
    private Domain domain;
    // logger
    private final Logger logger = Logger.getLogger(ExcelValidator.class);
    // RDF prefix
    private String PREFIX;
    // Message to be shown to the user
    private String message = "";
    // Obtain all information from the excel sheet template?
    private Map<String, ArrayList<Metadata>> metadataSet = ApplicationServiceInitListener.getMetadata();
    private HashMap<File, Domain> ontologies = new HashMap<File, Domain>();

    // String variables

    /**
     * @param fileName     of the input excel file
     * @param excelFile    input stream to be processed
     * @param irodsAccount with credentials to upload after validation succeeds when logged in (not obligatory)
     * @param logArea
     * @return the RDF object
     * @throws Exception when validation fails, the message can be expanded upon failure to show a clearer message to
     *                   the user
     */
    public String validate(String fileName, InputStream excelFile, IRODSAccount irodsAccount, TextArea logArea) throws Exception {
        this.setMessage("");

        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Obtaining information from reference file")));

        logger.debug("Obtaining information from reference file");

        // Ensuring metadata set is final
        metadataSet = Collections.unmodifiableMap(metadataSet);


        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Validation starting")));

        logger.debug("Validation starting");

        // Opening the workbook stream
        XSSFWorkbook workbook;
        try {
            workbook = new XSSFWorkbook(excelFile);
        } catch (Exception e) {
            logger.error("Not a valid excel file " + e.getMessage());
            return "Is this an excel file?";
        }

        // Creation of an RDF memory store
        domain = new Domain("");

        // Add generic prefixes to the RDF file for more compressed TURTLE format
        setPrefixes();

         PREFIX = "http://fairbydesign.nl/";

        // Base prefix added
        domain.getRDFSimpleCon().setNsPrefix("base", PREFIX);

        // Parse the project information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing projects")));

        projectCreation(workbook.getSheet("Project"), metadataSet.get("Project"), logArea);

        // Parse the investigation information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing investigations")));

        investigationCreation(workbook.getSheet("Investigation"), metadataSet.get("Investigation"), logArea);

        // Parse the study information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing studies")));

        studyCreation(workbook.getSheet("Study"), metadataSet.get("Study"), logArea);

        // Parse the observation unit information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing observation units")));

        // OBS unit sheet should support packages
        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("observationunit")) {
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                observationUnitCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("sample")) {
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                sampleCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("assay")) {
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                assayCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        // Couple data to sample when possible
        dataToSampleCoupling();

        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Validating...")));

        validate();

        // Setting up folders and files in irods
        if (irodsAccount != null) {
            logger.info("User logged in, setting up irods landingzone");
            String projectIdentifier = irodsSetup(fileName, workbook, new Connection());
            return projectIdentifier;
        }

        String rdfFile = ApplicationServiceInitListener.getStorage() + "/" + fileName.replace(".xlsx", "") + ".ttl";

        domain.save(rdfFile);

        logging("Validation successful, user not logged in. \nResult file not uploaded to the data storage facility");

        return rdfFile;
    }

    private ArrayList<Metadata> getPackage(String sheetName) throws Exception {
        String subjectName;
        String packageName;
        if (sheetName.toLowerCase().contains(" - ")) {
            subjectName = sheetName.split(" - ", 2)[0];
            packageName = sheetName.split(" - ", 2)[1];
        } else {
            subjectName = sheetName;
            packageName = "default";
        }
       // sampleFound = true;
        AtomicBoolean packageNameMatched = new AtomicBoolean(false);
        ArrayList<Metadata> metadataArrayList = new ArrayList<>();

        // Check the correct package due to excel sheet name limitations
        String likelyPackage = "default";
        for (Metadata sample : metadataSet.get(subjectName)) {
            if (sample.getPackageName().startsWith(packageName)) {
                if (likelyPackage == "default") {
                    likelyPackage = sample.getPackageName();
                }
                if (sample.getPackageName().length() < likelyPackage.length()) {
                    likelyPackage = sample.getPackageName();
                }
            }
        }

        packageName = likelyPackage;

        for (Metadata entry : metadataSet.get(subjectName)) {
            // Add elements from package name
            if (entry.getPackageName().matches(packageName)) {
                metadataArrayList.add(entry);
                packageNameMatched.set(true);
            }

            if (!packageName.matches("default") && entry.getPackageName().matches("default")) {
                metadataArrayList.add(entry);
            }
        }

        if (!packageNameMatched.get()) {
            logging("Sample package name " + packageName + " is not one of the accepted package.\n" +
                    "Please check the accepted package list in the template generator...");
            throw new Exception("");
        }

        return metadataArrayList;
    }

    private void dataToSampleCoupling() throws Exception {
        Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getSamplesAndData.txt", true);
        for (ResultLine resultLine : resultLines) {
            String sampleIRI = resultLine.getIRI("sample");
            Sample sample = domain.make(Sample.class, sampleIRI);

            String data = resultLine.getIRI("data");
            Data_sample dataSample = domain.make(Data_sample.class, data);

            String isa_process = "http://isa-tools.org/process/" + sample.getIdentifier() + "/" + dataSample.getIdentifier();
            domain.getRDFSimpleCon().add(isa_process, "rdf:type", "http://www.wikidata.org/wiki/Q3249551-Process");
            domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P2283-INPUT", sample.getResource().getURI());
            domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P1056-OUTPUT", dataSample.getResource().getURI());
            // If collection date present
            // String collection_date = Generic.getValueOfPredicate(domain, sample.getResource().getURI(), "base:collection_date");
//            if (collection_date != null) {
//                LocalDate date = LocalDate.parse(collection_date.split("\\^\\^")[0]);
//                domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P1390-Date", date);
//            }

        }
    }

    private String irodsSetup(String fileName, Workbook workbook, Connection connection) throws Exception {
        // Acquire the first project identifier used throughout the parsing

        // Create raw data folder / <project> / <investigation>
        String projectIdentifier = getProjectIdentifier(workbook.getSheet("Project"));
        Project project = getProject(projectIdentifier);
        if (project.getAllHasPart().size() == 0) {
            throw new Exception("No investigations found");
        }

        ArrayList<String> investigationIDs = new ArrayList<>();
        ArrayList<String> studyIDs = new ArrayList<>();
        for (Investigation investigation : project.getAllHasPart()) {
            String groupName = project.getIdentifier() + "_" + investigation.getIdentifier();
            // Get investigation identifier
            investigationIDs.add(investigation.getIdentifier());
            // Get study identifier
            investigation.getAllHasPart().forEach(study -> studyIDs.add(study.getIdentifier()));
            String homeFolder = "/" + connection.getIrodsAccount().getZone() + "/home/" + groupName;
            // Ensure the robot account has access to this instance
            CollectionAO collectionAO = connection.getAccessObjectFactory().getCollectionAO(connection.getIrodsAccount());
            // Technicians have own rights
            collectionAO.setAccessPermissionOwn(connection.getIrodsAccount().getZone(), homeFolder, "technicians", true);
            collectionAO.setAccessPermissionInherit(connection.getIrodsAccount().getZone(), homeFolder, true);
            if (!connection.getFileFactory().instanceIRODSFile(homeFolder).exists()) {
                logger.info("Setting up group environment");
                // Get the researchers and assign them to this landingzone
                HashSet<String> contributors = Generic.getObjectsOfPredicate(domain, investigation.getResource().getURI(), "http://schema.org/contributor");

                setUsers(connection, contributors, groupName);
                // Create directories
                connection.getFileFactory().instanceIRODSFile(homeFolder + "/data").mkdirs();
            }

            logging("\n\nSuccessfully parsed the excel sheet");

            // Only perform the rest when user is logged in?
            logger.debug("USERNAME BOT: " + connection.getIrodsAccount().getUserName());

            // When successful upload to a designated location in irods
            fileName = fileName.replaceAll(".xlsx$", "");
            File xlsxFile = new File(ApplicationServiceInitListener.getStorage() + "/" + fileName + ".xlsx");
            File turtleFile = new File(ApplicationServiceInitListener.getStorage() + "/" + fileName + ".ttl");
            File remotexlsxFile = new File(fileName.replaceAll(" +", "_") + ".xlsx");
            File remoteturtleFile = new File(fileName.replaceAll(" +", "_") + ".ttl");

            // RDF object is created, now pushed to the iRODS parser
            domain.save(turtleFile.getAbsolutePath());

            // Write to temp file
            OutputStream outputStream = new FileOutputStream(xlsxFile.getAbsolutePath());
            workbook.write(outputStream);
            outputStream.close();

            // Upload to the investigation
            logging("\n\nUploading excel file to the data management system");
            // File destination = new File(homeFolder + "/INV_" + investigation.getIdentifier() + "/");
            Data.uploadIrodsFile(new Connection().getIrodsAccount(), xlsxFile, new File(homeFolder + "/" + remotexlsxFile.getName()));
            logging("\n\nUploading database file to the data management system");
            Data.uploadIrodsFile(new Connection().getIrodsAccount(), turtleFile, new File(homeFolder + "/" + remoteturtleFile.getName()));
            logging("\n\nFiles uploaded to: " + homeFolder + "/");
            logging("\nExcel file: " + homeFolder + "/" + remotexlsxFile.getName());
            logging("\nTurtle file: " + homeFolder + "/" + remoteturtleFile.getName());

            // Remove all traces of Mr.Robot
            collectionAO.removeAccessPermissionForUser(connection.getIrodsAccount().getZone(), homeFolder, "Mr.Robot", true);
        }
        return projectIdentifier;
    }

    private void setUsers(Connection connection, HashSet<String> allResearcher, String groupName) throws JargonException {
        logger.info("Setting user permissions to the landingzone");

        if (allResearcher.size() == 0) {
            throw new JargonException("No contacts found");
        }

        // Create a group
        UserGroupAO userGroupAO = connection.getAccessObjectFactory().getUserGroupAO(connection.getIrodsAccount());

        UserGroup ug = userGroupAO.findByName(groupName);
        if (ug == null) {
            logger.info("Making group: " + groupName);
            ug = new UserGroup();
            ug.setUserGroupName(groupName);
            ug.setZone(connection.getIrodsAccount().getZone());
            userGroupAO.addUserGroup(ug);
            logger.info("Group made: " + ug.getUserGroupName());
        } else {
            logger.info("Group: " + groupName);
        }

        UserAO userAO = connection.getAccessObjectFactory().getUserAO(connection.getIrodsAccount());

        // Add group to investigation landingzone
        logger.info("Adding group to project as read only");
        // Group folder
        String homeFolder = "/" + connection.getIrodsAccount().getZone() + "/home/" + groupName;
        CollectionAO collectionAO = connection.getAccessObjectFactory().getCollectionAO(connection.getIrodsAccount());
        // Set read permission for group folder
        collectionAO.setAccessPermissionRead(connection.getIrodsAccount().getZone(), homeFolder, groupName, false);
        // Set write permission for investigation data folder
        collectionAO.setAccessPermissionWrite(connection.getIrodsAccount().getZone(), homeFolder + "/data", groupName, true);
        // Enable inheritance for group folder
        collectionAO.setAccessPermissionInherit(connection.getIrodsAccount().getZone(), homeFolder, true);

        for (String researcher : allResearcher) {
            Person person = domain.make(Person.class, researcher);
            String mbox = person.getEmail().toLowerCase().replaceAll("mailto:", "");
            try {
                userAO.findByName(mbox);
            } catch (DataNotFoundException e) {
                logger.info("Creating user: " + mbox);
                User user = new User();
                user.setName(mbox);
                user.setUserType(UserTypeEnum.RODS_USER);
                userAO.addUser(user);
            }

            logger.info("Adding user " + mbox + " to group " + ug.getUserGroupName());
            boolean inProjectGroup = false;
            for (User listUserGroupMember : userGroupAO.listUserGroupMembers(groupName)) {
                if (listUserGroupMember.getName().contains(mbox)) {
                    inProjectGroup = true;
                }
            }

            if (!inProjectGroup) {
                userGroupAO.addUserToGroup(groupName, mbox, null);
            }

            boolean inUsersGroup = false;
            for (User listUserGroupMember : userGroupAO.listUserGroupMembers("users")) {
                if (listUserGroupMember.getName().contains(mbox)) {
                    inUsersGroup = true;
                    break;
                }
            }

            if (!inUsersGroup) {
                userGroupAO.addUserToGroup("users", mbox, null);
            }
        }
    }

    public void logging(String message) {
        System.err.println(message);
        logger.info(message);
        this.message = this.message + "\n" + message;
    }

    private void setPrefixes() {
        Scanner scanner = new Scanner(WebGeneric.getFileContentFromResources("queries/prefixes.txt"));
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            domain.getRDFSimpleCon().setNsPrefix(line.split(" ")[1].replaceAll(":", ""), line.split(" ")[2].replaceAll("[<>]", ""));
        }
    }

    /**
     * Validation is an on going process but the Empusa API should take care of most things...
     */
    private void validate() throws Exception {

        // Check if duplicate identifiers have been used in the project
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getAllIdentifiers.txt", true).iterator();
        boolean failed = false;

        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String identifier = resultLine.getLitString("identifier");
            // Check for illegal characters for now it is only a "/"
            boolean allowed = identifier.matches("[A-Za-z0-9\\-_.]+");
            if (!allowed) {
                if (identifier.contains(" ")) {
                    logging("Identifier \"" + identifier + "\" contains a space, only alphanumeric and - _ : . are allowed");
                } else {
                    logging("Identifier \"" + identifier + "\" contains an illegal character, only alphanumeric and - _ : . are allowed");
                }
            }

            // Check for identifier counts
            int counts = resultLine.getLitInt("count");
            if (counts > 1) {
                failed = true;
                logging("Warning: Identifier " + identifier + " used in multiple places at least " + counts + " times");
            }
        }

        if (failed && ApplicationServiceInitListener.debug) {
            domain.save(ApplicationServiceInitListener.getStorage() + "/multipleidentifiers.ttl");
            throw new Exception();
        }

        if (ApplicationServiceInitListener.debug) {
            domain.save(ApplicationServiceInitListener.getStorage() + "/debug.ttl");
        }

        // Validates if all assays have the same name
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getAssayTitles.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            if (resultLine.getLitInt("count") > 1) {
                resultLine.getLitString("desc");
                resultLine.getLitString("title");
                logging("It is advised that title and description is unique for each assay (e.g. Amplicon data from chicken sampled on day 30 from Farm 1)");
                // throw new Exception("Multiple usage of title and description detected in the assay descriptions");
            }
        }

        // Validate barcodes if they are used only once per library (prevents copy paste errors)
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getBarcodeValidation.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            // Only enters this area when the same barcode is used multiple times for the same library
            logging("The same barcode " + resultLine.getLitString("fwd") + " " + resultLine.getLitString("rev") + " used multiple times in the library " + resultLine.getIRI("library"));
            throw new Exception("Multiple usage of the barcode detected");
        }

        // Validate amplicon assays if they have only 2 datasets when they are still multiplexed
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getLibraryCount.txt", true).iterator();
        if (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            logging("One of the assays in library name " + resultLine.getLitString("libraryName") + " is associated with the wrong library");
            logging("The following library files are involved");
            while (resultLineIterator.hasNext()) {
                resultLine = resultLineIterator.next();
                logging(resultLine.getLitString("filename"));
            }
            // Only enters this area when the same barcode is used multiple times for the same library
            throw new Exception("Wrong library association detected");
        }

        // Final validation

        // Obtain all Investigation, Study, Observation Units, Assays, Samples
        HashMap<String, ArrayList<String>> store = new HashMap<>();

        // Get all the projects
        ArrayList<String> projects = new ArrayList<>();
        domain.getRDFSimpleCon().runQuery("getProjects.txt", true).iterator().
                forEachRemaining(resultLine -> projects.add(resultLine.getIRI("iri")));
        logger.info("Detected " + projects.size() + " investigations");
        store.put("projects", projects);

        // Get all the investigations
        ArrayList<String> investigations = new ArrayList<>();
        domain.getRDFSimpleCon().runQuery("getInvestigations.txt", true).iterator().
                forEachRemaining(resultLine -> investigations.add(resultLine.getIRI("iri")));
        logger.info("Detected " + investigations.size() + " investigations");
        store.put("investigations", investigations);

        // Get all the studies
        ArrayList<String> studies = new ArrayList<>();
        domain.getRDFSimpleCon().runQuery("getStudies.txt", true).iterator().
                forEachRemaining(resultLine -> studies.add(resultLine.getIRI("iri")));
        logger.info("Detected " + studies.size() + " studies");
        store.put("studies", studies);

        // Get all the observation units
        ArrayList<String> observationUnits = new ArrayList<>();
        domain.getRDFSimpleCon().runQuery("getObservationUnits.txt", true).iterator().
                forEachRemaining(resultLine -> observationUnits.add(resultLine.getIRI("iri")));
        logger.info("Detected " + observationUnits.size() + " observationunits");
        store.put("observationunits", observationUnits);

        // Get all the samples
        ArrayList<String> samples = new ArrayList<>();
        domain.getRDFSimpleCon().runQuery("getSamples.txt", true).iterator().
                forEachRemaining(resultLine -> samples.add(resultLine.getIRI("sample")));
        logger.info("Detected " + samples.size() + " samples");
        store.put("samples", samples);

        Integer sum = store.values().stream().mapToInt(List::size).sum();
        logging("Total number of objects " + sum);
        // Perform query to get the full connection
        domain.getRDFSimpleCon().runQuery("getAllConnections.txt", true).iterator().
                forEachRemaining(resultLine -> {
                    store.get("projects").remove(resultLine.getIRI("project"));
                    store.get("investigations").remove(resultLine.getIRI("investigation"));
                    store.get("studies").remove(resultLine.getIRI("study"));
                    store.get("observationunits").remove(resultLine.getIRI("observationUnit"));
                    // store.get("assay").remove(resultLine.getIRI("assay"));
                    store.get("samples").remove(resultLine.getIRI("sample"));
                });

        ArrayList<String> results = new ArrayList<>();
        store.values().forEach(results::addAll);

        if (results.size() > 0) {
            ArrayList<String> sampleList = store.get("samples");
            if (sampleList.size() > 0) {
                if (sampleList.size() > 5) {
                    sampleList = new ArrayList<>(sampleList.subList(0, 4));
                    sampleList.add("... more samples have been found");
                }
                logging("Not all samples are coupled to assays which could be intentionally, please check.");
                for (String sample : sampleList) {
                    logging(sample);
                }
            }

            if (results.size() != store.get("samples").size()) {
                for (String result : results) {
                    logging("Mismatch with " + result + "... Indicating a typo in the identifiers between the different sheets");
                }

                throw new Exception("Mismatch with " + StringUtils.join(store.values(), " "));
            }
        } else {
            logging("All identifiers are accounted for saving the results");
        }

        // Double check with ontology for properties that are obligatory
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getProjects.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            Project project = (Project) domain.getObject(resultLineIterator.next().getIRI("iri"), Project.class);
            project.getIdentifier();
            for (Investigation investigation : project.getAllHasPart()) {
                for (Study study : investigation.getAllHasPart()) {
                    study.getIdentifier();
                    for (observation_unit observation_unit : study.getAllHasPart()) {
                        observation_unit.getIdentifier();
                        for (Sample sample : observation_unit.getAllHasPart()) {
                            sample.getIdentifier();
                            for (Assay assay : sample.getAllHasPart()) {
                                assay.getIdentifier();
                                for (Data_sample data_sample : assay.getAllHasPart()) {
                                    data_sample.getIdentifier();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Sample creation module from the sample sheet
     *
     * @param sheetName
     * @param sheet
     * @param metadataArrayList
     * @param logArea
     */
    private void sampleCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logger.info("Parsing sample sheet");
        boolean headerRow = true;

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(Arrays.asList(OBSERVATION_UNIT_IDENTIFIER));

        // To store the header of the excel sheet in
        ArrayList<String> header = new ArrayList<>();

        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing sample (" + row.getRowNum() + ")")));
            }
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }
                    String url = getPredicate(OBSERVATION_UNIT_IDENTIFIER, metadataArrayList);
                    Resource obsResource = getHigherLevel(OBSERVATION_UNIT_IDENTIFIER,row.getCell(header.indexOf(OBSERVATION_UNIT_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea);
                    observation_unit observation_unit = domain.make(observation_unit.class, obsResource.getURI());

                    Sample sample = domain.make(Sample.class, PREFIX + new URL(obsResource.getURI()).getPath().replaceAll("^/","") + "/sam_" + row.getCell(header.indexOf(SAMPLE_IDENTIFIER)).getStringCellValue().trim());

                    otherColumns(header, parsedHeaders, row, sample.getResource().getURI(), metadataArrayList);

                    observation_unit.addHasPart(sample);

                    // Generate a process event
                    String isa_process = "http://isa-tools.org/process/" + observation_unit.getIdentifier() + "/" + sample.getIdentifier();
                    domain.getRDFSimpleCon().add(isa_process, "rdf:type", "http://www.wikidata.org/wiki/Q3249551-Process");
                    domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P2283-INPUT", observation_unit.getResource().getURI());
                    domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P1056-OUTPUT", sample.getResource().getURI());
                    // If collection date present
                    String collection_date = Generic.getValueOfPredicate(domain, sample.getResource().getURI(), "base:collection_date");
                    if (collection_date != null) {
                        LocalDate date = LocalDate.parse(collection_date.split("\\^\\^")[0]);
                        domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P1390-Date", date);
                    }
                    // Observation unit type same as ISA source
                     domain.getRDFSimpleCon().add(observation_unit.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q53617407-Source");
                    // Sheet/Package name
                    if (sheetName.contains(" - "))
                        sample.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://fairbydesign.nl/ontology/packageName"), sheetName.split(" - ")[1]);
                }
            }
        }
    }

    /**
     * Checks if the obligatory fields are filled in for each row
     *
     * @param header            the specific column / header being requested
     * @param row               the row being parsed
     * @param metadataArrayList is a list of metadata objects
     * @return
     */
    private boolean checkObligatoryFields(ArrayList<String> header, Row row, ArrayList<Metadata> metadataArrayList) {
        boolean status = false;

        // String identifier = row.getCell(0).getStringCellValue().trim();
        for (Metadata metadata : metadataArrayList) {
            // Non obligatory elements are skipped
            if (!metadata.getRequirement().toLowerCase().contains("mandatory")) continue;
            // Obtain name of metadata element
            if (header.indexOf(metadata.getLabel()) < 0) {
                logging("in sheet \"" + row.getSheet().getSheetName() + "\" row " + row.getRowNum() + " does not contain column \"" + metadata.getLabel() + "\" which is obligatory");
                status = true;
            } else {
                CellType cellType = row.getCell(header.indexOf(metadata.getLabel()), Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType();
                if (cellType.equals(CellType.BLANK)) {
                    if (metadata.getRegex() != null && !metadata.getRegex().pattern().matches("\\.\\*")) {
                        String pattern = metadata.getRegex().pattern();
                        if (pattern.length() > 102) {
                            pattern = metadata.getRegex().pattern().substring(1,100) + "....";
                        }
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory. \nThis value needs to match the pattern of " + pattern +"\n");
                    } else if (metadata.getOntology() != null) {
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory (ontology)\nDefinition: " + metadata.getDefinition()+"\n");
                    } else {
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory (free text)\nDefinition: " + metadata.getDefinition()+"\n");
                    }
                    status = true;
                }
            }
        }
        return status;
    }

    /**
     * Corrects URLS which could contain a space due to naming conventions, all should eventually be replaced by PID's
     *
     * @param iriPart part of the IRI that needs to be corrected
     * @return corrected IRI
     */
    private String iriCorrector(String iriPart) {
        return iriPart.trim().replaceAll("[ \\(\\)]", "_").replaceAll(" +", "_").replaceAll("_+", "_").replaceAll("_$","");
    }

    /**
     * @param sheetName
     * @param sheet
     * @param metadataArrayList
     * @param logArea
     * @throws Exception
     */
    private void assayCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logger.info("Parsing " + sheet.getSheetName());
        ArrayList<String> header = new ArrayList<>();

        // Header capture
        boolean headerRow = true;
        // Iterating over each row
        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing assay (" + row.getRowNum() + ")")));
            }

            // First cell has to be a string... and due to numeric identifiers sometimes it will be parsed incorrectly
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            // Standard row/cell check
            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);

                    if (status) {
                        // Logging from validate obligatory contains information that have been passed to the interface
                        throw new Exception("");
                    }

                    // TODO Reduce list to the core elements
                    Set<String> parsedHeaders = new HashSet<>();

                    // Obtain identifier
                    int index = header.indexOf(ASSAY_IDENTIFIER);
                    String assayIdentifier = row.getCell(index).getStringCellValue().trim();

                    /**
                     * Specific sequence assay information
                     */

                    // Check if assay identifier exists?
                    if (domain.getRDFSimpleCon().runQuery("getAssay.txt", true, assayIdentifier).iterator().hasNext()) {
                        // Duplication detected, check if SameAs is present
                        if (header.indexOf(SAME_AS) == -1) {
                            throw new DuplicateDataException("Assay with identifier " + assayIdentifier + " already exists!\n" +
                                    "Check for duplicate row entry in your input file");
                        } else {
                            // Further, testing if it is of the same type assayType
                            // TODO Sheet types changed as everything is now of type Assay
                            if (row.getCell(header.indexOf(SAME_AS)) == null) {
                                throw new Exception("Identifier " + assayIdentifier + " has a duplicate entry but no value for the SameAs column.");
                            }
                            String sameAsValue = getStringValueCell(row.getCell(header.indexOf(SAME_AS))).trim();
                            // If identifier is not the same throw an error
                            if (!assayIdentifier.equals(sameAsValue)) {
                                logging("SameAs detected but assay identifier not the same as sameAs identifier " + assayIdentifier + " and " + sameAsValue);
                                throw new DuplicateDataException("SameAs detected but assay identifier not the same as sameAs identifier " + assayIdentifier + " and " + sameAsValue);
                            } else {
                                // Duplicate assay with same identifier detected
                                parsedHeaders.add("Same as");
                            }
                        }
                    }

                    Resource sampleResource = getHigherLevel(SAMPLE_IDENTIFIER,row.getCell(header.indexOf(SAMPLE_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea);
                    Sample sample = domain.make(Sample.class, sampleResource.getURI());

                    Assay assay = domain.make(Assay.class, PREFIX + new URL(sampleResource.getURI()).getPath().replaceAll("^/","") + "/asy_" + row.getCell(header.indexOf(ASSAY_IDENTIFIER)).getStringCellValue().trim());

                    /**
                     * Generic Assay information
                     */

                    Cell fileCell = null;
//                    if (header.indexOf(FORWARD_FILENAME) != -1) {
//                        fileCell = row.getCell(header.indexOf(FORWARD_FILENAME), Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
//                    } else {
//                        logger.debug("No file found for " + assay.getResource().getURI());
//                    }

                    // Not null and not blank
//                    if (fileCell != null && !fileCell.getCellType().equals(CellType.BLANK)) {
//                        String fileName = getStringValueCell(fileCell);
                        // Check the FileNameForwardPath and FileNameReversePath first
//                        boolean paired = header.contains(REVERSE_FILENAME) && row.getCell(header.indexOf(REVERSE_FILENAME)) != null && row.getCell(header.indexOf(REVERSE_FILENAME)).getStringCellValue().trim().length() > 0;

//                        if (paired) {
//                            Data_sample pairedSequenceDataSetForward = domain.make(Data_sample.class, PREFIX + fileName);
//                            pairedSequenceDataSetForward.setIdentifier(new File(fileName).getName().trim());
//                            pairedSequenceDataSetForward.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/fileFormat"), setFileFormat(pairedSequenceDataSetForward.getIdentifier()));
//                            // TODO reminder... this should be known at assay level
//
//                            String fileNameReverse = row.getCell(header.indexOf(REVERSE_FILENAME)).getStringCellValue().trim();
//                            if (fileNameReverse.contains("Enter only when data is demultiplexed"))
//                                fileNameReverse = "";
//
//                            if (fileNameReverse.length() > 0) {
//                                Data_sample pairedSequenceDataSetReverse = domain.make(Data_sample.class, PREFIX + fileNameReverse);
//                                pairedSequenceDataSetReverse.setIdentifier(new File(fileNameReverse).getName().trim());
//                                pairedSequenceDataSetReverse.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/fileFormat"), setFileFormat(pairedSequenceDataSetForward.getIdentifier()));
//                                // Cross-linking
//                                pairedSequenceDataSetReverse.addHasPart(pairedSequenceDataSetForward);
//                                pairedSequenceDataSetForward.addHasPart(pairedSequenceDataSetReverse);
//                                assay.addHasPart(pairedSequenceDataSetForward);
//                                assay.addHasPart(pairedSequenceDataSetReverse);
//                                parsedHeaders.addAll(Arrays.asList(FORWARD_FILENAME, REVERSE_FILENAME));
//                            }
//                            // If it is one of the sequence assay types
//                        } else if (!assay.getClassTypeIri().endsWith("/Assay")) {
//                            Data_sample singleSequenceDataSet = domain.make(Data_sample.class, PREFIX + fileName);
//                            singleSequenceDataSet.setIdentifier(new File(fileName).getName().trim());
//                            singleSequenceDataSet.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/fileFormat"), setFileFormat(singleSequenceDataSet.getIdentifier()));
//                            assay.addHasPart(singleSequenceDataSet);
//                            parsedHeaders.addAll(Arrays.asList(FORWARD_FILENAME));
//                        }
//                    }

//                    // Perform reference incorporation
//                    if (header.indexOf(REFERENCE) != -1) {
//                        parsedHeaders.addAll(Arrays.asList(REFERENCE));
//                        // Check if reference has value
//                        if (row.getCell(header.indexOf(REFERENCE)) != null) {
//                            String referenceName = row.getCell(header.indexOf(REFERENCE)).getStringCellValue().trim();
//                            Data_sample dataSample = domain.make(Data_sample.class, PREFIX + referenceName);
//                            dataSample.setIdentifier(referenceName.trim());
//                            assay.getResource().addProperty(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/reference"), dataSample.getResource().getURI());
//                        }
//                    }
                    // Parse all other columns
                    parsedHeaders.add(SAMPLE_IDENTIFIER);
                    otherColumns(header, parsedHeaders, row, assay.getResource().getURI(), metadataArrayList);

                    // Sheet/Package name
                    if (sheetName.contains(" - "))
                        assay.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://fairbydesign.nl/ontology/packageName"), sheetName.split(" - ")[1]);

                    // Coupling the assay to the sample
                    sample.addHasPart(assay);
                }
            }
        }
    }

    private Resource getHigherLevel(String identifier,String value, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        String url = getPredicate(identifier, metadataArrayList);
        ResIterator resIterator = domain.getRDFSimpleCon().getModel().listSubjectsWithProperty(domain.getRDFSimpleCon().getModel().createProperty(url), value);
        if (resIterator.hasNext())
            return resIterator.next();
        String message = "Parent not found with " + identifier +" "+ value;
        if (logArea !=  null) {
            logging(message);
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(message)));
        } else {
            System.err.println(message);
        }
        throw new Exception("");
    }

    private boolean isBlank(Row row, int columnIndex) {
        return row.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType().equals(CellType.BLANK);
    }


    /**
     * @param sampleIdentifier
     * @return
     * @throws Exception
     */
    private Sample getSample(String sampleIdentifier) throws Exception {
        try {
            return domain.make(Sample.class, domain.getRDFSimpleCon().runQuerySingleRes("getSample.txt", false, sampleIdentifier).getIRI("sample"));
        } catch (Exception e) {
            // Get all observed units
            domain.getRDFSimpleCon().runQuery("getSamples.txt", true).iterator().forEachRemaining(resultLine -> logger.debug("Samples available: " + resultLine.getIRI("sample")));
            // domain.save("getSamples.ttl");
            throw new Exception("Cannot find any assay with this sample identifier: \"" + sampleIdentifier + "\"");
        }
    }

    private observation_unit getObservationUnit(String observationUnitIdentifier) throws Exception {
        try {
            return domain.make(observation_unit.class, domain.getRDFSimpleCon().runQuerySingleRes("getObservationUnit.txt", false, observationUnitIdentifier).getIRI("observationUnit"));
        } catch (Exception e) {
            // Get all observed units
            domain.getRDFSimpleCon().runQuery("getObservationUnits.txt", true).iterator().forEachRemaining(resultLine -> logging("Observation units available: " + resultLine.getLitString("observationUnit")));
            domain.save(ApplicationServiceInitListener.getStorage() + "/issue.ttl");
            exception("Cannot find any sample coupled to this observation unit: \"" + observationUnitIdentifier + "\"");
            return null;
        }
    }

    private void exception(String exception) throws Exception {
        message = message + "\n" + exception;
        throw new Exception(exception);
    }

    private Study getStudy(String studyIdentifier) throws Exception {
        try {
            return domain.make(Study.class, domain.getRDFSimpleCon().runQuerySingleRes("getStudy.txt", false, studyIdentifier).getIRI("study"));
        } catch (Exception e) {
            // Get all studies
            domain.getRDFSimpleCon().runQuery("getStudies.txt", true).iterator().forEachRemaining(resultLine -> logging("Studies available: " + resultLine.getLitString("observationUnit")));
            exception("Cannot find any observation unit coupled to this study: \"" + studyIdentifier + "\"");
            return null;
        }
    }

    private Investigation getInvestigation(String investigationIdentifier) throws Exception {
        try {
            return domain.make(Investigation.class, domain.getRDFSimpleCon().runQuerySingleRes("getInvestigation.txt", false, investigationIdentifier).getIRI("investigation"));
        } catch (Exception e) {
            // Get all studies
            domain.getRDFSimpleCon().runQuery("getInvestigations.txt", true).iterator().forEachRemaining(resultLine -> logging("Investigations available: " + resultLine.getLitString("investigation")));
            exception("Cannot find a study coupled to this investigation: \"" + investigationIdentifier + "\"");
            return null;
        }
    }

    /**
     * Acquire project identifier which is created at the investigation sheet
     *
     * @param projectIdentifier the identifier for the project
     * @return the project identifier
     * @throws Exception
     */
    private Project getProject(String projectIdentifier) throws Exception {
        try {
            return domain.make(Project.class, domain.getRDFSimpleCon().runQuerySingleRes("getProject.txt", false, projectIdentifier).getIRI("project"));
        } catch (Exception e) {
            // Get all studies
            domain.getRDFSimpleCon().runQuery("getProjects.txt", true).iterator().forEachRemaining(resultLine -> logging("Projects available: " + resultLine.getLitString("project")));
            exception("Cannot find project with identifier: >" + projectIdentifier + "< assuming a mismatch with the investigation");
            return null;
        }
    }

    /**
     * @param sheetName
     * @param sheet     The observation unit sheet
     * @param logArea
     * @throws Exception
     */
    private void observationUnitCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Observation unit creation");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();
        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(Arrays.asList(STUDY_IDENTIFIER));

        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing observation unit (" + row.getRowNum() + ")")));
            }

            // Default row parser... First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    String url = getPredicate(STUDY_IDENTIFIER, metadataArrayList);
                    Resource studyResource = getHigherLevel(STUDY_IDENTIFIER,row.getCell(header.indexOf(STUDY_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea);
                    Study study = domain.make(Study.class, studyResource.getURI());

                    observation_unit observationUnit = domain.make(observation_unit.class, PREFIX + new URL(studyResource.getURI()).getPath().replaceAll("^/","") + "/obs_" + row.getCell(header.indexOf(OBSERVATION_UNIT_IDENTIFIER)).getStringCellValue().trim());

                    otherColumns(header, parsedHeaders, row, observationUnit.getResource().getURI(), metadataArrayList);

                    // Sheet/Package name
                    if (sheetName.contains(" - "))
                        observationUnit.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://fairbydesign.nl/ontology/packageName"), sheetName.split(" - ")[1]);
                    study.addHasPart(observationUnit);
                }
            }
        }
    }

    private void projectCreation(Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Project creation");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        Set<String> parsedHeaders = new HashSet<>();

        for (Row row : sheet) {
            // Default row parser... First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    // Obtaining project information from investigation
                    Project project = domain.make(Project.class, PREFIX + "prj_" + row.getCell(header.indexOf(PROJECT_IDENTIFIER)).getStringCellValue().trim());
                    otherColumns(header, parsedHeaders, row, project.getResource().getURI(), metadataArrayList);
                }
            }
        }
    }

    private void studyCreation(Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Study creation");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(Arrays.asList(INVESTIGATION_IDENTIFIER));

        for (Row row : sheet) {
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    // Init investigation
                    String url = getPredicate(INVESTIGATION_IDENTIFIER, metadataArrayList);
                    Resource investigationResource = getHigherLevel(INVESTIGATION_IDENTIFIER,row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea);
                    Investigation investigation = domain.make(Investigation.class, investigationResource.getURI());

                    // Obtaining study from Observation Unit
                    Study study = domain.make(Study.class, PREFIX + new URL(investigationResource.getURI()).getPath().replaceAll("^/","") + "/stu_" + row.getCell(header.indexOf(STUDY_IDENTIFIER)).getStringCellValue().trim());

                    // ISA Mapping
//                    domain.getRDFSimpleCon().add(investigation.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q170584-Investigation");
                    // investigation.getAllOwnedBy().forEach(element -> investigation.addOwnedBy(element));

                    otherColumns(header, parsedHeaders, row, study.getResource().getURI(), metadataArrayList);

                    investigation.addHasPart(study);
                }
            }
        }
    }

    private boolean checkNotEmpty(Row row) {
        Iterator<Cell> rowIter = row.iterator();
        while (rowIter.hasNext()) {
            Cell cell = rowIter.next();
            if (!cell.getCellType().equals(CellType.BLANK))
                return true;
        }
        return false;
    }

    private String getProjectIdentifier(Sheet sheet) {
        logging("Get project identifier");
        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        for (Row row : sheet) {
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    // Project identifier
                    int index = header.indexOf(PROJECT_IDENTIFIER);
                    return iriCorrector(row.getCell(index).getStringCellValue().trim().trim());
                }
            }
        }
        return null;
    }

    /**
     * @param sheet
     * @param logArea
     * @throws Exception
     */
    private void investigationCreation(Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Investigation creation");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(Arrays.asList("First name", "Last name", "lastname", "firstname", EMAIL, ORCID, ORGANIZATION, DEPARTMENT));

        for (Row row : sheet) {
            // Default row parser... First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }
                    String url = getPredicate(PROJECT_IDENTIFIER, metadataArrayList);
                    // Init project
                    Resource projectResource = getHigherLevel(PROJECT_IDENTIFIER,row.getCell(header.indexOf(PROJECT_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea);
                    Project project = domain.make(Project.class, projectResource.getURI());

                    Investigation investigation = domain.make(Investigation.class, PREFIX + new URL(projectResource.getURI()).getPath().replaceAll("^/","") + "/inv_" + row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue().trim());

                    if (investigation == null) {
                        throw new Exception("Investigation not matching to study, reason could be that there is no study with this investigation: " + row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue().trim().trim());
                    }

                    // FirstName	LastName	Email
                    // TODO remove this from the final file due to person information?
                    Person person = domain.make(Person.class, investigation.getResource().getURI() + "/person/" + DigestUtils.md5Hex(row.getCell(header.indexOf(EMAIL)).getStringCellValue().trim().trim()));
                    // Add same as for person to Q215627
                    domain.getRDFSimpleCon().add(person.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q215627-Person");
                    // Other information
                    person.setGivenName(row.getCell(header.indexOf(FIRSTNAME)).getStringCellValue().trim().trim());
                    person.setFamilyName(row.getCell(header.indexOf(LASTNAME)).getStringCellValue().trim().trim());
                    person.setEmail("mailto:" + row.getCell(header.indexOf(EMAIL)).getStringCellValue().trim().trim());
                    person.setGivenName(person.getGivenName() + " " + person.getFamilyName());
                    person.setDepartment(row.getCell(header.indexOf(DEPARTMENT)).getStringCellValue().trim().trim());

                    Organization organization = domain.make(Organization.class, "http://m-unlock.nl/ontology/organization/" + row.getCell(header.indexOf(ORGANIZATION)).getStringCellValue().trim().trim().replaceAll(" ", "_"));
                    organization.setLegalName(row.getCell(header.indexOf(ORGANIZATION)).getStringCellValue().trim().trim());

                    person.addMemberOf(organization);

                    // TODO clean this up
                    parsedHeaders.add("familyName");
                    parsedHeaders.add("givenName");
                    // If orcid is available, to be removed and generalised
                    if (header.indexOf(ORCID) >= 0 && row.getCell(header.indexOf(ORCID)) != null) {
                        String value = getStringValueCell(row.getCell(header.indexOf(ORCID))).trim().trim();
                        if (value.length() > 1)
                            person.setOrcid("https://orcid.org/" + value);
                    }

                    // TODO Check if contribution works
                    investigation.getResource().addProperty(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/contributor"), person.getResource());

                    // Check for other arguments
                    parsedHeaders.add("Project identifier");
                    otherColumns(header, parsedHeaders, row, investigation.getResource().getURI(), metadataArrayList);

                    // Investigation attachment to project
                    project.addHasPart(investigation);
                }
            }
        }
    }

    private String getPredicate(String label, ArrayList<Metadata> metadataArrayList) {
        for (Metadata metadata : metadataArrayList) {
            if (metadata.getLabel().equalsIgnoreCase(label)) {
                return metadata.getURL();
            }
        }
        return null;
    }

    /**
     * @param header            the header of the sheet
     * @param ignoredHeaders    headers that have already been parsed and can be ignored
     * @param row               the row that is being processed
     * @param iri               the base url used?
     * @param metadataArrayList the metadata objects for this particular class type e.g. assay, sample, investigation
     */
    private void otherColumns(ArrayList<String> header, Set<String> ignoredHeaders, Row row, String iri, ArrayList<Metadata> metadataArrayList) throws Exception {
        // Transform array into hashmap
        HashMap<String, Metadata> metadataLookup = new HashMap<>();
        for (Metadata metadata : metadataArrayList) {
            metadataLookup.put(metadata.getLabel(), metadata);
        }

        Iterator<String> headerIter = header.iterator();
        // Parsing all the headers
        while (headerIter.hasNext()) {
            // String headerItem = headerIter.next();
            String headerItemOriginal = headerIter.next();
            if (headerItemOriginal == null) continue;
            // Skip processed headers and ignore casing
            boolean containsSearchStr = ignoredHeaders.stream().anyMatch(headerItemOriginal::equalsIgnoreCase);
            if (!containsSearchStr) {
                // Predicate element
                String predicate = null;

                // Obtain the cell and if empty / null skip it
                Cell cell = row.getCell(header.indexOf(headerItemOriginal));
                if (cell == null) continue;

                // If the metadata key in the lookup list process accordingly otherwise process dynamically
                Metadata metadata = null;
                if (metadataLookup.containsKey(headerItemOriginal)) {
                    metadata = metadataLookup.get(headerItemOriginal);
                    String value = getStringValueCell(cell);
                    if (metadata.getOntology() != null) {
                        if (metadata.getRequirement().toLowerCase().matches("mandatory") && value.length() == 0) {
                            logging("There is no value for the obligatory field " + metadata.getLabel() + " and should match a label in the ontology " + metadata.getOntology() + " such as in example " + metadata.getExample());
                            throw new IllegalFormatException(metadata.getSyntax() + "\n" + value);
                        }
                        if (!ontologies.containsKey(metadata.getOntology())) {
                            // Load the directory as a domain
                            Domain ontologyDomain = new Domain("file://" + metadata.getOntology());
                            ontologies.put(metadata.getOntology(), ontologyDomain);
                        }
                        // Check for the label
                        Property rdfslabel = ontologies.get(metadata.getOntology()).getRDFSimpleCon().getModel().createProperty("http://www.w3.org/2000/01/rdf-schema#label");
                        ResIterator resIterator = ontologies.get(metadata.getOntology()).getRDFSimpleCon().getModel().listSubjectsWithProperty(rdfslabel, value);

                        if (!resIterator.hasNext()) {
                            boolean status = wideSearch(value, metadata.getOntology(), rdfslabel);
                            if (!status) {
                                // This label value is not part of the ontology
                                logging("The value \"" + value + "\" does not match any labels of the ontology <" + metadata.getSyntax() +
                                        ">\n for example \"" + metadata.getExample() + "\" could be used.\n" +
                                        metadata.getDefinition() + "\n" +
                                        "Possible terms similar to your statement are:\n");

                                // Another iteration to show possible names and then stop with the validation
                                resIterator = ontologies.get(metadata.getOntology()).getRDFSimpleCon().getModel().listSubjectsWithProperty(rdfslabel);
                                String finalValue = value;
                                AtomicBoolean hit = new AtomicBoolean(false);
                                resIterator.forEachRemaining(item -> {
                                    item.listProperties(rdfslabel).forEachRemaining(statement -> {
                                        String object = statement.getObject().toString().replaceAll("@..$","");
                                        if (object.toLowerCase().contains(finalValue.toLowerCase())) {
                                            // Language tag removal
                                            logging("\""+object+"\"");
                                            hit.set(true);
                                        }
                                    });
                                });

                                if (!hit.get()) {
                                    logging("Unfortunately no similar terms were detected please check for any obvious typo's or the ontology content online.");
                                }
                                throw new IllegalFormatException();
                            }
                        } else {
                            logger.debug("Match found with " + value);
                        }
                    } else {
                        Pattern pattern = metadata.getRegex();
                        // Clear out multi line strings
                        value = value.replaceAll("[\n\r]", " ");
                        // ignoring empty cells
                        if (metadata.getRequirement().toLowerCase().matches("mandatory") && value.length() == 0) {
                            logging("There is no value for the obligatory field " + metadata.getLabel() + " and should match pattern " + metadata.getSyntax() + " such as in example " + metadata.getExample());
                            throw new IllegalFormatException(metadata.getSyntax() + "\n" + value);
                        }

                        // Ignore empty cells that are not obligatory
                        if (value.length() == 0) continue;

                        Matcher m = pattern.matcher(value);
                        // It passes the regex validation
                        if (m.matches()) {
                            predicate = metadata.getURL();
                        } else {
                            // Message for normal field and obligatory field
                            String additionalLine = " does not match the pattern of " + metadata.getSyntax() + " regex " + metadata.getRegex().pattern() + " such as in example \"" + metadata.getExample() + "\"";
                            String prefixLine = "The value \"" + value + "\" of \"" + metadata.getLabel() + "\" in the \"" + metadata.getSheetName() + "\" sheet";
                            if (metadata.getRequirement().toLowerCase().contains("mandatory")) {
                                logging(prefixLine + " which is obligatory " + additionalLine);
                            } else {
                                logging(prefixLine + additionalLine);
                            }

                            // Minimum length
                            if (pattern.pattern().matches(".*\\{[0-9]+,}.*")) {
                                logging("A minimum length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+),}.*", "$1") + ") is set for " + metadata.getLabel());
                            }
                            // Exact length
                            if (pattern.pattern().matches(".*\\{[0-9]+}.*")) {
                                logging("An exact length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+)}.*", "$1") + ") is set for " + metadata.getLabel());
                            }
                            // Range length
                            if (pattern.pattern().matches(".*\\{[0-9]+,[0-9]+}.*")) {
                                logging("A range length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+,[0-9]+)}.*", "$1") + ") is set for " + metadata.getLabel());
                            }

                            throw new IllegalFormatException(metadata.getSyntax() + "\n" + value);
                        }
                    }
                }

                // If no predicate was available from the excel sheet
                if (predicate != null && predicate.trim().length() == 0)
                    predicate = null;

                if (predicate == null) {
                    // Parsing elements from the standardisation and also optionally added by the user not in the metadata excel sheet

                    // Processing the header name as it can contain non acceptable chars
                    String headerItem = iriCorrector(headerItemOriginal);

                    // Replace non ascii characters by closed character
                    headerItem = Normalizer.normalize(headerItem, Normalizer.Form.NFD);
                    headerItem = headerItem.replaceAll("[^\\x00-\\x7F]", "");
                    headerItem = headerItem.replaceAll("/", "_");

                    // It is null if the user created this column by itself
                    logger.debug("No predicate defined for " + headerItemOriginal);
                    predicate = PREFIX + headerItem.toLowerCase();
                }

                logger.debug("Predicate value: " + predicate);

                // If cell is not null / empty see if we can map to appropriate triple type
                switch (cell.getCellType()) {
                    case BOOLEAN:
                        domain.getRDFSimpleCon().add(iri, predicate, cell.getBooleanCellValue());
                        break;
                    case NUMERIC:
                        if (isCellDateFormatted(cell)) {
                            Date date = cell.getDateCellValue();
                            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            domain.getRDFSimpleCon().add(iri, predicate, localDate);
                        } else {
                            if (cell.getNumericCellValue() % 1 == 0) {
                                domain.getRDFSimpleCon().add(iri, predicate, (int) cell.getNumericCellValue());
                            } else {
                                domain.getRDFSimpleCon().add(iri, predicate, cell.getNumericCellValue());
                            }
                        }
                        break;
                    case BLANK:
                    case ERROR:
                    case _NONE:
                    case FORMULA:
                        String objectValue = getStringValueCell(cell).trim().trim();
                        if (objectValue.length() > 0)
                            domain.getRDFSimpleCon().addLit(iri, predicate, getStringValueCell(cell).trim().trim());
                    case STRING:
                        if (getStringValueCell(cell).trim().trim().length() == 0) break;
                        // Check if it is a normal string or a file object

                        if (metadata == null || !metadata.isFile()) {
                            objectValue = getStringValueCell(cell).trim().trim();
                            logger.debug("Metadata predicate " + predicate + " with value " + objectValue);
                            domain.getRDFSimpleCon().addLit(iri, predicate, objectValue);
                        } else {
                            String fileName = getStringValueCell(cell).trim().trim();
                            Data_sample data_sample = domain.make(Data_sample.class, PREFIX + fileName);
                            data_sample.setIdentifier(new File(fileName).getName().trim());
                            data_sample.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/description"), headerItemOriginal);
                            // Any level can have files attached now.
                            domain.getRDFSimpleCon().add(iri, "jerm:hasPart", data_sample.getResource());
                        }
                        break;
                    default:
                        logging("Unknown cell type, please contact admin to improve the code : " + cell.getCellType().name());
                        domain.getRDFSimpleCon().addLit(iri, predicate, getStringValueCell(cell).trim().trim());
                }

                // Check if predicate already has a type property and
                // if not create the predicate with rdf type and label
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(predicate, "rdf:type");
                if (!nodeIterator.hasNext()) {
                    // Add predicate RDFS statement
                    domain.getRDFSimpleCon().add(predicate, "rdf:type", "rdf:Property");
                    if (metadata != null) {
                        logger.debug("Standardised label: " + metadata.getLabel());
                        domain.getRDFSimpleCon().addLit(predicate, "rdfs:label", metadata.getLabel());
                        // Add other elements such as unit or regex?
                        if (metadata.getRegex() != null)
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", metadata.getRegex().pattern());
                        else
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", metadata.getOntology().toString());
                        if (metadata.getRequirement().equalsIgnoreCase("mandatory"))
                            domain.getRDFSimpleCon().add(predicate, "http://schema.org/valueRequired", true);
                        else
                            domain.getRDFSimpleCon().add(predicate, "http://schema.org/valueRequired", false);
                        // TODO unit can be a list of units? or a regex?
                        if (metadata.getPreferredUnit() != null && metadata.getPreferredUnit().length() > 0)
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/unitCode", metadata.getPreferredUnit());
                    } else {
                        // Self invented element by researcher, still important to capture!
                        logger.debug("Self made label: " + headerItemOriginal);
                        // String value = StringUtils.join(splitCamelCase(predicate.replaceAll(".*/", "")), " ").toLowerCase();
                        domain.getRDFSimpleCon().addLit(predicate, "rdfs:label", headerItemOriginal);
                        domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", ".+");
                    }
                }
            }
        }
    }

    private boolean wideSearch(String value, File ontology, Property rdfslabel) {
        // Obtain all partially matched labels
        ResIterator resIterator = ontologies.get(ontology).getRDFSimpleCon().getModel().listSubjectsWithProperty(rdfslabel);

        String finalValue = value;
        AtomicBoolean hit = new AtomicBoolean(false);
        resIterator.forEachRemaining(item -> {
            item.listProperties(rdfslabel).forEachRemaining(statement -> {
                String object = statement.getObject().toString().replaceAll("@..$","");
                if (object.equalsIgnoreCase(finalValue.toLowerCase())) {
                    // Language tag removal
                    hit.set(true);
                }
            });
        });
        return hit.get();
    }


    /**
     *
     * @param row Creates a column index lookup based on naming scheme
     * @return
     */
    private static ArrayList<String> getHeader(Row row) {
        ArrayList<String> headerLookup = new ArrayList<>();
        for (int j = 0; j < row.getLastCellNum(); j++) {
            Cell cell = row.getCell(j);
            try {
                headerLookup.add(cell.getStringCellValue().trim().trim().toLowerCase());
            } catch (NullPointerException e) {
                headerLookup.add(null);
            }
        }
        return headerLookup;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
