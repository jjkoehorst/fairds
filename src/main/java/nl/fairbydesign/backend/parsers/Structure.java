package nl.fairbydesign.backend.parsers;

import nl.fairbydesign.views.authentication.LoginView;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.jermontology.ontology.JERMOntology.domain.*;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class Structure {
    private static final Logger logger = LoggerFactory.getLogger(LoginView.class);

    public static void create(String rdfFile) {
        // Get project and iterated from there?
        try {
            Domain domain = new Domain("file://" + rdfFile);
            Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getProjects.txt", true);
            String root = "unlock";
            for (ResultLine resultLine : resultLines) {
                Project project = domain.make(Project.class, resultLine.getIRI("iri"));
                new File(root + "/PRJ_" + project.getIdentifier()).mkdirs();
                for (Investigation investigation : project.getAllHasPart()) {
                    new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier()).mkdirs();
                    for (Study study : investigation.getAllHasPart()) {
                        new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier()).mkdirs();
                        for (observation_unit observation_unit : study.getAllHasPart()) {
                            new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier()).mkdirs();
                            for (Sample sample : observation_unit.getAllHasPart()) {
                                new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier() + "/SAM_" + sample.getIdentifier()).mkdirs();
                                for (Assay assay : sample.getAllHasPart()) {
                                    String type = assay.getClassTypeIri().replaceAll(".*/", "").toLowerCase();
                                    new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier() + "/SAM_" + sample.getIdentifier() + "/"  + type + "/ASY_" + assay.getIdentifier()).mkdirs();
                                    for (Data_sample data_sample : assay.getAllHasPart()) {
                                        File folder = new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier() + "/SAM_" + sample.getIdentifier() + "/"  + type + "/ASY_" + assay.getIdentifier() + "/unprocessed");
                                        folder.mkdirs();
                                        new File(folder + "/" + data_sample.getIdentifier()).createNewFile();
                                    }
                                    // If there were no data files check for amplicon assay
                                    // Check for amplicon assays if it has data files if not
                                    if (assay.getAllHasPart().size() == 0 && assay.getClassTypeIri().endsWith("/AmpliconAssay")) {
                                        System.err.println("PROBABLY NEVER HERE...");
//                                        AmpliconAssay ampliconAssay = (AmpliconAssay) assay;
//                                        File folder = new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier() + "/SAM_" + sample.getIdentifier() + "/"  + type + "/ASY_" + assay.getIdentifier() + "/unprocessed");
//                                        folder.mkdirs();
//                                        new File(folder + "/REPLACE_" + assay.getIdentifier() + "_1.fastq.gz").createNewFile();
//                                        new File(folder + "/REPLACE_" + assay.getIdentifier() + "_2.fastq.gz").createNewFile();
                                    }
                                    String assaySnippetFile = assaySnippets(domain, assay);

                                    File moveTo = new File(root + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation.getIdentifier() + "/STU_" + study.getIdentifier() + "/OBS_" + observation_unit.getIdentifier() + "/SAM_" + sample.getIdentifier() + "/"  + type + "/ASY_" + assay.getIdentifier() + "/" + assaySnippetFile);
                                    new File(assaySnippetFile).renameTo(moveTo);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String assaySnippets(Domain domain, Assay assay) throws Exception {
        logger.debug("Creating assay snippet for " + assay.getIdentifier());
        // Making the RDF snippet for the assay set as long as it is not amplicon library data
        if (!assay.getClassTypeIri().endsWith("AmpliconLibraryAssay")) {
            System.err.println("Nothing is an amplicon library assay anymore right?...");
            // Get the sample corresponding to this assay as well
            ResultLine resultLine = domain.getRDFSimpleCon().runQuerySingleRes("getSampleFromAssay.txt", true, assay.getResource().getURI());
            Sample sample = domain.make(Sample.class, resultLine.getIRI("sample"));

            // Making a Assay specific RDF database with a snapshot of the data
            Domain assayDomain = new Domain("");
            assayDomain.getRDFSimpleCon().setNsPrefix("schema", "http://schema.org/");
            assayDomain.getRDFSimpleCon().setNsPrefix("unlock", "http://m-unlock.nl/ontology/");
            assayDomain.getRDFSimpleCon().setNsPrefix("jerm", "http://jermontology.org/ontology/JERMOntology#");

            Resource resource = domain.getRDFSimpleCon().getModel().getResource(sample.getResource().getURI());
            StmtIterator stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                // A skip step for assay predicate if not matching the original assay
                if (statement.getPredicate().getURI().contains("http://m-unlock.nl/ontology/assay")) {
                    // Check if iri matches
                    // System.err.println(statement.getObject().toString());
                    if (statement.getObject().toString().contains(assay.getResource().getURI())) {
                        assayDomain.getRDFSimpleCon().getModel().add(statement);
                    }
                } else {
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            resource = domain.getRDFSimpleCon().getModel().getResource(assay.getResource().getURI());
            stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                assayDomain.getRDFSimpleCon().getModel().add(statement);
            }

            for (Data_sample data_sample : assay.getAllHasPart()) {
                resource = domain.getRDFSimpleCon().getModel().getResource(data_sample.getResource().getURI());

                stmtIterator = resource.listProperties();
                while (stmtIterator.hasNext()) {
                    Statement statement = stmtIterator.next();
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            String assayFile = assay.getIdentifier() + ".ttl";
            assayDomain.save(assayFile, RDFFormat.TURTLE);
            assayDomain.close();

            return assayFile;
        }
        return null;
    }
}
