package nl.fairbydesign.backend.parsers;

import nl.fairbydesign.backend.data.objects.*;
import nl.fairbydesign.backend.data.objects.jerm.Investigation;
import nl.fairbydesign.backend.data.objects.jerm.ObservationUnit;
import nl.fairbydesign.backend.data.objects.jerm.Project;
import nl.fairbydesign.backend.data.objects.jerm.Study;
import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.commons.lang3.RandomStringUtils;
import org.jboss.logging.Logger;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.io.File;
import java.util.*;

import static nl.fairbydesign.backend.parsers.ExcelValidator.*;

/**
 * Main class for generating the excel file through the web interface
 */
public class ExcelGenerator {
    public static final Logger logger = Logger.getLogger(ExcelGenerator.class);

    public static final String OBSERVATION_UNIT_DESCRIPTION = "Observation unit description";
    public static final String OBSERVATION_UNIT_NAME = "Observation unit name";

    private static XSSFWorkbook workbook;
    public static CellStyle headerStyle;
    public static final int defaultCellWidth = 25;
    public static CellStyle obligatoryStyle;
    public static final String PROJECT_DESCRIPTION = "project description";
    public static final String PROJECT_TITLE = "project title";
    public static final String STUDY_DESCRIPTION = "study description";
    public static final String STUDY_TITLE = "study title";
    public static final String INVESTIGATION_TITLE = "investigation title";
    public static final String INVESTIGATION_DESCRIPTION = "investigation description";


    /**
     * @param project object containing project informaiton
     * @param investigation object containing investigation information
     * @param memberList that are part of this investigation
     * @param study the study involved
     * @param observationUnit the metadata of the observation units
     * @param gridSelection assay grid selection
     * @param samplePackage that was used from MIXS
     * @return the filename to be presented as download
     */
    public static File generateForm(Project project, Investigation investigation, List<Member> memberList, Study study, ObservationUnit observationUnit, HashMap<String, ArrayList<Metadata>> gridSelection, String samplePackage) {
        // Create workbook
        workbook = new XSSFWorkbook();

        // Create excel style object
        makeHeaderCellStyle();
        makeObligatoryHeaderCellStyle();

        // Create sheets
        generateProjectSheet(workbook, project);
        generateInvestigationSheet(workbook, project, investigation, memberList);
        generateStudySheet(workbook, investigation, study);

        // Creating observation units
        ArrayList<String> remove = new ArrayList<>();
        for (String key : gridSelection.keySet()) {
            // Key is likely to be path to filename for resource
            if (key.toLowerCase().contains("observationunit")) {
                generateObservationUnitSheet(workbook, study, observationUnit, gridSelection.get(key));
                remove.add(key);
                break;
            }
        }

        // Creating sample but can reuse assay code base, include package name into <sample - package> sheetname
        for (String key : gridSelection.keySet()) {
            if (key.toLowerCase().contains("sample")) {
                // Overwrite key name by including package name
                generateAssaySheet(workbook, key, gridSelection.get(key));
                remove.add(key);
            }
        }

        // Create assays, all for loops are separated to preserve order
        for (String key : gridSelection.keySet()) {
            if (key.toLowerCase().contains("assay")) {
                // Overwrite key name by including package name
                generateAssaySheet(workbook, key, gridSelection.get(key));
                remove.add(key);
            }
        }

        // Removing the observation unit and sample object
//        gridSelection.keySet().removeAll(remove);

        // Perform the rest of the assays
//        for (String key : gridSelection.keySet()) {
//            logger.info("Creating assay sheet " + key);
//            generateAssaySheet(workbook, key, gridSelection.get(key));
//        }

        // Save file and escape the / in a name
        File filename = new File(project.getIdentifier().replaceAll("/","-") + ".xlsx");
        try {
            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filename;
    }

    /** TODO merge with second function generateSampleSheet
     * @param workbook
     * @param key
     * @param metadataArrayList
     * @throws FileNotFoundException
     */
    private static void generateAssaySheet(Workbook workbook, String key, ArrayList<Metadata> metadataArrayList) {
        if (key.endsWith("Search a package"))
            return;

        Sheet sheet = workbook.createSheet(key);
        sheet.setDefaultColumnWidth(defaultCellWidth);
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < metadataArrayList.size(); i++) {
            Cell cell = headerRow.createCell(i);
            Metadata metadata = metadataArrayList.get(i);
            cell.setCellValue(metadataArrayList.get(i).getLabel());
            createComment(cell, metadata);
        }

        // Get obligatory elements
        HashSet<String> obligatory = new HashSet<>();
        for (Metadata metadata : metadataArrayList) {
            if (metadata.getRequirement() != null && metadata.getRequirement().toLowerCase().contains("mandatory")) {
                obligatory.add(metadata.getLabel());
            }
        }

        // Set header style with obligatory option
        headerRow.cellIterator().forEachRemaining(cell -> {
            if (obligatory.contains(cell.getStringCellValue())) {
                cell.setCellStyle(obligatoryStyle);
            } else {
                cell.setCellStyle(headerStyle);
            }
        });
    }

    /**
     * @param workbook object that will be transformed into an excel file
     * @param project object to be placed in the project sheet of the workbook object
     */
    private static void generateProjectSheet(Workbook workbook, Project project) {
        logger.info("Creating project sheet " + project.getIdentifier());

        // Create project sheet
        Sheet projectSheet = workbook.createSheet("Project");
        projectSheet.setDefaultColumnWidth(defaultCellWidth);

        // Create header row
        Row projectSheetRow = projectSheet.createRow(0);

        projectSheetRow.createCell(0).setCellValue(PROJECT_IDENTIFIER);
        projectSheetRow.createCell(1).setCellValue(PROJECT_DESCRIPTION);
        projectSheetRow.createCell(2).setCellValue(PROJECT_TITLE);

        // Header style
        projectSheetRow.cellIterator().forEachRemaining(cell -> cell.setCellStyle(obligatoryStyle));

        // Add information to the project sheet from the next row
        projectSheetRow = projectSheet.createRow(1);
        projectSheetRow.createCell(0).setCellValue(project.getIdentifier());
        projectSheetRow.createCell(1).setCellValue(project.getDescription());
        projectSheetRow.createCell(2).setCellValue(project.getTitle());
    }

    /**
     * @param workbook object that will be transformed into an excel file
     * @param project object for mapping the investigation to
     * @param investigation object to create the investigation sheet
     * @param memberList to be added to the investigation sheet
     */
    private static void generateInvestigationSheet(Workbook workbook, Project project, Investigation investigation, List<Member> memberList) {
        logger.info("Creating investigation sheet " + investigation.getIdentifier());

        Sheet investigationSheet = workbook.createSheet("Investigation");
        investigationSheet.setDefaultColumnWidth(defaultCellWidth);
        // Create header row?
        Row investigationSheetRow = investigationSheet.createRow(0);
        // Get all headers and add them to the row
        investigationSheetRow.createCell(0).setCellValue(INVESTIGATION_IDENTIFIER);
        investigationSheetRow.createCell(1).setCellValue(INVESTIGATION_TITLE);
        investigationSheetRow.createCell(2).setCellValue(INVESTIGATION_DESCRIPTION);
        investigationSheetRow.createCell(3).setCellValue(FIRSTNAME);
        investigationSheetRow.createCell(4).setCellValue(LASTNAME);
        investigationSheetRow.createCell(5).setCellValue(EMAIL);
        investigationSheetRow.createCell(6).setCellValue(ORCID);
        investigationSheetRow.createCell(7).setCellValue(ORGANIZATION);
        investigationSheetRow.createCell(8).setCellValue(DEPARTMENT);
        investigationSheetRow.createCell(9).setCellValue(PROJECT_IDENTIFIER);

        // Header style default obligatory style
        investigationSheetRow.cellIterator().forEachRemaining(cell -> cell.setCellStyle(obligatoryStyle));
        investigationSheetRow.getCell(6).setCellStyle(headerStyle);

        int rowCount = 1;

        for (Member member : memberList) {
            investigationSheetRow = investigationSheet.createRow(rowCount);
            investigationSheetRow.createCell(0).setCellValue(investigation.getIdentifier());
            investigationSheetRow.createCell(1).setCellValue(investigation.getTitle());
            investigationSheetRow.createCell(2).setCellValue(investigation.getDescription());
            investigationSheetRow.createCell(3).setCellValue(member.getFirstName());
            investigationSheetRow.createCell(4).setCellValue(member.getLastName());
            investigationSheetRow.createCell(5).setCellValue(member.getEmail());
            investigationSheetRow.createCell(6).setCellValue(member.getORCID());
            investigationSheetRow.createCell(7).setCellValue(member.getOrganization());
            investigationSheetRow.createCell(8).setCellValue(member.getDepartment());
            investigationSheetRow.createCell(9).setCellValue(project.getIdentifier());
            // New row creation
            rowCount = rowCount + 1;
        }
    }

    /**
     * @param workbook object that will be transformed into an excel file
     * @param investigation object containing the investigation identifier
     * @param study object to create the study sheet
     */
    private static void generateStudySheet(Workbook workbook, Investigation investigation, Study study) {
        logger.info("Creating study sheet " + study.getIdentifier());

        // Create study sheet
        Sheet studySheet = workbook.createSheet("Study");
        studySheet.setDefaultColumnWidth(defaultCellWidth);
        // Create header row?
        Row studySheetRow = studySheet.createRow(0);

        studySheetRow.createCell(0).setCellValue(STUDY_IDENTIFIER);
        studySheetRow.createCell(1).setCellValue(STUDY_DESCRIPTION);
        studySheetRow.createCell(2).setCellValue(STUDY_TITLE);
        studySheetRow.createCell(3).setCellValue(INVESTIGATION_IDENTIFIER);

        // Header style
        studySheetRow.cellIterator().forEachRemaining(cell -> cell.setCellStyle(obligatoryStyle));

        // Fill with content (through generation only 1 study can be generated but multiple studies can be crearted in the excel file)
        studySheetRow = studySheet.createRow(1);
        studySheetRow.createCell(0).setCellValue(study.getIdentifier());
        studySheetRow.createCell(1).setCellValue(study.getDescription());
        studySheetRow.createCell(2).setCellValue(study.getTitle());
        studySheetRow.createCell(3).setCellValue(investigation.getIdentifier());
    }

    /**
     * @param workbook object that will be transformed into an excel file
     * @param study object containing the identifier to map the ou to it
     * @param observationUnit object containing the elements to be filled in
     * @param selections what is selected for the observation unit metadata to be filled in
     */
    private static void generateObservationUnitSheet(Workbook workbook, Study study, ObservationUnit observationUnit, ArrayList<Metadata> selections) {
        // Create observation unit sheet
        Sheet observationUnitSheet = workbook.createSheet("ObservationUnit");
        observationUnitSheet.setDefaultColumnWidth(defaultCellWidth);
        // Create header row
        Row observationUnitSheetRow = observationUnitSheet.createRow(0);

        // Create cell and set header style
        for (int i = 0; i < selections.size(); i++) {
            Metadata selection = selections.get(i);
            Cell cell = observationUnitSheetRow.createCell(i);
            cell.setCellValue(selection.getLabel());
            createComment(cell, selection);

            if (selection.getRequirement().equalsIgnoreCase("mandatory")) {
                cell.setCellStyle(obligatoryStyle);
            } else {
                cell.setCellStyle(headerStyle);
            }
        }

        for (int i = 0; i < observationUnit.getObservationUnits(); i++) {
            // To always skip the header (+1)
            observationUnitSheetRow = observationUnitSheet.createRow(i + 1);

            String id = RandomStringUtils.randomAlphabetic(5) + "_" + i;

            observationUnitSheetRow.createCell(0).setCellValue(id);
            if (i == 0) {
                observationUnitSheetRow.createCell(1).setCellValue("You can change the Observation Unit identifiers to your liking");
            } else {
                observationUnitSheetRow.createCell(1).setCellValue("");
            }
            observationUnitSheetRow.createCell(2).setCellValue("");
            observationUnitSheetRow.createCell(3).setCellValue(study.getIdentifier());
        }
    }

    private static void createComment(Cell cell, Metadata selection) {
        String content = "" +
                "Requirement: " + selection.getRequirement() + "\n" +
                "Package: " + selection.getPackageName() + "\n" +
                "Label: " + selection.getLabel() + "\n" +
                "Example: " + selection.getExample() + "\n" +
                "Pattern: " + selection.getRegex() + "\n" +
                "Units: " + selection.getPreferredUnit() + "\n";

        // Default number of rows
        int maxRows = content.split("\\n").length;
        for (String line : content.split("\\n")) {
            int parts = line.length() / 30;
            if (parts > 0)
                maxRows = maxRows  + parts;
        }

        Drawing drawing = cell.getSheet().createDrawingPatriarch();
        CreationHelper factory = cell.getSheet().getWorkbook().getCreationHelper();
        ClientAnchor anchor = factory.createClientAnchor();
        anchor.setCol1(cell.getColumnIndex());
        anchor.setCol2(cell.getColumnIndex() + 2);
        anchor.setRow1(1);
        anchor.setRow2(maxRows);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);
        Comment comment = drawing.createCellComment(anchor);


        RichTextString str = factory.createRichTextString(content);
        comment.setVisible(Boolean.FALSE);
        comment.setString(str);

        cell.setCellComment(comment);
    }


    /**
     * To create the header identical in all sheets
     */
    public static void makeHeaderCellStyle() {
        headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        headerStyle.setFont(font);
    }

    /**
     * Header for obligatory headers
     */
    public static void makeObligatoryHeaderCellStyle() {
        obligatoryStyle = workbook.createCellStyle();
        obligatoryStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        obligatoryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        obligatoryStyle.setAlignment(HorizontalAlignment.CENTER);
        obligatoryStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        font.setColor(IndexedColors.RED.getIndex());
        obligatoryStyle.setFont(font);
    }

    /**
     * @param excelFile the excel file object
     * @param selectedItems the items selected for the assay sheet
     * @param sheetName the specific assay sheet as there can be a multitude of them
     */
    public static void makeSheetOnly(File excelFile, Set<Metadata> selectedItems, String sheetName) {
        // Create workbook
        workbook = new XSSFWorkbook();

        // set sample package property
        POIXMLProperties props = workbook.getProperties();
        // POIXMLProperties.CustomProperties custProp = props.getCustomProperties();

        // Create excel style object
        makeHeaderCellStyle();
        makeObligatoryHeaderCellStyle();

        logger.info("Creating assay sheet " + sheetName);
        generateAssaySheet(workbook, sheetName, new ArrayList<>(selectedItems));

        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(excelFile);
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
