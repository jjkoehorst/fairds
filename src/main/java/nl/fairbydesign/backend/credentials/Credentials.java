package nl.fairbydesign.backend.credentials;

import org.irods.jargon.core.connection.AuthScheme;
import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.UserGroupAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

/**
 * Class for the credentials object for irods authentication with authentication functionality.
 */
public class Credentials {
    private static final Logger logger = LoggerFactory.getLogger(Credentials.class);

    private String username;
    private String password;
    private String host;
    private int port;
    private String zone;
    private String sslNegotiationPolicy = "CS_NEG_REFUSE";
    private boolean success = false;
    private IRODSAccount irodsAccount;
    private boolean administrator = false;
    private String message;
    private String authenticationScheme;
    private String certificateTrustStore;
    private String certificateTrustStorePassword;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSslNegotiationPolicy() {
        return this.sslNegotiationPolicy;
    }

    public String getUsername() {
        return this.username;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public String getZone() {
        return this.zone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public void setSslNegotiationPolicy(String sslNegotiationPolicy) {
        this.sslNegotiationPolicy = sslNegotiationPolicy;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void authenticate() {
        try {
            this.irodsAccount = IRODSAccount.instance(
                    this.host,
                    this.port,
                    this.username,
                    this.password,
                    "",
                    this.zone,
                    "");

            // Perform the authentication
            // SSL Settings
            ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
            ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
            if (this.sslNegotiationPolicy.matches("CS_NEG_REQUIRE")) {
                sslPolicy = CS_NEG_REQUIRE;
            } else if (this.sslNegotiationPolicy.matches("CS_NEG_REFUSE")) {
                sslPolicy = CS_NEG_REFUSE;
            } else {
                sslPolicy = CS_NEG_REQUIRE;
            }
            clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
            irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

            // Perform the authentication
            if (this.authenticationScheme.matches("PAM")) {
                irodsAccount.setAuthenticationScheme(AuthScheme.PAM);
                // Certificate was added using: keytool -importcert -file irods.crt -keystore keystore.jks -alias "Alias"
                System.setProperty("javax.net.ssl.trustStore", this.certificateTrustStore);
                System.setProperty("javax.net.ssl.trustStorePassword", this.certificateTrustStorePassword);
                System.getProperties().forEach((o, o2) -> {
                    logger.info("System property: " + o + "\t" + o2);
                });
            } else if (this.authenticationScheme.matches("STANDARD")) {
                irodsAccount.setAuthenticationScheme(AuthScheme.STANDARD);
            }

            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();

            if (accessObjectFactory.authenticateIRODSAccount(irodsAccount).isSuccessful()) {
                this.setSuccess(true);
            } else {
                this.setSuccess(false);
                logger.error("Authenticate message: " + accessObjectFactory.authenticateIRODSAccount(irodsAccount).getAuthMessage());
            }

            // Get user information when possible
            UserGroupAO userGroupAO = accessObjectFactory.getUserGroupAO(irodsAccount);
            // Group of technicians or administrators in irods
            if (userGroupAO.isUserInGroup(this.username, "technicians") || userGroupAO.isUserInGroup(this.username, "administrators")) {
                this.administrator = true;
            }
        } catch (JargonException e) {
            this.setMessage(e.getMessage());
            e.printStackTrace();
        }
    }

    public void setPort(int port) {
        this.port = port;
    }

    public IRODSAccount getIrodsAccount() {
        return irodsAccount;
    }

    public void clear() {
        this.username = null;
        this.password = null;
        this.host = null;
        this.zone = null;
        this.sslNegotiationPolicy = null;
        this.success = false;
        this.port = -1;
        this.irodsAccount = null;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setAuthenticationScheme(String authenticationScheme) {
        this.authenticationScheme = authenticationScheme;
    }

    public String getAuthenticationScheme() {
        return authenticationScheme;
    }

    public void setCertificateTrustStore(String certificateTrustStore) {
        this.certificateTrustStore = certificateTrustStore;
    }

    public String getCertificateTrustStore() {
        return certificateTrustStore;
    }

    public void setCertificateTrustStorePassword(String certificateTrustStorePassword) {
        this.certificateTrustStorePassword = certificateTrustStorePassword;
    }

    public String getCertificateTrustStorePassword() {
        return certificateTrustStorePassword;
    }
}
