package nl.fairbydesign.backend;

import com.google.gson.Gson;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.MetadataParser;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.jboss.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/**
 * Startup listener accessed once during startup
 * Sets the config environment and the metadata excel file is parsed + copied outside this jar
 * Excel file is converted into a java disk object for fast access and timestamps are used to identify
 * if it needs to be reloaded.
 */
public class ApplicationServiceInitListener implements VaadinServiceInitListener {
    // Generic logger
    public static final Logger logger = Logger.getLogger(ApplicationServiceInitListener.class);
    // The config object
    private static Config config;
    // The excel metadata object
    public static final HashMap<String, ArrayList<Metadata>> metadataSet = MetadataParser.main();
    // Debug variable
    public static boolean debug;
    public static String storage = "./fairds_storage/";

    // Function to access the metadata object from anywhere
    public static HashMap<String, ArrayList<Metadata>> getMetadata() {
        return metadataSet;
    }

    @Override
    public void serviceInit(ServiceInitEvent event) {
        // Boot process check?
        System.err.println("...BOOTING UP...");

        // Create storage folder
        new File(storage).mkdirs();

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        // Read in the application.properties file for port / debug settings and more
        try (InputStream resourceStream = loader.getResourceAsStream("application.properties")) {
            properties.load(resourceStream);
        } catch (IOException e) {
            logger.error("Failed to parse the application.properties file");
        }

        // Old code, is now always overwritten by config file
        // debug = Boolean.parseBoolean(properties.getProperty("debug"));

        Properties prop = System.getProperties();

        if (prop.get("user.home") != null) {
            // Home config location is the default
            File configFile = new File(prop.get("user.home") + "/.fairds/config.json");
            // If the config json file exists load into config object
            if (configFile.exists()) {
                String content;
                try {
                    content = new String(Files.readAllBytes(configFile.toPath()));
                    // Map string to config object
                    config = new Gson().fromJson(content, Config.class);
                    logger.info("Config file loaded, checking xlsx path " + config.getMetadata());
                } catch (IOException e) {
                    logger.error("Failed to load config file");
                }
            }
        }
        // If config file was not available in either server.config or user.home load from jar and place it in user.home
        if (config == null) {
            logger.info("Default config location is " + new File(prop.get("user.home") + "/.fairds/config.json"));
            logger.warn("Obtaining config file from internal resources as no config file is available or given");
            InputStream inputStream;
            try {
                // Create input stream to write config file to the default location
                inputStream = Util.getResourceFile("config.json");
                File configFile = new File(prop.get("user.home") + "/.fairds/config.json");
                // Ensure that the folder is created
                configFile.getParentFile().mkdirs();
                // Copy to local config path
                Files.copy(inputStream, configFile.toPath());
                // Load file from new location
                String content = new String(Files.readAllBytes(configFile.toPath()));
                // Map into config object
                config = new Gson().fromJson(content, Config.class);
                // Set debug value based on config file
                debug = config.getDebug();
            } catch (IOException e) {
                logger.info("Failed to map config file to object");
            }
        }

        if (config.getDebug()) {
            logger.debug("DEBUG MODE ACTIVATED");
        }

    }

    public static Config getConfig() {
        return config;
    }

    public static String getStorage() {
        return storage;
    }
}