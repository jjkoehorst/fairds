package nl.fairbydesign.backend.data.yaml;

import java.util.ArrayList;
import java.util.HashMap;

public class WorkflowBiom extends Workflow {


    public String destination;
    public ArrayList<FileClass> job = new ArrayList<>();
    public HashMap<String, String> irods = new HashMap<>();
    private String identifier;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public ArrayList<FileClass> getJob() {
        return job;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public void setJob(String job) throws Exception {
        addIRODS(job);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(job);
        this.job.add(fileClass);
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}
