package nl.fairbydesign.backend.data.yaml;

import java.util.ArrayList;
import java.util.HashMap;

public class WorkflowClustalO extends Workflow {


    public String destination;
    public ArrayList<FileClass> fasta = new ArrayList<>();
    public HashMap<String, String> irods = new HashMap<>();
    private boolean provenance = false;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public ArrayList<FileClass> getFasta() {
        return fasta;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public void setFasta(String fasta) throws Exception {
        addIRODS(fasta);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(fasta);
        this.fasta.add(fileClass);
    }

    public boolean isProvenance() {
        return provenance;
    }

    @Override
    public void setProvenance(boolean provenance) {
        this.provenance = provenance;
    }
}
