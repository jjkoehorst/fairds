package nl.fairbydesign.backend.data.yaml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Workflow {
    public int threads = 2;
    public int memory = 5000;
    public boolean provenance = true;

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public int getThreads() {
        return threads;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getMemory() {
        return memory;
    }

    public void setProvenance(boolean provenance) {
        this.provenance = provenance;
    }

    public boolean getProvenance() {
        return provenance;
    }

    public static void fixClazz(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.replaceAll("clazz:","class:");
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }

    /**
     * Removes all the "- !" from the file
     * @param yaml
     * @throws FileNotFoundException
     */
    public static void fixComments(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.replaceAll("^- !.*","");
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }
}
