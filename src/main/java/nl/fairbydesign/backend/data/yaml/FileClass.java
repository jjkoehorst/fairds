package nl.fairbydesign.backend.data.yaml;


import static nl.fairbydesign.backend.WebGeneric.*;

public class FileClass {
    private String clazz;
    private String location;
    private String format;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) throws Exception {
        this.location = location;
        this.setFormat(location);
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String type) {
        this.clazz = type;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String fileName) throws Exception {
        String format = getEdamFormat(fileName);
        if (format != null) {
            this.format =format;
        }
    }
}
