package nl.fairbydesign.backend.data.objects;

public class DiskUsage {
    private String project;
    private String investigation;
    private long tapeSize;
    private long diskSize;

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }


    public String getInvestigation() {
        return investigation;
    }

    public void setInvestigation(String investigation) {
        this.investigation = investigation;
    }

    public void setTapeSize(long tapeSize) {
        this.tapeSize = tapeSize;
    }

    public long getTapeSize() {
        return tapeSize;
    }

    public void setDiskSize(long diskSize) {
        this.diskSize = diskSize;
    }

    public long getDiskSize() {
        return diskSize;
    }
}
