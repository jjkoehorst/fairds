package nl.fairbydesign.backend.data.objects.metadata;

public class GridSelection {
    // # Column ID	OBLIGATORY	REGEX	URI	HELP
    private String columnId;
    private String help;
    private String regex;
    private String URI;

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }

    public void setURI(String uri) {
        this.URI = uri;
    }

    public String getURI() {
        return URI;
    }
}

