package nl.fairbydesign.backend.data.objects;

import java.io.Serializable;

public class Process implements Serializable {
    private String identifier;
    private int running;
    private int finished;
    private int waiting;
    private int failed;
    private String workflow;
    private String projectIdentifier;
    private String investigationIdentifier;
    private String studyIdentifier;
    private String path;
    private int queue;

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public int getRunning() {
        return running;
    }

    public void setFinished(int finished) {
        this.finished = finished;
    }

    public int getFinished() {
        return finished;
    }

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setWorkflow(String workflow) {

        this.workflow = workflow;
    }

    public String getWorkflow() {
        return workflow;
    }

    public void setProjectIdentifier(String projectIdentifier) {
        this.projectIdentifier = projectIdentifier;
    }

    public String getProjectIdentifier() {
        return projectIdentifier;
    }

    public void setInvestigationIdentifier(String investigationIdentifier) {
        this.investigationIdentifier = investigationIdentifier;
    }

    public String getInvestigationIdentifier() {
        return investigationIdentifier;
    }

    public void setStudyIdentifier(String studyIdentifier) {
        this.studyIdentifier = studyIdentifier;
    }

    public String getStudyIdentifier() {
        return studyIdentifier;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public int getQueue() {
        return queue;
    }
}
