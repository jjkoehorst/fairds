package nl.fairbydesign.backend.data.objects;

/**
 * Metadata object for irods system
 */
public class AVU {
    private  String attribute;
    private String value;
    private String unit;

    public AVU(String attribute, String value, String units) {
        this.attribute = attribute;
        this.value = value;
        this.unit = units;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
}
