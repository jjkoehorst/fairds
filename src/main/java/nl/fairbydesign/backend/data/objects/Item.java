package nl.fairbydesign.backend.data.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Item {
    private final String uuid;
    private boolean download;
    private boolean info;
    private boolean trash;
    private String path;
    private String name;
    private Date modified;
    private String size;
    private boolean selected;
    // private String action;
    private Type type;
    private final List<AVU> avus;

    public String getUuid() {
        return uuid;
    }


    public boolean isTrash() {
        return trash;
    }

    public void setTrash(boolean trash) {
        this.trash = trash;
    }

    public boolean isInfo() {
        return info;
    }

    public void setInfo(boolean info) {
        this.info = info;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public enum Type {
        FILE,
        FOLDER,
        UNKNOWN
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<AVU> getAvus() {
        return avus;
    }

    public void addAvus(AVU avu) {
        this.avus.add(avu);
    }

    public void removeAllAvus() {
        this.avus.removeAll(this.avus);
    }

    public Item(String name, long modified, String size, Type type, String path) {
        this.name = name;
        this.type = type;
        setModified(modified);
        this.size = size;
        this.path = path;
        this.avus = new ArrayList<>();
        this.uuid = UUID.randomUUID().toString();
        this.info = false;
        this.trash = false;
        this.download = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(long modified) {
        if (modified != 0L) {
            this.modified = new Date(modified);
        }
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

//    public List<Icon> getAction() {
//        return action;
//    }

//    public void setAction(String action) {
//        this.action = action;
//    }
}