package nl.fairbydesign.backend.data.objects;

public class Job {
    private int running;
    private int finished;
    private int waiting;
    private String workflow;

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public int getRunning() {
        return running;
    }

    public void setFinished(int finished) {
        this.finished = finished;
    }

    public int getFinished() {
        return finished;
    }

    public void setWorkflow(String workflow) {
        this.workflow = workflow;
    }

    public String getWorkflow() {
        return workflow;
    }
}
