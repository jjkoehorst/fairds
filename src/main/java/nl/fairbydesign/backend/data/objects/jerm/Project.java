package nl.fairbydesign.backend.data.objects.jerm;

import nl.fairbydesign.backend.data.objects.Job;

import java.util.ArrayList;

public class Project {
    private String identifier;
    private String title;
    private String description;
    private String path;
    private ArrayList<Job> job;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setJob(ArrayList<Job> job) {
        this.job = job;
    }

    public ArrayList<Job> getJob() {
        return job;
    }
}