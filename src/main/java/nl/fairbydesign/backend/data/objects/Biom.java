package nl.fairbydesign.backend.data.objects;

public class Biom {
    private String project;
    private String investigation;
    private String study;
    private String observationUnit;
    private int length;
    // private String HDT;
    private String assay;
    private String folder;
    private String job;
    private String sample;

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getInvestigation() {
        return investigation;
    }

    public void setInvestigation(String investigation) {
        this.investigation = investigation;
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public String getObservationUnit() {
        return observationUnit;
    }

    public void setObservationUnit(String observationUnit) {
        this.observationUnit = observationUnit;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setAssay(String assay) {
        this.assay = assay;
    }

    public String getAssay() {
        return assay;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFolder() {
        return folder;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public String getSample() {
        return sample;
    }
}
