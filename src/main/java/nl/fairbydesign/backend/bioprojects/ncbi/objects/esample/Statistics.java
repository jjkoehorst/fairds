package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Statistics{
    @JsonProperty("Read")
    public ArrayList<Read> getRead() {
		 return this.read; } 
    public void setRead(ArrayList<Read> read) { 
		 this.read = read; } 
    ArrayList<Read> read;

    @JsonProperty("nreads")
    public String getNreads() {
        return this.nreads; }
    public void setNreads(String nreads) {
        this.nreads = nreads; }
    String nreads;

    @JsonProperty("nspots") 
    public long getNspots() {
		 return this.nspots; } 
    public void setNspots(long nspots) {
		 this.nspots = nspots; } 
    long nspots;
    @JsonProperty("source")
    public String getSource() {
        return this.source; }
    public void setSource(String source) {
        this.source = source; }
    String source;
    @JsonProperty("Elements")
    public Elements getElements() {
        return this.elements; }
    public void setElements(Elements elements) {
        this.elements = elements; }
    Elements elements;
    @JsonProperty("Rows")
    public Rows getRows() {
        return this.rows; }
    public void setRows(Rows rows) {
        this.rows = rows; }
    Rows rows;
}
