package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SRAFile{
    @JsonProperty("date")
    public String getDate() { 
		 return this.date; } 
    public void setDate(String date) { 
		 this.date = date; } 
    String date;
    @JsonProperty("cluster") 
    public String getCluster() { 
		 return this.cluster; } 
    public void setCluster(String cluster) { 
		 this.cluster = cluster; } 
    String cluster;
    @JsonProperty("supertype") 
    public String getSupertype() { 
		 return this.supertype; } 
    public void setSupertype(String supertype) { 
		 this.supertype = supertype; } 
    String supertype;
    @JsonProperty("filename") 
    public String getFilename() { 
		 return this.filename; } 
    public void setFilename(String filename) { 
		 this.filename = filename; } 
    String filename;
    @JsonProperty("size") 
    public Object getSize() { 
		 return this.size; } 
    public void setSize(Object size) { 
		 this.size = size; } 
    Object size;
    @JsonProperty("semantic_name") 
    public String getSemantic_name() { 
		 return this.semantic_name; } 
    public void setSemantic_name(String semantic_name) { 
		 this.semantic_name = semantic_name; } 
    String semantic_name;
    @JsonProperty("sratoolkit") 
    public int getSratoolkit() { 
		 return this.sratoolkit; } 
    public void setSratoolkit(int sratoolkit) { 
		 this.sratoolkit = sratoolkit; } 
    int sratoolkit;
    @JsonProperty("Alternatives") 
    public Object getAlternatives() { 
		 return this.alternatives; } 
    public void setAlternatives(Object alternatives) { 
		 this.alternatives = alternatives; } 
    Object alternatives;
    @JsonProperty("md5") 
    public String getMd5() { 
		 return this.md5; } 
    public void setMd5(String md5) { 
		 this.md5 = md5; } 
    String md5;
    @JsonProperty("url") 
    public String getUrl() { 
		 return this.url; } 
    public void setUrl(String url) { 
		 this.url = url; } 
    String url;
}
