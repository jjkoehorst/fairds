package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Elements{
    @JsonProperty("count")
    public long getCount() {
        return this.count; }
    public void setCount(Long count) {
        this.count = count; }
    long count;
}
