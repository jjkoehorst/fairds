package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TARGETEDLOCI {
    @JsonProperty("LOCUS")
    public LOCUS getLOCUS() {
        return this.lOCUS; }
    public void setLOCUS(LOCUS lOCUS) {
        this.lOCUS = lOCUS; }
    LOCUS lOCUS;
}
