package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROCESSING {
    @JsonProperty("PIPELINE")
    public PIPELINE getPIPELINE() {
        return this.pIPELINE; }
    public void setPIPELINE(PIPELINE pIPELINE) {
        this.pIPELINE = pIPELINE; }
    PIPELINE pIPELINE;
}
