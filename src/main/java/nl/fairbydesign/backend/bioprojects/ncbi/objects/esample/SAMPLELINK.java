package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SAMPLELINK{
    @JsonProperty("XREF_LINK")
    public XREFLINK getXREF_LINK() { 
		 return this.xREF_LINK; } 
    public void setXREF_LINK(XREFLINK xREF_LINK) { 
		 this.xREF_LINK = xREF_LINK; } 
    XREFLINK xREF_LINK;
    @JsonProperty("URL_LINK")
    public URLLINK getURL_LINK() {
        return this.uRL_LINK; }
    public void setURL_LINK(URLLINK uRL_LINK) {
        this.uRL_LINK = uRL_LINK; }
    URLLINK uRL_LINK;
}
