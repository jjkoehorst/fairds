package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class XREFLINK{
    @JsonProperty("ID")
    public Object getID() {
        return this.iD; }
    public void setID(Object iD) {
        this.iD = iD; }
    Object iD;
    @JsonProperty("DB") 
    public String getDB() { 
		 return this.dB; } 
    public void setDB(String dB) { 
		 this.dB = dB; } 
    String dB;
    @JsonProperty("LABEL") 
    public String getLABEL() { 
		 return this.lABEL; } 
    public void setLABEL(String lABEL) { 
		 this.lABEL = lABEL; } 
    String lABEL;
}
