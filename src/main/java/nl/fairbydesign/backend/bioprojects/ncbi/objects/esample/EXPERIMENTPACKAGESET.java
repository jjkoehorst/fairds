package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EXPERIMENTPACKAGESET{
    @JsonProperty("EXPERIMENT_PACKAGE")
    public EXPERIMENTPACKAGE getEXPERIMENT_PACKAGE() {
		 return this.eXPERIMENT_PACKAGE; } 
    public void setEXPERIMENT_PACKAGE(EXPERIMENTPACKAGE eXPERIMENT_PACKAGE) { 
		 this.eXPERIMENT_PACKAGE = eXPERIMENT_PACKAGE; } 
    EXPERIMENTPACKAGE eXPERIMENT_PACKAGE;
    @JsonProperty("ERROR")
    public ERROR eRROR;
    @JsonProperty("Error")
    public String error;
}
