package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SPOTDESCRIPTOR{
    @JsonProperty("SPOT_DECODE_SPEC")
    public SPOTDECODESPEC getSPOT_DECODE_SPEC() { 
		 return this.sPOT_DECODE_SPEC; } 
    public void setSPOT_DECODE_SPEC(SPOTDECODESPEC sPOT_DECODE_SPEC) { 
		 this.sPOT_DECODE_SPEC = sPOT_DECODE_SPEC; } 
    SPOTDECODESPEC sPOT_DECODE_SPEC;
}
