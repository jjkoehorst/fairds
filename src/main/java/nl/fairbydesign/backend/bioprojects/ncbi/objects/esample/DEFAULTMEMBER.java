package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DEFAULTMEMBER {
    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS; }
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS; }
    IDENTIFIERS iDENTIFIERS;
}
