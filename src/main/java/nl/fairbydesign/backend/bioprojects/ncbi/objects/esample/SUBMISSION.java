package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class SUBMISSION{
    @JsonProperty("submission_date")
    public Date getSubmission_date() {
        return this.submission_date; }
    public void setSubmission_date(Date submission_date) {
        this.submission_date = submission_date; }
    Date submission_date;
    @JsonProperty("center_name")
    public String getCenter_name() {
		 return this.center_name; }
    public void setCenter_name(String center_name) {
		 this.center_name = center_name; }
    String center_name;
    @JsonProperty("xsi:noNamespaceSchemaLocation")
    public String getXsiNoNamespaceSchemaLocation() {
        return this.xsiNoNamespaceSchemaLocation; }
    public void setXsiNoNamespaceSchemaLocation(String xsiNoNamespaceSchemaLocation) {
        this.xsiNoNamespaceSchemaLocation = xsiNoNamespaceSchemaLocation; }
    String xsiNoNamespaceSchemaLocation;
    @JsonProperty("alias") 
    public String getAlias() { 
		 return this.alias; } 
    public void setAlias(String alias) { 
		 this.alias = alias; } 
    String alias;
    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name; }
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name; }
    String broker_name;
    @JsonProperty("lab_name") 
    public String getLab_name() { 
		 return this.lab_name; } 
    public void setLab_name(String lab_name) { 
		 this.lab_name = lab_name; } 
    String lab_name;
    @JsonProperty("xmlns:xsi") 
    public String getXmlnsXsi() { 
		 return this.xmlnsXsi; } 
    public void setXmlnsXsi(String xmlnsXsi) { 
		 this.xmlnsXsi = xmlnsXsi; } 
    String xmlnsXsi;
    @JsonProperty("IDENTIFIERS") 
    public IDENTIFIERS getIDENTIFIERS() {
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) { 
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("xmlns") 
    public String getXmlns() { 
		 return this.xmlns; } 
    public void setXmlns(String xmlns) { 
		 this.xmlns = xmlns; } 
    String xmlns;
    @JsonProperty("submission_comment") 
    public String getSubmission_comment() { 
		 return this.submission_comment; } 
    public void setSubmission_comment(String submission_comment) { 
		 this.submission_comment = submission_comment; } 
    String submission_comment;
    @JsonProperty("TITLE")
    public String getTITLE() {
        return this.tITLE; }
    public void setTITLE(String tITLE) {
        this.tITLE = tITLE; }
    String tITLE;
    @JsonProperty("SUBMISSION_ATTRIBUTES")
    public SUBMISSIONATTRIBUTES getSUBMISSION_ATTRIBUTES() {
        return this.sUBMISSION_ATTRIBUTES; }
    public void setSUBMISSION_ATTRIBUTES(SUBMISSIONATTRIBUTES sUBMISSION_ATTRIBUTES) {
        this.sUBMISSION_ATTRIBUTES = sUBMISSION_ATTRIBUTES; }
    SUBMISSIONATTRIBUTES sUBMISSION_ATTRIBUTES;

}
