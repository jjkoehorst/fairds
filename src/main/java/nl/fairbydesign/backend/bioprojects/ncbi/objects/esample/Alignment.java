package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Alignment {
    @JsonProperty("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonProperty("seqid")
    public String getSeqid() {
        return this.seqid; }
    public void setSeqid(String seqid) {
        this.seqid = seqid; }
    String seqid;
    @JsonProperty("local")
    public String getLocal() {
        return this.local; }
    public void setLocal(String local) {
        this.local = local; }
    String local;
    @JsonProperty("gi")
    public int getGi() {
        return this.gi; }
    public void setGi(int gi) {
        this.gi = gi; }
    int gi;
}
