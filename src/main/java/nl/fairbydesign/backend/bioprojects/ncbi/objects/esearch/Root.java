package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root {
    @JsonProperty("eSearchResult")
    public ESearchResult getESearchResult() {
        return this.eSearchResult;
    }

    public void setESearchResult(ESearchResult eSearchResult) {
        this.eSearchResult = eSearchResult;
    }

    ESearchResult eSearchResult;
}
