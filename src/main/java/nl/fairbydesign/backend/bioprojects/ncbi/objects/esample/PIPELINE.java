package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class PIPELINE {
    @JsonProperty("PIPE_SECTION")
    public ArrayList<nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.PIPESECTION> getPIPE_SECTION() {
        return this.pIPE_SECTION; }
    public void setPIPE_SECTION(ArrayList<nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.PIPESECTION> pIPE_SECTION) {
        this.pIPE_SECTION = pIPE_SECTION; }
    ArrayList<nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.PIPESECTION> pIPE_SECTION;
}
