package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class EXPERIMENTPACKAGE{
    @JsonProperty("STUDY") 
    public STUDY getSTUDY() { 
		 return this.sTUDY;
    }
    public void setSTUDY(STUDY sTUDY) { 
		 this.sTUDY = sTUDY; } 
    STUDY sTUDY;
    @JsonProperty("SAMPLE")
    public ArrayList<SAMPLE> getSAMPLE() {
        return this.sAMPLE; }
    public void setSAMPLE(ArrayList<SAMPLE> sAMPLE) {
        this.sAMPLE = sAMPLE; }
    ArrayList<SAMPLE> sAMPLE;
    @JsonProperty("Organization") 
    public Organization getOrganization() {
		 return this.organization; } 
    public void setOrganization(Organization organization) { 
		 this.organization = organization; } 
    Organization organization;
    @JsonProperty("POOL")
    public POOL getPool() {
		 return this.pool; } 
    public void setPool(POOL pool) {
		 this.pool = pool; } 
    POOL pool;
    @JsonProperty("SUBMISSION") 
    public SUBMISSION getSUBMISSION() { 
		 return this.sUBMISSION; } 
    public void setSUBMISSION(SUBMISSION sUBMISSION) { 
		 this.sUBMISSION = sUBMISSION; } 
    SUBMISSION sUBMISSION;
    @JsonProperty("RUN_SET") 
    public RUNSET getRUN_SET() { 
		 return this.rUN_SET; } 
    public void setRUN_SET(RUNSET rUN_SET) { 
		 this.rUN_SET = rUN_SET; } 
    RUNSET rUN_SET;
    @JsonProperty("EXPERIMENT") 
    public EXPERIMENT getEXPERIMENT() {
		 return this.eXPERIMENT; } 
    public void setEXPERIMENT(EXPERIMENT eXPERIMENT) { 
		 this.eXPERIMENT = eXPERIMENT; } 
    EXPERIMENT eXPERIMENT;
    @JsonProperty("ControlledAccess")
    public ControlledAccess getControlledAccess() {
        return this.controlledAccess; }
    public void setControlledAccess(ControlledAccess controlledAccess) {
        this.controlledAccess = controlledAccess; }
    ControlledAccess controlledAccess;
}
