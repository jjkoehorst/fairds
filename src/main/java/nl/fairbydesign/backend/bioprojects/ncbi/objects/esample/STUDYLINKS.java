package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class STUDYLINKS{
    @JsonProperty("STUDY_LINK")
    public ArrayList<STUDYLINK> getSTUDY_LINK() {
		 return this.sTUDY_LINK; } 
    public void setSTUDY_LINK(ArrayList<STUDYLINK> sTUDY_LINK) { 
		 this.sTUDY_LINK = sTUDY_LINK; } 
    ArrayList<STUDYLINK> sTUDY_LINK;
}
