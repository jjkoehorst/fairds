package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SAMPLE {
    @JsonProperty("center_name")
    public String getCenter_name() {
        return this.center_name; }
    public void setCenter_name(String center_name) {
        this.center_name = center_name; }
    String center_name;

    @JsonProperty("SAMPLE_NAME")
    public SAMPLENAME getSAMPLE_NAME() {
        return this.sAMPLE_NAME;
    }
    public void setSAMPLE_NAME(SAMPLENAME sAMPLE_NAME) {
        this.sAMPLE_NAME = sAMPLE_NAME;
    }
    SAMPLENAME sAMPLE_NAME;

    @JsonProperty("alias")
    public String getAlias() {
        return this.alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }
    String alias;

    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS;
    }
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS;
    }
    IDENTIFIERS iDENTIFIERS;

    @JsonProperty("accession")
    public String getAccession() {
        return this.accession;
    }
    public void setAccession(String accession) {
        this.accession = accession;
    }
    String accession;

    @JsonProperty("SAMPLE_ATTRIBUTES")
    public SAMPLEATTRIBUTES getSAMPLE_ATTRIBUTES() {
        return this.sAMPLE_ATTRIBUTES;
    }
    public void setSAMPLE_ATTRIBUTES(SAMPLEATTRIBUTES sAMPLE_ATTRIBUTES) {
        this.sAMPLE_ATTRIBUTES = sAMPLE_ATTRIBUTES;
    }
    SAMPLEATTRIBUTES sAMPLE_ATTRIBUTES;

    @JsonProperty("SAMPLE_LINKS")
    public SAMPLELINKS getSAMPLE_LINKS() {
        return this.sAMPLE_LINKS;
    }
    public void setSAMPLE_LINKS(SAMPLELINKS sAMPLE_LINKS) {
        this.sAMPLE_LINKS = sAMPLE_LINKS;
    }
    SAMPLELINKS sAMPLE_LINKS;

    @JsonProperty("TITLE")
    public String getTITLE() {
        return this.tITLE;
    }
    public void setTITLE(String tITLE) {
        this.tITLE = tITLE;
    }
    String tITLE;

    @JsonProperty("DESCRIPTION")
    public String getDESCRIPTION() {
        return this.dESCRIPTION;
    }
    public void setDESCRIPTION(String dESCRIPTION) {
        this.dESCRIPTION = dESCRIPTION;
    }
    String dESCRIPTION;

    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name; }
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name; }
    String broker_name;

    @JsonProperty("xmlns")
    public String getXmlns() {
        return this.xmlns; }
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns; }
    String xmlns;
}
