package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class STUDYSET {
    @JsonProperty("STUDY")
    public STUDY getSTUDY() {
        return this.sTUDY; }
    public void setSTUDY(STUDY sTUDY) {
        this.sTUDY = sTUDY; }
    STUDY sTUDY;
}
