package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DESCRIPTOR{
    @JsonProperty("STUDY_DESCRIPTION")
    public String getSTUDY_DESCRIPTION() {
        return this.sTUDY_DESCRIPTION; }
    public void setSTUDY_DESCRIPTION(String sTUDY_DESCRIPTION) {
        this.sTUDY_DESCRIPTION = sTUDY_DESCRIPTION; }
    String sTUDY_DESCRIPTION;
    @JsonProperty("STUDY_TYPE") 
    public STUDYTYPE getSTUDY_TYPE() { 
		 return this.sTUDY_TYPE; } 
    public void setSTUDY_TYPE(STUDYTYPE sTUDY_TYPE) { 
		 this.sTUDY_TYPE = sTUDY_TYPE; } 
    STUDYTYPE sTUDY_TYPE;
    @JsonProperty("STUDY_TITLE")
    public String getSTUDY_TITLE() { 
		 return this.sTUDY_TITLE; } 
    public void setSTUDY_TITLE(String sTUDY_TITLE) { 
		 this.sTUDY_TITLE = sTUDY_TITLE; } 
    String sTUDY_TITLE;
    @JsonProperty("STUDY_ABSTRACT") 
    public String getSTUDY_ABSTRACT() { 
		 return this.sTUDY_ABSTRACT; } 
    public void setSTUDY_ABSTRACT(String sTUDY_ABSTRACT) { 
		 this.sTUDY_ABSTRACT = sTUDY_ABSTRACT; } 
    String sTUDY_ABSTRACT;
    @JsonProperty("CENTER_PROJECT_NAME") 
    public String getCENTER_PROJECT_NAME() { 
		 return this.cENTER_PROJECT_NAME; } 
    public void setCENTER_PROJECT_NAME(String cENTER_PROJECT_NAME) { 
		 this.cENTER_PROJECT_NAME = cENTER_PROJECT_NAME; } 
    String cENTER_PROJECT_NAME;
}
