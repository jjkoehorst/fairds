package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LIBRARYLAYOUT{
    @JsonProperty("PAIRED")
    public Object getPAIRED() { 
		 return this.pAIRED; } 
    public void setPAIRED(Object pAIRED) { 
		 this.pAIRED = pAIRED; } 
    Object pAIRED;
    @JsonProperty("SINGLE") 
    public String getSINGLE() { 
		 return this.sINGLE; } 
    public void setSINGLE(String sINGLE) { 
		 this.sINGLE = sINGLE; } 
    String sINGLE;
}
