package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SAMPLEATTRIBUTES{
    @JsonProperty("SAMPLE_ATTRIBUTE")
    public ArrayList<SAMPLEATTRIBUTE> getSAMPLE_ATTRIBUTE() {
		 return this.sAMPLE_ATTRIBUTE; } 
    public void setSAMPLE_ATTRIBUTE(ArrayList<SAMPLEATTRIBUTE> sAMPLE_ATTRIBUTE) { 
		 this.sAMPLE_ATTRIBUTE = sAMPLE_ATTRIBUTE; } 
    ArrayList<SAMPLEATTRIBUTE> sAMPLE_ATTRIBUTE;
}
