package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROJECT{
    @JsonProperty("center_name")
    public String getCenter_name() {
        return this.center_name; }
    public void setCenter_name(String center_name) {
        this.center_name = center_name; }
    String center_name;
    @JsonProperty("SUBMISSION_PROJECT")
    public SUBMISSIONPROJECT getSUBMISSION_PROJECT() {
        return this.sUBMISSION_PROJECT; }
    public void setSUBMISSION_PROJECT(SUBMISSIONPROJECT sUBMISSION_PROJECT) {
        this.sUBMISSION_PROJECT = sUBMISSION_PROJECT; }
    SUBMISSIONPROJECT sUBMISSION_PROJECT;
    @JsonProperty("PROJECT_LINKS")
    public PROJECTLINKS getPROJECT_LINKS() {
        return this.pROJECT_LINKS; }
    public void setPROJECT_LINKS(PROJECTLINKS pROJECT_LINKS) {
        this.pROJECT_LINKS = pROJECT_LINKS; }
    PROJECTLINKS pROJECT_LINKS;
    @JsonProperty("DESCRIPTION")
    public String getDESCRIPTION() {
        return this.dESCRIPTION; }
    public void setDESCRIPTION(String dESCRIPTION) {
        this.dESCRIPTION = dESCRIPTION; }
    String dESCRIPTION;
    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name; }
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name; }
    String broker_name;
    @JsonProperty("alias")
    public String getAlias() {
        return this.alias; }
    public void setAlias(String alias) {
        this.alias = alias; }
    String alias;
    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS; }
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS; }
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("TITLE")
    public String getTITLE() {
        return this.tITLE; }
    public void setTITLE(String tITLE) {
        this.tITLE = tITLE; }
    String tITLE;
    @JsonProperty("accession")
    public String getAccession() {
        return this.accession; }
    public void setAccession(String accession) {
        this.accession = accession; }
    String accession;
    @JsonProperty("NAME")
    public String getNAME() {
        return this.nAME; }
    public void setNAME(String nAME) {
        this.nAME = nAME; }
    String nAME;
    @JsonProperty("PROJECT_ATTRIBUTES")
    public PROJECTATTRIBUTES getPROJECT_ATTRIBUTES() {
        return this.pROJECT_ATTRIBUTES; }
    public void setPROJECT_ATTRIBUTES(PROJECTATTRIBUTES pROJECT_ATTRIBUTES) {
        this.pROJECT_ATTRIBUTES = pROJECT_ATTRIBUTES; }
    PROJECTATTRIBUTES pROJECT_ATTRIBUTES;
}