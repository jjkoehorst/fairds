package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LOCUS {
    @JsonProperty("PROBE_SET")
    public PROBESET getPROBE_SET() {
        return this.pROBE_SET; }
    public void setPROBE_SET(PROBESET pROBE_SET) {
        this.pROBE_SET = pROBE_SET; }
    PROBESET pROBE_SET;
    @JsonProperty("description")
    public String getDescription() {
        return this.description; }
    public void setDescription(String description) {
        this.description = description; }
    String description;
    @JsonProperty("locus_name")
    public String getLocus_name() {
        return this.locus_name; }
    public void setLocus_name(String locus_name) {
        this.locus_name = locus_name; }
    String locus_name;
}
