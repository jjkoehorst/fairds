package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ERROR {
    public int severity;
    public int number;
    @JsonProperty("public")
    public String mypublic;
    public int line;
    public String procedure;
    public String content;
}
