package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Contact{
    @JsonProperty("phone")
    public String getPhone() {
        return this.phone; }
    public void setPhone(String phone) {
        this.phone = phone; }
    String phone;
    @JsonProperty("fax")
    public String getFax() {
        return this.fax; }
    public void setFax(String fax) {
        this.fax = fax; }
    String fax;
    @JsonProperty("email") 
    public String getEmail() { 
		 return this.email; } 
    public void setEmail(String email) { 
		 this.email = email; } 
    String email;
    @JsonProperty("Name")
    public Name getName() { 
		 return this.name; } 
    public void setName(Name name) { 
		 this.name = name; } 
    Name name;
    @JsonProperty("sec_email") 
    public String getSec_email() { 
		 return this.sec_email; } 
    public void setSec_email(String sec_email) { 
		 this.sec_email = sec_email; } 
    String sec_email;
    @JsonProperty("Address") 
    public Address getAddress() { 
		 return this.address; } 
    public void setAddress(Address address) { 
		 this.address = address; } 
    Address address;
}
