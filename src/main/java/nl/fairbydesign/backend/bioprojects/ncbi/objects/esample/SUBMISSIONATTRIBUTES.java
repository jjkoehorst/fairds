package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SUBMISSIONATTRIBUTES {
    @JsonProperty("SUBMISSION_ATTRIBUTE")
    public ArrayList<SUBMISSIONATTRIBUTE> getSUBMISSION_ATTRIBUTE() {
        return this.sUBMISSION_ATTRIBUTE; }
    public void setSUBMISSION_ATTRIBUTE(ArrayList<SUBMISSIONATTRIBUTE> sUBMISSION_ATTRIBUTE) {
        this.sUBMISSION_ATTRIBUTE = sUBMISSION_ATTRIBUTE; }
    ArrayList<SUBMISSIONATTRIBUTE> sUBMISSION_ATTRIBUTE;
}
