package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Databases{
    @JsonProperty("Database")
    public Database getDatabase() {
        return this.database; }
    public void setDatabase(Database database) {
        this.database = database; }
    Database database;
}
