package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class RUNATTRIBUTES{
    @JsonProperty("RUN_ATTRIBUTE")
    public ArrayList<RUNATTRIBUTE> getRUN_ATTRIBUTE() {
        return this.rUN_ATTRIBUTE; }
    public void setRUN_ATTRIBUTE(ArrayList<RUNATTRIBUTE> rUN_ATTRIBUTE) {
        this.rUN_ATTRIBUTE = rUN_ATTRIBUTE; }
    ArrayList<RUNATTRIBUTE> rUN_ATTRIBUTE;
}
