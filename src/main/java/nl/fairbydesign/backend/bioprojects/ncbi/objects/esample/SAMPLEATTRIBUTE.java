package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SAMPLEATTRIBUTE{
    @JsonProperty("TAG")
    public String getTAG() { 
		 return this.tAG; } 
    public void setTAG(String tAG) { 
		 this.tAG = tAG; } 
    String tAG;
    @JsonProperty("VALUE") 
    public Object getVALUE() { 
		 return this.vALUE; } 
    public void setVALUE(Object vALUE) { 
		 this.vALUE = vALUE; } 
    Object vALUE;
    @JsonProperty("UNITS")
    public String getUNITS() {
        return this.uNITS; }
    public void setUNITS(String uNITS) {
        this.uNITS = uNITS; }
    String uNITS;
}
