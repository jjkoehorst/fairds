package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MEMBER {
    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS; }
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS; }
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("accession")
    public String getAccession() {
        return this.accession; }
    public void setAccession(String accession) {
        this.accession = accession; }
    String accession;
    @JsonProperty("member_name")
    public String getMember_name() {
        return this.member_name; }
    public void setMember_name(String member_name) {
        this.member_name = member_name; }
    String member_name;
    @JsonProperty("READ_LABEL")
    public READLABEL getREAD_LABEL() {
        return this.rEAD_LABEL; }
    public void setREAD_LABEL(READLABEL rEAD_LABEL) {
        this.rEAD_LABEL = rEAD_LABEL; }
    READLABEL rEAD_LABEL;
    @JsonProperty("bases")
    public Object getBases() {
        return this.bases; }
    public void setBases(Object bases) {
        this.bases = bases; }
    Object bases;
    @JsonProperty("organism")
    public String getOrganism() {
        return this.organism; }
    public void setOrganism(String organism) {
        this.organism = organism; }
    String organism;
    @JsonProperty("spots")
    public long getSpots() {
        return this.spots; }
    public void setSpots(long spots) {
        this.spots = spots; }
    long spots;
    @JsonProperty("tax_id")
    public int getTax_id() {
        return this.tax_id; }
    public void setTax_id(int tax_id) {
        this.tax_id = tax_id; }
    int tax_id;
    @JsonProperty("sample_name")
    public String getSample_name() {
        return this.sample_name; }
    public void setSample_name(String sample_name) {
        this.sample_name = sample_name; }
    String sample_name;
    @JsonProperty("sample_title")
    public String getSample_title() {
        return this.sample_title; }
    public void setSample_title(String sample_title) {
        this.sample_title = sample_title; }
    String sample_title;
}
