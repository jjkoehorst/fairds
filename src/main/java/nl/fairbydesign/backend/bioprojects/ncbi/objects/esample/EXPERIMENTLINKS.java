package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class EXPERIMENTLINKS {
    @JsonProperty("EXPERIMENT_LINK")
    public ArrayList<EXPERIMENTLINK> getEXPERIMENT_LINK() {
        return this.eXPERIMENT_LINK; }
    public void setEXPERIMENT_LINK(ArrayList<EXPERIMENTLINK> eXPERIMENT_LINK) {
        this.eXPERIMENT_LINK = eXPERIMENT_LINK; }
    ArrayList<EXPERIMENTLINK> eXPERIMENT_LINK;
}
