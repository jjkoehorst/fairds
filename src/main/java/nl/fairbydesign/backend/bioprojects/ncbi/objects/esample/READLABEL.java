package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class READLABEL {
    @JsonProperty("read_group_tag")
    public int getRead_group_tag() {
        return this.read_group_tag; }
    public void setRead_group_tag(int read_group_tag) {
        this.read_group_tag = read_group_tag; }
    int read_group_tag;
    @JsonProperty("content")
    public int getContent() {
        return this.content; }
    public void setContent(int content) {
        this.content = content; }
    int content;
}
