package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PLATFORM{
    @JsonProperty("ILLUMINA")
    public ILLUMINA getILLUMINA() {
		 return this.iLLUMINA; } 
    public void setILLUMINA(ILLUMINA iLLUMINA) { 
		 this.iLLUMINA = iLLUMINA; } 
    ILLUMINA iLLUMINA;
    @JsonProperty("LS454") 
    public LS454 getLS454() {
		 return this.lS454; } 
    public void setLS454(LS454 lS454) { 
		 this.lS454 = lS454; } 
    LS454 lS454;
    @JsonProperty("ABI_SOLID")
    public ABISOLID getABI_SOLID() {
        return this.aBI_SOLID; }
    public void setABI_SOLID(ABISOLID aBI_SOLID) {
        this.aBI_SOLID = aBI_SOLID; }
    ABISOLID aBI_SOLID;
    @JsonProperty("PACBIO_SMRT")
    public PACBIOSMRT getPACBIO_SMRT() {
        return this.pACBIO_SMRT; }
    public void setPACBIO_SMRT(PACBIOSMRT pACBIO_SMRT) {
        this.pACBIO_SMRT = pACBIO_SMRT; }
    PACBIOSMRT pACBIO_SMRT;
    @JsonProperty("ION_TORRENT")
    public IONTORRENT getION_TORRENT() {
        return this.iON_TORRENT; }
    public void setION_TORRENT(IONTORRENT iON_TORRENT) {
        this.iON_TORRENT = iON_TORRENT; }
    IONTORRENT iON_TORRENT;
    @JsonProperty("OXFORD_NANOPORE")
    public OXFORDNANOPORE getOXFORD_NANOPORE() {
        return this.oXFORD_NANOPORE; }
    public void setOXFORD_NANOPORE(OXFORDNANOPORE oXFORD_NANOPORE) {
        this.oXFORD_NANOPORE = oXFORD_NANOPORE; }
    OXFORDNANOPORE oXFORD_NANOPORE;
    @JsonProperty("BGISEQ")
    public BGISEQ getBGISEQ() {
        return this.bGISEQ; }
    public void setBGISEQ(BGISEQ bGISEQ) {
        this.bGISEQ = bGISEQ; }
    BGISEQ bGISEQ;
    @JsonProperty("COMPLETE_GENOMICS")
    public COMPLETEGENOMICS getCOMPLETE_GENOMICS() {
        return this.cOMPLETE_GENOMICS; }
    public void setCOMPLETE_GENOMICS(COMPLETEGENOMICS cOMPLETE_GENOMICS) {
        this.cOMPLETE_GENOMICS = cOMPLETE_GENOMICS; }
    COMPLETEGENOMICS cOMPLETE_GENOMICS;
    @JsonProperty("CAPILLARY")
    public CAPILLARY getCAPILLARY() {
        return this.cAPILLARY; }
    public void setCAPILLARY(CAPILLARY cAPILLARY) {
        this.cAPILLARY = cAPILLARY; }
    CAPILLARY cAPILLARY;
    @JsonProperty("HELICOS")
    public HELICOS getHELICOS() {
        return this.hELICOS; }
    public void setHELICOS(HELICOS hELICOS) {
        this.hELICOS = hELICOS; }
    HELICOS hELICOS;
}
