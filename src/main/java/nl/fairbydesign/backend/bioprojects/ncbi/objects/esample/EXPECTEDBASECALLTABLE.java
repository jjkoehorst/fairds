package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class EXPECTEDBASECALLTABLE {
    @JsonProperty("BASECALL")
    public ArrayList<Object> getBASECALL() {
        return this.bASECALL; }
    public void setBASECALL(ArrayList<Object> bASECALL) {
        this.bASECALL = bASECALL; }
    ArrayList<Object> bASECALL;
    @JsonProperty("base_coord")
    public int getBase_coord() {
        return this.base_coord; }
    public void setBase_coord(int base_coord) {
        this.base_coord = base_coord; }
    int base_coord;
    @JsonProperty("default_length")
    public int getDefault_length() {
        return this.default_length; }
    public void setDefault_length(int default_length) {
        this.default_length = default_length; }
    int default_length;
}
