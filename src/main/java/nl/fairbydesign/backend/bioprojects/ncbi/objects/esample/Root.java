package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root{
    @JsonProperty("EXPERIMENT_PACKAGE_SET")
    public EXPERIMENTPACKAGESET getEXPERIMENT_PACKAGE_SET() {
		 return this.eXPERIMENT_PACKAGE_SET; } 
    public void setEXPERIMENT_PACKAGE_SET(EXPERIMENTPACKAGESET eXPERIMENT_PACKAGE_SET) { 
		 this.eXPERIMENT_PACKAGE_SET = eXPERIMENT_PACKAGE_SET; } 
    EXPERIMENTPACKAGESET eXPERIMENT_PACKAGE_SET;

    // Also possible is a direct
    @JsonProperty("EXPERIMENT_PACKAGE")
   public void setEXPERIMENT_PACKAGE(EXPERIMENTPACKAGE eXPERIMENT_PACKAGE) {
        // this.eXPERIMENT_PACKAGE = eXPERIMENT_PACKAGE;
        this.eXPERIMENT_PACKAGE_SET = new EXPERIMENTPACKAGESET();
        this.eXPERIMENT_PACKAGE_SET.setEXPERIMENT_PACKAGE(eXPERIMENT_PACKAGE);
    }

    @JsonProperty("PROJECT_SET")
    public PROJECTSET getPROJECT_SET() {
        return this.pROJECT_SET; }
    public void setPROJECT_SET(PROJECTSET pROJECT_SET) {
        this.pROJECT_SET = pROJECT_SET; }
    PROJECTSET pROJECT_SET;


    @JsonProperty("STUDY_SET")
    public STUDYSET getSTUDY_SET() {
        return this.sTUDY_SET; }
    public void setSTUDY_SET(STUDYSET sTUDY_SET) {
        this.sTUDY_SET = sTUDY_SET; }
    STUDYSET sTUDY_SET;
}