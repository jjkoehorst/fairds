package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class TranslationStack {
    @JsonProperty("OP")
    public ArrayList<String> getOP() {
        return this.oP; }
    public void setOP(ArrayList<String> oP) {
        this.oP = oP; }
    ArrayList<String> oP;


    @JsonProperty("TermSet")
    public ArrayList<TermSet> getTermSet() {
        return this.termSet; }
    public void setTermSet(ArrayList<TermSet> termSet) {
        this.termSet = termSet; }
    ArrayList<TermSet> termSet;
}
