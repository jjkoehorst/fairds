package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class STUDYATTRIBUTES {
    @JsonProperty("STUDY_ATTRIBUTE")
    public ArrayList<STUDYATTRIBUTE> getSTUDY_ATTRIBUTE() {
        return this.sTUDY_ATTRIBUTE; }
    public void setSTUDY_ATTRIBUTE(ArrayList<STUDYATTRIBUTE> sTUDY_ATTRIBUTE) {
        this.sTUDY_ATTRIBUTE = sTUDY_ATTRIBUTE; }
    ArrayList<STUDYATTRIBUTE> sTUDY_ATTRIBUTE = new ArrayList<>();
}
