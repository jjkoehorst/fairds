package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROJECTSET{
    @JsonProperty("PROJECT")
    public PROJECT getPROJECT() {
        return this.pROJECT; }
    public void setPROJECT(PROJECT pROJECT) {
        this.pROJECT = pROJECT; }
    PROJECT pROJECT;
}