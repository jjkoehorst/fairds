package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ESearchResult {
    @JsonProperty("ErrorList")
    public ErrorList getErrorList() {
        return this.errorList; }
    public void setErrorList(ErrorList errorList) {
        this.errorList = errorList; }
    ErrorList errorList;

    @JsonProperty("TranslationSet")
    public String getTranslationSet() {
        return this.translationSet;}
    public void setTranslationSet(String translationSet) {
        this.translationSet = translationSet;}
    String translationSet;

    @JsonProperty("RetMax")
    public int getRetMax() {
        return this.retMax;}
    public void setRetMax(int retMax) {
        this.retMax = retMax;}
    int retMax;

    @JsonProperty("TranslationStack")
    public TranslationStack getTranslationStack() {
        return this.translationStack;}
    public void setTranslationStack(TranslationStack translationStack) {
        this.translationStack = translationStack;}
    TranslationStack translationStack;

    @JsonProperty("Count")
    public int getCount() {
        return this.count;}
    public void setCount(int count) {
        this.count = count;}
    int count;

    @JsonProperty("QueryTranslation")
    public String getQueryTranslation() {
        return this.queryTranslation;}
    public void setQueryTranslation(String queryTranslation) {
        this.queryTranslation = queryTranslation;}
    String queryTranslation;

    @JsonProperty("RetStart")
    public int getRetStart() {
        return this.retStart;}
    public void setRetStart(int retStart) {
        this.retStart = retStart;}
    int retStart;

    @JsonProperty("IdList")
    public IdList getIdList() {
        return this.idList;}
    public void setIdList(IdList idList) {
        this.idList = idList;}
    IdList idList;

    @JsonProperty("WarningList")
    public WarningList getWarningList() {
        return this.warningList;}
    public void setWarningList(WarningList warningList) {
        this.warningList = warningList;}
    WarningList warningList;

    @JsonProperty("ERROR")
    public String getERROR() {
        return this.eRROR;}
    public void setERROR(String eRROR) {
        this.eRROR = eRROR;}
    String eRROR;
}