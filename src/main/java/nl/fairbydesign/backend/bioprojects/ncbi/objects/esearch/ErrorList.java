package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ErrorList{
    @JsonProperty("PhraseNotFound")
    public ArrayList<String> getPhraseNotFound() {
        return this.phraseNotFound; }
    public void setPhraseNotFound(ArrayList<String> phraseNotFound) {
        this.phraseNotFound = phraseNotFound; }
    ArrayList<String> phraseNotFound;
}
