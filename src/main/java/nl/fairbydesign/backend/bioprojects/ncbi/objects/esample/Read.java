package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Read{
    @JsonProperty("average") 
    public double getAverage() { 
		 return this.average; } 
    public void setAverage(double average) { 
		 this.average = average; } 
    double average;
    @JsonProperty("count")
    public long getCount() {
		 return this.count; } 
    public void setCount(long count) {
		 this.count = count; } 
    long count;
    @JsonProperty("index") 
    public int getIndex() { 
		 return this.index; } 
    public void setIndex(int index) { 
		 this.index = index; } 
    int index;
    @JsonProperty("stdev") 
    public double getStdev() { 
		 return this.stdev; } 
    public void setStdev(double stdev) { 
		 this.stdev = stdev; } 
    double stdev;
}
