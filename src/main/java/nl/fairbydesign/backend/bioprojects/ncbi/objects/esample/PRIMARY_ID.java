package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PRIMARY_ID {
    @JsonProperty("label")
    public String getLabel() {
        return this.label; }
    public void setLabel(String label) {
        this.label = label; }
    String label;
    @JsonProperty("content")
    public String getContent() {
        return this.content; }
    public void setContent(String content) {
        this.content = content; }
    String content;
}
