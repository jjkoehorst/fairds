package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PIPESECTION {
    @JsonProperty("STEP_INDEX")
    public String getSTEP_INDEX() {
        return this.sTEP_INDEX; }
    public void setSTEP_INDEX(String sTEP_INDEX) {
        this.sTEP_INDEX = sTEP_INDEX; }
    String sTEP_INDEX;
    @JsonProperty("VERSION")
    public String getVERSION() {
        return this.vERSION; }
    public void setVERSION(String vERSION) {
        this.vERSION = vERSION; }
    String vERSION;
    @JsonProperty("PREV_STEP_INDEX")
    public String getPREV_STEP_INDEX() {
        return this.pREV_STEP_INDEX; }
    public void setPREV_STEP_INDEX(String pREV_STEP_INDEX) {
        this.pREV_STEP_INDEX = pREV_STEP_INDEX; }
    String pREV_STEP_INDEX;
    @JsonProperty("PROGRAM")
    public String getPROGRAM() {
        return this.pROGRAM; }
    public void setPROGRAM(String pROGRAM) {
        this.pROGRAM = pROGRAM; }
    String pROGRAM;
    @JsonProperty("section_name")
    public String getSection_name() {
        return this.section_name; }
    public void setSection_name(String section_name) {
        this.section_name = section_name; }
    String section_name;
    @JsonProperty("NOTES")
    public String nOTES;

}
