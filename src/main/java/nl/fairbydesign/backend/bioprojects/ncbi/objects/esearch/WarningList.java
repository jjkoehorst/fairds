package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WarningList{
    @JsonProperty("OutputMessage")
    public String getOutputMessage() {
        return this.outputMessage; }
    public void setOutputMessage(String outputMessage) {
        this.outputMessage = outputMessage; }
    String outputMessage;
}
