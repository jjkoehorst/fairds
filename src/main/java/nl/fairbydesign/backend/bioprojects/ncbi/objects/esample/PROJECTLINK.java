package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROJECTLINK{
    @JsonProperty("XREF_LINK")
    public XREFLINK getXREF_LINK() {
        return this.xREF_LINK; }
    public void setXREF_LINK(XREFLINK xREF_LINK) {
        this.xREF_LINK = xREF_LINK; }
    XREFLINK xREF_LINK;
}