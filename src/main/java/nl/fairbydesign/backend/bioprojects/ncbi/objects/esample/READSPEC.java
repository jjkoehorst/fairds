package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class READSPEC{
    @JsonProperty("READ_CLASS")
    public String getREAD_CLASS() { 
		 return this.rEAD_CLASS; } 
    public void setREAD_CLASS(String rEAD_CLASS) { 
		 this.rEAD_CLASS = rEAD_CLASS; } 
    String rEAD_CLASS;
    @JsonProperty("BASE_COORD") 
    public int getBASE_COORD() { 
		 return this.bASE_COORD; } 
    public void setBASE_COORD(int bASE_COORD) { 
		 this.bASE_COORD = bASE_COORD; } 
    int bASE_COORD;
    @JsonProperty("READ_INDEX") 
    public int getREAD_INDEX() { 
		 return this.rEAD_INDEX; } 
    public void setREAD_INDEX(int rEAD_INDEX) { 
		 this.rEAD_INDEX = rEAD_INDEX; } 
    int rEAD_INDEX;
    @JsonProperty("READ_LABEL")
    public String getREAD_LABEL() {
        return this.rEAD_LABEL; }
    public void setREAD_LABEL(String rEAD_LABEL) {
        this.rEAD_LABEL = rEAD_LABEL; }
    String rEAD_LABEL;
    @JsonProperty("READ_TYPE") 
    public String getREAD_TYPE() { 
		 return this.rEAD_TYPE; } 
    public void setREAD_TYPE(String rEAD_TYPE) { 
		 this.rEAD_TYPE = rEAD_TYPE; } 
    String rEAD_TYPE;
    @JsonProperty("EXPECTED_BASECALL_TABLE")
    public EXPECTEDBASECALLTABLE getEXPECTED_BASECALL_TABLE() {
        return this.eXPECTED_BASECALL_TABLE; }
    public void setEXPECTED_BASECALL_TABLE(EXPECTEDBASECALLTABLE eXPECTED_BASECALL_TABLE) {
        this.eXPECTED_BASECALL_TABLE = eXPECTED_BASECALL_TABLE; }
    EXPECTEDBASECALLTABLE eXPECTED_BASECALL_TABLE;
    @JsonProperty("RELATIVE_ORDER")
    public RELATIVEORDER getRELATIVE_ORDER() {
        return this.rELATIVE_ORDER; }
    public void setRELATIVE_ORDER(RELATIVEORDER rELATIVE_ORDER) {
        this.rELATIVE_ORDER = rELATIVE_ORDER; }
    RELATIVEORDER rELATIVE_ORDER;
}
