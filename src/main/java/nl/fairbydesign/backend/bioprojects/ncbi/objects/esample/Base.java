package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Base{
    @JsonProperty("count")
    public Object getCount() { 
		 return this.count; } 
    public void setCount(Object count) { 
		 this.count = count; } 
    Object count;
    @JsonProperty("value") 
    public String getValue() { 
		 return this.value; } 
    public void setValue(String value) { 
		 this.value = value; } 
    String value;
}
