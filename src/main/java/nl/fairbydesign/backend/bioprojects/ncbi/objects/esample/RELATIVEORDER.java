package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RELATIVEORDER {
    @JsonProperty("precedes_read_index")
    public int getPrecedes_read_index() {
        return this.precedes_read_index; }
    public void setPrecedes_read_index(int precedes_read_index) {
        this.precedes_read_index = precedes_read_index; }
    int precedes_read_index;
    @JsonProperty("follows_read_index")
    public int getFollows_read_index() {
        return this.follows_read_index; }
    public void setFollows_read_index(int follows_read_index) {
        this.follows_read_index = follows_read_index; }
    int follows_read_index;
}
