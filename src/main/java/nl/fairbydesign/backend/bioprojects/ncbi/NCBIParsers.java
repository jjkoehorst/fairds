package nl.fairbydesign.backend.bioprojects.ncbi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class NCBIParsers {
    static String newline = System.getProperty("line.separator");
    static String startTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
    static String targetDir = "all";

    @Test
    public void sampleAttributeValueCount() throws IOException {
        File root = new File("./json/"+targetDir+"/fetch");
        Map<String, Map<String, Integer>> SAMap = new HashMap<>();
        int fileCounter = 0;

        for (File dir : Objects.requireNonNull(root.listFiles())) {
            for (File bpFile : Objects.requireNonNull(dir.listFiles())) {
                fileCounter++;

                JsonFactory jsonFactory = new MappingJsonFactory();
                JsonParser jsonParser = jsonFactory.createParser(bpFile);
                JsonToken current = jsonParser.nextToken();

                if (current != JsonToken.START_OBJECT) {
                    System.out.println("ERROR: root should be object: quiting.");
                    return;
                }

                while (jsonParser.nextToken() != null) {
                    String fieldName = jsonParser.getCurrentName();
                    current = jsonParser.nextToken();
                    if (Objects.equals(fieldName, "SAMPLE_ATTRIBUTE")) {
                        if (current == JsonToken.START_ARRAY) {
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                SAVCAppendMap(SAMap, jsonParser.readValueAsTree());
                            }
                        } else if (current == JsonToken.START_OBJECT) {
                            SAVCAppendMap(SAMap, jsonParser.readValueAsTree());
                        } else if (current != JsonToken.END_ARRAY && current != JsonToken.END_OBJECT) {
                            System.out.println("Error: " + current.name() + " in " + jsonParser.getCurrentName() + ": skipping to next in file: " + bpFile.getAbsolutePath());
                            jsonParser.skipChildren();
                        }
//                    } else {
//                        System.out.println("Unprocessed property: " + fieldName);
                    }
                }
            }
        }
        String dir = "./json/"+targetDir+"/parsings";
        boolean isDirCreated = new File(dir).exists() || new File(dir).mkdirs();
        PrintWriter writer = new PrintWriter(new FileWriter(dir+"/sampleAttributeValueCount"+startTime+".txt"));
        System.out.println("Processed " + fileCounter + " files.");
        for (Map.Entry<String, Map<String, Integer>> tag : SAMap.entrySet()) {
            System.out.println(tag.getKey() + " => ");
            writer.println(tag.getKey() + " => ");
            for (Map.Entry<String, Integer> entry : tag.getValue().entrySet()) {
                System.out.println("\t" + entry.getKey() + " => " + entry.getValue());
                writer.println("\t" + entry.getKey() + " => " + entry.getValue());
            }
        }
        writer.close();
    }

    private void SAVCAppendMap(Map<String, Map<String, Integer>> SAMap, JsonNode jsonNode) {
        String tag = jsonNode.get("TAG").asText();
        String value = jsonNode.get("VALUE").asText();

        Map<String, Integer> values = SAMap.get(tag);
        if (values == null) {
            values = new HashMap<>();
            values.put(value, 1);
            SAMap.put(tag, values);
        } else if (SAMap.get(tag).get(value) == null) {
            SAMap.get(tag).put(value, 1);
        } else {
            Integer count = SAMap.get(tag).get(value);
            SAMap.get(tag).put(value, ++count);
        }
    }

    @Test
    public void attributeAbundanceMatrix() throws IOException {
        File root = new File("./json/"+targetDir+"/fetch");
        // Map<projectID, Set[attributes]>
        Map<String, Set<String>> attributeMap = new HashMap<>();
        Set<String> sampleAttributes = new HashSet<>();
//        Set<String> experimentPlatforms = new LinkedHashSet<>();
//        Set<String> experimentDesigns = new LinkedHashSet<>();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        int counter = 0;
        for (File dir : Objects.requireNonNull(root.listFiles())) {
            for (File bpFile : Objects.requireNonNull(dir.listFiles())) {
                counter++;
                if (counter%100000 == 0) {
                    System.out.println("processed "+counter+" files");
                }

                String jsonString = Files.readString(bpFile.toPath());
                Root experimentRoot = null;
                String projectID;
                try {
                    experimentRoot = objectMapper.readValue(jsonString, Root.class);
                    projectID = experimentRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSTUDY().getIDENTIFIERS().getEXTERNAL_ID().get(0).getContent();
                } catch (Exception e) {
                    System.out.println("1 file ("+ bpFile.getAbsolutePath() +") failed, continuing");
                    continue;
                }
                attributeMap.computeIfAbsent(projectID, k -> new HashSet<>());

                AAMSampleAttributes(projectID, experimentRoot, attributeMap, sampleAttributes);
            }
        }
        writeAttributes(sampleAttributes);
        AAMtoCSV(sampleAttributes, attributeMap);
//        AAMtoXLSX(sampleAttributes, attributeMap);
    }

    /**
     * @param projectID        Identifier for the NCBI Bio-Project
     * @param experimentRoot   ObjectMapper read value
     * @param attributeMap     A HashMap of HashSets with projectID as keys
     * @param sampleAttributes A HashSet of possible sample attributes
     */
    private void AAMSampleAttributes(String projectID, Root experimentRoot, Map<String, Set<String>> attributeMap, Set<String> sampleAttributes) {
        HashSet<String> nonsensical = new HashSet<>(Arrays.asList("not applicable", "not collected"));
        // Add all sample attributes to the project
        try {
            for (SAMPLE sample : experimentRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSAMPLE()) {
                for (SAMPLEATTRIBUTE sampleattribute : sample.getSAMPLE_ATTRIBUTES().getSAMPLE_ATTRIBUTE()) {
                    String tag = sampleattribute.getTAG();
                    String value = sampleattribute.getVALUE().toString();

                    if (tag != null && tag.contains(",")) {
                        tag = tag.replace(",",";");
                    }

                    sampleAttributes.add(tag);
                    if (tag != null && !tag.isBlank() && value != null && !value.isBlank() && !nonsensical.contains(value)) {
                        attributeMap.get(projectID).add(tag);
                    }
                }
            }
        } catch (NullPointerException ignored) {}
    }

    /**
     * @param projectID      Identifier for the NCBI Bio-Project
     * @param experimentRoot ObjectMapper read value
     * @param attributeMap   A HashMap of HashSets with projectID as keys
     */
    private void AAMExperimentPlatform(String projectID, Root experimentRoot, Map<String, Set<String>> attributeMap) {
        // Add the experiment platform to the project
        PLATFORM platform = experimentRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getPLATFORM();
        if (platform.getABI_SOLID() != null) {
            attributeMap.get(projectID).add(platform.getABI_SOLID().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getABI_SOLID().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getBGISEQ() != null) {
            attributeMap.get(projectID).add(platform.getBGISEQ().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getBGISEQ().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getCAPILLARY() != null) {
            attributeMap.get(projectID).add(platform.getCAPILLARY().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getCAPILLARY().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getCOMPLETE_GENOMICS() != null) {
            attributeMap.get(projectID).add(platform.getCOMPLETE_GENOMICS().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getCOMPLETE_GENOMICS().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getHELICOS() != null) {
            attributeMap.get(projectID).add(platform.getHELICOS().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getHELICOS().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getILLUMINA() != null) {
            attributeMap.get(projectID).add(platform.getILLUMINA().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getILLUMINA().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getION_TORRENT() != null) {
            attributeMap.get(projectID).add(platform.getION_TORRENT().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getION_TORRENT().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getLS454() != null) {
            attributeMap.get(projectID).add(platform.getLS454().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getLS454().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getOXFORD_NANOPORE() != null) {
            attributeMap.get(projectID).add(platform.getOXFORD_NANOPORE().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getOXFORD_NANOPORE().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
        if (platform.getPACBIO_SMRT() != null) {
            attributeMap.get(projectID).add(platform.getPACBIO_SMRT().getClass().getSimpleName());
            if (platform.getABI_SOLID().getINSTRUMENT_MODEL() != null && !platform.getABI_SOLID().getINSTRUMENT_MODEL().isBlank()) {
                attributeMap.get(projectID).add(platform.getPACBIO_SMRT().getINSTRUMENT_MODEL().getClass().getSimpleName());
            }
        }
    }

    /**
     * @param projectID      Identifier for the NCBI Bio-Project
     * @param experimentRoot ObjectMapper read value
     * @param attributeMap   A HashMap of HashSets with projectID as keys
     */
    private void AAMExperimentDesign(String projectID, Root experimentRoot, Map<String, Set<String>> attributeMap) {
        DESIGN design = experimentRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getDESIGN();
        // Add all the design attributes to the project
        if (design.getDESIGN_DESCRIPTION() != null && !design.getDESIGN_DESCRIPTION().isBlank()) {
            attributeMap.get(projectID).add(design.getDESIGN_DESCRIPTION().getClass().getSimpleName());
        }
        try {
            design.getSPOT_DESCRIPTOR().getSPOT_DECODE_SPEC().getSPOT_LENGTH();
        } catch (NullPointerException ignored) {}
        // Add all the read specifications to the project
        for (READSPEC readspec : design.getSPOT_DESCRIPTOR().getSPOT_DECODE_SPEC().getREAD_SPEC()) {
            if (readspec.getREAD_CLASS() != null && !readspec.getREAD_CLASS().isBlank()) {
                attributeMap.get(projectID).add(readspec.getREAD_CLASS().getClass().getSimpleName());
            }
            try {
//                attributeMap.get(projectID).add(readspec.getBASE_COORD());
            } catch (NullPointerException ignored) {}
        }
        // Add all the library descriptors to the project
    }

    /**
     * @param sampleAttributes A HashSet of possible sample attributes
     * @param attributeMap     A HashMap of HashSets with projectID as keys
     */
    private void AAMtoXLSX(Set<String> sampleAttributes, Map<String, Set<String>> attributeMap) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("AttributeAbundanceMatrix");

        TreeSet<String> treeSet = new TreeSet<>(sampleAttributes);
        List<String> headers = treeSet.stream().toList();
        sampleAttributes.clear();
        treeSet.clear();
        int currentRowNumber = 0;

        // Create the header row
        Row headerRow = sheet.createRow(currentRowNumber);
        headerRow.createCell(0);
        for (int i = 0; i < headers.size(); i++) {
            Cell headerCell = headerRow.createCell(i+1);
            headerCell.setCellValue(headers.get(i));
        }
        // Create the data rows
        for (Map.Entry<String, Set<String>> row : attributeMap.entrySet()) {
            currentRowNumber++;
            String projectID = row.getKey();
            System.out.println(projectID);
            Set<String> attributes = row.getValue();

            Row dataRow = sheet.createRow(currentRowNumber);
            dataRow.createCell(0).setCellValue(projectID);

            // Populate row with cells
            for (int i = 0; i < headers.size(); i++) {
                Cell dataCell = dataRow.createCell(i+1);

                String currentColumnName = headers.get(i);
                // Populate the cell
                if (attributes.contains(currentColumnName)) {
                    dataCell.setCellValue(1);
                } else {
                    dataCell.setCellValue(0);
                }
            }
        }
        String folder = "./json/"+targetDir+"/parsings/";
        boolean isFolderCreated = new File(folder).exists() || new File(folder).mkdirs();
        File fileName = new File(folder+"AAM"+startTime+".xlsx");
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param sampleAttributes A HashSet of possible sample attributes
     * @param attributeMap     A HashMap of HashSets with projectID as keys
     */
    private void AAMtoCSV(Set<String> sampleAttributes, Map<String, Set<String>> attributeMap) {
        String folder = "./json/"+targetDir+"/parsings/";
        boolean isFolderCreated = new File(folder).exists() || new File(folder).mkdirs();
        File file = new File(folder+"AAM"+startTime+".csv");

        System.out.println("Writing results to file:\n" + file.getAbsolutePath());

        TreeSet<String> treeSet = new TreeSet<>(sampleAttributes);
        List<String> headers = treeSet.stream().toList();
        sampleAttributes.clear();
        treeSet.clear();

        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw))
        {
            out.println("Bio-Project_ID," + StringUtils.join(headers, ","));

            for (Map.Entry<String, Set<String>> row : attributeMap.entrySet()) {
                String projectID = row.getKey();
                Set<String> attributes = row.getValue();

                StringBuilder csvLine = new StringBuilder(projectID);

                for (String currentColumnName : headers) {
                    if (attributes.contains(currentColumnName)) {
                        csvLine.append(",1");
                    } else {
                        csvLine.append(",0");
                    }
                }
                out.println(csvLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeAttributes (Set<String> sampleAttributes) {
        String folder = "./json/"+targetDir+"/parsings/";
        boolean isFolderCreated = new File(folder).exists() || new File(folder).mkdirs();
        File file = new File(folder+"AllAttributes.txt");

        TreeSet<String> treeSet = new TreeSet<>(sampleAttributes);
        try (FileWriter fw = new FileWriter(file, false);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw))
        {
            for (String attribute : treeSet) {
                out.println(attribute);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test () throws IOException, NoSuchFieldException, ParseException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        objectMapper.configOverride(int.class).setSetterInfo(JsonSetter.Value.forValueNulls());
        String jsonString = Files.readString(Path.of("C:/Users/Martlan/Downloads/test.json"));
//        JSONObject jsonObject = new JSONObject(jsonString);
//        System.out.println(jsonObject);
//        if (jsonObject.has("BASE_COORD")) {
//            System.out.println("Has BASE_COORD: "+jsonObject.getString("BASE_COORD"));
//        }
        Root experimentRoot = objectMapper.readValue(jsonString, Root.class);

        READSPEC readspec = experimentRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getDESIGN().getSPOT_DESCRIPTOR().getSPOT_DECODE_SPEC().getREAD_SPEC().get(0);
        JSONObject object = new JSONObject(readspec);
        System.out.println(object);
    }

}
