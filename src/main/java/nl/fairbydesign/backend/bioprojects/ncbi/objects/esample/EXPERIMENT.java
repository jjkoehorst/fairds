package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EXPERIMENT{
    @JsonProperty("PLATFORM") 
    public PLATFORM getPLATFORM() { 
		 return this.pLATFORM; } 
    public void setPLATFORM(PLATFORM pLATFORM) { 
		 this.pLATFORM = pLATFORM; } 
    PLATFORM pLATFORM;
    @JsonProperty("DESIGN")
    public DESIGN getDESIGN() {
		 return this.dESIGN; } 
    public void setDESIGN(DESIGN dESIGN) { 
		 this.dESIGN = dESIGN; } 
    DESIGN dESIGN;
    @JsonProperty("center_name") 
    public String getCenter_name() { 
		 return this.center_name; } 
    public void setCenter_name(String center_name) { 
		 this.center_name = center_name; } 
    String center_name;

    @JsonProperty("PROCESSING")
    public Object getPROCESSING() {
        return this.pROCESSING; }
    public void setPROCESSING(Object pROCESSING) {
        this.pROCESSING = pROCESSING; }
    Object pROCESSING;

    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name; }
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name; }
    String broker_name;

    @JsonProperty("EXPERIMENT_ATTRIBUTES")
    public EXPERIMENTATTRIBUTES getEXPERIMENT_ATTRIBUTES() {
        return this.eXPERIMENT_ATTRIBUTES; }
    public void setEXPERIMENT_ATTRIBUTES(EXPERIMENTATTRIBUTES eXPERIMENT_ATTRIBUTES) {
        this.eXPERIMENT_ATTRIBUTES = eXPERIMENT_ATTRIBUTES; }
    EXPERIMENTATTRIBUTES eXPERIMENT_ATTRIBUTES;

    @JsonProperty("xmlns")
    public String getXmlns() {
        return this.xmlns; }
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns; }
    String xmlns;

    @JsonProperty("xmlns:xsi")
    public String getXmlnsXsi() {
        return this.xmlnsXsi; }
    public void setXmlnsXsi(String xmlnsXsi) {
        this.xmlnsXsi = xmlnsXsi; }
    String xmlnsXsi;

    @JsonProperty("alias") 
    public String getAlias() { 
		 return this.alias; } 
    public void setAlias(String alias) { 
		 this.alias = alias; } 
    String alias;
    @JsonProperty("IDENTIFIERS") 
    public IDENTIFIERS getIDENTIFIERS() { 
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) { 
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("TITLE") 
    public String getTITLE() { 
		 return this.tITLE; } 
    public void setTITLE(String tITLE) { 
		 this.tITLE = tITLE; } 
    String tITLE;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("STUDY_REF") 
    public STUDYREF getSTUDY_REF() { 
		 return this.sTUDY_REF; } 
    public void setSTUDY_REF(STUDYREF sTUDY_REF) { 
		 this.sTUDY_REF = sTUDY_REF; } 
    STUDYREF sTUDY_REF;
    @JsonProperty("EXPERIMENT_LINKS")
    public EXPERIMENTLINKS getEXPERIMENT_LINKS() {
        return this.eXPERIMENT_LINKS; }
    public void setEXPERIMENT_LINKS(EXPERIMENTLINKS eXPERIMENT_LINKS) {
        this.eXPERIMENT_LINKS = eXPERIMENT_LINKS; }
    EXPERIMENTLINKS eXPERIMENT_LINKS;
}
