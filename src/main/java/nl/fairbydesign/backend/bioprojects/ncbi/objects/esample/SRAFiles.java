package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SRAFiles{
    @JsonProperty("SRAFile")
    public ArrayList<SRAFile> getSRAFile() {
		 return this.sRAFile; } 
    public void setSRAFile(ArrayList<SRAFile> sRAFile) { 
		 this.sRAFile = sRAFile; } 
    ArrayList<SRAFile> sRAFile;
}
