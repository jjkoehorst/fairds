package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ILLUMINA{
    @JsonProperty("INSTRUMENT_MODEL")
    public String getINSTRUMENT_MODEL() { 
		 return this.iNSTRUMENT_MODEL; } 
    public void setINSTRUMENT_MODEL(String iNSTRUMENT_MODEL) { 
		 this.iNSTRUMENT_MODEL = iNSTRUMENT_MODEL; } 
    String iNSTRUMENT_MODEL;
}
