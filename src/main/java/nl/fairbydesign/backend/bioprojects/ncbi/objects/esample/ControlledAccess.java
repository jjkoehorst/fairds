package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ControlledAccess {
    @JsonProperty("study")
    public String getStudy() {
        return this.study; }
    public void setStudy(String study) {
        this.study = study; }
    String study;
    @JsonProperty("consent")
    public String getConsent() {
        return this.consent; }
    public void setConsent(String consent) {
        this.consent = consent; }
    String consent;
}
