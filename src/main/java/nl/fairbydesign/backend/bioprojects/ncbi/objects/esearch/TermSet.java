package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString), Root.class); */
public class TermSet {
    @JsonProperty("Field")
    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    String field;

    @JsonProperty("Explode")
    public String getExplode() {
        return this.explode;
    }

    public void setExplode(String explode) {
        this.explode = explode;
    }

    String explode;

    @JsonProperty("Term")
    public String getTerm() {
        return this.term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    String term;

    @JsonProperty("Count")
    public int getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    int count;
}
