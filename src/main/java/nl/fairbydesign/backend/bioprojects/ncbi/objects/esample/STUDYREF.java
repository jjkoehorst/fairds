package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class STUDYREF{
    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) { 
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("refcenter")
    public String getRefcenter() {
        return this.refcenter; }
    public void setRefcenter(String refcenter) {
        this.refcenter = refcenter; }
    String refcenter;
    @JsonProperty("refname")
    public String getRefname() {
        return this.refname; }
    public void setRefname(String refname) {
        this.refname = refname; }
    String refname;

}
