package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Table{
    @JsonProperty("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonProperty("Statistics")
    public Statistics getStatistics() {
        return this.statistics; }
    public void setStatistics(Statistics statistics) {
        this.statistics = statistics; }
    Statistics statistics;
}