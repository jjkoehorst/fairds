package nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class IdList{
    @JsonProperty("Id")
    public ArrayList<String> getId() {
        return this.id; }
    public void setId(ArrayList<String> id) {
        this.id = id; }
    ArrayList<String> id;
}
