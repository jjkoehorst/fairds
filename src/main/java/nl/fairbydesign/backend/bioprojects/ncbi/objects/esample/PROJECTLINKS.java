package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class PROJECTLINKS{
    @JsonProperty("PROJECT_LINK")
    public ArrayList<PROJECTLINK> getPROJECT_LINK() {
        return this.pROJECT_LINK; }
    public void setPROJECT_LINK(ArrayList<PROJECTLINK> pROJECT_LINK) {
        this.pROJECT_LINK = pROJECT_LINK; }
    ArrayList<PROJECTLINK> pROJECT_LINK;
}