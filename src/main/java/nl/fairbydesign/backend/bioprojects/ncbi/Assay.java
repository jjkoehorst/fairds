package nl.fairbydesign.backend.bioprojects.ncbi;

import nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import static nl.fairbydesign.backend.bioprojects.ncbi.NCBI.roots;
import static nl.fairbydesign.backend.bioprojects.ncbi.XLSX.createSheet;
import static nl.fairbydesign.backend.bioprojects.ncbi.XLSX.updateHeaderRow;
import static nl.fairbydesign.backend.parsers.ExcelValidator.SAMPLE_IDENTIFIER;

public class Assay {
    public static final Logger logger = Logger.getLogger(XLSX.class);

    static void createAssay() {
        HashSet<String> assayIdentifiers = new HashSet<>();
        for (Root root : roots) {
            int rowIndex;
            String identifier = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getRUN_SET().getRUN().get(0).getIDENTIFIERS().getPRIMARY_ID().toString();
            // Ensuring that duplicate identifiers are ignored
            if (assayIdentifiers.contains(identifier)) continue;
            assayIdentifiers.add(identifier);
            // Core headers to be removed...
            ArrayList<String> headerList = new ArrayList<>(); // List.of(new String[]{ASSAY_IDENTIFIER, "Instrument model"})); // , "run URL"}));

            ArrayList<SRAFile> sraFiles = new ArrayList<>();
            Sheet assaySheet;
            // Based on the library descriptor assay type is to be decided
            PLATFORM platform = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getPLATFORM();
            Row assayRow;
            String instrument_model;
            if (platform.getABI_SOLID() != null) {
                // Create the sheet with the headers
                assaySheet = createSheet(headerList, "Assay - ABI Solid");
                instrument_model = platform.getABI_SOLID().getINSTRUMENT_MODEL();
            } else if (platform.getBGISEQ() != null) {
                assaySheet = createSheet(headerList, "Assay - BigSeq");
                instrument_model = platform.getBGISEQ().getINSTRUMENT_MODEL();
            } else if (platform.getCAPILLARY() != null) {
                assaySheet = createSheet(headerList, "Assay - Capillary");
                instrument_model = platform.getCAPILLARY().getINSTRUMENT_MODEL();
            } else if (platform.getHELICOS() != null) {
                assaySheet = createSheet(headerList, "Assay - Helicos");
                instrument_model = platform.getHELICOS().getINSTRUMENT_MODEL();
            } else if (platform.getILLUMINA() != null) {
                // Check for amplicon data
                if (root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getDESIGN().getLIBRARY_DESCRIPTOR().getLIBRARY_STRATEGY().equalsIgnoreCase("AMPLICON")) {
                    assaySheet = createSheet(headerList, "Assay - Amplicon");
                } else {
                    assaySheet = createSheet(headerList, "Assay - Illumina");
                }
                instrument_model = platform.getILLUMINA().getINSTRUMENT_MODEL();
            } else if (platform.getCOMPLETE_GENOMICS() != null) {
                assaySheet = createSheet(headerList, "Assay - Complete Genomics");
                instrument_model = platform.getCOMPLETE_GENOMICS().getINSTRUMENT_MODEL();
            } else if (platform.getION_TORRENT() != null) {
                assaySheet = createSheet(headerList, "Assay - Ion Torrent");
                instrument_model = platform.getION_TORRENT().getINSTRUMENT_MODEL();
            } else if (platform.getLS454() != null) {
                assaySheet = createSheet(headerList, "Assay - LS454");
                instrument_model = platform.getLS454().getINSTRUMENT_MODEL();
            } else if (platform.getOXFORD_NANOPORE() != null) {
                assaySheet = createSheet(headerList, "Assay - Nanopore");
                instrument_model = platform.getOXFORD_NANOPORE().getINSTRUMENT_MODEL();
            } else if (platform.getPACBIO_SMRT() != null) {
                assaySheet = createSheet(headerList, "Assay - Pacbio SMRT");
                instrument_model = platform.getPACBIO_SMRT().getINSTRUMENT_MODEL();
            } else {
                logger.error("Skipping entry as no machine type detected");
                continue;
            }

            rowIndex = assaySheet.getLastRowNum() + 1;
            assayRow = assaySheet.createRow(rowIndex);

            headerList = getHeaderList(assaySheet);
            assayRow.createCell(headerList.indexOf("instrument model")).setCellValue(instrument_model);

            if (root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getRUN_SET().getRUN().get(0).getIDENTIFIERS().getPRIMARY_ID().getClass() == String.class) {
                assayRow.createCell(headerList.indexOf("assay identifier")).setCellValue(identifier);
            }

            // Run data layout
            createDataLayout(root, headerList, assaySheet, assayRow, sraFiles);
            // Run library layout
            createLibraryLayout(root, headerList, assaySheet, assayRow);
            // Add sample link
            if (root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSAMPLE().size() > 1)
                logger.error("Warning! More than one sample detected");
            for (SAMPLE sample : root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSAMPLE()) {
                updateHeaderRow(headerList, assaySheet, SAMPLE_IDENTIFIER);
                assayRow.createCell(headerList.indexOf(SAMPLE_IDENTIFIER)).setCellValue(sample.getAccession());
            }
        }
    }

    private static ArrayList<String> getHeaderList(Sheet assaySheet) {
        ArrayList<String> headerList = new ArrayList<>();
        assaySheet.getRow(0).cellIterator().forEachRemaining(cell -> headerList.add(cell.getStringCellValue()));
        return headerList;
    }

    private static void createLibraryLayout(Root root, ArrayList<String> headerList, Sheet assaySheet, Row assayRow) {
        DESIGN design  = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getDESIGN();

        if (design.getDESIGN_DESCRIPTION() != null && design.getDESIGN_DESCRIPTION().length() > 0) {
            headerList = updateHeaderRow(headerList, assaySheet, "design description");
            assayRow.createCell(headerList.indexOf("design description")).setCellValue(design.getDESIGN_DESCRIPTION());
        }

        if (design.getSPOT_DESCRIPTOR() != null) {
            SPOTDECODESPEC spotDecodeSpec = design.getSPOT_DESCRIPTOR().getSPOT_DECODE_SPEC();
            if (spotDecodeSpec.getREAD_SPEC().size() == 1) {
                READSPEC readspec = spotDecodeSpec.getREAD_SPEC().get(0);
                headerList = updateHeaderRow(headerList, assaySheet, "read base coord");
                assayRow.createCell(headerList.indexOf("read base coord")).setCellValue(readspec.getBASE_COORD());

                headerList = updateHeaderRow(headerList, assaySheet, "read base class");
                assayRow.createCell(headerList.indexOf("read base class")).setCellValue(readspec.getREAD_CLASS());

                headerList = updateHeaderRow(headerList, assaySheet, "read base index");
                assayRow.createCell(headerList.indexOf("read base index")).setCellValue(readspec.getREAD_INDEX());

                headerList = updateHeaderRow(headerList, assaySheet, "read base table");
                assayRow.createCell(headerList.indexOf("read base table")).setCellValue(readspec.getREAD_LABEL());

                headerList = updateHeaderRow(headerList, assaySheet, "read base type");
                assayRow.createCell(headerList.indexOf("read base type")).setCellValue(readspec.getREAD_TYPE());
            } else if (spotDecodeSpec.getREAD_SPEC().size() == 2) {
                ArrayList<String> fwdrevlist = new ArrayList<>();
                fwdrevlist.addAll(List.of(new String[]{"forward", "reverse"}));

                for (int i = 0; i < fwdrevlist.size(); i++) {
                    String type = fwdrevlist.get(i);
                    READSPEC readspec = spotDecodeSpec.getREAD_SPEC().get(i);
                    headerList = updateHeaderRow(headerList, assaySheet, type + " read base coord");
                    assayRow.createCell(headerList.indexOf(type + " read base coord")).setCellValue(readspec.getBASE_COORD());

                    headerList = updateHeaderRow(headerList, assaySheet, type + " read base class");
                    assayRow.createCell(headerList.indexOf(type + " read base class")).setCellValue(readspec.getREAD_CLASS());

                    headerList = updateHeaderRow(headerList, assaySheet, type + " read base index");
                    assayRow.createCell(headerList.indexOf(type + " read base index")).setCellValue(readspec.getREAD_INDEX());

                    headerList = updateHeaderRow(headerList, assaySheet, type + " read base label");
                    assayRow.createCell(headerList.indexOf(type + " read base label")).setCellValue(readspec.getREAD_LABEL());

                    headerList = updateHeaderRow(headerList, assaySheet, type + " read base type");
                    assayRow.createCell(headerList.indexOf(type + " read base type")).setCellValue(readspec.getREAD_TYPE());
                }
            } else {
                logger.error("More than 2 spot decode specifications detected");
            }
        }

        LIBRARYDESCRIPTOR libraryDescriptor = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getEXPERIMENT().getDESIGN().getLIBRARY_DESCRIPTOR();

        if (libraryDescriptor.getLIBRARY_SOURCE() != null) {
            headerList = updateHeaderRow(headerList, assaySheet, "library source");
            assayRow.createCell(headerList.indexOf("library source")).setCellValue(libraryDescriptor.getLIBRARY_SOURCE());
        }

        if (libraryDescriptor.getLIBRARY_NAME() != null && libraryDescriptor.getLIBRARY_NAME().length() > 0) {
            headerList = updateHeaderRow(headerList, assaySheet, "library name");
            assayRow.createCell(headerList.indexOf("library name")).setCellValue(libraryDescriptor.getLIBRARY_NAME());
        }

        if (libraryDescriptor.getLIBRARY_SELECTION() != null) {
            headerList = updateHeaderRow(headerList, assaySheet, "library selection");
            assayRow.createCell(headerList.indexOf("library selection")).setCellValue(libraryDescriptor.getLIBRARY_SELECTION());
        }

        if (libraryDescriptor.getLIBRARY_STRATEGY() != null) {
            headerList = updateHeaderRow(headerList, assaySheet, "library strategy");
            assayRow.createCell(headerList.indexOf("library strategy")).setCellValue(libraryDescriptor.getLIBRARY_STRATEGY());
        }

        if (libraryDescriptor.getLIBRARY_CONSTRUCTION_PROTOCOL() != null && libraryDescriptor.getLIBRARY_CONSTRUCTION_PROTOCOL().length() > 0) {
            headerList = updateHeaderRow(headerList, assaySheet, "library construction protocol");
            assayRow.createCell(headerList.indexOf("library construction protocol")).setCellValue(libraryDescriptor.getLIBRARY_CONSTRUCTION_PROTOCOL());
        }

        // headerList = updateHeaderRow(headerList, assaySheet, "library layout");
        if (libraryDescriptor.getLIBRARY_LAYOUT().getPAIRED() != null) {
            // assayRow.createCell(headerList.indexOf("library layout")).setCellValue("paired");
            if (libraryDescriptor.getLIBRARY_LAYOUT().getPAIRED().getClass() == LinkedHashMap.class) {
                LinkedHashMap<String, Integer> linkedHashMap = (LinkedHashMap) libraryDescriptor.getLIBRARY_LAYOUT().getPAIRED();
                for (String key : linkedHashMap.keySet()) {
                    headerList = updateHeaderRow(headerList, assaySheet, "library " + key);
                    assayRow.createCell(headerList.indexOf("library " + key)).setCellValue(linkedHashMap.get(key));
                }
            } else {
                // System.err.println(libraryDescriptor.getLIBRARY_LAYOUT().getPAIRED().toString().length());
            }
        } else {
            // assayRow.createCell(headerList.indexOf("library layout")).setCellValue("single");
        }
    }

    private static void createDataLayout(Root root, ArrayList<String> headerList, Sheet assaySheet, Row assayRow, ArrayList<SRAFile> sraFiles) {
        for (RUN run : root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getRUN_SET().getRUN()) {
            if (run.getSRAFiles() == null) {
                // What to do?
            } else {
                for (SRAFile sraFile : run.getSRAFiles().getSRAFile()) {
                    if (sraFile.getSemantic_name().equals("run")) {
                        if (!headerList.contains("run URL")) {
                            headerList.add("run URL");
                            updateHeaderRow(headerList, assaySheet);
                        }
                        assayRow.createCell(headerList.indexOf("run URL")).setCellValue(sraFile.getUrl());
                    } else {
                        sraFiles.add(sraFile);
                    }
                }
                createFileEntries(headerList, sraFiles, assaySheet, assayRow);

                // Statistics
                if (run.getSize() != null) {
                    headerList = updateHeaderRow(headerList, assaySheet, "Size");
                    assayRow.createCell(headerList.indexOf("Size")).setCellValue(run.getSize().toString());
                }

                if (run.getStatistics() != null) {
                    if (run.getStatistics().getNreads() != null) {
                        headerList = updateHeaderRow(headerList, assaySheet, "Nreads");
                        assayRow.createCell(headerList.indexOf("Nreads")).setCellValue(run.getStatistics().getNreads());
                    }
                    if (run.getStatistics().getNspots() > 0) {
                        headerList = updateHeaderRow(headerList, assaySheet, "Nspots");
                        assayRow.createCell(headerList.indexOf("Nspots")).setCellValue(run.getStatistics().getNspots());
                    }
                    if (run.getStatistics().getSource() != null) {
                        headerList = updateHeaderRow(headerList, assaySheet, "Source");
                        assayRow.createCell(headerList.indexOf("Source")).setCellValue(run.getStatistics().getSource());
                    }

                    if (run.getStatistics().getRead() == null) continue;

                    if (run.getStatistics().getRead().size() == 1) {
                        // Single read information
                        headerList = updateHeaderRow(headerList, assaySheet, "Read average");
                        assayRow.createCell(headerList.indexOf("Read average")).setCellValue(run.getStatistics().getRead().get(0).getAverage());
                        headerList = updateHeaderRow(headerList, assaySheet, "Read index");
                        assayRow.createCell(headerList.indexOf("Read index")).setCellValue(run.getStatistics().getRead().get(0).getIndex());
                        headerList = updateHeaderRow(headerList, assaySheet, "Read count");
                        assayRow.createCell(headerList.indexOf("Read count")).setCellValue(run.getStatistics().getRead().get(0).getCount());
                        headerList = updateHeaderRow(headerList, assaySheet, "Read stdev");
                        assayRow.createCell(headerList.indexOf("Read stdev")).setCellValue(run.getStatistics().getRead().get(0).getStdev());
                    } else if (run.getStatistics().getRead().size() == 2) {
                        // Forward read information
                        headerList = updateHeaderRow(headerList, assaySheet, "Forward read average");
                        assayRow.createCell(headerList.indexOf("Forward read average")).setCellValue(run.getStatistics().getRead().get(0).getAverage());
                        headerList = updateHeaderRow(headerList, assaySheet, "Forward read index");
                        assayRow.createCell(headerList.indexOf("Forward read index")).setCellValue(run.getStatistics().getRead().get(0).getIndex());
                        headerList = updateHeaderRow(headerList, assaySheet, "Forward read count");
                        assayRow.createCell(headerList.indexOf("Forward read count")).setCellValue(run.getStatistics().getRead().get(0).getCount());
                        headerList = updateHeaderRow(headerList, assaySheet, "Forward read stdev");
                        assayRow.createCell(headerList.indexOf("Forward read stdev")).setCellValue(run.getStatistics().getRead().get(0).getStdev());
                        // Reverse read information
                        headerList = updateHeaderRow(headerList, assaySheet, "Reverse read average");
                        assayRow.createCell(headerList.indexOf("Reverse read average")).setCellValue(run.getStatistics().getRead().get(1).getAverage());
                        headerList = updateHeaderRow(headerList, assaySheet, "Reverse read index");
                        assayRow.createCell(headerList.indexOf("Reverse read index")).setCellValue(run.getStatistics().getRead().get(1).getIndex());
                        headerList = updateHeaderRow(headerList, assaySheet, "Reverse read count");
                        assayRow.createCell(headerList.indexOf("Reverse read count")).setCellValue(run.getStatistics().getRead().get(1).getCount());
                        headerList = updateHeaderRow(headerList, assaySheet, "Reverse read stdev");
                        assayRow.createCell(headerList.indexOf("Reverse read stdev")).setCellValue(run.getStatistics().getRead().get(1).getStdev());
                    } else {
                        logger.error("Warning more than 2 read sets identified");
                    }
                }
            }

            if (run.getTITLE() != null) {
                headerList = updateHeaderRow(headerList, assaySheet, "assay description");
                assayRow.createCell(headerList.indexOf("assay description")).setCellValue(run.getTITLE());
            }

            if (run.getRUN_ATTRIBUTES() != null) {
                for (RUNATTRIBUTE runattribute : run.getRUN_ATTRIBUTES().getRUN_ATTRIBUTE()) {
                    headerList = updateHeaderRow(headerList, assaySheet, "Run " + runattribute.getTAG());
                    assayRow.createCell(headerList.indexOf("Run " + runattribute.getTAG())).setCellValue(runattribute.getVALUE());
                }
            }

            if (run.getBases() != null) {
                for (Base base : run.getBases().getBase()) {
                    headerList = updateHeaderRow(headerList, assaySheet, "Base " + base.getValue());
                    if (base.getCount().getClass() == Long.class) {
                        assayRow.createCell(headerList.indexOf("Base " + base.getValue())).setCellValue((Long) base.getCount());
                    } else if (base.getCount().getClass() == Integer.class) {
                        assayRow.createCell(headerList.indexOf("Base " + base.getValue())).setCellValue((Integer) base.getCount());
                    } else {
                        // TODO?
                    }
                }
            }

            headerList = updateHeaderRow(headerList, assaySheet, "Public");
            assayRow.createCell(headerList.indexOf("Public")).setCellValue(run.getIs_public());

            if (run.getRun_date() != null) {
                headerList = updateHeaderRow(headerList, assaySheet, "Run date");
                assayRow.createCell(headerList.indexOf("Run date")).setCellValue(run.getRun_date());
            }
        }
    }

    private static void createFileEntries(ArrayList<String> headerList, ArrayList<SRAFile> sraFiles, Sheet assaySheet, Row assayRow) {
        if (sraFiles.size() == 1) {
            headerList = updateHeaderRow(headerList, assaySheet, "Filename");
            assayRow.createCell(headerList.indexOf("Filename")).setCellValue(sraFiles.get(0).getFilename());
            headerList = updateHeaderRow(headerList, assaySheet, "Filename URL");
            assayRow.createCell(headerList.indexOf("Filename URL")).setCellValue(sraFiles.get(0).getUrl());
        } else if (sraFiles.size() == 2) {
            headerList = updateHeaderRow(headerList, assaySheet, "Forward Filename");
            headerList = updateHeaderRow(headerList, assaySheet, "Reverse Filename");
            assayRow.createCell(headerList.indexOf("Forward Filename")).setCellValue(sraFiles.get(0).getFilename());
            assayRow.createCell(headerList.indexOf("Reverse Filename")).setCellValue(sraFiles.get(1).getFilename());

            headerList = updateHeaderRow(headerList, assaySheet, "Forward Filename URL");
            headerList = updateHeaderRow(headerList, assaySheet, "Reverse Filename URL");
            assayRow.createCell(headerList.indexOf("Forward Filename URL")).setCellValue(sraFiles.get(0).getUrl());
            assayRow.createCell(headerList.indexOf("Reverse Filename URL")).setCellValue(sraFiles.get(1).getUrl());
        } else {
            logger.error("More sra data files than expected");
        }
    }
}
