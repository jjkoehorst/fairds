package nl.fairbydesign.backend.bioprojects.ncbi;

import nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.Name;
import nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.*;
import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static nl.fairbydesign.backend.ApplicationServiceInitListener.metadataSet;
import static nl.fairbydesign.backend.bioprojects.ncbi.Assay.createAssay;
import static nl.fairbydesign.backend.parsers.ExcelGenerator.*;
import static nl.fairbydesign.backend.parsers.ExcelValidator.*;

public class XLSX {

    public static CellStyle headerStyle;
    public static final int defaultCellWidth = 25;
    public static CellStyle obligatoryStyle;

    public static final org.apache.log4j.Logger logger = Logger.getLogger(XLSX.class);
    private static XSSFWorkbook workbook;
    private static HashSet<Root> roots;

    /**
     * @param rootEntries       sample objects from NCBI
     */
    public void setup(HashSet<Root> rootEntries, String excelFileName) {
        roots = rootEntries;

        logger.info("Number of samples: " + roots.size());
        // Create Excel file
        workbook = new XSSFWorkbook();
        makeHeaderCellStyle();
        makeObligatoryHeaderCellStyle();

        // Create project
        createProject();

        // Create investigation
        createInvestigation();

        // Create study
        createStudy();

        // Create observation unit
        createObservationUnit();

        // Create sample
        createSample();

        // Create assay
        createAssay();

        // Fix obligatory headers
        fixHeaders();

        // Create excel file
        createExcelFile(excelFileName);
    }

    private void fixHeaders() {
        makeObligatoryHeaderCellStyle();
        workbook.sheetIterator().forEachRemaining(sheet -> {
            Row headerRow = sheet.getRow(sheet.getFirstRowNum());
            ArrayList<String> headerList = getObligatoryColumns(sheet, true);
            headerList.stream().map(s -> s.toLowerCase()).collect(Collectors.toList());
            headerRow.cellIterator().forEachRemaining(cell -> {
                if (headerList.contains(cell.getStringCellValue().toLowerCase())) {
                    cell.setCellStyle(obligatoryStyle);
                }
            });
        });
    }

    private static void createExcelFile(String excelFileName) {
        // Write excel file
        File filename = new File(excelFileName);
        try {
            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static ArrayList<String> getObligatoryColumns(Sheet sheet) {
        // Default is to include also the existing columns unless strict is used
        return getObligatoryColumns(sheet, false);
    }

    static ArrayList<String> getObligatoryColumns(Sheet sheet, boolean strict) {
        // Use the metadata object for the right obligatory headers?
        ArrayList<String> headerList = new ArrayList<>();
        for (String key : metadataSet.keySet()) {
            if (sheet.getSheetName().startsWith(key)) {
                ArrayList<Metadata> metadataArrayList = metadataSet.get(key);
                for (Metadata metadata : metadataArrayList) {
                    if (metadata.getPackageName().equalsIgnoreCase("default") && metadata.getRequirement().equalsIgnoreCase("mandatory")) {
                        headerList.add(metadata.getLabel());
                    } else if (sheet.getSheetName().endsWith(metadata.getPackageName()) && metadata.getRequirement().equalsIgnoreCase("mandatory")) {
                        headerList.add(metadata.getLabel());
                    }
                }
            }
        }

        if (!strict) {
            // Expansion of headerList with already existing columns
            if (sheet.getLastRowNum() != -1)
                sheet.getRow(0).cellIterator().forEachRemaining(cell -> headerList.add(cell.getStringCellValue()));
        }
        return headerList;
    }

    private static void createSample() {
        // Core headers
        ArrayList<String> headerList = new ArrayList<>(List.of(new String[]{SAMPLE_IDENTIFIER, "Sample description", "sample name", "NCBI taxonomy ID", "Sample organism", "Biosafety level"}));
        // Expand headers using the metadata of all roots
        for (Root root : roots) {
            for (SAMPLE sample : root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSAMPLE()) {
                for (SAMPLEATTRIBUTE sampleattribute : sample.getSAMPLE_ATTRIBUTES().getSAMPLE_ATTRIBUTE()) {
                    String tag = sampleattribute.getTAG();
                    if (!headerList.contains(tag))
                        headerList.add(sampleattribute.getTAG());
                }
            }
        }
        headerList.add("Observation unit identifier");

        // Create the sheet with the headers
        Sheet sampleSheet = createSheet(headerList, "Sample");

        // Fill the sheet with content
        int rowIndex = 0;
        HashSet<String> sampleAccessions = new HashSet<>();
        for (Root root : roots) {
            for (SAMPLE sample : root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSAMPLE()) {
                // Ensures that duplicate samples are ignored
                if (sampleAccessions.contains(sample.getAccession())) continue;
                sampleAccessions.add(sample.getAccession());
                // New row
                rowIndex++;
                Row sampleRow = sampleSheet.createRow(rowIndex);
                if (sample.getAccession() != null) {
                    sampleRow.createCell(headerList.indexOf(SAMPLE_IDENTIFIER)).setCellValue(sample.getAccession());
                }
                if (sample.getDESCRIPTION() != null && sample.getDESCRIPTION().length() > 0) {
                    sampleRow.createCell(headerList.indexOf("Sample description")).setCellValue(sample.getDESCRIPTION());
                }

                sampleRow.createCell(headerList.indexOf("Observation unit identifier")).setCellValue("X" + sample.getAccession());

                sampleRow.createCell(headerList.indexOf("NCBI taxonomy ID")).setCellValue(sample.getSAMPLE_NAME().getTAXON_ID());
                updateHeaderRow(headerList, sampleSheet, "Sample organism");
                sampleRow.createCell(headerList.indexOf("Sample organism")).setCellValue(sample.getSAMPLE_NAME().getSCIENTIFIC_NAME());

                // All sample attributes
                for (SAMPLEATTRIBUTE sampleattribute : sample.getSAMPLE_ATTRIBUTES().getSAMPLE_ATTRIBUTE()) {
                    String tag = sampleattribute.getTAG();
                    Object value = sampleattribute.getVALUE();

                    if (value.getClass() == String.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((String) value);
                    } else if (value.getClass() == Integer.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((Integer) value);
                    } else if (value.getClass() == Date.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((Date) value);
                    } else if (value.getClass() == Double.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((Double) value);
                    } else if (value.getClass() == Boolean.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((Boolean) value);
                    } else if (value.getClass() == LocalDate.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((LocalDate) value);
                    } else if (value.getClass() == LocalDateTime.class) {
                        int index = headerList.indexOf(tag);
                        sampleRow.createCell(index).setCellValue((LocalDateTime) value);
                    }
                }

                // Sample links
                if (sample.getSAMPLE_LINKS() != null) {
                    for (SAMPLELINK sample_link : sample.getSAMPLE_LINKS().getSAMPLE_LINK()) {
                        if (sample_link.getURL_LINK() != null) {
                            String sampleLinkLabel = "Sample link " + sample_link.getURL_LINK().getLABEL() + " URL";
                            headerList = updateHeaderRow(headerList, sampleSheet, sampleLinkLabel);
                            sampleRow.createCell(headerList.indexOf(sampleLinkLabel)).setCellValue(sample_link.getURL_LINK().getURL());
                        }

                        if (sample_link.getXREF_LINK() != null) {
                            String sampleLinkLabel = "Sample link " + sample_link.getXREF_LINK().getDB() + " XREF";
                            headerList = updateHeaderRow(headerList, sampleSheet, sampleLinkLabel);
                            sampleRow.createCell(headerList.indexOf(sampleLinkLabel)).setCellValue(sample_link.getXREF_LINK().getID().toString());
                        }
                    }
                }
            }
        }
    }

    private static void createObservationUnit() {
        // Core headers
        ArrayList<String> headerList = new ArrayList<>(List.of(new String[]{OBSERVATION_UNIT_IDENTIFIER, OBSERVATION_UNIT_NAME, OBSERVATION_UNIT_DESCRIPTION, STUDY_IDENTIFIER}));

        Sheet observationUnitSheet = createSheet(headerList, "ObservationUnit");

        // Build up the environment for each document
        HashSet<String> accessions = new HashSet<>();
        for (Root root : roots) {
            EXPERIMENTPACKAGE experimentPackage = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE();
            for (SAMPLE sample : experimentPackage.getSAMPLE()) {
                String accession = "X" + sample.getAccession();
                if (!accessions.contains(accession)) {
                    Row observationUnitSheetRow = observationUnitSheet.createRow(accessions.size() + 1);
                    // Set OBSERVATION_UNIT_IDENTIFIER
                    observationUnitSheetRow.createCell(headerList.indexOf(OBSERVATION_UNIT_IDENTIFIER)).setCellValue(accession);
                    //
                    observationUnitSheetRow.createCell(headerList.indexOf(OBSERVATION_UNIT_DESCRIPTION)).setCellValue("xxxx");
                    //
                    observationUnitSheetRow.createCell(headerList.indexOf(OBSERVATION_UNIT_NAME)).setCellValue("xxxx");
                    //
                    String studyAccession = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSTUDY().getAccession();
                    observationUnitSheetRow.createCell(headerList.indexOf(STUDY_IDENTIFIER)).setCellValue(studyAccession);
                    //
                    accessions.add(accession);
                }
            }
        }
    }

    private static void createStudy() {
        ArrayList<String> headerList = new ArrayList<>(List.of(new String[]{STUDY_IDENTIFIER, STUDY_TITLE, STUDY_DESCRIPTION, INVESTIGATION_IDENTIFIER, "External IDs"}));

        Sheet studySheet = createSheet(headerList, "Study");

        HashSet<String> accessions = new HashSet<>();
        for (Root root : roots) {
            STUDY STUDY = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSTUDY();
            if (!accessions.contains(STUDY.getAccession())) {
                logger.debug("Creating study " + STUDY.getAccession());
                Row studySheetRow = studySheet.createRow(accessions.size() + 1);
                studySheetRow.createCell(headerList.indexOf(STUDY_IDENTIFIER)).setCellValue(STUDY.getAccession());
                studySheetRow.createCell(headerList.indexOf(STUDY_TITLE)).setCellValue(STUDY.getDESCRIPTOR().getSTUDY_TITLE());
                studySheetRow.createCell(headerList.indexOf(STUDY_DESCRIPTION)).setCellValue(STUDY.getDESCRIPTOR().getSTUDY_ABSTRACT());
                studySheetRow.createCell(headerList.indexOf(INVESTIGATION_IDENTIFIER)).setCellValue("INV_" + STUDY.getAccession());

                headerList = updateHeaderRow(headerList, studySheet, "Center Project Name");
                studySheetRow.createCell(headerList.indexOf("Center Project Name")).setCellValue(STUDY.getDESCRIPTOR().getCENTER_PROJECT_NAME());

                headerList = updateHeaderRow(headerList, studySheet, "Center Name");
                studySheetRow.createCell(headerList.indexOf("Center Name")).setCellValue(STUDY.getCenter_name());

                headerList = updateHeaderRow(headerList, studySheet, "alias");
                studySheetRow.createCell(headerList.indexOf("alias")).setCellValue(STUDY.getAlias());

                if (STUDY.getSTUDY_ATTRIBUTES() != null) {
                    for (STUDYATTRIBUTE studyattribute : STUDY.getSTUDY_ATTRIBUTES().getSTUDY_ATTRIBUTE()) {
                        headerList = updateHeaderRow(headerList, studySheet, studyattribute.getTAG());
                        studySheetRow.createCell(headerList.indexOf(studyattribute.getTAG())).setCellValue(studyattribute.getVALUE());
                    }
                }

                ArrayList<String> identifiers = new ArrayList<>(STUDY.getIDENTIFIERS().getSECONDARY_ID());
                for (EXTERNALID externalid : STUDY.getIDENTIFIERS().getEXTERNAL_ID()) {
                    identifiers.add(externalid.getContent());
                }
                if (identifiers.size() > 0) {
                    studySheetRow.createCell(headerList.indexOf("External IDs")).setCellValue(StringUtils.join(identifiers,","));
                }

                // STUDY Links
                if (STUDY.getSTUDY_LINKS() != null) {
                    for (STUDYLINK studylink : STUDY.getSTUDY_LINKS().getSTUDY_LINK()) {
                        if (studylink.getURL_LINK() != null) {
                            String studyLinkLabel = "Study link " + studylink.getURL_LINK().getLABEL() + " URL";
                            headerList = updateHeaderRow(headerList, studySheet, studyLinkLabel);
                            studySheetRow.createCell(headerList.indexOf(studyLinkLabel)).setCellValue(studylink.getURL_LINK().getURL());
                        }

                        if (studylink.getXREF_LINK() != null) {
                            String studyLinkLabel = "Study link " + studylink.getXREF_LINK().getDB() + " XREF";
                            headerList = updateHeaderRow(headerList, studySheet, studyLinkLabel);
                            studySheetRow.createCell(headerList.indexOf(studyLinkLabel)).setCellValue(studylink.getXREF_LINK().getID().toString());
                        }
                    }
                }

                // Submission details
                SUBMISSION submission = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSUBMISSION();
                if (submission.getAccession() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission accession");
                    studySheetRow.createCell(headerList.indexOf("Submission accession")).setCellValue(submission.getAccession());
                }

                if (submission.getSubmission_date() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission date");
                    studySheetRow.createCell(headerList.indexOf("Submission date")).setCellValue(submission.getSubmission_date());
                }
                if (submission.getSubmission_comment() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission comment");
                    studySheetRow.createCell(headerList.indexOf("Submission comment")).setCellValue(submission.getSubmission_comment());
                }

                if (submission.getAlias() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission alias");
                    studySheetRow.createCell(headerList.indexOf("Submission alias")).setCellValue(submission.getAlias());
                }

                if (submission.getCenter_name() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission center name");
                    studySheetRow.createCell(headerList.indexOf("Submission center name")).setCellValue(submission.getCenter_name());
                }

                if (submission.getBroker_name() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission broker name");
                    studySheetRow.createCell(headerList.indexOf("Submission broker name")).setCellValue(submission.getBroker_name());
                }

                if (submission.getLab_name() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission lab name");
                    studySheetRow.createCell(headerList.indexOf("Submission lab name")).setCellValue(submission.getLab_name());
                }

                if (submission.getTITLE() != null) {
                    headerList = updateHeaderRow(headerList, studySheet, "Submission title");
                    studySheetRow.createCell(headerList.indexOf("Submission title")).setCellValue(submission.getTITLE());
                }

                if (submission.getSUBMISSION_ATTRIBUTES() != null) {
                    for (SUBMISSIONATTRIBUTE submissionattribute : submission.getSUBMISSION_ATTRIBUTES().getSUBMISSION_ATTRIBUTE()) {
                        headerList = updateHeaderRow(headerList, studySheet, submissionattribute.getTAG());
                        studySheetRow.createCell(headerList.indexOf(submissionattribute.getTAG())).setCellValue(submissionattribute.getVALUE());
                    }
                }
                accessions.add(STUDY.getAccession());
            }
        }
    }

    private static void createInvestigation() {
        // Core study headers
        ArrayList<String> headerList = new ArrayList<>(List.of(new String[]{INVESTIGATION_IDENTIFIER, INVESTIGATION_TITLE, INVESTIGATION_DESCRIPTION, FIRSTNAME, LASTNAME, EMAIL, ORCID, ORGANIZATION, DEPARTMENT}));
        Sheet investigationSheet = createSheet(headerList, "Investigation");

        int indexRow = investigationSheet.getLastRowNum() + 1;
        HashSet<String> processedIDs = new HashSet<>();
        for (Root root : roots) {
            Row invRow = investigationSheet.createRow(indexRow);
            // Only increment row with new investigations
            if (root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSTUDY().getIDENTIFIERS().getPRIMARY_ID().getClass() == String.class) {
                String primaryID = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getSTUDY().getIDENTIFIERS().getPRIMARY_ID().toString();
                // Get project file
                String rootFolder = "./json/";
//                try {
//                    Root projectRoot = NCBI.processProject(primaryID, rootFolder);
//                    System.err.println(projectRoot);
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
                if (processedIDs.contains(primaryID)) continue;
                invRow.createCell(headerList.indexOf(INVESTIGATION_IDENTIFIER)).setCellValue("INV_" + primaryID);
                processedIDs.add(primaryID);
                indexRow++;
            } else {
                logger.error("Primary id is not of type string?");
            }

            Organization organization = root.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getOrganization();
            headerList = updateHeaderRow(headerList, investigationSheet, "Organization type");
            invRow.createCell(headerList.indexOf("Organization type")).setCellValue(organization.getType());

            if (organization.getName().getClass() == LinkedHashMap.class) {
                LinkedHashMap<String, String> linkedHashMap = (LinkedHashMap<String, String>) organization.getName();
                for (String key : linkedHashMap.keySet()) {
                    headerList = updateHeaderRow(headerList, investigationSheet, "Organization " + key);
                    invRow.createCell(headerList.indexOf("Organization " + key)).setCellValue(linkedHashMap.get(key));
                }
            } else if (organization.getName().getClass() == String.class) {
                headerList = updateHeaderRow(headerList, investigationSheet, "Organization name");
                invRow.createCell(headerList.indexOf("Organization name")).setCellValue(organization.getName().toString());
            }

            if (organization.getAddress() != null) {
                createAddress(organization.getAddress(), headerList, investigationSheet, invRow);
            }

            // Might need to duplicate the row?
            if (organization.getContact() != null) {
                ArrayList<Contact> contacts = organization.getContact();
                // Range of row numbers
                ArrayList<Integer> rowNumbers = new ArrayList<>(contacts.size());
                for (int i = 0; i < contacts.size(); i++) {
                    rowNumbers.add(invRow.getRowNum() + i);
                    // Create the rows
                    if (i > 0) {
                        copyRow(workbook, investigationSheet, invRow.getRowNum(), invRow.getRowNum() + i);
                    }
                }

                // Write down the contact details
                for (Integer rowNumber : rowNumbers) {
                    invRow = investigationSheet.getRow(rowNumber);
                    Contact contact = organization.getContact().get(rowNumbers.indexOf(rowNumber));

                    headerList = updateHeaderRow(headerList, investigationSheet, "Email address");
                    invRow.createCell(headerList.indexOf("Email address")).setCellValue(contact.getEmail());

                    headerList = updateHeaderRow(headerList, investigationSheet, "Secondary email address");
                    invRow.createCell(headerList.indexOf("Secondary email address")).setCellValue(contact.getSec_email());

                    headerList = updateHeaderRow(headerList, investigationSheet, "Fax");
                    invRow.createCell(headerList.indexOf("Fax")).setCellValue(contact.getFax());

                    headerList = updateHeaderRow(headerList, investigationSheet, "Phone");
                    invRow.createCell(headerList.indexOf("Phone")).setCellValue(contact.getPhone());

                    if (contact.getName() != null) {
                        Name name = contact.getName();
                        headerList = updateHeaderRow(headerList, investigationSheet, "Firstname");
                        invRow.createCell(headerList.indexOf("Firstname")).setCellValue(name.getFirst());

                        headerList = updateHeaderRow(headerList, investigationSheet, "Lastname");
                        invRow.createCell(headerList.indexOf("Lastname")).setCellValue(name.getLast());
                    }

                    if (contact.getAddress() != null) {
                        createAddress(contact.getAddress(), headerList, investigationSheet, invRow);
                    }
                }
            }
        }
    }

    private static void createAddress(Address address, ArrayList<String> headerList, Sheet investigationSheet, Row invRow) {
        if (address.getCity() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "City");
            invRow.createCell(headerList.indexOf("City")).setCellValue(address.getCity());
        }
        if (address.getCountry() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Country");
            invRow.createCell(headerList.indexOf("Country")).setCellValue(address.getCountry());
        }
        if (address.getDepartment() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Department");
            invRow.createCell(headerList.indexOf("Department")).setCellValue(address.getDepartment());
        }
        if (address.getInstitution() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Institution");
            invRow.createCell(headerList.indexOf("Institution")).setCellValue(address.getInstitution());
        }
        if (address.getPostal_code() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Postal code");
            invRow.createCell(headerList.indexOf("Postal code")).setCellValue(address.getPostal_code());
        }
        if (address.getStreet() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Street");
            invRow.createCell(headerList.indexOf("Street")).setCellValue(address.getStreet());
        }
        if (address.getSub() != null) {
            headerList = updateHeaderRow(headerList, investigationSheet, "Sub");
            invRow.createCell(headerList.indexOf("Sub")).setCellValue(address.getSub());
        }
    }

    private static void copyRow(XSSFWorkbook workbook, Sheet worksheet, int sourceRowNum, int destinationRowNum) {
        // Get the source / new row
        Row newRow = worksheet.getRow(destinationRowNum);
        Row sourceRow = worksheet.getRow(sourceRowNum);

        // If the row exist in destination, push down all rows by 1 else create a new row
        if (newRow != null) {
            worksheet.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
        } else {
            newRow = worksheet.createRow(destinationRowNum);
        }

        // Loop through source columns to add to new row
        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            // Grab a copy of the old/new cell
            Cell oldCell = sourceRow.getCell(i);
            Cell newCell = newRow.createCell(i);

            // If the old cell is null jump to next cell
            if (oldCell == null) {
                continue;
            }

            // Copy style from old cell and apply to new cell
            XSSFCellStyle newCellStyle = workbook.createCellStyle();
            newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
            newCell.setCellStyle(newCellStyle);

            // If there is a cell comment, copy
            if (oldCell.getCellComment() != null) {
                newCell.setCellComment(oldCell.getCellComment());
            }

            // If there is a cell hyperlink, copy
            if (oldCell.getHyperlink() != null) {
                newCell.setHyperlink(oldCell.getHyperlink());
            }

            // Set the cell data type
            newCell.setCellType(oldCell.getCellType());

            // Set the cell data value
            switch (oldCell.getCellType()) {
                case BLANK:
                    newCell.setCellValue(oldCell.getStringCellValue());
                    break;
                case BOOLEAN:
                    newCell.setCellValue(oldCell.getBooleanCellValue());
                    break;
                case ERROR:
                    newCell.setCellErrorValue(oldCell.getErrorCellValue());
                    break;
                case FORMULA:
                    newCell.setCellFormula(oldCell.getCellFormula());
                    break;
                case NUMERIC:
                    newCell.setCellValue(oldCell.getNumericCellValue());
                    break;
                case STRING:
                    newCell.setCellValue(oldCell.getRichStringCellValue());
                    break;
            }
        }

        // If there are are any merged regions in the source row, copy to new row
        for (int i = 0; i < worksheet.getNumMergedRegions(); i++) {
            CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
            if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
                        (newRow.getRowNum() +
                                (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow()
                                )),
                        cellRangeAddress.getFirstColumn(),
                        cellRangeAddress.getLastColumn());
                worksheet.addMergedRegion(newCellRangeAddress);
            }
        }
    }

    private static void createProject() {
        // Core study headers
        ArrayList<String> headerList = new ArrayList<>(List.of(new String[]{PROJECT_IDENTIFIER, PROJECT_DESCRIPTION, PROJECT_TITLE}));
        Sheet projectSheet = createSheet(headerList, "Project");
    }

    static Sheet createSheet(ArrayList<String> headerList, String sheetName) {
        // Create sheet if it does not exist (important for assay sheets)
        if (workbook.getSheetIndex(sheetName) != -1)
            return workbook.getSheet(sheetName);

        Sheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(defaultCellWidth);

        headerList = getObligatoryColumns(sheet);
        updateHeaderRow(headerList, sheet);
        return sheet;
    }

    static ArrayList<String> updateHeaderRow(ArrayList<String> headerList, Sheet assaySheet, String variable) {
        if (!headerList.contains(variable)) {
            headerList.add(variable);
            updateHeaderRow(headerList, assaySheet);
        }
        return headerList;
    }

    static void updateHeaderRow(ArrayList<String> headerList, Sheet sheet) {
        // Create header row?
        Row sheetRow = sheet.createRow(0);

        // Creating the headers
        HashSet<String> finishedHeaderElements = new HashSet<>();
        int index = 0;
        for (int i = 0; i < headerList.size(); i++) {
            if (finishedHeaderElements.contains(headerList.get(i))) continue;
            sheetRow.createCell(index).setCellValue(headerList.get(i));
            finishedHeaderElements.add(headerList.get(i));
            index++;
        }

        // Header style
        sheetRow.cellIterator().forEachRemaining(cell -> cell.setCellStyle(headerStyle));
    }

    /**
     * To create the header identical in all sheets
     */
    public static void makeHeaderCellStyle() {
        headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        headerStyle.setFont(font);
    }

    /**
     * Header for obligatory headers
     */
    public static void makeObligatoryHeaderCellStyle() {
        obligatoryStyle = workbook.createCellStyle();
        obligatoryStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        obligatoryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        obligatoryStyle.setAlignment(HorizontalAlignment.CENTER);
        obligatoryStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        font.setColor(IndexedColors.RED.getIndex());
        obligatoryStyle.setFont(font);
    }
}
