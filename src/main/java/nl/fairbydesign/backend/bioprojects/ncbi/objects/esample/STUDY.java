package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class STUDY{
    @JsonProperty("DESCRIPTOR") 
    public DESCRIPTOR getDESCRIPTOR() {
		 return this.dESCRIPTOR; } 
    public void setDESCRIPTOR(DESCRIPTOR dESCRIPTOR) { 
		 this.dESCRIPTOR = dESCRIPTOR; } 
    DESCRIPTOR dESCRIPTOR;
    @JsonProperty("center_name") 
    public String getCenter_name() { 
		 return this.center_name; } 
    public void setCenter_name(String center_name) { 
		 this.center_name = center_name; } 
    String center_name;
    @JsonProperty("STUDY_LINKS") 
    public STUDYLINKS getSTUDY_LINKS() {
		 return this.sTUDY_LINKS; } 
    public void setSTUDY_LINKS(STUDYLINKS sTUDY_LINKS) { 
		 this.sTUDY_LINKS = sTUDY_LINKS; } 
    STUDYLINKS sTUDY_LINKS;
    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name; }
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name; }
    String broker_name;
    @JsonProperty("alias")
    public String getAlias() { 
		 return this.alias; } 
    public void setAlias(String alias) { 
		 this.alias = alias; } 
    String alias;
    @JsonProperty("IDENTIFIERS") 
    public IDENTIFIERS getIDENTIFIERS() {
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) { 
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("STUDY_ATTRIBUTES")
    public STUDYATTRIBUTES getSTUDY_ATTRIBUTES() {
        return this.sTUDY_ATTRIBUTES; }
    public void setSTUDY_ATTRIBUTES(STUDYATTRIBUTES sTUDY_ATTRIBUTES) {
        this.sTUDY_ATTRIBUTES = sTUDY_ATTRIBUTES; }
    STUDYATTRIBUTES sTUDY_ATTRIBUTES;

    @JsonProperty("xmlns:xsi")
    public String xmlnsXsi;
}
