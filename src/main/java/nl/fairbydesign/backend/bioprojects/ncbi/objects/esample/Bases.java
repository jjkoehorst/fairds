package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Bases{
    @JsonProperty("cs_native")
    public boolean getCs_native() { 
		 return this.cs_native; } 
    public void setCs_native(boolean cs_native) { 
		 this.cs_native = cs_native; } 
    boolean cs_native;
    @JsonProperty("count") 
    public Object getCount() { 
		 return this.count; } 
    public void setCount(Object count) { 
		 this.count = count; } 
    Object count;
    @JsonProperty("Base") 
    public ArrayList<Base> getBase() {
		 return this.base; } 
    public void setBase(ArrayList<Base> base) { 
		 this.base = base; } 
    ArrayList<Base> base;
}
