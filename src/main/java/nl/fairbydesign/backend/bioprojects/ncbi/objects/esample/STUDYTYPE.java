package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class STUDYTYPE{
    @JsonProperty("existing_study_type")
    public String getExisting_study_type() { 
		 return this.existing_study_type; } 
    public void setExisting_study_type(String existing_study_type) { 
		 this.existing_study_type = existing_study_type; } 
    String existing_study_type;
}
