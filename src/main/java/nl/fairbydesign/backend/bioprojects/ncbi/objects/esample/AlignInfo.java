package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class AlignInfo {
    @JsonProperty("path")
    public String getPath() {
        return this.path; }
    public void setPath(String path) {
        this.path = path; }
    String path;
    @JsonProperty("Alignment")
    public ArrayList<Alignment> getAlignment() {
        return this.alignment; }
    public void setAlignment(ArrayList<Alignment> alignment) {
        this.alignment = alignment; }
    ArrayList<Alignment> alignment;
    @JsonProperty("cnt")
    public int getCnt() {
        return this.cnt; }
    public void setCnt(int cnt) {
        this.cnt = cnt; }
    int cnt;
}