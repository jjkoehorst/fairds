package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SUBMISSIONATTRIBUTE {
    @JsonProperty("TAG")
    public String getTAG() {
        return this.tAG; }
    public void setTAG(String tAG) {
        this.tAG = tAG; }
    String tAG;
    @JsonProperty("VALUE")
    public String getVALUE() {
        return this.vALUE; }
    public void setVALUE(String vALUE) {
        this.vALUE = vALUE; }
    String vALUE;
}
