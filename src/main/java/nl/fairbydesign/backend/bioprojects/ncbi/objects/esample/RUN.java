package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class RUN {
    @JsonProperty("filtered_data_available")
    public boolean getFiltered_data_available() {
        return this.filtered_data_available; }
    public void setFiltered_data_available(boolean filtered_data_available) {
        this.filtered_data_available = filtered_data_available; }
    boolean filtered_data_available;

    @JsonProperty("EXPERIMENT_REF")
    public EXPERIMENTREF getEXPERIMENT_REF() {
        return this.eXPERIMENT_REF;}
    public void setEXPERIMENT_REF(EXPERIMENTREF eXPERIMENT_REF) {
        this.eXPERIMENT_REF = eXPERIMENT_REF;}
    EXPERIMENTREF eXPERIMENT_REF;

    @JsonProperty("cluster_name")
    public String getCluster_name() {
        return this.cluster_name;}
    public void setCluster_name(String cluster_name) {
        this.cluster_name = cluster_name;}
    String cluster_name;

    @JsonProperty("CloudFiles")
    public CloudFiles getCloudFiles() {
        return this.cloudFiles;}
    public void setCloudFiles(CloudFiles cloudFiles) {
        this.cloudFiles = cloudFiles;}
    CloudFiles cloudFiles;

    @JsonProperty("static_data_available")
    public int getStatic_data_available() {
        return this.static_data_available;}
    public void setStatic_data_available(int static_data_available) {
        this.static_data_available = static_data_available;}
    int static_data_available;

    @JsonProperty("SRAFiles")
    public SRAFiles getSRAFiles() {
        return this.sRAFiles;}
    public void setSRAFiles(SRAFiles sRAFiles) {
        this.sRAFiles = sRAFiles;}
    SRAFiles sRAFiles;

    @JsonProperty("total_spots")
    public long getTotal_spots() {
        return this.total_spots;}
    public void setTotal_spots(long total_spots) {
        this.total_spots = total_spots;}
    long total_spots;

    @JsonProperty("xmlns:xsi")
    public String getXmlnsXsi() {
        return this.xmlnsXsi;}
    public void setXmlnsXsi(String xmlnsXsi) {
        this.xmlnsXsi = xmlnsXsi;}
    String xmlnsXsi;

    @JsonProperty("broker_name")
    public String getBroker_name() {
        return this.broker_name;}
    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name;}
    String broker_name;

    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS;}
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS;}
    IDENTIFIERS iDENTIFIERS;

    @JsonProperty("PROCESSING")
    public PROCESSING getPROCESSING() {
        return this.pROCESSING;}
    public void setPROCESSING(PROCESSING pROCESSING) {
        this.pROCESSING = pROCESSING;}
    PROCESSING pROCESSING;

    @JsonProperty("accession")
    public String getAccession() {
        return this.accession;}
    public void setAccession(String accession) {
        this.accession = accession;}
    String accession;

    @JsonProperty("published")
    public String getPublished() {
        return this.published;}
    public void setPublished(String published) {
        this.published = published;}
    String published;

    @JsonProperty("Statistics")
    public Statistics getStatistics() {
        return this.statistics;}
    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;}
    Statistics statistics;

    @JsonProperty("Bases")
    public Bases getBases() {
        return this.bases;}
    public void setBases(Bases bases) {
        this.bases = bases;}
    Bases bases;

    @JsonProperty("xmlns")
    public String getXmlns() {
        return this.xmlns;}
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;}
    String xmlns;

    @JsonProperty("center_name")
    public String getCenter_name() {
        return this.center_name;}
    public void setCenter_name(String center_name) {
        this.center_name = center_name;}
    String center_name;

    @JsonProperty("size")
    public Object getSize() {
        return this.size;}
    public void setSize(Object size) {
        this.size = size;}
    Object size;

    @JsonProperty("load_done")
    public boolean getLoad_done() {
        return this.load_done;}
    public void setLoad_done(boolean load_done) {
        this.load_done = load_done;}
    boolean load_done;

    @JsonProperty("is_public")
    public boolean getIs_public() {
        return this.is_public;}
    public void setIs_public(boolean is_public) {
        this.is_public = is_public;}
    boolean is_public;

    @JsonProperty("alias")
    public String getAlias() {
        return this.alias;}
    public void setAlias(String alias) {
        this.alias = alias;}
    String alias;

    @JsonProperty("total_bases")
    public Object getTotal_bases() {
        return this.total_bases;}
    public void setTotal_bases(Object total_bases) {
        this.total_bases = total_bases;}
    Object total_bases;

    @JsonProperty("POOL")
    public POOL getPool() {
        return this.pool;}
    public void setPool(POOL pool) {
        this.pool = pool;}
    POOL pool;

    @JsonProperty("RUN_ATTRIBUTES")
    public RUNATTRIBUTES getRUN_ATTRIBUTES() {
        return this.rUN_ATTRIBUTES;}
    public void setRUN_ATTRIBUTES(RUNATTRIBUTES rUN_ATTRIBUTES) {
        this.rUN_ATTRIBUTES = rUN_ATTRIBUTES;}
    RUNATTRIBUTES rUN_ATTRIBUTES;

    @JsonProperty("Databases")
    public Databases getDatabases() {
        return this.databases;}
    public void setDatabases(Databases databases) {
        this.databases = databases;}
    Databases databases;

    @JsonProperty("TITLE")
    public String getTITLE() {
        return this.tITLE;}
    public void setTITLE(String tITLE) {
        this.tITLE = tITLE;}
    String tITLE;

    @JsonProperty("run_center")
    public String getRun_center() {
        return this.run_center;}
    public void setRun_center(String run_center) {
        this.run_center = run_center;}
    String run_center;

    @JsonProperty("run_date")
    public Date getRun_date() {
        return this.run_date;}
    public void setRun_date(Date run_date) {
        this.run_date = run_date;}
    Date run_date;

    @JsonProperty("unavailable")
    public boolean getUnavailable() {
        return this.unavailable; }
    public void setUnavailable(boolean unavailable) {
        this.unavailable = unavailable; }
    boolean unavailable;

    @JsonProperty("AlignInfo")
    public AlignInfo getAlignInfo() {
        return this.alignInfo; }
    public void setAlignInfo(AlignInfo alignInfo) {
        this.alignInfo = alignInfo; }
    AlignInfo alignInfo;

    @JsonProperty("assembly")
    public String getAssembly() {
        return this.assembly; }
    public void setAssembly(String assembly) {
        this.assembly = assembly; }
    String assembly;

    @JsonProperty("has_taxanalysis")
    public String getHas_taxanalysis() {
        return this.has_taxanalysis; }
    public void setHas_taxanalysis(String has_taxanalysis) {
        this.has_taxanalysis = has_taxanalysis; }
    String has_taxanalysis;


}


