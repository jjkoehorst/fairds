package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class RUNSET{
    @JsonProperty("RUN")
    public ArrayList<RUN> getRUN() {
        return this.rUN; }
    public void setRUN(ArrayList<RUN> rUN) {
        this.rUN = rUN; }
    ArrayList<RUN> rUN = new ArrayList<>();

    @JsonProperty("bases")
    public int getBases() {
        return this.bases; }
    public void setBases(int bases) {
        this.bases = bases; }
    int bases;
    @JsonProperty("bytes")
    public int getBytes() {
        return this.bytes; }
    public void setBytes(int bytes) {
        this.bytes = bytes; }
    int bytes;
    @JsonProperty("spots")
    public int getSpots() {
        return this.spots; }
    public void setSpots(int spots) {
        this.spots = spots; }
    int spots;
    @JsonProperty("runs")
    public int getRuns() {
        return this.runs; }
    public void setRuns(int runs) {
        this.runs = runs; }
    int runs;
}