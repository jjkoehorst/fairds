package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class EXPERIMENTATTRIBUTES {
    @JsonProperty("EXPERIMENT_ATTRIBUTE")
    public ArrayList<EXPERIMENTATTRIBUTE> getEXPERIMENT_ATTRIBUTE() {
        return this.eXPERIMENT_ATTRIBUTE; }
    public void setEXPERIMENT_ATTRIBUTE(ArrayList<EXPERIMENTATTRIBUTE> eXPERIMENT_ATTRIBUTE) {
        this.eXPERIMENT_ATTRIBUTE = eXPERIMENT_ATTRIBUTE; }
    ArrayList<EXPERIMENTATTRIBUTE> eXPERIMENT_ATTRIBUTE;
}
