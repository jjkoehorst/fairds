package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LIBRARYDESCRIPTOR{
    @JsonProperty("LIBRARY_SOURCE") 
    public String getLIBRARY_SOURCE() { 
		 return this.lIBRARY_SOURCE; } 
    public void setLIBRARY_SOURCE(String lIBRARY_SOURCE) { 
		 this.lIBRARY_SOURCE = lIBRARY_SOURCE; } 
    String lIBRARY_SOURCE;
    @JsonProperty("LIBRARY_STRATEGY")
    public String getLIBRARY_STRATEGY() { 
		 return this.lIBRARY_STRATEGY; } 
    public void setLIBRARY_STRATEGY(String lIBRARY_STRATEGY) { 
		 this.lIBRARY_STRATEGY = lIBRARY_STRATEGY; } 
    String lIBRARY_STRATEGY;
    @JsonProperty("LIBRARY_LAYOUT") 
    public LIBRARYLAYOUT getLIBRARY_LAYOUT() { 
		 return this.lIBRARY_LAYOUT; } 
    public void setLIBRARY_LAYOUT(LIBRARYLAYOUT lIBRARY_LAYOUT) { 
		 this.lIBRARY_LAYOUT = lIBRARY_LAYOUT; } 
    LIBRARYLAYOUT lIBRARY_LAYOUT;
    @JsonProperty("LIBRARY_NAME") 
    public String getLIBRARY_NAME() { 
		 return this.lIBRARY_NAME; } 
    public void setLIBRARY_NAME(String lIBRARY_NAME) { 
		 this.lIBRARY_NAME = lIBRARY_NAME; } 
    String lIBRARY_NAME;
    @JsonProperty("LIBRARY_SELECTION") 
    public String getLIBRARY_SELECTION() { 
		 return this.lIBRARY_SELECTION; } 
    public void setLIBRARY_SELECTION(String lIBRARY_SELECTION) { 
		 this.lIBRARY_SELECTION = lIBRARY_SELECTION; } 
    String lIBRARY_SELECTION;

    @JsonProperty("LIBRARY_CONSTRUCTION_PROTOCOL")
    public String getLIBRARY_CONSTRUCTION_PROTOCOL() {
        return this.lIBRARY_CONSTRUCTION_PROTOCOL; }
    public void setLIBRARY_CONSTRUCTION_PROTOCOL(String lIBRARY_CONSTRUCTION_PROTOCOL) {
        this.lIBRARY_CONSTRUCTION_PROTOCOL = lIBRARY_CONSTRUCTION_PROTOCOL; }
    String lIBRARY_CONSTRUCTION_PROTOCOL;

    @JsonProperty("TARGETED_LOCI")
    public TARGETEDLOCI getTARGETED_LOCI() {
        return this.tARGETED_LOCI; }
    public void setTARGETED_LOCI(TARGETEDLOCI tARGETED_LOCI) {
        this.tARGETED_LOCI = tARGETED_LOCI; }
    TARGETEDLOCI tARGETED_LOCI;

}
