package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Name{
    @JsonProperty("Last")
    public String getLast() { 
		 return this.last; } 
    public void setLast(String last) { 
		 this.last = last; } 
    String last;
    @JsonProperty("Middle")
    public String getMiddle() {
        return this.middle; }
    public void setMiddle(String middle) {
        this.middle = middle; }
    String middle;
    @JsonProperty("First") 
    public String getFirst() { 
		 return this.first; } 
    public void setFirst(String first) { 
		 this.first = first; } 
    String first;
}
