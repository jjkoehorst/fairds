package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class CloudFiles{
    @JsonProperty("CloudFile")
    public ArrayList<CloudFile> getCloudFile() {
		 return this.cloudFile; } 
    public void setCloudFile(ArrayList<CloudFile> cloudFile) { 
		 this.cloudFile = cloudFile; } 
    ArrayList<CloudFile> cloudFile;
}
