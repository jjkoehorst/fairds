package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Organization{
    @JsonProperty("org_id")
    public int getOrg_id() {
        return this.org_id; }
    public void setOrg_id(int org_id) {
        this.org_id = org_id; }
    int org_id;
    @JsonProperty("type")
    public String getType() { 
		 return this.type; } 
    public void setType(String type) { 
		 this.type = type; } 
    String type;
    @JsonProperty("Name") 
    public Object getName() { 
		 return this.name; } 
    public void setName(Object name) { 
		 this.name = name; } 
    Object name;
    @JsonProperty("Contact") 
    public ArrayList<Contact> getContact() {
		 return this.contact; } 
    public void setContact(ArrayList<Contact> contact) {
		 this.contact = contact; }
    ArrayList<Contact> contact;
    @JsonProperty("Address") 
    public Address getAddress() { 
		 return this.address; } 
    public void setAddress(Address address) { 
		 this.address = address; } 
    Address address;
    @JsonProperty("url") 
    public String getUrl() { 
		 return this.url; } 
    public void setUrl(String url) { 
		 this.url = url; } 
    String url;
    @JsonProperty("xmlns:xsi")
    public String getXmlnsXsi() {
        return this.xmlnsXsi; }
    public void setXmlnsXsi(String xmlnsXsi) {
        this.xmlnsXsi = xmlnsXsi; }
    String xmlnsXsi;
}
