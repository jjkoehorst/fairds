package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class POOL {
    @JsonProperty("DEFAULT_MEMBER")
    public DEFAULTMEMBER getDEFAULT_MEMBER() {
        return this.dEFAULT_MEMBER; }
    public void setDEFAULT_MEMBER(DEFAULTMEMBER dEFAULT_MEMBER) {
        this.dEFAULT_MEMBER = dEFAULT_MEMBER; }
    DEFAULTMEMBER dEFAULT_MEMBER;
    @JsonProperty("MEMBER")
    public ArrayList<MEMBER> getMEMBER() {
        return this.mEMBER; }
    public void setMEMBER(ArrayList<MEMBER> mEMBER) {
        this.mEMBER = mEMBER; }
    ArrayList<MEMBER> mEMBER;
}
