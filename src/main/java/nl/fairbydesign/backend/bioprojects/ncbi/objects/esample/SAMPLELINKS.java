package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SAMPLELINKS{
    @JsonProperty("SAMPLE_LINK")
    public ArrayList<SAMPLELINK> getSAMPLE_LINK() {
		 return this.sAMPLE_LINK; } 
    public void setSAMPLE_LINK(ArrayList<SAMPLELINK> sAMPLE_LINK) {
		 this.sAMPLE_LINK = sAMPLE_LINK; }
    ArrayList<SAMPLELINK> sAMPLE_LINK = new ArrayList<>();
}
