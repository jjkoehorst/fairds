package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class IDENTIFIERS{
    @JsonProperty("PRIMARY_ID")
    public Object getPRIMARY_ID() {
        return this.pRIMARY_ID; }
    public void setPRIMARY_ID(Object pRIMARY_ID) {
        this.pRIMARY_ID = pRIMARY_ID; }
    Object pRIMARY_ID;

    @JsonProperty("SUBMITTER_ID")
    public Object getSUBMITTER_ID() {
        return this.sUBMITTER_ID; }
    public void setSUBMITTER_ID(Object sUBMITTER_ID) {
        this.sUBMITTER_ID = sUBMITTER_ID; }
    Object sUBMITTER_ID;

    @JsonProperty("SECONDARY_ID")
    public ArrayList<String> getSECONDARY_ID() {
        return this.sECONDARY_ID; }
    public void setSECONDARY_ID(ArrayList<String> sECONDARY_ID) {
        this.sECONDARY_ID = sECONDARY_ID; }
    ArrayList<String> sECONDARY_ID = new ArrayList<>();
    @JsonProperty("EXTERNAL_ID")
    public ArrayList<EXTERNALID> getEXTERNAL_ID() {
		 return this.eXTERNAL_ID; } 
    public void setEXTERNAL_ID(ArrayList<EXTERNALID> eXTERNAL_ID) {
		 this.eXTERNAL_ID = eXTERNAL_ID; }
    ArrayList<EXTERNALID> eXTERNAL_ID;
    @JsonProperty("UUID")
    public String getUUID() {
        return this.uUID; }
    public void setUUID(String uUID) {
        this.uUID = uUID; }
    String uUID;
}
