package nl.fairbydesign.backend.bioprojects.ncbi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fairbydesign.backend.bioprojects.ncbi.objects.esample.Root;
import org.jboss.logging.Logger;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class NCBI {
    public static final Logger logger = Logger.getLogger(NCBI.class);
    public static HashSet<Root> roots = new HashSet<>();
    public XLSX xlsx = new XLSX();
//    public int progress = 0;

    public void fetch(HashSet<String> bioprojects) throws IOException {
        fetch(bioprojects, null);
    }

    public void fetch(HashSet<String> bioprojects, String excelFileName) throws IOException {
        logger.info("Running fetch");
        // Folder structure
        String rootFolder = "./json/";
        new File(rootFolder).mkdirs();

        ArrayList<String> bioprojectsList = new ArrayList<>(bioprojects);
        for (String bioprojectID : bioprojectsList) {
            ArrayList<String> ids = searchSamples(rootFolder, bioprojectID);
            // Skip ebi obtained data for now
            if (!ids.get(0).startsWith("ebi")) {
                processSamples(ids, rootFolder);
            }
            processProject(bioprojectID, rootFolder);
        }
        if (excelFileName != null) {
            logger.info("Running excel generation");
            xlsx.setup(roots, excelFileName);
        }
    }

    public void processSamples(String id, String rootFolder) throws IOException {
        ArrayList<String> ids = new ArrayList<>();
        ids.add(id);
        processSamples(ids, rootFolder);
    }

    public void processSamples(ArrayList<String> ids, String rootFolder) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();

        for (int i = 0; i < ids.size(); i++) {
            if (i % 10 == 0 && i > 0) {
                logger.info("Processed " + i + " of " + ids.size() + " ids");
            }
            String id = ids.get(i);
            File path = makePath(rootFolder + "/fetch/", id);
            File outputFile = new File(path + "/" + id + ".json");
            // Download the json sample/data file
            String jsonString;
            if (!outputFile.exists()) {
                jsonString = search("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=sra&id=" + id + "&rettype=xml&retmode=text&tool=fairds&email=contact.unlock%40wur.nl");
                PrintWriter printWriter = new PrintWriter(outputFile);
                printWriter.println(jsonString);
                printWriter.close();
            }
            jsonString = Files.readString(outputFile.toPath());

            // For validation purposes
            try {
                if (jsonString.strip().length() > 1) {
                    Root sampleRoot = objectMapper.readValue(jsonString, Root.class);
                    // Skipping weird samples
                    if (sampleRoot.getEXPERIMENT_PACKAGE_SET().getEXPERIMENT_PACKAGE().getRUN_SET() == null) {
                        logger.error("Skipping " + path + " due to incomplete mapping");
                        continue;
                    }
                    roots.add(sampleRoot);
                } else {
                    logger.error("Empty json file detected");
                }
            } catch (IOException e) {
                e.printStackTrace();
                PrintWriter printWriter = new PrintWriter("failed.json");
                printWriter.println(jsonString);
                printWriter.close();
                System.err.println("Json processing failed for " + id);
            }
        }
    }

    private static ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        // Skips unknown properties
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        objectMapper.configure(JsonParser.Feature.IGNORE_UNDEFINED, true);
        objectMapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return objectMapper;
    }

    public ArrayList<String> searchSamples(String rootFolder, String term) throws IOException {
        File path = makePath(rootFolder + "/search/", term);
        File outputFile = new File(path + "/" + term + ".json");
        File ebiOutputFile = new File(makePath(rootFolder + "/ebi/search/", term) + "/" + term + ".json");

        // EBI output files cannot yet be mapped to NCBI SRA metadata files
        if (ebiOutputFile.exists()) {
            return searchEBISamples(rootFolder, term);
        }

        // Download the json sample/data file if there is no EBI search file
        if (!outputFile.exists()) {
            String jsonString = search("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=sra&term=" + term + "&retmax=3000000&tool=fairds&email=contact.unlock%40wur.nl");
            PrintWriter printWriter = new PrintWriter(outputFile);
            printWriter.println(jsonString);
            printWriter.close();
        }

        String jsonString = Files.readString(outputFile.toPath());

        // TODO multiple type mapper for IdList, string or list of integers
        if (jsonString.contains("IdList\": \"\"")) {
            logger.info("Nothing found for " + term);
            // Try EBI?
            return searchEBISamples(rootFolder, term);
        } else {
            logger.info("Obtaining results for " + term);
        }

        // JSON string mapper to object with several configuration options
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch.Root root = objectMapper.readValue(jsonString, nl.fairbydesign.backend.bioprojects.ncbi.objects.esearch.Root.class);
        ArrayList<String> ids = root.getESearchResult().getIdList().getId();
        logger.info("Obtaining " + ids.size() + " ids");
        return ids;
    }

    private ArrayList<String> searchEBISamples(String rootFolder, String term) throws IOException {
        File path = makePath(rootFolder + "/ebi/search/", term);
        File outputFile = new File(path + "/" + term + ".json");

        // Download the json sample/data file
        if (!outputFile.exists()) {
            String jsonString = search("https://www.ebi.ac.uk/ena/portal/api/filereport?accession=" + term + "&result=analysis&fields=study_accession,secondary_study_accession,sample_accession,secondary_sample_accession,analysis_accession,sample_alias&format=json&download=true&limit=0");
            PrintWriter printWriter = new PrintWriter(outputFile);
            printWriter.println(jsonString);
            printWriter.close();
        }

        String jsonString = Files.readString(outputFile.toPath());
        if (jsonString.strip().length() == 0) {
            return new ArrayList<>(List.of("ebi"));
        }
        // JSON string mapper to object with several configuration options
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        // To be able to map a list to the root
        TypeReference<List<nl.fairbydesign.backend.bioprojects.ebi.objects.esearch.Root>> mapType = new TypeReference<>() {};
        List<nl.fairbydesign.backend.bioprojects.ebi.objects.esearch.Root> root = objectMapper.readValue(jsonString, mapType);
        ArrayList<String> samples = new ArrayList<>();
        // Marker for ebi search which is unfinished at this moment
        samples.add("ebi");
        for (nl.fairbydesign.backend.bioprojects.ebi.objects.esearch.Root element : root) {
            samples.add(element.secondary_sample_accession);
        }
        return samples;
    }

    public static File makePath(String rootFolder, String id) {
        StringBuilder rootFolderBuilder = new StringBuilder(rootFolder);
        int length = Integer.parseInt(String.valueOf(id.toCharArray().length / 4.0).split("\\.")[0]);

        for (int i = 0; i < id.toCharArray().length; i++) {
            if (i % 4 == 0 && i > 0) {
                rootFolderBuilder.append("/");
                new File(rootFolderBuilder.toString()).mkdirs();
                return new File(rootFolderBuilder.toString());
            }
            rootFolderBuilder.append(id.toCharArray()[i]);
        }
        new File(rootFolderBuilder.toString()).mkdirs();
        return new File(rootFolderBuilder.toString());
//        new File(rootFolderBuilder.toString()).mkdirs();
//        return new File(rootFolderBuilder.toString());
    }

    public static String search(String urlString) {
        int attempt = 0;
        while (attempt < 5) {
            try {
                URL url = new URL(urlString);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestProperty("Accept", "*/*");

                // System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
                InputStream inputStream = http.getInputStream();
                String contents = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

                // Convert to json object as the json mapper works better
                String jsonString;
                if (!url.getHost().contains("ebi")) {
                    JSONObject json = XML.toJSONObject(contents);
                    jsonString = json.toString(4);
                } else {
                    jsonString = contents;
                }

                http.disconnect();

                return jsonString;
            } catch (IOException e) {
                logger.error("Failed to fetch " + urlString);
                attempt = attempt + 1;
            }
        }
        return "";
    }

    public static void processProject(String bioproject, String rootFolder) throws IOException {
        String id = bioproject;
        File path = makePath(rootFolder + "/fetch/", id);
        File outputFile = new File(path + "/" + id + ".json");
        // Download the json sample/data file
        String jsonString;
        if (!outputFile.exists()) {
            jsonString = search("https://www.ebi.ac.uk/ena/browser/api/xml/" + id);
            PrintWriter printWriter = new PrintWriter(outputFile);
            printWriter.println(jsonString);
            printWriter.close();
        }
        JSONObject json = XML.toJSONObject(Files.readString(outputFile.toPath()));
        jsonString = json.toString(4);
        ObjectMapper objectMapper = getObjectMapper();
        // Load check
        Root projectRoot = objectMapper.readValue(jsonString, Root.class);
    }
}
