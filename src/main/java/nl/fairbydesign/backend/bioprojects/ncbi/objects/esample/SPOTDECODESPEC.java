package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SPOTDECODESPEC{
    @JsonProperty("SPOT_LENGTH")
    public int getSPOT_LENGTH() { 
		 return this.sPOT_LENGTH; } 
    public void setSPOT_LENGTH(int sPOT_LENGTH) { 
		 this.sPOT_LENGTH = sPOT_LENGTH; } 
    int sPOT_LENGTH;
    @JsonProperty("READ_SPEC") 
    public ArrayList<READSPEC> getREAD_SPEC() {
		 return this.rEAD_SPEC; } 
    public void setREAD_SPEC(ArrayList<READSPEC> rEAD_SPEC) { 
		 this.rEAD_SPEC = rEAD_SPEC; } 
    ArrayList<READSPEC> rEAD_SPEC;
}
