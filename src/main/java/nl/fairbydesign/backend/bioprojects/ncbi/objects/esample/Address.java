package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address{
    @JsonProperty("Department") 
    public String getDepartment() { 
		 return this.department; } 
    public void setDepartment(String department) { 
		 this.department = department; } 
    String department;
    @JsonProperty("Street")
    public String getStreet() { 
		 return this.street; } 
    public void setStreet(String street) { 
		 this.street = street; } 
    String street;
    @JsonProperty("Country") 
    public String getCountry() { 
		 return this.country; } 
    public void setCountry(String country) { 
		 this.country = country; } 
    String country;
    @JsonProperty("Institution") 
    public String getInstitution() { 
		 return this.institution; } 
    public void setInstitution(String institution) { 
		 this.institution = institution; } 
    String institution;
    @JsonProperty("City") 
    public String getCity() { 
		 return this.city; } 
    public void setCity(String city) { 
		 this.city = city; } 
    String city;
    @JsonProperty("postal_code")
    public String getPostal_code() {
        return this.postal_code; }
    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code; }
    String postal_code;
    @JsonProperty("Sub")
    public String getSub() {
        return this.sub; }
    public void setSub(String sub) {
        this.sub = sub; }
    String sub;

}
