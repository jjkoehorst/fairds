package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BASECALL {
    @JsonProperty("match_edge")
    public String getMatch_edge() {
        return this.match_edge; }
    public void setMatch_edge(String match_edge) {
        this.match_edge = match_edge; }
    String match_edge;
    @JsonProperty("min_match")
    public int getMin_match() {
        return this.min_match; }
    public void setMin_match(int min_match) {
        this.min_match = min_match; }
    int min_match;
    @JsonProperty("content")
    public String getContent() {
        return this.content; }
    public void setContent(String content) {
        this.content = content; }
    String content;
    @JsonProperty("max_mismatch")
    public int getMax_mismatch() {
        return this.max_mismatch; }
    public void setMax_mismatch(int max_mismatch) {
        this.max_mismatch = max_mismatch; }
    int max_mismatch;
    @JsonProperty("read_group_tag")
    public String getRead_group_tag() {
        return this.read_group_tag; }
    public void setRead_group_tag(String read_group_tag) {
        this.read_group_tag = read_group_tag; }
    String read_group_tag;
}
