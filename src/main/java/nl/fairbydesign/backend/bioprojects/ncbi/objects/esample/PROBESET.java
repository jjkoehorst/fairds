package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROBESET {
    @JsonProperty("ID")
    public String getID() {
        return this.iD; }
    public void setID(String iD) {
        this.iD = iD; }
    String iD;
    @JsonProperty("DB")
    public String getDB() {
        return this.dB; }
    public void setDB(String dB) {
        this.dB = dB; }
    String dB;
}
