package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CloudFile{
    @JsonProperty("filetype")
    public String getFiletype() { 
		 return this.filetype; } 
    public void setFiletype(String filetype) { 
		 this.filetype = filetype; } 
    String filetype;
    @JsonProperty("provider") 
    public String getProvider() { 
		 return this.provider; } 
    public void setProvider(String provider) { 
		 this.provider = provider; } 
    String provider;
    @JsonProperty("location") 
    public String getLocation() { 
		 return this.location; } 
    public void setLocation(String location) { 
		 this.location = location; } 
    String location;
}
