package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Database{
    @JsonProperty("Table")
    public ArrayList<Table> getTable() {
        return this.table; }
    public void setTable(ArrayList<Table> table) {
        this.table = table; }
    ArrayList<Table> table;
}