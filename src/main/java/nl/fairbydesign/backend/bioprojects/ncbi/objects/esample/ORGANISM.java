package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ORGANISM{
    @JsonProperty("TAXON_ID")
    public int getTAXON_ID() {
        return this.tAXON_ID; }
    public void setTAXON_ID(int tAXON_ID) {
        this.tAXON_ID = tAXON_ID; }
    int tAXON_ID;
    @JsonProperty("SCIENTIFIC_NAME")
    public String getSCIENTIFIC_NAME() {
        return this.sCIENTIFIC_NAME; }
    public void setSCIENTIFIC_NAME(String sCIENTIFIC_NAME) {
        this.sCIENTIFIC_NAME = sCIENTIFIC_NAME; }
    String sCIENTIFIC_NAME;
}