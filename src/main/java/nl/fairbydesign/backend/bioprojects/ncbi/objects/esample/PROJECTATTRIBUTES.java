package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PROJECTATTRIBUTES {
    @JsonProperty("TAG")
    public String getTAG() {
        return this.tAG; }
    public void setTAG(String tAG) {
        this.tAG = tAG; }
    String tAG;
    @JsonProperty("VALUE")
    public Object getVALUE() {
        return this.vALUE; }
    public void setVALUE(Object vALUE) {
        this.vALUE = vALUE; }
    Object vALUE;
}
