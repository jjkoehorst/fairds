package nl.fairbydesign.backend.bioprojects.ncbi.objects.esample;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SUBMISSIONPROJECT {
    @JsonProperty("SEQUENCING_PROJECT")
    public String getSEQUENCING_PROJECT() {
        return this.sEQUENCING_PROJECT; }
    public void setSEQUENCING_PROJECT(String sEQUENCING_PROJECT) {
        this.sEQUENCING_PROJECT = sEQUENCING_PROJECT; }
    String sEQUENCING_PROJECT;
    @JsonProperty("ORGANISM")
    public ORGANISM getORGANISM() {
        return this.oRGANISM; }
    public void setORGANISM(ORGANISM oRGANISM) {
        this.oRGANISM = oRGANISM; }
    ORGANISM oRGANISM;
}
