package nl.fairbydesign.backend;

/**
 * Config object containing all configurations needed for this program
 */
public class Config {
    /*
    Default SETTINGS
     */

    // Project folder location for localhost usage (to be implemented)
    private String project;
    // Raw data folder location for localhost usage (to be implemented)
    private String data;

    // Metadata excel file location
    private String metadata;
    // Debug value should overwrite the application.properties
    private boolean debug = false;
    // Prefix for metadata conversion to RDF
    private String prefix;

    /*
    IRODS Configuration settings
     */

    // IRODS admin user
    private String IRODSAdminUser;
    private String IRODSAdminPassword;
    // Settings
    private String IRODSHost;
    private String IRODSZone = "tempZone";
    private int IRODSPort = 1247;
    // SSL Negotiation Policy
    private String IRODSSSLNegotiationPolicy = "CS_NEG_REFUSE";
    // Authentication schema
    private String IRODSAuthScheme = "STANDARD";
    // Certificate trustStore and password
    private String IRODScertificateTrustStore;
    private String IRODScertificateTrustStorePassword;

    /*
    METHODS
     */

    public void setIRODSAdminUser(String irodsAdminUser) {
        this.IRODSAdminUser = irodsAdminUser;
    }

    public String getIRODSAdminUser() {
        return IRODSAdminUser;
    }

    public void setIRODSHost(String irodsHost) {
        this.IRODSHost = irodsHost;
    }

    public String getIRODSHost() {
        return IRODSHost;
    }

    public void setIRODSZone(String irodsZone) {
        this.IRODSZone = irodsZone;
    }

    public String getIRODSZone() {
        return IRODSZone;
    }

    public void setIRODSPort(int irodsPort) {
        this.IRODSPort = irodsPort;
    }

    public int getIRODSPort() {
        return IRODSPort;
    }

    public void setIRODSSSLNegotiationPolicy(String irodsAuthentication) {
        this.IRODSSSLNegotiationPolicy = irodsAuthentication;
    }

    public String getIRODSSSLNegotiationPolicy() {
        return IRODSSSLNegotiationPolicy;
    }

    public void setIRODSAdminPassword(String irodsAdminPassword) {
        this.IRODSAdminPassword = irodsAdminPassword;
    }

    public String getIRODSAdminPassword() {
        return IRODSAdminPassword;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public boolean getDebug() {
        return debug;
    }

    public String getIRODSAuthScheme() {
        return IRODSAuthScheme;
    }

    public void setIRODSAuthScheme(String IRODSAuthScheme) {
        this.IRODSAuthScheme = IRODSAuthScheme;
    }

    public String getIRODScertificateTrustStore() {
        return IRODScertificateTrustStore;
    }

    public void setIRODScertificateTrustStore(String IRODScertificateTrustStore) {
        this.IRODScertificateTrustStore = IRODScertificateTrustStore;
    }

    public String getIRODScertificateTrustStorePassword() {
        return IRODScertificateTrustStorePassword;
    }

    public void setIRODScertificateTrustStorePassword(String IRODScertificateTrustStorePassword) {
        this.IRODScertificateTrustStorePassword = IRODScertificateTrustStorePassword;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
