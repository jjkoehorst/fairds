package nl.fairbydesign.backend.ena.xml;

import java.util.HashMap;

public class ENAObject {
    private String packageIdentifier;
    private HashMap<String, String> labelContent = new HashMap<>();

    public void setPackageIdentifier(String packageIdentifier) {
        this.packageIdentifier = packageIdentifier;
    }

    public String getPackageIdentifier() {
        return packageIdentifier;
    }

    public void addLabelContent(String propertyLabel, String contentValue) {
        this.labelContent.put(propertyLabel, contentValue);
    }

    public String getLabelContent(String propertyLabel) {
        return this.labelContent.get(propertyLabel);
    }

    public HashMap<String, String> getAllLabelContent() {
        return this.labelContent;
    }
}
