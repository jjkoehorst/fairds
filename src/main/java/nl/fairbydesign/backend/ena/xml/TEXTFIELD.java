package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TEXTFIELD{
    @JsonProperty("REGEX_VALUE")
    public String getREGEX_VALUE() {
        return this.rEGEX_VALUE; }
    public void setREGEX_VALUE(String rEGEX_VALUE) {
        this.rEGEX_VALUE = rEGEX_VALUE; }
    String rEGEX_VALUE;
}
