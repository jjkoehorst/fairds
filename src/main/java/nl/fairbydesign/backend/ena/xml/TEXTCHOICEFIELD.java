package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class TEXTCHOICEFIELD {
    @JsonProperty("TEXT_VALUE")
    public ArrayList<TEXTVALUE> getTEXT_VALUE() {
        return this.tEXT_VALUE;
    }

    public void setTEXT_VALUE(ArrayList<TEXTVALUE> tEXT_VALUE) {
        this.tEXT_VALUE = tEXT_VALUE;
    }

    ArrayList<TEXTVALUE> tEXT_VALUE;
}

