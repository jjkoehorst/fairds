package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TEXTVALUE {
    @JsonProperty("VALUE")
    public String getVALUE() {
        return this.vALUE;
    }

    public void setVALUE(String vALUE) {
        this.vALUE = vALUE;
    }

    String vALUE;
}
