package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString, Root.class); */
public class CHECKLIST {
    @JsonProperty("DESCRIPTOR")
    public DESCRIPTOR getDESCRIPTOR() {
        return this.dESCRIPTOR;
    }

    public void setDESCRIPTOR(DESCRIPTOR dESCRIPTOR) {
        this.dESCRIPTOR = dESCRIPTOR;
    }

    DESCRIPTOR dESCRIPTOR;

    @JsonProperty("checklistType")
    public String getChecklistType() {
        return this.checklistType;
    }

    public void setChecklistType(String checklistType) {
        this.checklistType = checklistType;
    }

    String checklistType;

    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
        return this.iDENTIFIERS;
    }

    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
        this.iDENTIFIERS = iDENTIFIERS;
    }

    IDENTIFIERS iDENTIFIERS;

    @JsonProperty("accession")
    public String getAccession() {
        return this.accession;
    }

    public void setAccession(String accession) {
        this.accession = accession;
    }

    String accession;


    @JsonProperty("_accession")
    public void set_accession(String accession) {
        System.err.println("Accession: " + this.accession + " changed to " + accession);
        this.accession = accession;
    }

    @JsonProperty("_checklistType")
    public String get_checklistType() {
        return this._checklistType;
    }
    public void set_checklistType(String _checklistType) {
        this._checklistType = _checklistType;
    }
    String _checklistType;
}
