package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IDENTIFIERS {
    @JsonProperty("PRIMARY_ID")
    public String getPRIMARY_ID() {
        return this.pRIMARY_ID;
    }

    public void setPRIMARY_ID(String pRIMARY_ID) {
        this.pRIMARY_ID = pRIMARY_ID;
    }

    String pRIMARY_ID;
}
