package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class UNITS {
    @JsonProperty("UNIT")
    public ArrayList<String> getUNIT() {
        return this.uNIT;
    }

    public void setUNIT(ArrayList<String> uNIT) {
        this.uNIT = uNIT;
    }

    ArrayList<String> uNIT;
}
