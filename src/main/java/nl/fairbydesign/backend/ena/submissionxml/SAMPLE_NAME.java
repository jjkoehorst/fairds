package nl.fairbydesign.backend.ena.submissionxml;

import java.util.List;

public class SAMPLE_NAME {
    public int getTAXON_ID() {
        return this.TAXON_ID;
    }

    public void setTAXON_ID(int TAXON_ID) {
        this.TAXON_ID = TAXON_ID;
    }

    int TAXON_ID;

    public String getSCIENTIFIC_NAME() {
        return this.SCIENTIFIC_NAME;
    }

    public void setSCIENTIFIC_NAME(String SCIENTIFIC_NAME) {
        this.SCIENTIFIC_NAME = SCIENTIFIC_NAME;
    }

    String SCIENTIFIC_NAME;

    public String getCOMMON_NAME() {
        return this.COMMON_NAME;
    }

    public void setCOMMON_NAME(String COMMON_NAME) {
        this.COMMON_NAME = COMMON_NAME;
    }

    String COMMON_NAME;
}


