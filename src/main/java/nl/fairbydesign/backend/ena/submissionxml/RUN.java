package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"EXPERIMENT_REF", "DATA_BLOCK"})
public class RUN {
    public EXPERIMENT_REF getEXPERIMENT_REF() {
        return this.EXPERIMENT_REF;
    }

    public void setEXPERIMENT_REF(EXPERIMENT_REF EXPERIMENT_REF) {
        this.EXPERIMENT_REF = EXPERIMENT_REF;
    }

    EXPERIMENT_REF EXPERIMENT_REF = new EXPERIMENT_REF();

    public DATA_BLOCK getDATA_BLOCK() {
        return this.DATA_BLOCK;
    }

    public void setDATA_BLOCK(DATA_BLOCK DATA_BLOCK) {
        this.DATA_BLOCK = DATA_BLOCK;
    }

    DATA_BLOCK DATA_BLOCK = new DATA_BLOCK();

    public String getalias() {
        return this.alias;
    }

    @XmlAttribute(name = "alias")
    public void setalias(String alias) {
        this.alias = alias;
    }

    String alias;

    public String getcenter_name() {
        return this.center_name;
    }

    @XmlAttribute(name = "center_name")
    public void setcenter_name(String center_name) {
        this.center_name = center_name;
    }

    String center_name;
}
