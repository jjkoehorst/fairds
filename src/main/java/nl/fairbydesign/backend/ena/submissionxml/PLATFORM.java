package nl.fairbydesign.backend.ena.submissionxml;

import nl.fairbydesign.backend.ena.submissionxml.platforms.*;

public class PLATFORM {
    public nl.fairbydesign.backend.ena.submissionxml.platforms.ILLUMINA getILLUMINA() {
        return this.ILLUMINA; }
    public void setILLUMINA(ILLUMINA ILLUMINA) {
        this.ILLUMINA = ILLUMINA; }
    ILLUMINA ILLUMINA;

    /////////


    public LS454 getLS454() {
        return this.LS454; }
    public void setLS454(LS454 lS454) {
        this.LS454 = lS454; }
    LS454 LS454;

    public ABISOLID getABISOLID() {
        return this.ABISOLID; }
    public void setABISOLID(ABISOLID ABISOLID) {
        this.ABISOLID = ABISOLID; }
    ABISOLID ABISOLID;

    public PACBIOSMRT getPACBIOSMRT() {
        return this.PACBIOSMRT; }
    public void setPACBIOSMRT(PACBIOSMRT PACBIOSMRT) {
        this.PACBIOSMRT = PACBIOSMRT; }
    PACBIOSMRT PACBIOSMRT;

    public IONTORRENT getIONTORRENT() {
        return this.IONTORRENT; }
    public void setIONTORRENT(IONTORRENT IONTORRENT) {
        this.IONTORRENT = IONTORRENT; }
    IONTORRENT IONTORRENT;

    public OXFORDNANOPORE getOXFORDNANOPORE() {
        return this.OXFORDNANOPORE; }
    public void setOXFORDNANOPORE(OXFORDNANOPORE OXFORDNANOPORE) {
        this.OXFORDNANOPORE = OXFORDNANOPORE; }
    OXFORDNANOPORE OXFORDNANOPORE;

    public BGISEQ getBGISEQ() {
        return this.BGISEQ; }
    public void setBGISEQ(BGISEQ BGISEQ) {
        this.BGISEQ = BGISEQ; }
    BGISEQ BGISEQ;

    public COMPLETEGENOMICS getCOMPLETEGENOMICS() {
        return this.COMPLETEGENOMICS; }
    public void setCOMPLETEGENOMICS(COMPLETEGENOMICS COMPLETEGENOMICS) {
        this.COMPLETEGENOMICS = COMPLETEGENOMICS; }
    COMPLETEGENOMICS COMPLETEGENOMICS;

    public CAPILLARY getCAPILLARY() {
        return this.CAPILLARY; }
    public void setCAPILLARY(CAPILLARY cAPILLARY) {
        this.CAPILLARY = CAPILLARY; }
    CAPILLARY CAPILLARY;

    public HELICOS getHELICOS() {
        return this.HELICOS; }
    public void setHELICOS(HELICOS HELICOS) {
        this.HELICOS = HELICOS; }
    HELICOS HELICOS;
}