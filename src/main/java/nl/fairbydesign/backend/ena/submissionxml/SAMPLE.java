package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

 @XmlType(propOrder={"TITLE", "text", "SAMPLE_NAME", "SAMPLE_ATTRIBUTES"})
public class SAMPLE {
    String center_name;
    String TITLE;
    SAMPLE_NAME SAMPLE_NAME;
    SAMPLE_ATTRIBUTES SAMPLE_ATTRIBUTES = new SAMPLE_ATTRIBUTES();
    String alias;
    String text;

    public String getTITLE() {
        return this.TITLE;
    }
    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public SAMPLE_NAME getSAMPLE_NAME() {
        return this.SAMPLE_NAME;
    }

    public void setSAMPLE_NAME(SAMPLE_NAME SAMPLE_NAME) {
        this.SAMPLE_NAME = SAMPLE_NAME;
    }

    public String getAlias() {
        return this.alias;
    }

    @XmlAttribute(name = "alias")
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCenterName() {
        return this.center_name;
    }

    @XmlAttribute(name = "center_name")
    public void setCenterName(String center_name) {
        this.center_name = center_name;
    }


    public String gettext() {
        return this.text;
    }

    public void settext(String text) {
        this.text = text;
    }

    public SAMPLE_ATTRIBUTES getSAMPLE_ATTRIBUTES() {
        return this.SAMPLE_ATTRIBUTES;
    }

    public void setSAMPLE_ATTRIBUTES(SAMPLE_ATTRIBUTES SAMPLE_ATTRIBUTES) {
        this.SAMPLE_ATTRIBUTES = SAMPLE_ATTRIBUTES;
    }
}
