package nl.fairbydesign.backend.ena.submissionxml;

import java.util.ArrayList;
import java.util.List;

public class FILES {
    public ArrayList<FILE> getFILE() {
        return this.FILE;
    }

    public void setFILE(ArrayList<FILE> FILE) {
        this.FILE = FILE;
    }
    public void addFILE(FILE FILE) {
        this.FILE.add(FILE);
    }

    ArrayList<FILE> FILE = new ArrayList<>();
}
