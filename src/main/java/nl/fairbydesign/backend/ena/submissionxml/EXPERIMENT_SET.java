package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "EXPERIMENT_SET")
public class EXPERIMENT_SET {
    public ArrayList<nl.fairbydesign.backend.ena.submissionxml.EXPERIMENT> getEXPERIMENT() {
        return this.EXPERIMENT;
    }

    public void setEXPERIMENT(ArrayList<EXPERIMENT> EXPERIMENT) {
        this.EXPERIMENT = EXPERIMENT;
    }

    public void addEXPERIMENT(EXPERIMENT experiment) {
        this.EXPERIMENT.add(experiment);
    }

    ArrayList<nl.fairbydesign.backend.ena.submissionxml.EXPERIMENT> EXPERIMENT = new ArrayList<>();
}
