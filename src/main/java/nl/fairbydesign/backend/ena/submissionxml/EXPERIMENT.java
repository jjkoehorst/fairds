package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"TITLE", "STUDY_REF", "DESIGN", "PLATFORM", "EXPERIMENT_ATTRIBUTES","text"})
public class EXPERIMENT {
    public String getTITLE() {
        return this.TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    String TITLE;

    public STUDY_REF getSTUDY_REF() {
        return this.STUDY_REF;
    }

    public void setSTUDY_REF(STUDY_REF STUDY_REF) {
        this.STUDY_REF = STUDY_REF;
    }

    STUDY_REF STUDY_REF = new STUDY_REF();

    public DESIGN getDESIGN() {
        return this.DESIGN;
    }

    public void setDESIGN(DESIGN DESIGN) {
        this.DESIGN = DESIGN;
    }

    DESIGN DESIGN = new DESIGN();

    public PLATFORM getPLATFORM() {
        return this.PLATFORM;
    }

    public void setPLATFORM(PLATFORM PLATFORM) {
        this.PLATFORM = PLATFORM;
    }

    PLATFORM PLATFORM = new PLATFORM();

    public EXPERIMENT_ATTRIBUTES getEXPERIMENT_ATTRIBUTES() {
        return this.EXPERIMENT_ATTRIBUTES;
    }

    public void setEXPERIMENT_ATTRIBUTES(EXPERIMENT_ATTRIBUTES EXPERIMENT_ATTRIBUTES) {
        this.EXPERIMENT_ATTRIBUTES = EXPERIMENT_ATTRIBUTES;
    }

    EXPERIMENT_ATTRIBUTES EXPERIMENT_ATTRIBUTES = new EXPERIMENT_ATTRIBUTES();

    public String getalias() {
        return this.alias;
    }

    @XmlAttribute(name = "alias")
    public void setalias(String alias) {
        this.alias = alias;
    }

    String alias;

    public String gettext() {
        return this.text;
    }

    public void settext(String text) {
        this.text = text;
    }

    String text;
}
