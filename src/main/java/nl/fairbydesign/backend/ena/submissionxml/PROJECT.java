package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"TITLE", "DESCRIPTION", "SUBMISSION_PROJECT", "text"})

public class PROJECT {
    public String getTITLE() {
        return this.TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    String TITLE;

    public String getDESCRIPTION() {
        return this.DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    String DESCRIPTION;

    public SUBMISSION_PROJECT getSUBMISSION_PROJECT() {
        return this.SUBMISSION_PROJECT;
    }

    public void setSUBMISSION_PROJECT(SUBMISSION_PROJECT SUBMISSION_PROJECT) {
        this.SUBMISSION_PROJECT = SUBMISSION_PROJECT;
    }

    SUBMISSION_PROJECT SUBMISSION_PROJECT = new SUBMISSION_PROJECT();

    public String getalias() {
        return this.alias;
    }

    @XmlAttribute(name = "alias")
    public void setalias(String alias) {
        this.alias = alias;
    }

    String alias;

    public String gettext() {
        return this.text;
    }

    public void settext(String text) {
        this.text = text;
    }

    String text;
}
