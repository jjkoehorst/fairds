package nl.fairbydesign.backend.ena.submissionxml;

public class SAMPLE_ATTRIBUTE {
    public String getTAG() {
        return this.TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    String TAG;

    public String getVALUE() {
        return this.VALUE;
    }

    public void setVALUE(String VALUE) {
        this.VALUE = VALUE;
    }

    String VALUE;

    public String getUNITS() {
        return this.UNITS;
    }

    public void setUNITS(String UNITS) {
        this.UNITS = UNITS;
    }

    String UNITS;
}
