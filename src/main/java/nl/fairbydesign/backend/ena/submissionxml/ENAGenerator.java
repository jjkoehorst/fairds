package nl.fairbydesign.backend.ena.submissionxml;

import nl.fairbydesign.backend.Generic;
import nl.fairbydesign.backend.data.objects.jerm.Study;
import nl.fairbydesign.backend.ena.submissionxml.platforms.*;
import nl.fairbydesign.backend.ena.xml.ENAObject;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.FilenameUtils;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.jena.rdf.model.*;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.fairbydesign.backend.Generic.getValueOfPredicate;
import static nl.fairbydesign.backend.metadata.MetadataParser.generateRegex;
import static nl.fairbydesign.backend.metadata.MetadataParser.logger;

public class ENAGenerator {
    private static ENAObject enaObject;

    // Lookup hashmap with corrections
    static HashMap<String, String> formatters = new HashMap<>();
    private static HashSet<String> ignoreLabels = new HashSet<>();

    /**
     * @param domain
     * @param study  object with metadata for ENA submission
     * @param assays list with metadata for ENA submission
     * @return
     */
    public static File start(Domain domain, Study study, Set<Assay> assays) throws RuntimeException {
        // Create submission root object
        SAMPLE_SET sample_set = new SAMPLE_SET();
        EXPERIMENT_SET experiment_set = new EXPERIMENT_SET();
        RUN_SET run_set = new RUN_SET();
        PROJECT_SET project_set = new PROJECT_SET();

        // Set all other things up
        setFormatters();
        ignoreLables();
        ArrayList<String> identifiers = new ArrayList<>();
        HashMap<String, HashSet<String>> samplePackage = new HashMap<>();
        HashMap<String, HashMap<String, ArrayList<String>>> predicateLabels = new HashMap<>();
        for (Assay assay : assays) {
            identifiers.add(assay.getIdentifier());
        }

        ArrayList<ENAObject> enaObjects = new ArrayList<>();

        // Project set
        project_set.PROJECT.alias = study.getRDFObject().getIdentifier();
        project_set.PROJECT.TITLE = getValueOfPredicate(domain, study.getRDFObject().getResource().getURI(), "schema:title");;
        project_set.PROJECT.DESCRIPTION = getValueOfPredicate(domain, study.getRDFObject().getResource().getURI(), "schema:description");

        // All other sets
        for (observation_unit observation_unit : study.getRDFObject().getAllHasPart()) {
            for (Sample sample : observation_unit.getAllHasPart()) {
                boolean selected = false;
                HashSet<String> platforms = new HashSet<>();
                for (Assay assay : sample.getAllHasPart()) {
                    if (identifiers.contains(assay.getIdentifier())) {
                        selected = true;
                        Property platformProperty = domain.getRDFSimpleCon().getModel().getProperty("http://fairbydesign.nl/platform");
                        Statement statement = assay.getResource().getProperty(platformProperty);
                        platforms.add(statement.getObject().asLiteral().toString());
                    }
                }
                if (selected) {
                    SAMPLE enaSample = new SAMPLE();

                    // Observation unit metadata
                    getAllValues(domain, observation_unit.getResource(), enaSample);
                    getAllValues(domain, sample.getResource(), enaSample);
                    enaSample.setTITLE(sample.getName());
                    enaSample.setAlias(sample.getIdentifier());
                    enaSample.setCenterName("");
                    enaSample.setSAMPLE_NAME(new SAMPLE_NAME());

                    enaSample.getSAMPLE_NAME().setSCIENTIFIC_NAME(getValueFromPredicate(sample.getResource(), "http://gbol.life/0.1/scientificName"));
                    enaSample.getSAMPLE_NAME().setTAXON_ID(Integer.parseInt(getValueFromPredicate(sample.getResource(), "http://purl.uniprot.org/core/organism")));

                    sample_set.addSAMPLE(enaSample);


                    /**
                     * Raw reads
                     */

                    for (Assay assay : sample.getAllHasPart()) {
                        if (identifiers.contains(assay.getIdentifier())) {
                            // EXPERIMENT SECTION
                            selected = true;
                            EXPERIMENT experiment = new EXPERIMENT();
                            experiment.alias = "exp_" + assay.getIdentifier();
                            experiment.setTITLE(getValueOfPredicate(domain, assay.getResource().getURI(), "schema:description"));
                            experiment_set.addEXPERIMENT(experiment);
                            // DESIGN
                            experiment.getDESIGN().SAMPLE_DESCRIPTOR.accession = sample.getIdentifier();
                            experiment.getDESIGN().getLIBRARY_DESCRIPTOR().LIBRARY_STRATEGY = getValueFromPredicate(assay.getResource(), "http://fairbydesign.nl/library_strategy");
                            experiment.getDESIGN().getLIBRARY_DESCRIPTOR().LIBRARY_SOURCE = getValueFromPredicate(assay.getResource(), "http://fairbydesign.nl/library_source");
                            experiment.getDESIGN().getLIBRARY_DESCRIPTOR().LIBRARY_SELECTION = getValueFromPredicate(assay.getResource(), "http://fairbydesign.nl/library_selection");

                            ArrayList<Data_sample> data_samples = new ArrayList<>();
                            for (Data_sample data_sample : assay.getAllHasPart()) {
                                NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(data_sample.getResource().getURI(), "schema:description");
                                if (nodeIterator.hasNext()) {
                                    String description = domain.getRDFSimpleCon().getObjects(data_sample.getResource().getURI(), "schema:description").next().toString();
                                    if (description.contains("library forward filename")) continue;
                                    if (description.contains("library reverse filename")) continue;
                                }
                                data_samples.add(data_sample);
                            }

                            if (data_samples.size() == 1) {
                                // Single check
                                SINGLE single = new SINGLE();
                                experiment.getDESIGN().getLIBRARY_DESCRIPTOR().LIBRARY_LAYOUT.setSINGLE(single);
                            } else if (data_samples.size() == 2) {
                                // Paired check
                                PAIRED paired = new PAIRED();
                                experiment.getDESIGN().getLIBRARY_DESCRIPTOR().LIBRARY_LAYOUT.setPAIRED(paired);
                            } else {
                                logger.error("Multiple data files (>2) detected for " + assay.getIdentifier() + " which is not supported");
                            }

                            experiment.getSTUDY_REF().setaccession(study.getRDFObject().getIdentifier());

                            // PLATFORM
                            String platform = getValueFromPredicate(assay.getResource(), "http://fairbydesign.nl/platform");
                            String instrument_model = getValueFromPredicate(assay.getResource(), "http://fairbydesign.nl/instrument_model");

                            setInstrumentModel(experiment, platform, instrument_model);

                            // Experimental attributes
                            getAllValues(domain, assay.getResource(), experiment.getEXPERIMENT_ATTRIBUTES());

                            // RUN settings
                            RUN run = new RUN();
                            run.alias = "run_" + assay.getIdentifier();
                            run.EXPERIMENT_REF.refname = "exp_" + assay.getIdentifier();

                            for (Data_sample data_sample : data_samples) {
                                FILE file = new FILE();
                                file.filename = getValueOfPredicate(domain, data_sample.getResource().getURI(), "schema:name");
                                // Set file type
                                file.filetype = getFileType(file.filename);
                                run.getDATA_BLOCK().getFILES().addFILE(file);
                            }
                            run_set.addRUN(run);
                        }
                    }
                }
            }
        }

        try {
            File tempDirectory = Files.createTempDir();
            tempDirectory.mkdirs();
            // PROJECT_SET WRITER
            JAXBContext jaxbContext = JAXBContext.newInstance(PROJECT_SET.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(project_set, sw);
            File projectFile = new File(tempDirectory + "/project.xml");
            PrintWriter printWriter = new PrintWriter(projectFile);
            printWriter.print(sw.toString());
            printWriter.close();

            // SAMPLE_SET WRITER
            jaxbContext = JAXBContext.newInstance(SAMPLE_SET.class);
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            sw = new StringWriter();
            jaxbMarshaller.marshal(sample_set, sw);
            File sampleFile = new File(tempDirectory + "/sample.xml");
            printWriter = new PrintWriter(sampleFile);
            printWriter.print(sw.toString());
            printWriter.close();

            // EXPERIMENT XML WRITER
            jaxbContext = JAXBContext.newInstance(EXPERIMENT_SET.class);
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            sw = new StringWriter();
            jaxbMarshaller.marshal(experiment_set, sw);
            File experimentFile = new File(tempDirectory + "/experiment.xml");
            printWriter = new PrintWriter(experimentFile);
            printWriter.print(sw.toString());
            printWriter.close();

            // EXPERIMENT RUN WRITER
            jaxbContext = JAXBContext.newInstance(RUN_SET.class);
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            sw = new StringWriter();
            jaxbMarshaller.marshal(run_set, sw);
            File runFile = new File(tempDirectory + "/run.xml");
            printWriter = new PrintWriter(runFile);
            printWriter.print(sw.toString());
            printWriter.close();

            // Submission xml
            File submissionFile = new File(tempDirectory + "/submission.xml");
            printWriter = new PrintWriter(submissionFile);
            printWriter.print(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<SUBMISSION>\n" +
                    "   <ACTIONS>\n" +
                    "      <ACTION>\n" +
                    "         <ADD/>\n" +
                    "      </ACTION>\n" +
                    "   </ACTIONS>\n" +
                    "</SUBMISSION>"
            );
            printWriter.close();

            File zipFile = Generic.createZipArchive(tempDirectory);
            return zipFile;
        } catch (JAXBException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getFileType(String filename) {
        ArrayList<String> compressions = new ArrayList<>(List.of(new String[]{"gz", "zip","tar"}));
        String extension = FilenameUtils.getExtension(filename);
        // tar, tar.gz, zip, gz.gz.gz.gz
        while (compressions.contains(extension)) {
            // Removes extension and removes dot at end of the line
            filename = filename.replaceAll(extension, "").replaceAll("\\.$", "");
            extension = FilenameUtils.getExtension(filename);
        }
        return extension;
    }

    private static void getAllValues(Domain domain, Resource resource, EXPERIMENT_ATTRIBUTES experiment_attributes) {
        resource.listProperties().forEachRemaining(property -> {
            String contentValue = null;
            if (property.getObject().isLiteral()) {
                contentValue = property.getObject().asLiteral().getString();
            }
            // Get label from predicate if available
            Resource resourcePredicate = domain.getRDFSimpleCon().getModel().getResource(property.getPredicate().getURI());
            String propertyLabel;
            if (property.getPredicate().getURI().startsWith("http://schema.org/")) {
                propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                Property a = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resource, a);
                String type = nodeIterator.next().asResource().getURI().replaceAll(".*#", "").replaceAll(".*/", "").toLowerCase();
                propertyLabel = correctLabel(type + " " + propertyLabel);
            } else {
                Property labelProperty = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/2000/01/rdf-schema#label");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resourcePredicate, labelProperty);
                // Ontology items without direct labels
                if (!nodeIterator.hasNext()) {
                    propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                    propertyLabel = correctLabel(propertyLabel);
                } else {
                    // All properties with labels
                    propertyLabel = nodeIterator.next().toString();
                    propertyLabel = correctLabel(propertyLabel);
                }
            }
            if (contentValue != null && contentValue.length() > 0 && !contentValue.equals("null")) {
                EXPERIMENT_ATTRIBUTE experiment_attribute = new EXPERIMENT_ATTRIBUTE();
                experiment_attribute.TAG = propertyLabel;
                experiment_attribute.VALUE = contentValue;
                experiment_attributes.addEXPERIMENT_ATTRIBUTE(experiment_attribute);
            }
        });
    }

    private static void setInstrumentModel(EXPERIMENT experiment, String platform, String instrument_model) {
        if (platform.equals("ILLUMINA")) {
            experiment.getPLATFORM().setILLUMINA(new ILLUMINA(instrument_model));
        } else if (platform.equals("OXFORD_NANOPORE")) {
            experiment.getPLATFORM().setOXFORDNANOPORE(new OXFORDNANOPORE(instrument_model));
        }
        else if (platform.equals("ABISOLID")) {
            experiment.getPLATFORM().setABISOLID(new ABISOLID(instrument_model));
        } else if (platform.equals("BGISEQ")) {
            experiment.getPLATFORM().setBGISEQ(new BGISEQ(instrument_model));
        } else if (platform.equals("CAPILLARY")) {
            experiment.getPLATFORM().setCAPILLARY(new CAPILLARY(instrument_model));
        } else if (platform.equals("COMPLETEGENOMICS")) {
            experiment.getPLATFORM().setCOMPLETEGENOMICS(new COMPLETEGENOMICS(instrument_model));
        } else if (platform.equals("HELICOS")) {
            experiment.getPLATFORM().setHELICOS(new HELICOS(instrument_model));
        } else if (platform.equals("ILLUMINA")) {
            experiment.getPLATFORM().setILLUMINA(new ILLUMINA(instrument_model));
        } else if (platform.equals("IONTORRENT")) {
            experiment.getPLATFORM().setIONTORRENT(new IONTORRENT(instrument_model));
        } else if (platform.equals("LS454")) {
            experiment.getPLATFORM().setLS454(new LS454(instrument_model));
        } else if (platform.equals("PACBIOSMRT")) {
            experiment.getPLATFORM().setPACBIOSMRT(new PACBIOSMRT(instrument_model));
        }
    }

    private static String getValueFromPredicate(Resource resource, String predicate) {
        Property property = resource.getModel().getProperty(predicate);
        if (resource.hasProperty(property)) {
            RDFNode object = resource.getProperty(property).getObject();
            if (object.isLiteral()) {
                return object.asLiteral().getValue().toString();
            }
        }
        return null;
    }

    private static void ignoreLables() {
        ignoreLabels.add("logicalPath");
        ignoreLabels.add("samplePackage");
        ignoreLabels.add("type");
        ignoreLabels.add("hasPart");
    }

    private static void setFormatters() {
        formatters.put("ncbi taxonomy id", "tax_id");
        formatters.put("scientific name", "scientific_name");
    }

    private static ArrayList<String> correctLabels(ArrayList<String> header) {
        for (String key : formatters.keySet()) {
            if (header.contains(key)) {
                header.set(header.indexOf(key), formatters.get(key));
            }
        }
        return header;
    }

    private static String correctLabel(String label) {
        if (formatters.containsKey(label)) {
            return formatters.get(label);
        }
        return label;
    }

    private static void getAllValues(Domain domain, Resource resource, SAMPLE sample) {
        SAMPLE_ATTRIBUTES sampleAttributes = sample.getSAMPLE_ATTRIBUTES();
        sample.setSAMPLE_ATTRIBUTES(sampleAttributes);
        resource.listProperties().forEachRemaining(property -> {
            String contentValue = null;
            if (property.getObject().isLiteral()) {
                contentValue = property.getObject().asLiteral().getString();
            }
            // Get label from predicate if available
            Resource resourcePredicate = domain.getRDFSimpleCon().getModel().getResource(property.getPredicate().getURI());
            String propertyLabel;
            if (property.getPredicate().getURI().startsWith("http://schema.org/")) {
                propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                Property a = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resource, a);
                String type = nodeIterator.next().asResource().getURI().replaceAll(".*#", "").replaceAll(".*/", "").toLowerCase();
                propertyLabel = correctLabel(type + " " + propertyLabel);
            } else {
                Property labelProperty = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/2000/01/rdf-schema#label");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resourcePredicate, labelProperty);
                // Ontology items without direct labels
                if (!nodeIterator.hasNext()) {
                    propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                    propertyLabel = correctLabel(propertyLabel);
                } else {
                    // All properties with labels
                    propertyLabel = nodeIterator.next().toString();
                    propertyLabel = correctLabel(propertyLabel);
                }
            }

            // Check for unit code
            Property unitCodeProperty = domain.getRDFSimpleCon().getModel().getProperty("https://schema.org/unitCode");
            String unitValue = null;
            if (resourcePredicate.hasProperty(unitCodeProperty)) {
                String unitCode = "(" + resourcePredicate.getProperty(unitCodeProperty).getObject().asLiteral().getString() + ")";
                Pattern pattern = generateRegex(null, ".*(" + unitCode + ")", contentValue);
                Matcher matcher = pattern.matcher(contentValue);
                if (matcher.matches()) {
                    unitValue = matcher.group(1);
                }
            }

            if (contentValue != null && !contentValue.equals("null") && contentValue.length() > 0) {
                SAMPLE_ATTRIBUTE sampleAttribute = new SAMPLE_ATTRIBUTE();
                sampleAttribute.setTAG(propertyLabel);
                if (unitValue != null) {
                    sampleAttribute.setUNITS(unitValue);
                    sampleAttribute.setVALUE(contentValue.replaceAll(" +"+unitValue + "$", ""));
                } else {
                    sampleAttribute.setVALUE(contentValue);
                }
                sampleAttributes.addSAMPLE_ATTRIBUTE(sampleAttribute);
            }
        });
    }
    private static ArrayList<String> getAllPredicates(Domain domain, Resource resource) {
        ArrayList<String> predicateLabels = new ArrayList<>();
        resource.listProperties().forEachRemaining(property -> {
            String contentValue = null;
            if (property.getObject().isLiteral()) {
                contentValue = property.getObject().asLiteral().getString();
            }
            // Get label from predicate if available
            Resource resourcePredicate = domain.getRDFSimpleCon().getModel().getResource(property.getPredicate().getURI());
            if (property.getPredicate().getURI().startsWith("http://schema.org/")) {
                String propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                Property a = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resource, a);
                String type = nodeIterator.next().asResource().getURI().replaceAll(".*#", "").replaceAll(".*/", "").toLowerCase();
                propertyLabel = correctLabel(type + " " + propertyLabel);
                predicateLabels.add(propertyLabel);
                enaObject.addLabelContent(propertyLabel, contentValue);
            } else {
                Property labelProperty = domain.getRDFSimpleCon().getModel().getProperty("http://www.w3.org/2000/01/rdf-schema#label");
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getModel().listObjectsOfProperty(resourcePredicate, labelProperty);
                // Ontology items without direct labels
                if (!nodeIterator.hasNext()) {
                    String propertyLabel = resourcePredicate.getURI().replaceAll(".*#", "").replaceAll(".*/", "");
                    propertyLabel = correctLabel(propertyLabel);
                    predicateLabels.add(propertyLabel);
                    enaObject.addLabelContent(correctLabel(propertyLabel), contentValue);
                } else {
                    // All properties with labels
                    String propertyLabel = nodeIterator.next().toString();
                    propertyLabel = correctLabel(propertyLabel);
                    predicateLabels.add(propertyLabel);
                    enaObject.addLabelContent(correctLabel(propertyLabel), contentValue);
                }
            }
        });
        return predicateLabels;
    }
}





