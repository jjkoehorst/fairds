package nl.fairbydesign.backend.ena.submissionxml;

import java.util.ArrayList;
import java.util.List;

public class SAMPLE_ATTRIBUTES {
    public SAMPLE_ATTRIBUTES() {
        nl.fairbydesign.backend.ena.submissionxml.SAMPLE_ATTRIBUTE sample_attribute = new SAMPLE_ATTRIBUTE();
        sample_attribute.setTAG("Metadata submission software");
        sample_attribute.setVALUE("FAIR Data Station");
        this.addSAMPLE_ATTRIBUTE(sample_attribute);
    }
    public List<SAMPLE_ATTRIBUTE> getSAMPLE_ATTRIBUTE() {
        return this.SAMPLE_ATTRIBUTE;
    }

    public void setSAMPLE_ATTRIBUTE(List<SAMPLE_ATTRIBUTE> SAMPLE_ATTRIBUTE) {
        this.SAMPLE_ATTRIBUTE = SAMPLE_ATTRIBUTE;
    }

    public void addSAMPLE_ATTRIBUTE(SAMPLE_ATTRIBUTE SAMPLE_ATTRIBUTE) {
        this.SAMPLE_ATTRIBUTE.add(SAMPLE_ATTRIBUTE);
    }

    List<SAMPLE_ATTRIBUTE> SAMPLE_ATTRIBUTE = new ArrayList<>();
}
