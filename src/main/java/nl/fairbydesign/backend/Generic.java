package nl.fairbydesign.backend;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.NodeIterator;
import org.jboss.logging.Logger;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Generic {
    public static final Logger logger = Logger.getLogger(Generic.class);
    public static File createZipArchive(File directory) throws IOException {

        // create a ZipOutputStream object
        FileOutputStream fos = new FileOutputStream(directory + "/ena_submission.zip");
        ZipOutputStream zos = new ZipOutputStream(fos);

        File[] filesToBeWritten = directory.listFiles();
        for (int i = 0; i < filesToBeWritten.length; i++) {

            File srcFile = filesToBeWritten[i];
            FileInputStream fis = new FileInputStream(srcFile);

            // Start writing a new file entry
            zos.putNextEntry(new ZipEntry(srcFile.getName()));

            int length;
            // create byte buffer
            byte[] buffer = new byte[1024];

            // read and write the content of the file
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            // current file entry is written and current zip entry is closed
            zos.closeEntry();

            // close the InputStream of the file
            fis.close();
        }

        // close the ZipOutputStream
        zos.close();
        return new File(directory + "/ena_submission.zip");
    }

    // Get value of predicate when available otherwise return null
    public static HashSet<String> getObjectsOfPredicate(Domain domain, String subject, String predicate) {
        try {
            HashSet<String> uris = new HashSet<>();
            NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(subject, predicate);
            while (nodeIterator.hasNext()) {
                uris.add(nodeIterator.next().toString());
            }
            return uris;
        } catch (Exception e) {
            logger.error("No results for " + predicate + " from " + subject);
            return null;
        }
    }

    public static String getValueOfPredicate(Domain domain, String subject, String predicate) {
        try {
            return domain.getRDFSimpleCon().getObjects(subject, predicate).next().toString();
        } catch (Exception e) {
            logger.error("No results for " + predicate + " from " + subject);
            return null;
        }
    }
}
