package nl.fairbydesign.backend;

import com.vaadin.flow.component.Html;

import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Iterator;
import java.util.Scanner;

public class WebGeneric {
    private static final Logger logger = LoggerFactory.getLogger(WebGeneric.class);

    public static Html getTextFromResource(String filePath) {
        String helpText = new Scanner(getFileContentFromResources(filePath)).useDelimiter("\\Z").next();
        Html text = new Html("<span>" + helpText + "</span>");
        return text;
    }

    /**
     * Reads file from local internal resource
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public static String getFileContentFromResources(String fileName) {
        logger.debug("Parsing " + fileName);
        try {
            StringWriter writer = new StringWriter();
            InputStream inputStream = Util.getResourceFile(fileName);
            IOUtils.copy(inputStream, writer, "UTF-8");
            String theString = writer.toString();
            IOUtils.closeQuietly(inputStream);
            return theString;
        } catch (IOException | NullPointerException e) {
            logger.debug(e.getMessage());
        }
        return "Reading failed for " + fileName;
    }


    public static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }

    public static String getEdamFormat(String fileName) {
        // Always remove compression end when gz
        fileName = fileName.replaceAll(".gz$", "");

        String extension = FilenameUtils.getExtension(fileName);

        logger.info("File extension " + extension + " detected");
        if (fileName.startsWith("http")) {
            return fileName;
        } else if (extension.matches("bam")) {
            return "http://edamontology.org/format_2572";
        } else if (extension.matches("(fq|fastq)")) {
            return "http://edamontology.org/format_1930";
        } else if (extension.matches("(fa|fasta)")) {
            return "http://edamontology.org/format_1929";
        } else {
            logger.error("Unknown file format " + extension);
            return null;
        }
    }

    public static boolean checkNotEmpty(Row row) {
        Iterator<Cell> rowIter = row.iterator();
        while (rowIter.hasNext()) {
            Cell cell = rowIter.next();
            if (!cell.getCellType().equals(CellType.BLANK))
                return true;
        }
        return false;
    }

    /**
     * @param cell
     * @return
     */
    public static String getStringValueCell(Cell cell) {
        if (cell == null) {
            logger.error("Cell is empty");
        }
        CellType type = cell.getCellType();
        String value = null;
        if (type.equals(CellType.FORMULA)) {
            if(cell.getCellType() == CellType.FORMULA) {
                logger.debug("Formula is " + cell.getCellFormula());
                switch(cell.getCachedFormulaResultType()) {
                    case NUMERIC:
                        logger.debug("Last evaluated as: " + cell.getNumericCellValue());
                        System.err.println(cell.getNumericCellValue());
                        value = String.valueOf(cell.getNumericCellValue()).trim();
                        break;
                    case STRING:
                        logger.debug("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
                        value = cell.getStringCellValue().trim();
                        break;
                }
            }
        } else if (type.equals(CellType.NUMERIC)) {
            // Transforms all values ending with .0 to a full integer
            value = String.valueOf(cell.getNumericCellValue()).trim().replaceAll("\\.0$", "");
        } else if (type.equals(CellType.STRING)) {
            value = cell.getStringCellValue().trim();
        } else if (type.equals(CellType.BLANK)) {
            value = "";
        } else if (type.equals(CellType.BOOLEAN)) {
            boolean cellValue = cell.getBooleanCellValue();
            value = "false";
            if (cellValue)
                value = "true";
        } else {
            logger.info("Celltype not captured " + type);
        }
        return value.trim();
}

    /**
     * Guess whether given file is binary. Just checks for anything under 0x09.
     *
     * @param in
     */
    public static boolean isBinaryFile(BufferedReader in) throws IOException {
        int size = 1024; // in.available();
        if (size > 1024) size = 1024;
        char[] data = new char[size];

        in.read(data);
        in.close();

        int ascii = 0;
        int other = 0;

        for (int i = 0; i < data.length; i++) {
            char b = data[i];
            if (b < 0x09) return true;

            if (b == 0x09 || b == 0x0A || b == 0x0C || b == 0x0D) ascii++;
            else if (b >= 0x20 && b <= 0x7E) ascii++;
            else other++;
        }

        if (other == 0) return false;

        return 100 * other / (ascii + other) > 95;
    }
}
