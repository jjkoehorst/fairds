package nl.fairbydesign.backend.kubernetes;

import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.BatchV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import nl.fairbydesign.backend.data.yaml.Workflow;
import nl.fairbydesign.backend.irods.Connection;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileInputStream;
import org.irods.jargon.core.query.AVUQueryElement;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.query.MetaDataAndDomainData;
import org.irods.jargon.core.query.QueryConditionOperators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Kubernetes class for submitting jobs to a kubernetes compute cluster for job processing
 */
public class Kubernetes {
    private static final Logger log = LoggerFactory.getLogger(Kubernetes.class);

    public static V1Job main(String yamlFile) throws Exception {

        // Requires the kube config file
        ApiClient client = Config.defaultClient();
        client.setDebugging(true);
        Configuration.setDefaultApiClient(client);

        V1Job v1Job = generateKubernetesJobObject(yamlFile);

        // v1Job.setKind("my-kind");
        BatchV1Api batchV1Api = new BatchV1Api();

        // Submit the job to nl.wur.ssb.kubernetes
        log.info("Dispatching kubernetes job");

        batchV1Api.createNamespacedJob("unlock", v1Job, "", null, null);
        batchV1Api.createNamespacedJobCall("unlock", v1Job, null, null, null, null);

        return v1Job;
    }

    private static V1Job generateKubernetesJobObject(String yamlFile) throws JargonException, JargonQueryException, IOException {
        Connection connection = new Connection();

        // Load yaml file for cores and memory restrictions?
        log.info("Loading ." + yamlFile);

        Workflow workflow = load(connection, yamlFile);
        log.info("Memory set to " + workflow.memory);

        String uuid = UUID.randomUUID().toString();
        String yamlName = new File(yamlFile).getName();
        V1PodTemplateSpec v1PodTemplateSpec = new V1PodTemplateSpec();

        // Defines the docker container
        V1Container containerItem = new V1Container();
        containerItem.name(uuid + "_" + yamlName.replaceAll(" +","_"));
        containerItem.image("docker-registry.wur.nl/unlock/docker:kubernetes");

        V1ResourceRequirements resource = new V1ResourceRequirements();
        // Set limits to the job
        Map<String, Quantity> limit = new HashMap<>();
        Quantity quantity_mem = new Quantity(workflow.memory + "Mi");
        Quantity quantity_cpu = new Quantity(workflow.threads * 1000 + "m");
        limit.put("memory", quantity_mem);
        limit.put("cpu", quantity_cpu);
        resource.limits(limit);

        // Set requests for the job
        Map<String, Quantity> request = new HashMap<>();
        quantity_mem = new Quantity(workflow.memory + "Mi");
        quantity_cpu = new Quantity(workflow.threads * 1000 + "m");
        request.put("memory", quantity_mem);
        request.put("cpu", quantity_cpu);
        resource.requests(request);

        containerItem.resources(resource);

        // List of arguments for the command during startup
        List<String> args = new ArrayList<>();
        // Execute.sh should sync the collection after the run
        args.add("/run.sh");
        args.add("-c");

        // Obtain metadata from file object?
        List<AVUQueryElement> avuQueryElements = new ArrayList<>();
        avuQueryElements.add(AVUQueryElement.instanceForValueQuery(AVUQueryElement.AVUQueryPart.ATTRIBUTE, QueryConditionOperators.EQUAL, "cwl"));
        List<MetaDataAndDomainData> metaDataAndDomainData = connection.getAccessObjectFactory().getDataObjectAO(connection.getIrodsAccount()).findMetadataValuesForDataObjectUsingAVUQuery(avuQueryElements, yamlFile);
        String cwlFile = metaDataAndDomainData.get(0).asAvu().getValue();

        log.info("CWL: " + cwlFile);
        if (cwlFile != null) {
            // Using the metadata field (required) for cwl identification
            args.add(cwlFile);
        } else if (yamlFile.contains("phyloseq")) {
            // Requires the clustalo workflow
            args.add("/" + connection.getIrodsAccount().getZone() + "/infrastructure/cwl/clustalo/clustalo.cwl");
        } else {
            // The project manager workflow
            args.add("/" + connection.getIrodsAccount().getZone() + "/infrastructure/cwl/irods/irods_manager.cwl");
        }
        args.add("-y");
        args.add(yamlFile);
        args.add("-p");
        args.add("false");
        containerItem.args(args);

        // Base command
        List<String> command = new ArrayList<>();
        command.add("bash");
        containerItem.command(command);


        // TESTING TO SEE CAN WE MOUNT LOCAL DIRS?
        containerItem.addVolumeMountsItem(new V1VolumeMount().name("unlock").mountPath("/unlock"));

        // HashMap
        HashMap<String, String> envMap = new HashMap<>();

        envMap.put("PATH", "/root/.sdkman/candidates/maven/current/bin:/root/.sdkman/candidates/java/current/bin:/root/.sdkman/candidates/gradle/current/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin");

        List<V1EnvVar> env = new ArrayList();
        for (String key : envMap.keySet()) {
            V1EnvVar v1EnvVar = new V1EnvVar();
            v1EnvVar.setName(key);
            v1EnvVar.setValue(envMap.get(key));
            env.add(v1EnvVar);
        }
        containerItem.env(env);

        // Set the name
        containerItem.name("cwl-" + new File(yamlFile).getName().toLowerCase().replaceAll("[ _\\.]", ""));

        // Setting secrets
        containerItem.addEnvItem(setSecret("irodsHost","unlock-secret", "irodsHost"));
        containerItem.addEnvItem(setSecret("irodsPort","unlock-secret", "irodsPort"));
        containerItem.addEnvItem(setSecret("irodsUserName","unlock-secret", "irodsUserName"));
        containerItem.addEnvItem(setSecret("irodsZone","unlock-secret", "irodsZone"));
        containerItem.addEnvItem(setSecret("irodsAuthScheme","unlock-secret", "irodsAuthScheme"));
        containerItem.addEnvItem(setSecret("irodsHome","unlock-secret", "irodsHome"));
        containerItem.addEnvItem(setSecret("irodsCwd","unlock-secret", "irodsCwd"));
        containerItem.addEnvItem(setSecret("irodsPassword","unlock-secret", "irodsPassword"));
        containerItem.addEnvItem(setSecret("irodsSSL","unlock-secret", "irodsSSL"));

        // Job specifications
        V1JobSpec specs = new V1JobSpec();
        specs.ttlSecondsAfterFinished(1000);
        specs.setTtlSecondsAfterFinished(1000);
        specs.setBackoffLimit(1);

        // 60 min runtime, disabled as when queue is large jobs get killed when not even started
        // specs.activeDeadlineSeconds((long) 3600);
        specs.template(v1PodTemplateSpec);

        V1PodSpec v1PodSpec = new V1PodSpec();
        // https://rancher.com/docs/k3s/latest/en/storage/
        v1PodSpec.addVolumesItem(new V1Volume().name("unlock").persistentVolumeClaim(new V1PersistentVolumeClaimVolumeSource().claimName("unlock")));
        v1PodSpec.addContainersItem(containerItem);
        v1PodSpec.restartPolicy("Never");
        v1PodTemplateSpec.spec(v1PodSpec);
//        spec2.setPriority(1000);

        V1ObjectMeta v1ObjectMeta = new V1ObjectMeta();
        v1ObjectMeta.generateName("cwl-" + new File(yamlFile).getName().toLowerCase().replaceAll("[ _\\.]", "") +"-");

        V1Job v1Job = new V1Job();
        v1Job.apiVersion("batch/v1");
        v1Job.kind("Job");
        v1Job.metadata(v1ObjectMeta);
        v1Job.spec(specs);

        return v1Job;
    }

    private static Workflow load(Connection connection, String yamlFile) throws IOException, JargonException {
        IRODSFile irodsFile = connection.getFileFactory().instanceIRODSFile(yamlFile);
        IRODSFileInputStream fis = connection.getFileFactory().instanceIRODSFileInputStream(irodsFile);

        // YamlReader reader = new YamlReader();
        Representer representer = new Representer();
        representer.getPropertyUtils().setSkipMissingProperties(true);
        Yaml yaml = new Yaml(new Constructor(Workflow.class),representer);
        Workflow workflow = yaml.load(new InputStreamReader(fis));
        // Workflow workflow = (Workflow) reader.read();
        return workflow;
    }

    private static V1EnvVar setSecret(String name, String secretName, String secretKey) {
        // Container secrets? TODO validate...
        V1EnvVarSource v1EnvVarSource = new V1EnvVarSource();
        V1SecretKeySelector v1SecretKeySelector = new V1SecretKeySelector();
        v1SecretKeySelector.setName(secretName);
        v1SecretKeySelector.setKey(secretKey);
        v1EnvVarSource.secretKeyRef(v1SecretKeySelector);

        V1EnvVar v1EnvVar = new V1EnvVar();
        v1EnvVar.setValueFrom(v1EnvVarSource);
        v1EnvVar.setName(name);
        return v1EnvVar;
    }
}

