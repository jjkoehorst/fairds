package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static nl.fairbydesign.backend.ApplicationServiceInitListener.metadataSet;

@Route(value = "terms", layout = MainView.class)
@PageTitle("Terms")
@CssImport("./styles/views/empty/empty-view.css")
public class TermsView extends Div {

    public TermsView() {

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        // TODO make tab with multiple help elements

        Grid<Metadata> grid = new Grid<>(Metadata.class, false);
        Grid.Column<Metadata> sheetNameColumn = grid.addColumn(Metadata::getSheetName);
        Grid.Column<Metadata> packageNameColumn = grid.addColumn(Metadata::getPackageName);
        Grid.Column<Metadata> labelColumn = grid.addColumn(Metadata::getLabel);
        Grid.Column<Metadata> exampleColumn = grid.addColumn(Metadata::getExample);
        Grid.Column<Metadata> patternColumn = grid.addColumn(Metadata::getRegex);

        List<Metadata> metadataList = new ArrayList<>();
        metadataSet.forEach((s, metadatas) -> {
            metadatas.forEach(metadata -> {
                metadata.setSheetName(s);
                metadataList.add(metadata);
            });
        });

        ListDataProvider<Metadata> dataProvider = new ListDataProvider<>(metadataList);

        // grid.setItems(metadataList);
        grid.setDataProvider(dataProvider);
        grid.getHeaderRows().clear();

        HeaderRow filterRow = grid.appendHeaderRow();

        // Sheet filter
        TextField sheetField = new TextField();
        sheetField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getSheetName(), sheetField.getValue())));
        sheetField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(sheetNameColumn).setComponent(sheetField);
        sheetField.setSizeFull();
        sheetField.setPlaceholder("Filter sheet name");

        // Label filter
        TextField labelField = new TextField();
        labelField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getLabel(), labelField.getValue())));
        labelField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(labelColumn).setComponent(labelField);
        labelField.setSizeFull();
        labelField.setPlaceholder("Filter field name");

        // Package filter
        TextField packageField = new TextField();
        packageField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getPackageName(), packageField.getValue())));
        packageField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(packageNameColumn).setComponent(packageField);
        packageField.setSizeFull();
        packageField.setPlaceholder("Filter package name");

        // Package filter
        TextField exampleField = new TextField();
        exampleField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getExample(), exampleField.getValue())));
        exampleField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(exampleColumn).setComponent(exampleField);
        exampleField.setSizeFull();
        exampleField.setPlaceholder("Filter example");

        // Package filter
        TextField regexField = new TextField();
        regexField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getRegex().pattern(), regexField.getValue())));
        regexField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(patternColumn).setComponent(regexField);
        regexField.setSizeFull();
        regexField.setPlaceholder("Filter pattern");

        final VerticalLayout bottomPanel = new VerticalLayout();

        add(grid, bottomPanel);

        Html rawdocContent = WebGeneric.getTextFromResource("views/terms.html");

        grid.addItemClickListener(clicked -> {
            Metadata metadata = clicked.getItem();
            Notification.show("Clicked " + metadata.getLabel());
            // Update the bottom part...
            //bottomPanel.removeAll();
            String htmlCode = rawdocContent.getInnerHtml();
            htmlCode = fixHtml(htmlCode,"LABEL", metadata.getLabel());
            htmlCode = fixHtml(htmlCode, "LABEL", metadata.getLabel());
            htmlCode = fixHtml(htmlCode, "REQUIREMENT", metadata.getRequirement());
            htmlCode = fixHtml(htmlCode, "DEFINITION", metadata.getDefinition());
            htmlCode = fixHtml(htmlCode, "EXAMPLE", metadata.getExample());
            htmlCode = fixHtml(htmlCode, "UNITS", metadata.getPreferredUnit());
            htmlCode = fixHtml(htmlCode, "SYNTAX", metadata.getSyntax());
            htmlCode = fixHtml(htmlCode, "PACKAGEID", metadata.getPackageIdentifier());
            htmlCode = fixHtml(htmlCode, "PACKAGENAME", metadata.getPackageName());
            htmlCode = fixHtml(htmlCode, "LINK", metadata.getURL());

            Html docContent = new Html(htmlCode);
            Div docs = new Div();
            docs.setMaxWidth(80, Unit.PERCENTAGE);
            docs.add(docContent);
            bottomPanel.removeAll();
            bottomPanel.add(docs);
            bottomPanel.setHorizontalComponentAlignment(FlexComponent.Alignment.AUTO, docs);
            add(bottomPanel);

        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }

    private String fixHtml(String htmlCode, String label, String value) {
        if (value != null)
            return htmlCode.replaceAll(label, value);
        else {
            return htmlCode.replaceAll(".*"+label+".*", "");
        }
    }


    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
