package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.views.main.MainView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Route(value = "setup", layout = MainView.class)
@PageTitle("Setup")
@CssImport("./styles/views/empty/empty-view.css")
public class Setup extends Div {

    public Setup() {
        // Setup
        Html docContent = WebGeneric.getTextFromResource("views/setup.html");
        Div docs = new Div();
        docs.setMaxWidth(50, Unit.PERCENTAGE);
        docs.add(docContent);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(docs);
        verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, docs);
        add(verticalLayout);


        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}
