package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.views.main.MainView;

@Route(value = "faq", layout = MainView.class)
@PageTitle("F.A.Q")
@CssImport("./styles/views/empty/empty-view.css")
public class FAQ extends Div {

    public FAQ() {
        // Setup
        Html docContent = WebGeneric.getTextFromResource("views/faq.html");
        Div docs = new Div();
        docs.setMaxWidth(50, Unit.PERCENTAGE);
        docs.add(docContent);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(docs);
        verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, docs);
        add(verticalLayout);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}
