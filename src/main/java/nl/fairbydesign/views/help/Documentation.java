package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static nl.fairbydesign.backend.ApplicationServiceInitListener.metadataSet;
import static nl.fairbydesign.views.main.MainView.createTab;

@Route(value = "docs", layout = MainView.class)
@PageTitle("Docs")
@CssImport("./styles/views/about/about-view.css")
public class Documentation extends Div {

    public Documentation() {
        setId("about-view");
        Html docContent = WebGeneric.getTextFromResource("views/docs.html");
        Div docs = new Div();
        docs.setMaxWidth(50, Unit.PERCENTAGE);
        docs.add(docContent);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(docs);
        verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, docs);
        add(verticalLayout);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}
