package nl.fairbydesign.views.config;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.ApplicationServiceInitListener;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.views.main.MainView;
import org.jboss.logging.Logger;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The config view to edit elements in the config file
 */
@Route(value = "config", layout = MainView.class)
@PageTitle("Config view")
@CssImport("./styles/views/empty/empty-view.css")
public class Config extends Div {
    public static final Logger logger = Logger.getLogger(Config.class);
    private ArrayList<TextField> fields = new ArrayList<>();
    public Config() {
        setId("master-detail-view");
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");
        // Some check that this user is allowed to enter
        String url = ((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerName();
        if (credentials != null && credentials.isSuccess() || url.contains("localhost")) {
            Button saveButton = new Button("Save changes");
            add(saveButton);

            nl.fairbydesign.backend.Config config = ApplicationServiceInitListener.getConfig();

            HashMap<String, ArrayList> configMap = new HashMap<>();
            try {
                for (PropertyDescriptor propertyDescriptor :
                        Introspector.getBeanInfo(nl.fairbydesign.backend.Config.class, Object.class).getPropertyDescriptors()) {
                    Method readMethod = propertyDescriptor.getReadMethod();
                    Method writeMethod = propertyDescriptor.getWriteMethod();
                    ArrayList<Method> methods = new ArrayList<>();
                    methods.add(readMethod);
                    methods.add(writeMethod);
                    configMap.put(readMethod.getName().replaceFirst("get", ""), methods);
                }
                fields = new ArrayList<>();
                for (Object key : configMap.keySet().stream().sorted().toArray()) {
                    TextField labelField = new TextField(String.valueOf(key));
                    Method readMethod = (Method) configMap.get(key).get(0);
                    labelField.setValue(String.valueOf(readMethod.invoke(config, (Object[]) null)));
                    labelField.setId(readMethod.getName());
                    fields.add(labelField);
                }
                VerticalLayout verticalLayout = new VerticalLayout();
                verticalLayout.add(fields.toArray(new TextField[0]));
                add(verticalLayout);
            } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
            saveButton.addClickListener(clicked -> {
                // Update config object
                try {
                    for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(nl.fairbydesign.backend.Config.class, Object.class).getPropertyDescriptors()) {
                        Method readMethod = propertyDescriptor.getReadMethod();
                        Method writeMethod = propertyDescriptor.getWriteMethod();
                        for (TextField field : fields) {
                            String value = field.getValue();
                            String id = field.getId().get();
                            // Read method names match
                            if (readMethod.getName().matches("get" + id)) {
                                String parameterType = writeMethod.getParameterTypes()[0].toString();
                                if (parameterType.endsWith("String"))
                                    writeMethod.invoke(config, value);
                                else if (parameterType.endsWith("int"))
                                    writeMethod.invoke(config, Integer.parseInt(value));
                                else if (parameterType.endsWith("boolean"))
                                    writeMethod.invoke(config, Boolean.parseBoolean(value));
                                else
                                    logger.error("UNKNOWN TYPE " + parameterType);
                            }
                        }
                        // Update config file
                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                        String json = gson.toJson(config);
                        File configFile = new File(System.getProperties().get("user.home") + "/.fairds/config.json");
                        PrintWriter printWriter = new PrintWriter(configFile);
                        printWriter.println(json);
                        printWriter.close();

                    }
                } catch (IntrospectionException | IllegalAccessException | InvocationTargetException | FileNotFoundException e) {
                    e.printStackTrace();
                }
            });
        } else {
            Text text = new Text("Config environment only available on a local machine. The config file is located in the user.home/.fairds/config.json.");
            add(text);
        }
    }
}
