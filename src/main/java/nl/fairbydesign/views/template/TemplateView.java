package nl.fairbydesign.views.template;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import nl.fairbydesign.backend.ApplicationServiceInitListener;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.data.objects.*;
import nl.fairbydesign.backend.data.objects.jerm.Investigation;
import nl.fairbydesign.backend.data.objects.jerm.ObservationUnit;
import nl.fairbydesign.backend.data.objects.jerm.Project;
import nl.fairbydesign.backend.data.objects.jerm.Study;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.parsers.ExcelGenerator;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.File;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;
import static nl.fairbydesign.backend.WebGeneric.getFileContentFromResources;
import static nl.fairbydesign.backend.parsers.ExcelGenerator.makeSheetOnly;

/**
 * The view to create an Excel file to fill in project metadata
 */
@Route(value = "template", layout = MainView.class)
@PageTitle("Metadata Configurator")
@CssImport("./styles/views/empty/empty-view.css")
public class TemplateView extends Div {

    public static final Logger logger = LogManager.getLogger(TemplateView.class);
    private final Button buttonDownload;

    // Obtain the metadata information which is loaded on startup
    public HashMap<String, ArrayList<Metadata>> metadataSet = ApplicationServiceInitListener.getMetadata();
    // Create empty person grid list
    private final List<Member> memberList = new ArrayList<>();
    // public static final Logger logger = Logger.getLogger(AccordionForm.class);

    // Variables needed for the Excel sheet
    Binder<Project> projectBinder = new Binder<>();
    Binder<Investigation> investigationBinder = new Binder<>();
    Binder<Study> studyBinder = new Binder<>();
    Binder<ObservationUnit> observationUnitBinder = new Binder<>();

    // Grids needed for Sample / Assay objects
    ArrayList<Grid> gridList = new ArrayList<>();

    // Package names for 'observation unit', 'sample' and 'assay'
    ArrayList<String> packages = new ArrayList<>(Arrays.asList("Search a package", "Search a package", "Search a package"));
    private Checkbox privacyCheckBox;

    /**
     * Creates the template generator page
     */
    public TemplateView() {
        // Setup interface
        Accordion accordionPIS = new Accordion();
        accordionPIS.setWidth("80%");
        accordionPIS.getElement().getStyle().set("border", "2px solid #990099");

        Accordion accordionOSA = new Accordion();
        accordionOSA.setWidth("80%");
        accordionOSA.getElement().getStyle().set("border", "2px solid #0080ff");
        accordionOSA.close();

        // Adding project information form
        VerticalLayout projectInformation = new VerticalLayout();
        createProject(projectInformation);
        accordionPIS.add("Project Information", projectInformation).addThemeVariants(DetailsVariant.FILLED);

        // Adding investigation information form
        VerticalLayout InvestigationInformation = new VerticalLayout();
        createInvestigationLayout(InvestigationInformation);
        accordionPIS.add("Investigation Information", InvestigationInformation);

        // study form
        VerticalLayout studyInformation = new VerticalLayout();
        createStudy(studyInformation);
        accordionPIS.add("Study Information", studyInformation);

        // Observation Unit form with package selection
        VerticalLayout observationUnitInformation = new VerticalLayout();
        // createObservationUnit(observationUnitInformation);
        createPackageSelectionGrids(observationUnitInformation, "ObservationUnit", "help/observationUnit.txt", true);
        accordionOSA.add("Observation Unit Information", observationUnitInformation);

        // Sample form with package selection
        VerticalLayout sampleInformation = new VerticalLayout();
        createPackageSelectionGrids(sampleInformation, "Sample", "help/sample.txt", true);
        accordionOSA.add("Sample Information", sampleInformation);

        // Assay form with + one more button
        VerticalLayout assayLayout = new VerticalLayout();
        createPackageSelectionGrids(assayLayout, "Assay", "help/assay/assay.txt", false);
        accordionOSA.add("Assay Information", assayLayout);

        // Generate and "download" button
        Button buttonGenerate = new Button("Generate workbook");
        Button buttonGenerate2 = new Button("Generate workbook");

        // Placeholder for the automatic download click
        buttonDownload = new Button("");

        // Hide download button
        buttonDownload.addClickListener(clicked -> {
            // Cannot do it directly so make it invisible via a listener
            buttonDownload.setVisible(false);
        });

        buttonGenerate2.addClickListener(e -> {
            buttonGenerate.click();
        });

        buttonGenerate.addClickListener(e -> {
            // Close both views to make it easier to open the correct panel
            accordionOSA.close();
            accordionPIS.close();

            Project project = new Project();
            Investigation investigation = new Investigation();
            Study study = new Study();
            ObservationUnit observationUnit = new ObservationUnit();
            if (!privacyCheckBox.getValue()) {
                Notification.show("Privacy checkbox is not checked under investigation, we cannot continue otherwise");
                accordionPIS.open(1);
            } else if (
                    projectBinder.writeBeanIfValid(project) &&
                            investigationBinder.writeBeanIfValid(investigation) &&
                            studyBinder.writeBeanIfValid(study) &&
                            observationUnitBinder.writeBeanIfValid(observationUnit)
            ) {
                // Perform check if any of the assays has been enabled
                AtomicBoolean initialValidation = new AtomicBoolean(false);
                gridList.forEach(grid -> {
                    if (grid.isEnabled() && grid.getId().get().endsWith("Assay")) {
                        initialValidation.set(true);
                    } else {
                        // Assay selection check is disabled by true in the following line
                        initialValidation.set(true);
                    }
                });

                if (!initialValidation.get()) {
                    Notification.show("Please select at least one assay type for registration", 5000, Notification.Position.MIDDLE);
                }

                // Perform check if investigation contains at least 1 member
                if (memberList.size() == 0) {
                    initialValidation.set(false);
                    accordionPIS.open(1);
                    Notification.show("Please register at least one member under the investigation tab", 5000, Notification.Position.MIDDLE);
                }

                // Observation unit selection requirement
                AtomicBoolean secondValidation = new AtomicBoolean(false);
                if (packages.get(0).matches("Search a package")) {
                    secondValidation.set(false);
//                    Notification.show("Please select an observation unit package type before continuing");
                    // Opens the 4th (0-4) panel which at the moment is the observation unit panel
//                    accordionOSA.open(0);
                }

                // Sample selection requirement
                if (packages.get(1).matches("Search a package")) {
                    secondValidation.set(false);
//                    Notification.show("Please select a sample package type before continuing");
                    // Opens the 5th (0-4) panel which at the moment is the sample panel
//                    accordionOSA.open(1);
                }

                // Assay selection requirement
                if (packages.get(2).matches("Search a package")) {
                    secondValidation.set(false);
//                    Notification.show("Please select an assay package type before continuing");
                    // Opens the 5th (0-4) panel which at the moment is the sample panel
//                    accordionOSA.open(2);
                }

                if (!secondValidation.get()) {
                    Notification notification = Notification.show("Packages have not been selected, you can export the sheets for Observation Unit, Sample and Assay at a later stage... and merge them with the generated excel file", 10000, Notification.Position.MIDDLE);
                    notification.addThemeVariants(NotificationVariant.LUMO_CONTRAST);
                }

                if (initialValidation.get()) {
                    HashMap<String, ArrayList<Metadata>> gridSelection = getHashMapFromGrid(gridList);
                    File excelFile = ExcelGenerator.generateForm(project, investigation, memberList, study, observationUnit, gridSelection, packages.get(1));

                    FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(new StreamResource(excelFile.getName(), () -> createResource(excelFile)));
                    buttonWrapper.wrapComponent(buttonDownload);
                    // Adding the button wrapper to the interface
                    add(buttonWrapper);
                    buttonDownload.clickInClient();
                }
            } else {
                String message = null;
                if (projectBinder.validate().hasErrors()) {
                    message = "The project tab has not been filled in properly, please have a look and try again";
                    accordionPIS.open(0);
                } else if (investigationBinder.validate().hasErrors()) {
                    message = "The investigation tab has not been filled in properly, please have a look and try again";
                    accordionPIS.open(1);
                } else if (studyBinder.validate().hasErrors()) {
                    message = "The study tab has not been filled in properly, please have a look and try again";
                    accordionPIS.open(2);
                } else if (observationUnitBinder.validate().hasErrors()) {
                    message = "The observation unit tab has not been filled in properly, please have a look and try again";
                    accordionOSA.open(0);
                } else {

                }

                if (message != null) {
                    // TODO make a proper CSS file

                    // @formatter:off
                    String styles = ".my-style { "
                            + "  color: red;"
                            + " }";
                    // @formatter:on

                    /*
                     * The code below register the style file dynamically. Normally you
                     * use @StyleSheet annotation for the component class. This way is
                     * chosen just to show the style file source code.
                     */
                    StreamRegistration resource = UI.getCurrent().getSession()
                            .getResourceRegistry()
                            .registerResource(new StreamResource("styles.css", () -> {
                                byte[] bytes = styles.getBytes(StandardCharsets.UTF_8);
                                return new ByteArrayInputStream(bytes);
                            }));
                    UI.getCurrent().getPage().addStyleSheet(
                            "base://" + resource.getResourceUri().toString());

                    Div content = new Div();
                    content.addClassName("my-style");
                    content.setText(message);

                    Notification notification = new Notification(content);
                    notification.setDuration(3000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();
                }
            }
        });

        // Finalise accordion and align center
        Div htmlSection1 = new Div();
        htmlSection1.setWidth("80%");
        htmlSection1.add(getHelpText("help/configurator_section1.txt"));

        Div htmlSection2 = new Div();
        htmlSection2.setWidth("80%");
        htmlSection2.add(getHelpText("help/configurator_section2.txt"));

        Div htmlSection3 = new Div();
        htmlSection3.setWidth("80%");
        htmlSection3.add(getHelpText("help/configurator_section3.txt"));

        Div htmlSection4 = new Div();
        htmlSection4.setWidth("80%");
        htmlSection4.add(getHelpText("help/configurator_section4.txt"));

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(htmlSection1, accordionPIS, buttonGenerate2, htmlSection2, accordionOSA, htmlSection3, buttonGenerate, htmlSection4);
        verticalLayout.setWidthFull();
        verticalLayout.setDefaultHorizontalComponentAlignment(CENTER);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        verticalLayout.add(footer);

        add(verticalLayout);
        // add(buttonDownload);
    }

    private void createPackageSelectionGrids(VerticalLayout packageLayout, String metadata, String help, boolean visible) {
        // assay layout is the overall placeholder with a button to add more assay layouts
        ArrayList<VerticalLayout> verticalLayouts = new ArrayList<>();
        Button addTemplate = new Button("Add template");
        // Button removeAssayButton = new Button("Remove assay");

        // Holder for a specific assay
        VerticalLayout verticalLayout = new VerticalLayout();
        createPackageSelectionGrid(verticalLayout, metadata, help, true);
        verticalLayouts.add(verticalLayout);
        HorizontalLayout assayButtons = new HorizontalLayout();
        assayButtons.add(addTemplate);
        packageLayout.add(assayButtons);
        packageLayout.add(verticalLayouts.get(0));

        // Button to add another assay to the assay layout
        addTemplate.addClickListener(clicked -> {
            // Add another assay!
            createPackageSelectionGrid(verticalLayout, metadata, help, true);
            verticalLayouts.add(verticalLayout);
            packageLayout.add(verticalLayout);
        });


        // Button to remove another assay from the assay layout
//        removeAssayButton.addClickListener(clicked -> {
//            if (verticalLayouts.size() > 0) {
//                // Remove the last layout and grid
//                VerticalLayout verticalLayout = verticalLayouts.get(verticalLayouts.size() - 1);
//                gridList.remove(gridList.size() -1);
//                packageLayout.remove(verticalLayout);
//                verticalLayouts.remove(verticalLayouts.size() - 1);
//            }
//        });
    }
    /**
     * Creating the package selection interface based on multiple environmental packages according to MIxS
     *
     * @param verticalLayout  the verticalLayout to store all the wrappers with the grid and buttons in
     * @param metadata the metadata object
     * @param help     general help information
     * @param visible  make this visible or not
     */
    private void createPackageSelectionGrid(VerticalLayout verticalLayout, String metadata, String help, boolean visible) {
        VerticalLayout wrapper = new VerticalLayout();
        // User specific items
        // ArrayList<Metadata> userList = new ArrayList<>();
        // Obligatory items
        ArrayList<Metadata> obligatoryList = new ArrayList<>();
        // All items using an atomic reference to be able to modify it within
        AtomicReference<ArrayList<Metadata>> items = new AtomicReference<>(new ArrayList<>());

        // Setup grid
        Grid<Metadata> gridSelection = new Grid<>(Metadata.class);
        gridSelection.removeAllColumns();

        Grid.Column<Metadata> packageNameColumn = gridSelection.addColumn(Metadata::getPackageName).setHeader("Package");
        Grid.Column<Metadata> labelColumn = gridSelection.addColumn(Metadata::getLabel).setHeader("Label");
        Grid.Column<Metadata> syntaxColumn = gridSelection.addColumn(Metadata::getSyntax).setHeader("Syntax");
        Grid.Column<Metadata> exampleColumn = gridSelection.addColumn(Metadata::getExample).setHeader("Example");
        Grid.Column<Metadata> definitionColumn = gridSelection.addColumn(Metadata::getDefinition).setHeader("Definition");

        gridSelection.setSelectionMode(Grid.SelectionMode.MULTI);
        gridSelection.asMultiSelect().select(obligatoryList);
        gridSelection.setId(metadata);
        gridSelection.setItems(items.get());
        gridSelection.asMultiSelect().select(obligatoryList);
        gridSelection.setEnabled(visible);
        gridSelection.setVisible(visible);

        // Add filters
        AtomicReference<ListDataProvider<Metadata>> dataProvider = new AtomicReference<>(new ListDataProvider<>(items.get()));

        HeaderRow filterRow = gridSelection.appendHeaderRow();
        // Label filter
        TextField labelFieldFilter = new TextField();
        labelFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getLabel(), labelFieldFilter.getValue())));
        labelFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(labelColumn).setComponent(labelFieldFilter);
        labelFieldFilter.setSizeFull();
        labelFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField packageFieldFilter = new TextField();
        packageFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getPackageName(), packageFieldFilter.getValue())));
        packageFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(packageNameColumn).setComponent(packageFieldFilter);
        packageFieldFilter.setSizeFull();
        packageFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField exampleFieldFilter = new TextField();
        exampleFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getExample(), exampleFieldFilter.getValue())));
        exampleFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(exampleColumn).setComponent(exampleFieldFilter);
        exampleFieldFilter.setSizeFull();
        exampleFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField syntaxFieldFilter = new TextField();
        syntaxFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getSyntax(), syntaxFieldFilter.getValue())));
        syntaxFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(syntaxColumn).setComponent(syntaxFieldFilter);
        syntaxFieldFilter.setSizeFull();
        syntaxFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField definitionFieldFilter = new TextField();
        definitionFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getDefinition(), definitionFieldFilter.getValue())));
        definitionFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(definitionColumn).setComponent(definitionFieldFilter);
        definitionFieldFilter.setSizeFull();
        definitionFieldFilter.setPlaceholder("Filter");

        // When grid changes select obligatory list (when an obligatory element is unselected it is selected again)
        gridSelection.asMultiSelect().addValueChangeListener(event -> {
            // Check if the package was selected
            gridSelection.asMultiSelect().select(obligatoryList);
            logger.debug("Selected " + gridSelection.getSelectedItems().size() + " items");
        });

        TextField labelField = new TextField();

        labelField.setEnabled(visible);
        labelField.setVisible(visible);
//        Button addButton = new Button("Add field", new Icon(VaadinIcon.ARROW_LEFT));
//        addButton.setEnabled(visible);
//        addButton.setVisible(visible);

        Button exportButton = new Button("Export", new Icon(VaadinIcon.FILE));
        exportButton.setEnabled(visible);
        exportButton.setVisible(visible);

        // Export this directly to a sheet...
        exportButton.addClickListener(buttonClickEvent -> exportSheet(gridSelection));

        Button removeAssayButton = new Button("Remove", new Icon(VaadinIcon.TRASH));

        removeAssayButton.addClickListener(buttonClickEvent -> {
            Notification.show("Clicked on  button: " + removeAssayButton.getId());
            verticalLayout.remove(wrapper);
        });

        // List box for different environments
        ComboBox<String> listBox = new ComboBox<>();

        ArrayList<String> packagesViewList = new ArrayList<>();
        // Add select a package and core at the top
        packagesViewList.add("Search a package");
        packagesViewList.add("default");
        // Add all packages from the metadata file as an ordered list according to the metadata Excel file
        ArrayList<String> subPackages = new ArrayList<>();

        // Add metadata fields from selected package
        for (Metadata m : metadataSet.get(metadata)) {
            // Skip core package as it is manually added
            if (m.getPackageName().matches("default")) continue;
            if (subPackages.contains(m.getPackageName())) continue;
            subPackages.add(m.getPackageName());
        }

        // Add all other packages in a sorted fashion
        packagesViewList.addAll(subPackages); // .stream().collect(Collectors.toList()));
        listBox.setItems(packagesViewList);
        listBox.setVisible(visible);
        listBox.setEnabled(visible);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(exportButton, removeAssayButton, listBox); // labelField, addButton,

        // Listener for package selection
        listBox.addValueChangeListener(listener -> {
            // Ensure erase when new type is being selected
            obligatoryList.removeAll(obligatoryList);
            if (metadata.matches("ObservationUnit"))
                packages.set(0, listener.getValue());
            if (metadata.matches("Sample"))
                packages.set(1, listener.getValue());
            if (metadata.matches("Assay"))
                packages.set(2, listener.getValue());

            // TODO add user and core previously selected elements
            // Remove all previous selections
            gridSelection.deselectAll();
            // Reset item list
            items.set(new ArrayList<>());
            // Only show items when a true package is selected
            if (!listener.getValue().contains("Search a package")) {
                // Add all user items when package change occurs
//                for (Metadata metadataUser : userList) {
//                    logger.info(">> User item:" + metadataUser.getSessionID() + metadataUser.getSessionID().equals(UI.getCurrent().getSession().getSession().getId()));
//                    if (metadataUser.getSessionID().equals(UI.getCurrent().getSession().getSession().getId())) {
//                        logger.info("User item:" + metadataUser.getLabel());
//                        items.get().add(metadataUser);
//                    }
//                }

                // Add the metadata elements from selected package
                for (Metadata m : metadataSet.get(metadata)) {
                    // Add all elements matching the package, core or user specified values
                    logger.info("Package: " + m.getPackageName() + " " + listBox.getValue() + " " + m.getPackageName().matches(listBox.getValue()));
                    // User session specific input
//                    if (m.getPackageName().matches("user")) {
//                        if (m.getSessionID().equals(UI.getCurrent().getSession().getSession().getId())) {
//                            items.get().add(m);
//                            obligatoryList.add(m);
//                        }
//                    }
                    // General check for core and selected package
                    if (m.getPackageName().matches(listBox.getValue()) || m.getPackageName().matches("default")) {
                        items.get().add(m);
                        // Obligatory items to be added to list for selection
                        if (m.getRequirement().toLowerCase().trim().contains("mandatory")) {
                            obligatoryList.add(m);
                        }
                    }
                }

                if (metadata.toLowerCase().matches(".*assay$")) {
                    for (Metadata m : metadataSet.get("Assay")) {
                        // Add all elements matching the package, core or user specified values
                        if (m.getPackageName().matches(listBox.getValue()) || m.getPackageName().matches("default")) { // || m.getPackageName().matches("user")
                            items.get().add(m);
                            // Obligatory items to be added to list for selection
                            if (m.getRequirement().toLowerCase().trim().contains("mandatory")) {
                                obligatoryList.add(m);
                            }
                        }
                    }
                }
            }

            // Check for duplicate items?
            ArrayList<Metadata> uniqueList = new ArrayList<>();
            for (Metadata metadata1 : items.get()) {
                boolean skip = false;
                for (Metadata metadata2 : uniqueList) {
                    if (metadata1.getLabel().equals(metadata2.getLabel())) {
                        skip = true;
                    }
                }
                if (!skip) {
                    uniqueList.add(metadata1);
                }
            }
            // Only retain the final list?...
            logger.info("Unique list");
            for (Metadata metadata1 : uniqueList) {
                logger.info(metadata1.getPackageName() + " " + metadata1.getLabel());
            }
            items.get().removeAll(items.get());
            items.get().addAll(uniqueList);

            // Change label of grid based on package including its type
            gridSelection.setId(metadata + " - " + listener.getValue());
            wrapper.setId(metadata + " - " + listener.getValue());
            // Add the items and select the obligatory items
            dataProvider.set(new ListDataProvider<>(items.get()));
            // grid.setItems(metadataList);
            gridSelection.setDataProvider(dataProvider.get());
            // gridSelection.setItems(items.get());
            // Select obligatory items
            gridSelection.asMultiSelect().select(obligatoryList);
        });

        // Set default value to core if there are no other packages present
        listBox.setValue("Search a package");
        if (packagesViewList.size() == 2) {
            listBox.setValue("default");
        }

        // When clicked an additional sample thing is added to the grid
//        addButton.addClickListener(buttonClickEvent -> {
//            // Add to grid TODO automatically select the added element
//            Metadata gridSelectionUserAdded = new Metadata();
//            gridSelectionUserAdded.setLabel(labelField.getValue());
//            gridSelectionUserAdded.setDefinition("User added field");
//            gridSelectionUserAdded.setPackageName("user");
//            gridSelectionUserAdded.setRequirement("mandatory");
//            gridSelectionUserAdded.setSessionID(UI.getCurrent().getSession().getSession().getId());
//            metadataSet.get(metadata).add(gridSelectionUserAdded);
//            // Add to items to immediately update the grid
//            items.get().add(gridSelectionUserAdded);
//            // Add to user list
//            // userList.add(gridSelectionUserAdded);
//            gridSelection.getDataProvider().refreshAll();
//            gridSelection.select(gridSelectionUserAdded);
//        });

//        // Enables all fields
//        enableBox.addValueChangeListener(checkboxClickEvent -> {
//            if (checkboxClickEvent.getValue()) {
//                addButton.setEnabled(true);
//                gridSelection.setEnabled(true);
//                labelField.setEnabled(true);
//                exportButton.setEnabled(true);
//                listBox.setEnabled(true);
//
//                addButton.setVisible(true);
//                gridSelection.setVisible(true);
//                labelField.setVisible(true);
//                exportButton.setVisible(true);
//                listBox.setVisible(true);
//                enableBox.setLabel("Page enabled");
//            } else {
//                addButton.setEnabled(false);
//                gridSelection.setEnabled(false);
//                labelField.setEnabled(false);
//                exportButton.setEnabled(false);
//                listBox.setEnabled(false);
//
//                addButton.setVisible(false);
//                gridSelection.setVisible(false);
//                labelField.setVisible(false);
//                exportButton.setVisible(false);
//                listBox.setVisible(false);
//                // optionalText.setVisible(false);
//
//                enableBox.setLabel("Enable this page");
//            }
//        });

        // Help
        VerticalLayout header = new VerticalLayout();
        Html helpDetails = getHelpText(help);
        Div helpText = new Div();

        helpText.add(helpDetails);

        header.add(helpText, horizontalLayout, gridSelection);

        wrapper.add(header); // header text

        // List of grid objects according to the GridSelection object
        gridList.add(gridSelection);

        removeAssayButton.setId(String.valueOf(gridList.size()));
        // Finalise the vertical layout with the wrapper
        verticalLayout.add(wrapper);
    }

    private HashMap<String, ArrayList<Metadata>> getHashMapFromGrid(ArrayList<Grid> gridList) {
        HashMap<String, ArrayList<Metadata>> hashMap = new HashMap<>();
        for (Grid grid : gridList) {
            if (grid.isEnabled()) {
                logger.info("Grid selection: " + grid.getId().get());
                String key = grid.getId().get();
                ArrayList<Metadata> selection = new ArrayList<>();
                Iterator<Metadata> gridSelectionIterator = grid.getSelectedItems().iterator();
                while (gridSelectionIterator.hasNext()) {
                    Metadata gridSelection = gridSelectionIterator.next();
                    selection.add(gridSelection);
                    logger.info(gridSelection.getLabel());
                }
                hashMap.put(key, selection);
            }
        }
        return hashMap;
    }

    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void createProject(VerticalLayout wrapper) {
        FormLayout layoutWithBinder = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Create the fields for the project section
        TextField projectIdentifier = new TextField();
        projectIdentifier.setPlaceholder("e.g. NWO_UNLOCK");
        projectIdentifier.setValueChangeMode(ValueChangeMode.EAGER);

        TextField projectTitle = new TextField();
        projectTitle.setValueChangeMode(ValueChangeMode.EAGER);

        TextArea projectDescription = new TextArea();
        projectDescription.setWidthFull();
        projectDescription.setValueChangeMode(ValueChangeMode.EAGER);

        projectBinder.forField(projectIdentifier)
                .withValidator(new StringLengthValidator("Please add project identifier (4-25 characters)", 4, 25))
                .bind(Project::getIdentifier, Project::setIdentifier);

        projectBinder.forField(projectTitle)
                .withValidator(new StringLengthValidator("Please add a project title (>10 characters)", 10, null))
                .bind(Project::getTitle, Project::setTitle);

        projectBinder.forField(projectDescription)
                .withValidator(new StringLengthValidator("Please add the project description (>50 characters)", 50, null))
                .bind(Project::getDescription, Project::setDescription);


        projectIdentifier.setWidthFull();
        projectIdentifier.setLabel("Project Identifier");

        projectTitle.setWidthFull();
        projectTitle.setLabel("Project Title");

        projectDescription.setWidthFull();
        projectDescription.setLabel("Project Description");

        layoutWithBinder.add(projectIdentifier, 3);
        layoutWithBinder.add(projectTitle, 3);
        layoutWithBinder.add(projectDescription, 3);

        // Help
        Html helpDetails = getHelpText("help/project.txt");
        verticalLayout.add(layoutWithBinder, helpDetails);

        wrapper.add(verticalLayout);

        if (ApplicationServiceInitListener.getConfig().getDebug()) {
            projectIdentifier.setValue("PROJECT_ID");
            projectDescription.setValue("PROJECT DESCRIPTION THAT CONTAINS A LOT OF TEXT SO IT IS ACCEPTED BY THE VALIDATOR ETC ...");
            projectTitle.setValue("PROJECT TITLE INFORMATION");
        }
    }

    private void createInvestigationLayout(VerticalLayout wrapper) {
        FormLayout layoutWithBinder = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Create the fields for the project section
        TextField investigationIdentifier = new TextField();
        investigationIdentifier.setPlaceholder("e.g. NWO_UNLOCK");
        investigationIdentifier.setValueChangeMode(ValueChangeMode.EAGER);

        TextField investigationTitle = new TextField();
        investigationTitle.setValueChangeMode(ValueChangeMode.EAGER);

        TextArea investigationDescription = new TextArea();
        investigationDescription.setWidthFull();
        investigationDescription.setValueChangeMode(ValueChangeMode.EAGER);

        privacyCheckBox = new Checkbox("I understand that the metadata will be according to the GDPR requirements");

        // Already checked when in debug mode
        if (ApplicationServiceInitListener.debug) {
            privacyCheckBox.setValue(true);
        }

        investigationBinder.forField(investigationIdentifier)
                .withValidator(new StringLengthValidator("Please add a investigation identifier (5-25 characters)", 4, 25))
                .bind(Investigation::getIdentifier, Investigation::setIdentifier);

        investigationBinder.forField(investigationTitle)
                .withValidator(new StringLengthValidator("Please add a investigation title (>10 characters)", 10, null))
                .bind(Investigation::getTitle, Investigation::setTitle);

        investigationBinder.forField(investigationDescription)
                .withValidator(new StringLengthValidator("Please add the investigation description (>50 characters)", 50, null))
                .bind(Investigation::getDescription, Investigation::setDescription);

//        layoutWithBinder.addFormItem(investigationIdentifier, "Investigation Name");
//        layoutWithBinder.addFormItem(investigationTitle, "Investigation Title");
//        layoutWithBinder.addFormItem(investigationDescription, "Investigation Description");

        investigationIdentifier.setWidthFull();
        investigationIdentifier.setLabel("Investigation identifier");

        investigationTitle.setWidthFull();
        investigationTitle.setLabel("Investigation Title");

        investigationDescription.setWidthFull();
        investigationDescription.setLabel("Investigation Description");

        layoutWithBinder.add(investigationIdentifier, 3);
        layoutWithBinder.add(investigationTitle, 3);
        layoutWithBinder.add(investigationDescription, 3);

        // Help
        Html helpDetails = getHelpText("help/investigation.txt");
        verticalLayout.add(layoutWithBinder, privacyCheckBox, helpDetails);


        wrapper.add(verticalLayout);

        if (ApplicationServiceInitListener.getConfig().getDebug()) {
            investigationIdentifier.setValue("INV_ID");
            investigationDescription.setValue("INVESTIGATION DESCRIPTION THAT CONTAINS A LOT OF TEXT SO IT IS ACCEPTED BY THE VALIDATOR ETC ...");
            investigationTitle.setValue("INVESTIGATION TITLE INFORMATION");
        }

        /**
         * MEMBER SECTION
         */

        FormLayout layoutWithBinder2 = new FormLayout();
        Binder<Member> memberBinder = new Binder<>();

        // Label infoLabel = new Label();

        // Create the fields for member section
        TextField firstName = new TextField();
        firstName.setValueChangeMode(ValueChangeMode.EAGER);

        TextField lastName = new TextField();
        lastName.setValueChangeMode(ValueChangeMode.EAGER);

        TextField email = new TextField();
        email.setValueChangeMode(ValueChangeMode.EAGER);

        TextField orcid = new TextField();
        orcid.setValueChangeMode(ValueChangeMode.EAGER);


        TextField organization = new TextField();
        organization.setValueChangeMode(ValueChangeMode.EAGER);

        TextField department = new TextField();
        department.setValueChangeMode(ValueChangeMode.EAGER);

        TextField role = new TextField();
        role.setValueChangeMode(ValueChangeMode.EAGER);

        // Generate UUID (hidden) for editing a member.
        TextField identifier = new TextField();
        identifier.setValue(UUID.randomUUID().toString());

        // Create the buttons used on the member section
        Button save = new Button("Add");
        Button reset = new Button("Clear");
        Button delete = new Button("Remove Member");

        // Add all elements to the layout
        layoutWithBinder2.addFormItem(firstName, "First name");
        layoutWithBinder2.addFormItem(lastName, "Last name");
        layoutWithBinder2.addFormItem(email, "E-mail");
        layoutWithBinder2.addFormItem(orcid, "ORCID");
        layoutWithBinder2.addFormItem(organization, "Organization");
        layoutWithBinder2.addFormItem(department, "Department");
        layoutWithBinder2.addFormItem(role, "Role");

        // Button bar
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.add(save, reset);
        save.getStyle().set("marginRight", "10px");

        // E-mail and phone have specific validators
        Binder.Binding<Member, String> emailBinding = memberBinder.forField(email)
                .withValidator(new EmailValidator("Incorrect email address"))
                .bind(Member::getEmail, Member::setEmail);

        // Set Required Fields
        firstName.setRequiredIndicatorVisible(true);
        lastName.setRequiredIndicatorVisible(true);
        organization.setRequiredIndicatorVisible(true);
        department.setRequiredIndicatorVisible(true);

        memberBinder.forField(firstName)
                .withValidator(new StringLengthValidator(
                        "Please add the first name", 1, null))
                .bind(Member::getFirstName, Member::setFirstName);
        memberBinder.forField(lastName)
                .withValidator(new StringLengthValidator(
                        "Please add the last name", 1, null))
                .bind(Member::getLastName, Member::setLastName);
        memberBinder.forField(organization)
                .withValidator(new StringLengthValidator(
                        "Please add the organization", 1, null))
                .bind(Member::getOrganization, Member::setOrganization);
        memberBinder.forField(department)
                .withValidator(new StringLengthValidator(
                        "Please add the department", 1, null))
                .bind(Member::getDepartment, Member::setDepartment);

        memberBinder.forField(orcid)
                .bind(Member::getORCID, Member::setORCID);

        memberBinder.forField(identifier)
                .bind(Member::getIdentifier, Member::setIdentifier);

        memberBinder.forField(role)
                .bind(Member::getRole, Member::setRole);

        // Grid section
        Grid<Member> grid = new Grid<>();
        grid.setItems(memberList);

        // Add Grid Columns
        grid.addColumn(Member::getFirstName).setHeader("First Name");
        grid.addColumn(Member::getLastName).setHeader("Last Name");
        grid.addColumn(Member::getLastName).setHeader("E-Mail");
        grid.addColumn(Member::getORCID).setHeader("ORCID");
        grid.addColumn(Member::getOrganization).setHeader("Organization");
        grid.addColumn(Member::getDepartment).setHeader("Department");
        grid.addColumn(Member::getRole).setHeader("Role");

        grid.asSingleSelect().addValueChangeListener(event -> {
            if (grid.getSelectedItems().size() > 0) {
                String message = String.format("Selection changed from %s to %s with identifier %s", event.getOldValue(), event.getValue(), grid.getSelectedItems().iterator().next().getIdentifier());
                // Notification.show(message);
                memberBinder.readBean(grid.getSelectedItems().iterator().next());
            }
        });

        // Click listeners for the buttons
        save.addClickListener(event -> {
            Member memberBeingEdited = new Member();
            // The object that will be edited VALIDATE IF UUID ALREADY EXISTS...
            if (memberBinder.writeBeanIfValid(memberBeingEdited)) {
                logger.info(memberBeingEdited.getIdentifier());
                int index = -1;
                for (Member member : memberList) {
                    // There is an empty identifier?
                    if (member.getIdentifier().length() > 0 && member.getIdentifier().equals(memberBeingEdited.getIdentifier())) {
                        // Delete member and add member being edited
                        index = memberList.indexOf(member);
                        break;
                    }
                }
                if (index != -1) {
                    memberList.remove(index);
                }

                memberList.add(memberBeingEdited);
                grid.getDataProvider().refreshAll();
                memberBinder.readBean(null);

            } else {
                BinderValidationStatus<Member> validate = memberBinder.validate();
                String errorText = validate.getFieldValidationStatuses()
                        .stream().filter(BindingValidationStatus::isError)
                        .map(BindingValidationStatus::getMessage)
                        .map(Optional::get).distinct()
                        .collect(Collectors.joining(", "));
            }
        });
        reset.addClickListener(event -> {
            // clear fields by setting null
            memberBinder.readBean(null);
            identifier.setValue(UUID.randomUUID().toString());
            // infoLabel.setText("");
        });

        delete.addClickListener(event -> {
            // clear fields by setting null
            String tobeRemoved = grid.getSelectedItems().iterator().next().getIdentifier();
            int index = -1;
            for (Member member : memberList) {
                if (member.getIdentifier().equals(tobeRemoved)) {
                    // Delete selected member
                    index = memberList.indexOf(member);
                    break;
                }
            }
            if (index != -1) {
                memberList.remove(index);
            }
            grid.getDataProvider().refreshAll();
            memberBinder.readBean(null);
            identifier.setValue(UUID.randomUUID().toString());
        });

        // Help
        VerticalLayout verticalLayout2 = new VerticalLayout();
        // If anything other than unlock
        Html helpDetails2 = getHelpText("help/member.txt");
        verticalLayout2.add(layoutWithBinder2, helpDetails2);

        // Adding the message box, field layout, grid and delete button
        wrapper.add(verticalLayout2, buttons, grid, delete); // infoLabel

        if (ApplicationServiceInitListener.getConfig().getDebug()) {
            Member member = new Member();
            member.setDepartment("DEPARTMENT");
            member.setEmail("member@member.com");
            member.setFirstName("FIRSTNAME");
            member.setIdentifier(UUID.randomUUID().toString());
            member.setLastName("LASTNAME");
            member.setORCID("123456789");
            member.setOrganization("ORGANIZATION");
            member.setRole("A ROLE");
            memberList.add(member);
        }
    }

    private Html getHelpText(String help) {
        // HELP
        String content = getFileContentFromResources(help);
        Html text;
        if (content.contains("Reading failed")) {
            logger.error("Reading of help text from " + help + " failed. " + content);
            text = new Html("<span></span>");
        } else {
            String helpText = new Scanner(getFileContentFromResources(help)).useDelimiter("\\Z").next();
            text = new Html("<span>" + helpText + "</span>");
        }
        return text;
    }

    private void createStudy(VerticalLayout wrapper) {
        FormLayout layoutWithBinder = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Create the fields for the project section
        TextField studyIdentifier = new TextField();
        studyIdentifier.setPlaceholder("e.g. Healthy_vs_Infected");
        studyIdentifier.setValueChangeMode(ValueChangeMode.EAGER);

        TextField studyTitle = new TextField();
        studyTitle.setValueChangeMode(ValueChangeMode.EAGER);

        TextArea studyDescription = new TextArea();
        studyDescription.setWidthFull();
        studyDescription.setValueChangeMode(ValueChangeMode.EAGER);

        studyBinder.forField(studyIdentifier)
                .withValidator(new StringLengthValidator("Please add study identifier (5-25 characters)", 5, 25))
                .bind(Study::getIdentifier, Study::setIdentifier);

        studyBinder.forField(studyTitle)
                .withValidator(new StringLengthValidator("Please add a study title (>10 characters)", 10, null))
                .bind(Study::getTitle, Study::setTitle);

        studyBinder.forField(studyDescription)
                .withValidator(new StringLengthValidator("Please add the study description (>50 characters)", 50, null))
                .bind(Study::getDescription, Study::setDescription);


        studyIdentifier.setWidthFull();
        studyIdentifier.setLabel("Study identifier");

        studyTitle.setWidthFull();
        studyTitle.setLabel("Study title");

        studyDescription.setWidthFull();
        studyDescription.setLabel("Study description / Experimental design");

        layoutWithBinder.add(studyIdentifier, 3);
        layoutWithBinder.add(studyTitle, 3);
        layoutWithBinder.add(studyDescription, 3);

        // Help
        Html helpDetails = getHelpText("help/study.txt");
        verticalLayout.add(layoutWithBinder, helpDetails);

        wrapper.add(verticalLayout);

        if (ApplicationServiceInitListener.getConfig().getDebug()) {
            studyIdentifier.setValue("STUDY_IDENTIFIER");
            studyDescription.setValue("STUDY DESCRIPTION THAT CONTAINS A LOT OF TEXT SO IT IS ACCEPTED BY THE VALIDATOR ETC ...");
            studyTitle.setValue("STUDY TITLE INFORMATION");
        }
    }

    private void createSelectableGrid(VerticalLayout wrapper, String metadata, String help, boolean visible) {
        List<Metadata> gridListAll = new ArrayList<>();
        List<Metadata> gridListObligatory = new ArrayList<>();

        // If assay then also get the assay type sheet next to the assay sheet
        if (metadata.toLowerCase().contains("assay")) {
            for (Metadata element : metadataSet.get("Assay")) {
                if (element.getRequirement().toLowerCase().trim().contains("mandatory")) {
                    gridListObligatory.add(element);
                }
                gridListAll.add(element);
            }
        }

        for (Metadata element : metadataSet.get(metadata)) {
            if (element.getRequirement().toLowerCase().trim().contains("mandatory")) {
                gridListObligatory.add(element);
            }
            gridListAll.add(element);
        }

        Collections.sort(gridListAll, Comparator.comparing(Metadata::getLabel));

        Grid<Metadata> gridSelection = new Grid<>();
        gridSelection.addColumn(Metadata::getPackageName).setHeader("Package");
        gridSelection.addColumn(Metadata::getLabel).setHeader("Name");
        gridSelection.addColumn(Metadata::getSyntax).setHeader("Syntax");
        gridSelection.addColumn(Metadata::getExample).setHeader("Example");
        gridSelection.addColumn(Metadata::getDefinition).setHeader("Description");
        gridSelection.setSelectionMode(Grid.SelectionMode.MULTI);
        gridSelection.asMultiSelect().select(gridListObligatory);
        gridSelection.setId(metadata);
        gridSelection.setItems(gridListAll);
        gridSelection.asMultiSelect().select(gridListObligatory);
        gridSelection.setEnabled(visible);
        gridSelection.setVisible(visible);

        gridSelection.asMultiSelect().addValueChangeListener(event -> {
            gridSelection.asMultiSelect().select(gridListObligatory);
        });

        // Additional fields to be added and enable field
        Checkbox enableBox = new Checkbox();
        enableBox.setEnabled(true);
        if (visible) {
            enableBox.setLabel("Page enabled");
            enableBox.setValue(visible);
        } else {
            enableBox.setLabel("Enable this page");
            enableBox.setValue(visible);
        }

        Button exportButton = new Button("Export", new Icon(VaadinIcon.FILE));
        exportButton.setEnabled(visible);
        exportButton.setVisible(visible);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(exportButton, enableBox); // labelField, addButton,

        // Export this directly to a sheet...
        exportButton.addClickListener(buttonClickEvent -> {
            exportSheet(gridSelection);
        });

        // Enables all fields
        enableBox.addValueChangeListener(checkboxClickEvent -> {
            if (checkboxClickEvent.getValue()) {
                exportButton.setEnabled(true);
                gridSelection.setEnabled(true);
                exportButton.setVisible(true);
                gridSelection.setVisible(true);
                enableBox.setLabel("Page enabled");
            } else {
                exportButton.setEnabled(false);
                gridSelection.setEnabled(false);
                exportButton.setVisible(false);
                gridSelection.setVisible(false);
                enableBox.setLabel("Enable this page");
            }
        });

        // Help
        VerticalLayout verticalLayout = new VerticalLayout();
        Html helpDetails = getHelpText(help);
        Div helpText = new Div();

        helpText.add(helpDetails);

        verticalLayout.add(helpText, horizontalLayout, gridSelection);

        wrapper.add(verticalLayout);

        // List of grid objects according to the GridSelection object
        gridList.add(gridSelection);

        // Under debug mode enable panel and select everything
        if (ApplicationServiceInitListener.debug) {
            // Enables assay panel
            enableBox.setValue(true);
            // Selects all rows
            gridSelection.asMultiSelect().select(gridListAll);
        }
    }

    private void exportSheet(Grid<Metadata> metadataGrid) {
        // Create Excel sheet
        HashMap<String, ArrayList<Metadata>> gridSelection = getHashMapFromGrid(gridList);

        File excelFile = new File(metadataGrid.getId().get() + ".xlsx");

        makeSheetOnly(excelFile, metadataGrid.getSelectedItems(), metadataGrid.getId().get());

        FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(new StreamResource(excelFile.getName(), () -> createResource(excelFile)));
        buttonWrapper.wrapComponent(buttonDownload);
        add(buttonWrapper);
        buttonDownload.clickInClient();
    }
}
