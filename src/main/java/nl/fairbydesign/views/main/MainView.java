package nl.fairbydesign.views.main;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.ApplicationServiceInitListener;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.views.about.AboutView;
import nl.fairbydesign.views.browser.BrowserView;
import nl.fairbydesign.views.help.Documentation;
import nl.fairbydesign.views.help.FAQ;
import nl.fairbydesign.views.help.Setup;
import nl.fairbydesign.views.help.TermsView;
import nl.fairbydesign.views.template.TemplateView;
import nl.fairbydesign.views.unlock.archive.Archive;
import nl.fairbydesign.views.unlock.dashboard.DashboardView;
import nl.fairbydesign.views.unlock.export.Export;
import nl.fairbydesign.views.unlock.management.managementView;
import nl.fairbydesign.views.validation.ValidationView;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.Optional;

/**
 * The main view is a top-level placeholder for all other views.
 */
@JsModule("./styles/shared-styles.js")
@CssImport("./styles/views/main/main-view.css")
@PWA(name = "FAIR Data Station", shortName = "FAIRDS", enableInstallPrompt = false)
@Push
public class MainView extends AppLayout {
    // General variable to enable debug everywhere
    public static boolean debug = ApplicationServiceInitListener.debug;
    // The menu buttons
    private final Tabs menu;
    // H1 header
    private H1 viewTitle;
    // Logger
    public static final Logger logger = Logger.getLogger(MainView.class);

    /**
     * The main view of the interface
     */
    public MainView() {
        setPrimarySection(Section.DRAWER);
        addToNavbar(true, createHeaderContent());
        menu = createMenu();
        addToDrawer(createDrawerContent(menu));
    }

    /**
     * Function to create header content
     * @return the layout of the header
     */
    private Component createHeaderContent() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setId("header");
        layout.getThemeList().set("dark", true);
        layout.setWidthFull();
        layout.setSpacing(false);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.add(new DrawerToggle());
        viewTitle = new H1();
        layout.add(viewTitle);

        Anchor authenticate;
        // Added user icon
        StreamResource imageResource = new StreamResource("user.svg", () -> getClass().getResourceAsStream("/META-INF/resources/images/user.svg"));
        layout.add(new Image(imageResource, "Avatar"));

        // Obtain credentials for authentication purposes
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        // Check authentication with irods instance when successful show username otherwise show login button
        if (credentials != null && credentials.isSuccess()) {
            authenticate = new Anchor("user", credentials.getUsername());
        } else {
            authenticate = new Anchor("", ""); // new Anchor("login", "Login");
        }
        layout.add(UI.getCurrent().getSession().getSession().getId() + " ");

        layout.add(authenticate);
        return layout;
    }

    /**
     * @param menu the menu on the left side
     * @return update of the layout with unlock or generic logo
     */
    private Component createDrawerContent(Tabs menu) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.getThemeList().set("spacing-s", true);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        HorizontalLayout logoLayout = new HorizontalLayout();
        logoLayout.setId("logo");
        logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        // Get url host path if unlock in name use unlock logo
        UI.getCurrent().getPage().executeJs("return window.location.href").then(String.class, location -> {
            if (location.contains("unlock")) {
                StreamResource imageResource = new StreamResource("unlock_logo.png", () -> getClass().getResourceAsStream("/META-INF/resources/images/unlock_logo.png"));
                logoLayout.add(new Image(imageResource, "Unlock logo"));
            } else {
                StreamResource imageResource = new StreamResource("logo.png", () -> getClass().getResourceAsStream("/META-INF/resources/images/logo.png"));
                logoLayout.add(new Image(imageResource, "Unlock logo"));
            }
        });

        layout.add(logoLayout, menu);
        return layout;
    }

    /**
     * @return the tabs created for the menu
     */
    private Tabs createMenu() {
        final Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs.add(createMenuItems());
        return tabs;
    }

    /**
     * This function creates the buttons on the left side
     * @return the tabs for the menu in array format
     */
    private Component[] createMenuItems() {
        // Tab views
        ArrayList<Tab> tabs = new ArrayList<>();
        // About / frontpage view
        tabs.add(createTab("About", AboutView.class));
        // The metadata template generator view
        tabs.add(createTab("Metadata Configurator", TemplateView.class));
        // The view to upload and validate the metadata + conversion to RDF
        tabs.add(createTab("Validate Metadata", ValidationView.class));
        // The view to generate excel files based on bioproject identifiers
        tabs.add(createTab("BioProjects Export (Beta)", nl.fairbydesign.views.bioprojects.BioProjectView.class));
        // The view to generate ENA compatible submission files
        tabs.add(createTab("ENA Submission (Beta)", nl.fairbydesign.views.ena.ENAView.class));
        // Overview of all the terms used in the metadata.xlsx file
        tabs.add(createTab("Terms Overview", TermsView.class));
        // Documentation
        tabs.add(createTab("Documentation", Documentation.class));
        // Setup
        tabs.add(createTab("Setup", Setup.class));
        // F.A.Q
        tabs.add(createTab("F.A.Q", FAQ.class));

        // Retrieve the credentials object
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");
        // Check credentials for irods based modules
        if (credentials != null && credentials.isSuccess()) {
            // registerViewIfApplicable();
            // File browser for iRODS
            tabs.add(createTab("Browser", BrowserView.class));
            // Dashboard overview
            tabs.add(createTab("Dashboard", DashboardView.class));
            // Export and search functionality for bioinformatic workflows
            tabs.add(createTab("Export", Export.class));
        }

        // For admin users
        if (credentials != null && credentials.isAdministrator()) {
            // Management layer for validated RDF file submission
            tabs.add(createTab("Management", managementView.class));
            // tabs.add(createTab("System", systemView.class));
            // File size search system to archive data
            tabs.add(createTab("Archive", Archive.class));
        }
        return tabs.toArray(new Tab[0]);
    }

    public static Tab createTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
        viewTitle.setText(getCurrentPageTitle());
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

    private String getCurrentPageTitle() {
        return getContent().getClass().getAnnotation(PageTitle.class).value();
    }

}
