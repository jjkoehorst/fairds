package nl.fairbydesign.views.unlock.management;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import io.kubernetes.client.openapi.models.V1Job;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.irods.Connection;
import nl.fairbydesign.backend.kubernetes.Kubernetes;
import nl.fairbydesign.views.main.MainView;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jboss.logging.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;

import static nl.fairbydesign.backend.irods.Data.uploadIrodsFile;

//import io.kubernetes.client.openapi.models.V1Job;


/**
 * UNLOCK management view to submit initiation jobs to kubernetes
 */

// TEMP SOLUTION, should be behind a management login...
@Route(value = "management", layout = MainView.class)
@PageTitle("Management")
@CssImport("./styles/views/empty/empty-view.css")
public class managementView extends VerticalLayout {

    public static final String VIEW_NAME = "Admin";
    public static final Logger logger = Logger.getLogger(managementView.class);

    public managementView() {

        add(new H3("Projects"));

        // Get overview of projects
        HashMap<String, HashSet<String>> processed = new HashMap<>();
        HashMap<String, HashSet<String>> unprocessed = new HashMap<>();

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null && credentials.isSuccess() && credentials.isAdministrator()) {
            try {
                Connection connection = new Connection();
                String landingZoneProjectsPath = "/" + connection.getIrodsAccount().getZone() + "/home/";
                IRODSFile landingZoneProjects = connection.getFileFactory().instanceIRODSFile(landingZoneProjectsPath);
                // Obtain all projects from landingzone
                for (File landingZoneProject : landingZoneProjects.listFiles()) {
                    // Skip files
                    if (landingZoneProject.isFile()) continue;
                    // Obtain all investigations from landingzone
                    for (File landingZoneInvestigation : landingZoneProject.listFiles()) {
                        // Find all excel files in the project folder (meaning possible different investigations)
                        for (File landingZoneInvestigationFile : landingZoneInvestigation.listFiles()) {
                            if (landingZoneInvestigationFile.getName().endsWith("xlsx")) {
                                // Check if RDF file is present
                                IRODSFile rdfFile = connection.getFileFactory().instanceIRODSFile(landingZoneInvestigationFile.getAbsolutePath().replaceAll(".xlsx", ".ttl"));
                                if (!rdfFile.exists()) continue;
                                // If excel metadata file
                                IRODSFile excelFile = connection.getFileFactory().instanceIRODSFile(landingZoneInvestigationFile.getAbsolutePath());
                                // Check this file in project folder
                                IRODSFile excelMetadataProcessedFile = connection.getFileFactory().instanceIRODSFile("/" + connection.getIrodsAccount().getZone() + "/projects/" + landingZoneProjects.getName() + "/" + landingZoneInvestigation.getName() + "/" + excelFile.getName());
                                // If excel file also exists on the project location
                                if (excelMetadataProcessedFile.exists()) {
                                    ChecksumValue excelMetadataProcessedFileChecksum = connection.getIrodsFileSystem().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.getIrodsAccount()).computeChecksumOnDataObject(excelMetadataProcessedFile);
                                    ChecksumValue excelMetadataFileChecksum = connection.getIrodsFileSystem().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.getIrodsAccount()).computeChecksumOnDataObject(excelFile);
                                    if (excelMetadataProcessedFileChecksum.getBase64ChecksumValue().equals(excelMetadataFileChecksum.getBase64ChecksumValue())) {
                                        // Processed
                                        String key = landingZoneProject.getName() + "_#_#_" + landingZoneInvestigation.getName();
                                        if (!processed.containsKey(key)) {
                                            processed.put(key, new HashSet());
                                        }
                                        processed.get(key).add(rdfFile.getAbsolutePath());
                                    } else {
                                        // Ready for processing as checksum changed
                                        String key = landingZoneProject.getName() + "_#_#_" + landingZoneInvestigation.getName();
                                        if (!processed.containsKey(key)) {
                                            processed.put(key, new HashSet());
                                        }
                                        processed.get(key).add(rdfFile.getAbsolutePath());
                                    }
                                } else {
                                    // ready for processing
                                    String key = landingZoneProject.getName() + "_#_#_" + landingZoneInvestigation.getName();
                                    if (!processed.containsKey(key)) {
                                        processed.put(key, new HashSet());
                                    }
                                    processed.get(key).add(rdfFile.getAbsolutePath());
                                }
                            }
                        }
                    }
                }

                // Accordion viewer for the different projects separation for processed and unprocessed
                Accordion accordionUnprocessed = new Accordion();
                accordionUnprocessed.setId("Unprocessed");
                // Processed accordion viewer
                Accordion accordionProcessed = new Accordion();
                accordionProcessed.setId("Processed");

                add(accordionUnprocessed);
                add(accordionProcessed);

                for (String projInv : unprocessed.keySet()) {
                    VerticalLayout projectLayout = new VerticalLayout();
                    accordionUnprocessed.add("Project " + projInv.replaceAll("_#_#_", " "), projectLayout);
                    for (String rdfFile : unprocessed.get(projInv)) {

                        Button rejectButton = new Button("Reject information");
                        Button acceptButton = new Button("Accept information");
                        // Set RDF file as ID
                        acceptButton.setId(rdfFile);
                        // Click listeners
                        acceptButton.addClickListener(this::acceptListener);
                        rejectButton.addClickListener(this::rejectListener);
                        HorizontalLayout buttonLayout = new HorizontalLayout();
                        buttonLayout.add(rejectButton, acceptButton, new Text(rdfFile.replaceAll(".*/", "")));
                        projectLayout.add(buttonLayout);
                    }
                }

                for (String projInv : processed.keySet()) {
                    VerticalLayout projectLayout = new VerticalLayout();
                    accordionProcessed.add("Project " + projInv.replaceAll("_#_#_", " "), projectLayout);

                    for (String rdfFile : processed.get(projInv)) {

                        Button rejectButton = new Button("Reject information");
                        Button acceptButton = new Button("Accept information");
                        // Set RDF file as ID
                        acceptButton.setId(rdfFile);
                        // Click listeners
                        acceptButton.addClickListener(this::acceptListener);
                        rejectButton.addClickListener(this::rejectListener);
                        HorizontalLayout buttonLayout = new HorizontalLayout();
                        buttonLayout.add(rejectButton, acceptButton, new Text(rdfFile.replaceAll(".*/", "")));
                        projectLayout.add(buttonLayout);
                    }
                }
            } catch (JargonException e) {
                e.printStackTrace();
            }
        } else {
            add(new Label("To use this you need to be admin and login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }
//            for (File irodsRawProject : irodsRawProjects.listFiles()) {
                // boolean requiresValidation = true;
//                VerticalLayout projectLayout = new VerticalLayout();
//                accordion.add("Project " + irodsRawProject.getName(), projectLayout);

                // Find all excel files in the project folder (meaning possible different investigations)
//                ArrayList<IRODSFile> irodsRawProjectFiles = new ArrayList<>();
//                ArrayList<IRODSFile> irodsProjectFiles = new ArrayList<>();

//                for (File file : irodsRawProject.listFiles()) {
//                    if (file.getName().endsWith("xlsx")) {
//                        IRODSFile irodsFile = connection.getFileFactory().instanceIRODSFile(file.getAbsolutePath());
//                        irodsRawProjectFiles.add(irodsFile);
//                    }
//                }

                // Get the excel files from the project
//                IRODSFile irodsProjectFolder = connection.getFileFactory().instanceIRODSFile("/" + connection.getIrodsAccount().getZone() + "/projects/" + irodsRawProject.getName() + "/");
//                if (irodsProjectFolder.exists()) {
//                    for (File file : irodsProjectFolder.listFiles()) {
//                        if (file.getName().endsWith("xlsx")) {
//                            IRODSFile irodsFile = connection.getFileFactory().instanceIRODSFile(file.getAbsolutePath());
//                            irodsProjectFiles.add(irodsFile);
//                        }
//                    }
//                }

                // Compare the files?
//                for (IRODSFile irodsRawProjectFile : irodsRawProjectFiles) {
//                    if (irodsRawProjectFile.isHidden()) continue;
//                    // Each raw project file requires processing unless stated otherwise
//                    boolean requiresProcessing = true;
//                    for (IRODSFile irodsProjectFile : irodsProjectFiles) {
//                        // Check if the two names are identical to each other
//                        if (irodsRawProjectFile.getName() == irodsProjectFile.getName()) {
//                            ChecksumValue irodsProjectFileChecksum = connection.getIrodsFileSystem().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.getIrodsAccount()).computeChecksumOnDataObject(irodsProjectFile);
//                            ChecksumValue irodsRawProjectFileChecksum = connection.getIrodsFileSystem().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.getIrodsAccount()).computeChecksumOnDataObject(irodsRawProjectFile);
//                            if (irodsProjectFileChecksum.getBase64ChecksumValue().equals(irodsRawProjectFileChecksum.getBase64ChecksumValue())) {
//                                projectLayout.add(new Text("This project has already been accepted and processed"));
//                                requiresProcessing = false;
//                                break;
//                            } else {
//                                projectLayout.add(new Text("Project EXCEL file exist but checksum has changed\n"));
//                            }
//                        }
//                    }
//
//                    if (requiresProcessing) {
//                        projectLayout.add(new Text("Project RDF and EXCEL file ready for reviewing\n" + irodsRawProjectFile.getName().replaceAll(".xlsx$", "")));
//
//                        Button rejectButton = new Button("Reject information");
//                        Button acceptButton = new Button("Accept information");
//                        acceptButton.setId(irodsRawProjectFile.getAbsolutePath() + "_#_" + "/" + connection.getIrodsAccount().getZone() + "/projects/" + irodsRawProject.getName() + "/");
//
//                        acceptButton.addClickListener(this::acceptListener);
//                        rejectButton.addClickListener(this::rejectListener);
//                        HorizontalLayout buttonLayout = new HorizontalLayout();
//                        buttonLayout.add(rejectButton, acceptButton);
//                        projectLayout.add(buttonLayout);
//                    }
//                }
//            }
//            add(accordion);
//        } catch (JargonException e) {
//            e.printStackTrace();
//        }
//    }

    private void rejectListener(ClickEvent<Button> buttonClickEvent) {
        Notification.show(buttonClickEvent.getSource().getId().get() + " has been rejected (if the function was working)");
    }

    private void acceptListener(ClickEvent<Button> buttonClickEvent) {
        // Obtain RDF metadata path from button id
        String irodsRDFPath = buttonClickEvent.getSource().getId().get();

        Notification.show(new File(irodsRDFPath).getName() + " has been clicked!");

        try {
            Connection connection = new Connection();
            IRODSFile irodsInvestigationFolder = connection.getFileFactory().instanceIRODSFile(new File(irodsRDFPath).getParent());
            if (irodsInvestigationFolder.exists()) {
                Notification.show("Warning... project folder already exists", 5000, Notification.Position.MIDDLE);
            } else {
                // Create project folder
                Notification.show("Creating project folder " + irodsInvestigationFolder, 5000, Notification.Position.MIDDLE);
                irodsInvestigationFolder.mkdirs();
            }

            // Create YAML file for project metadata registration
            File yamlFile = new File(new File(irodsRDFPath).getName().replaceAll(".ttl$","") + ".yaml");
            PrintWriter printWriter = new PrintWriter(yamlFile);
            // !workflow.workflow_irods_management
            // turtle: /unlock/projects/NWO_unlock_test/NWO_unlock_test.ttl
            // destination: /unlock/projects/NWO_unlock_test/
            printWriter.println("!workflow.workflow_irods_management");
            printWriter.println("turtle: " + irodsRDFPath);
            printWriter.println("destination: " + irodsInvestigationFolder);
            printWriter.println("ngtaxTools: " + "/unlock/infrastructure/binaries/NG-Tax2/NGTax-2.1.66.jar");
            printWriter.close();

            File yamlFilePath = new File(new File(irodsRDFPath).getParent() + "/" + yamlFile.getName());

            uploadIrodsFile(connection.getIrodsAccount(), yamlFile, yamlFilePath);

            // Perform kubernetes call
            V1Job v1job = Kubernetes.main(yamlFilePath.getAbsolutePath());

            if (v1job != null && v1job.getMetadata() != null && v1job.getMetadata().getName() != null) {
                Notification.show("Starting job  " + v1job.getMetadata().getName());
            } else {
                Notification.show("Starting job on kubernetes"); // + v1job);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
