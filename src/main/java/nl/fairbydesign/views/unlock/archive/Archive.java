package nl.fairbydesign.views.unlock.archive;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.File;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.MetaDataAndDomainData;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Route(value = "archive", layout = MainView.class)
@PageTitle("Archiving view")
@CssImport("./styles/views/empty/empty-view.css")
public class Archive extends Div {

    public static final Logger logger = Logger.getLogger(Archive.class);
    private ListDataProvider<File> dataProvider;

    public Archive() {
        setId("master-detail-view");
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");
        if (credentials != null && credentials.isSuccess()) {

            // Minimum file size module
            TextField sizeField = new TextField();
            sizeField.setLabel("Minimal size in MB");
            sizeField.setValue("10000");

            Button searchButton = new Button("Search");
            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.add(sizeField, searchButton);
            add(horizontalLayout);

            Grid<File> grid = new Grid<>();
            grid.setMultiSort(true);
            Grid.Column<File> sizeColumn = grid.addColumn(File::getSize).setHeader("Size");
            sizeColumn.setSortable(true);
            Grid.Column<File> resourceColumn = grid.addColumn(File::getResource).setHeader("Resource");
            resourceColumn.setSortable(true);
            Grid.Column<File> pathColumn = grid.addColumn(File::getPath).setHeader("Path");
            pathColumn.setSortable(true);
            pathColumn.setAutoWidth(true);

            // Obtain data using the default value
            ArrayList<File> files = Data.getFilesBySize(credentials.getIrodsAccount(), sizeField.getValue());
            dataProvider = new ListDataProvider<>(files);
            grid.setDataProvider(dataProvider);

            HeaderRow filterRow = grid.appendHeaderRow();
            // Path filter
            TextField pathField = new TextField();
            pathField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getPath(), pathField.getValue())));
            pathField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(pathColumn).setComponent(pathField);
            pathField.setSizeFull();
            pathField.setPlaceholder("Filter");
            // Resource filter
            TextField resourceField = new TextField();
            resourceField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getResource(), resourceField.getValue())));
            resourceField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(resourceColumn).setComponent(resourceField);
            resourceField.setSizeFull();
            resourceField.setPlaceholder("Filter");

            searchButton.addClickListener(listener -> {
                // Get files by size
                ArrayList<File> filesBySize = Data.getFilesBySize(credentials.getIrodsAccount(), sizeField.getValue());
                // Only add filter once as you might click the button again...
                dataProvider = new ListDataProvider<>(filesBySize);
                // Set the provider
                grid.setDataProvider(dataProvider);
                // Ensure refresh?
                grid.getDataProvider().refreshAll();
            });

            grid.setSelectionMode(Grid.SelectionMode.MULTI);

            grid.asMultiSelect().addValueChangeListener(event -> {
                Notification.show("Selected " + grid.getSelectedItems().size());
            });

            add(grid);

            HorizontalLayout buttonArchiveLayout = new HorizontalLayout();
            Label archiveLabel = new Label("Sent to: ");
            Button archive = new Button("Archive");
            Button disk = new Button("Disk");
            Button dual = new Button("Dual");
            buttonArchiveLayout.add(archiveLabel, archive, disk, dual);
            add(buttonArchiveLayout);

            archive.addClickListener(clicked -> {
                logger.info("Archive clicked");
                // Set metadata to archive
                Set<File> files_to_archive = grid.getSelectedItems();
                // To make a progress bar
                for (File file : files_to_archive) {
                    Set<File> tmp = new HashSet<>();
                    tmp.add(file);
                    sendTo(credentials, tmp, "to_archive");
                }
                // Force refresh of grid
                 searchButton.clickInClient();
            });
            disk.addClickListener(clicked -> {
                Notification.show("Disk clicked");
                // Set metadata to disk store
                sendTo(credentials, grid.getSelectedItems(), "to_disk");
                // Force refresh of grid
                searchButton.clickInClient();
            });
            dual.addClickListener(clicked -> {
                // Set metadata to dual store (disk and archive)
                Notification.show("Disk & Tape clicked");
                sendTo(credentials, grid.getSelectedItems(), "to_dual");
                // Force refresh of grid
                searchButton.clickInClient();
            });

        } else {
            add(new Label("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }

    private void sendTo(Credentials credentials, Set<File> files, String value) {
        try {
            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount());
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();
            for (File file : files) {
                IRODSFile irodsFile = fileFactory.instanceIRODSFile(file.getPath());
                AvuData newAvuData = new AvuData();
                newAvuData.setAttribute("RESOURCE");
                newAvuData.setValue(value);
                newAvuData.setUnit("");
                DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(credentials.getIrodsAccount());
                boolean replaced = false;
                for (MetaDataAndDomainData metaDataAndDomainData : dataObjectAO.findMetadataValuesForDataObject(irodsFile)) {
                    AvuData avuDataIrodsFile = metaDataAndDomainData.asAvu();
                    if (avuDataIrodsFile.getAttribute().contentEquals("RESOURCE")) {
                        // Replace AVU object by this new one
                        logger.info("Replacing AVU RESOURCE object");
                        // dataObjectAO.modifyAVUMetadata(irodsFile.getAbsolutePath(), avuDataIrodsFile, newAvuData);
                        dataObjectAO.setAVUMetadata(irodsFile.getAbsolutePath(), newAvuData);
                        replaced = true;
                        break;
                    }
                }
                if (!replaced) {
                    logger.info("Adding AVU RESOURCE object");
                    dataObjectAO.setAVUMetadata(file.getPath(), newAvuData);
                }
            }
        } catch (JargonException e) {
            e.printStackTrace();
        }
    }
}
