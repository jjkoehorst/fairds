package nl.fairbydesign.views.unlock.dashboard;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.*;
import nl.fairbydesign.backend.data.objects.Process;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.views.main.MainView;
import org.apache.tomcat.jni.Proc;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.jboss.logging.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

import static nl.fairbydesign.backend.irods.Data.getDiskUsage;
import static nl.fairbydesign.backend.irods.Data.getJobInformation;

/**
 * Dashboard view for jobs to be processed and disk space usage
 */

@Route(value = "dashboard", layout = MainView.class)
@PageTitle("Dashboard")
@CssImport("./styles/views/empty/empty-view.css")
public class DashboardView extends Div implements BeforeLeaveObserver {

    public static final Logger logger = Logger.getLogger(DashboardView.class);
    private boolean threadStop = false;

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        logger.error("Person leaves page!");
        threadStop = true;
    }
    public DashboardView() {
        setId("master-detail-view");

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null && credentials.isSuccess()) {
            // Create a new thread to obtain the information
            ArrayList<DiskUsage> diskUsages = new ArrayList<>();
            DiskUsage loading = new DiskUsage();
            loading.setProject("Loading project");
            loading.setInvestigation("and investigation data");
            loading.setDiskSize(0L);
            loading.setTapeSize(0L);
            diskUsages.add(loading);
            // Disk usage overview
            Grid<DiskUsage> diskUsageGrid = new Grid<>(DiskUsage.class);
            // diskUsageGrid.setItems(diskUsages);
            ListDataProvider<DiskUsage> dataProvider = new ListDataProvider<>(diskUsages);
            diskUsageGrid.setDataProvider(dataProvider);
            diskUsageGrid.removeAllColumns();
            diskUsageGrid.addColumn(DiskUsage::getProject).setHeader("Project");
            diskUsageGrid.addColumn(DiskUsage::getInvestigation).setHeader("Investigation");
            diskUsageGrid.addColumn(DiskUsage::getTapeSize).setHeader("Tape size (GB)");
            diskUsageGrid.addColumn(DiskUsage::getDiskSize).setHeader("Disk size (GB)");

            add(diskUsageGrid);

            // Thread to collect information, TODO stop thread upon page disconnect...
            Thread t = new Thread(() -> {
                // Get information
                logger.info("Obtaining projects and information");
                // Drop down menu for projects?
                try {
                    IRODSFileFactory irodsFileFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getIRODSFileFactory(credentials.getIrodsAccount());
                    for (File project : irodsFileFactory.instanceIRODSFile("/" + credentials.getZone() + "/projects/").listFiles()) {
                        for (File investigation : Objects.requireNonNull(project.listFiles())) {
                            if (threadStop) {
                                logger.error("Thread stopped as user says bye...");
                                return;
                            }
                            if (investigation.isFile()) continue;
                            logger.error("Processing for diskusage " + project + " " + investigation);
                            System.err.println("Processing for diskusage " + project + " " + investigation);
                            // Process project folder
                            Process process = new Process();
                            process.setProjectIdentifier(project.getName());
                            process.setInvestigationIdentifier(investigation.getName());
                            // Obtain disk usage
                            diskUsages.add(getDiskUsage(credentials.getIrodsAccount(), process));
                            // Refresh grid
                            diskUsageGrid.getUI().ifPresent(ui -> ui.access(() -> diskUsageGrid.getDataProvider().refreshAll()));
                        }
                    }
                } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
                    throw new RuntimeException(e);
                }
            });
            t.start();


//            ArrayList<Process> processes = Data.getPIS(credentials.getIrodsAccount());
            // Obtain disk usage overview for project and investigation in landingzone and projects
//            ArrayList<DiskUsage> diskUsages = new ArrayList<>();
//            HashSet<String> diskUsageAnalysed = new HashSet<>();
//            for (Process process : processes) {
//                if (!diskUsageAnalysed.contains(process.getProjectIdentifier() + "/" + process.getInvestigationIdentifier())) {
//                    try {
//                        diskUsages.add(Data.getDiskUsage(credentials.getIrodsAccount(), process));
//                        diskUsageAnalysed.add(process.getProjectIdentifier() + "/" + process.getInvestigationIdentifier());
//                    } catch (GenQueryBuilderException | JargonQueryException | JargonException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }

            // Check for landing zone processes after disk usage as it is not relevant
//            Process landingzoneProcess = new Process();
//            landingzoneProcess.setPath("/" + credentials.getIrodsAccount().getZone() + "/landingzone");
//            landingzoneProcess.setProjectIdentifier("LandingZone");
//            landingzoneProcess.setInvestigationIdentifier(".");
//            landingzoneProcess.setStudyIdentifier(".");
//
//            try {
//                Collection<Process> landingzoneProcessCollection = getJobInformation(credentials.getIrodsAccount(), landingzoneProcess);
//                processes.addAll(landingzoneProcessCollection);
//            } catch (GenQueryBuilderException | JargonException | JargonQueryException e) {
//                e.printStackTrace();
//            }
//
//            // Process overview
//            Grid<Process> grid = new Grid<>(Process.class);
//            grid.setSelectionMode(Grid.SelectionMode.SINGLE);
//            grid.setItems(processes);
//            grid.removeAllColumns();
//            grid.addColumn(Process::getProjectIdentifier).setHeader("Project");
//            grid.addColumn(Process::getInvestigationIdentifier).setHeader("Investigation");
//            grid.addColumn(Process::getStudyIdentifier).setHeader("Study");
//            grid.addColumn(Process::getWorkflow).setHeader("Workflow");
//            grid.addColumn(Process::getWaiting).setHeader("Waiting");
//            grid.addColumn(Process::getQueue).setHeader("Queue");
//            grid.addColumn(Process::getRunning).setHeader("Running");
//            grid.addColumn(Process::getFailed).setHeader("Failed");
//            grid.addColumn(Process::getFinished).setHeader("Finished");
//            grid.setMultiSort(true);
//            add(grid);



            // Obtain overall disk usage
//            try {
//                Process process = new Process();
//                process.setProjectIdentifier("%");
//                process.setInvestigationIdentifier("%");
//                ArrayList<DiskUsage> irodsUsages = new ArrayList<>();
//                irodsUsages.add(getDiskUsage(credentials.getIrodsAccount(), process));
//
//                Grid<DiskUsage> iRODSUsageGrid = new Grid<>(DiskUsage.class);
//                iRODSUsageGrid.setItems(irodsUsages);
//                iRODSUsageGrid.removeAllColumns();
//                iRODSUsageGrid.addColumn(DiskUsage::getProject).setHeader("Project");
//                iRODSUsageGrid.addColumn(DiskUsage::getInvestigation).setHeader("Investigation");
//                iRODSUsageGrid.addColumn(DiskUsage::getTapeSize).setHeader("Tape size (GB)");
//                iRODSUsageGrid.addColumn(DiskUsage::getDiskSize).setHeader("Disk size (GB)");
//
//                add(iRODSUsageGrid);
//            } catch (GenQueryBuilderException | JargonException | JargonQueryException e) {
//                e.printStackTrace();
//            }

        } else {
            add(new Label("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }
}
