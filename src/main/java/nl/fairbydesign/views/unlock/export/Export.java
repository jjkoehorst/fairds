package nl.fairbydesign.views.unlock.export;


import com.esotericsoftware.yamlbeans.YamlWriter;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1Job;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.Biom;
import nl.fairbydesign.backend.data.yaml.Workflow;
import nl.fairbydesign.backend.data.yaml.WorkflowBiom;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.kubernetes.Kubernetes;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.units.qual.N;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFileFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;

@Route(value = "export", layout = MainView.class)
@PageTitle("Export module")
@CssImport("./styles/views/empty/empty-view.css")
public class Export extends Div {

    public static final Logger logger = LogManager.getLogger(Export.class);
    private static HashSet<Biom> jobs = new HashSet<>();
    private ListDataProvider<Biom> dataProvider;

    public Export() {
        // Reset jobs content
        jobs = new HashSet<>();

        setId("master-detail-view");
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");
        if (credentials != null && credentials.isSuccess()) {
            // Combo box for different export modules
            ComboBox<String> comboBox = new ComboBox();
            ArrayList<String> selections = new ArrayList<>();
            selections.add("Phyloseq");
            selections.add("PICRUST2");
            comboBox.setItems(selections);
            comboBox.setPlaceholder("Select export type");

            // Biom selection overview enforce reset
            dataProvider = new ListDataProvider<>(jobs);

            Grid<Biom> grid = new Grid<>();
            grid.setDataProvider(dataProvider);
            Grid.Column<Biom> projectColumn = grid.addColumn(Biom::getProject).setHeader("Project");
            Grid.Column<Biom> investigationColumn = grid.addColumn(Biom::getInvestigation).setHeader("Investigation");
            Grid.Column<Biom> studyColumn = grid.addColumn(Biom::getStudy).setHeader("Study");
            Grid.Column<Biom> ouColumn = grid.addColumn(Biom::getObservationUnit).setHeader("Observation Unit");
            Grid.Column<Biom> assayColumn = grid.addColumn(Biom::getAssay).setHeader("Assay");
            Grid.Column<Biom> jobColumn = grid.addColumn(Biom::getJob).setHeader("Job");

            assayColumn.setAutoWidth(true);

            HeaderRow filterRow = grid.appendHeaderRow();

            // Project filter
            TextField projectField = new TextField();
            projectField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getProject(), projectField.getValue())));
            projectField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(projectColumn).setComponent(projectField);
            projectField.setSizeFull();
            projectField.setPlaceholder("Filter");

            // Investigation filter
            TextField investigationField = new TextField();
            investigationField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getInvestigation(), investigationField.getValue())));
            investigationField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(investigationColumn).setComponent(investigationField);
            investigationField.setSizeFull();
            investigationField.setPlaceholder("Filter");

            // Study filter
            TextField studyField = new TextField();
            studyField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getStudy(), studyField.getValue())));
            studyField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(studyColumn).setComponent(studyField);
            studyField.setSizeFull();
            studyField.setPlaceholder("Filter");

            // OU filter
            TextField observationUnitField = new TextField();
            observationUnitField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getObservationUnit(), observationUnitField.getValue())));
            observationUnitField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(ouColumn).setComponent(observationUnitField);
            observationUnitField.setSizeFull();
            observationUnitField.setPlaceholder("Filter");

            // Assay run filter
            TextField assayUnitField = new TextField();
            assayUnitField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getAssay(), assayUnitField.getValue())));
            assayUnitField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(assayColumn).setComponent(assayUnitField);
            assayUnitField.setSizeFull();
            assayUnitField.setPlaceholder("Filter");

            // Job run filter
            TextField jobUnitField = new TextField();
            jobUnitField.addValueChangeListener(event -> dataProvider.addFilter(biom -> StringUtils.containsIgnoreCase(biom.getJob(), jobUnitField.getValue())));
            jobUnitField.setValueChangeMode(ValueChangeMode.EAGER);
            filterRow.getCell(jobColumn).setComponent(jobUnitField);
            jobUnitField.setSizeFull();
            jobUnitField.setPlaceholder("Filter");

            grid.setSelectionMode(Grid.SelectionMode.MULTI);

            grid.asMultiSelect().addValueChangeListener(event -> {
                Notification.show("Selected " + grid.getSelectedItems().size());
            });

            Button button = new Button("Generate");

            TextField textFieldPrefix = new TextField();
            textFieldPrefix.setPlaceholder("Prefix");

            comboBox.addValueChangeListener(listener -> {
                String value = listener.getValue();
                jobs = new HashSet<>();
                grid.deselectAll();
                if (value.toLowerCase().contains("phyloseq")) {
                    jobs.addAll(Data.getJobOutputs(credentials.getIrodsAccount(), "%/%_PHYLOSEQ"));
                } else if (value.toLowerCase().matches("picrust2")) {
                    jobs.addAll(Data.getJobOutputs(credentials.getIrodsAccount(), "%/%_PICRUSt2"));
                } else {
                    Notification.show("Current selection not supported");
                }
                logger.error("Jobs found: " + jobs.size());
                // Reload dataprovider
                dataProvider = new ListDataProvider<>(jobs);
                // Set the provider
                grid.setDataProvider(dataProvider);
                // Ensure refresh?
                grid.getDataProvider().refreshAll();
            });

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.add(comboBox, textFieldPrefix); // , textFieldReadLength, database, rejectedASVcheckBox);
            verticalLayout.setAlignItems(CENTER);
            add(verticalLayout);

            add(grid);
            VerticalLayout verticalLayoutButton = new VerticalLayout();
            verticalLayoutButton.add(button);
            verticalLayoutButton.setAlignItems(CENTER);
            add(verticalLayoutButton);

            button.addClickListener(e -> {
                if (textFieldPrefix.getValue().length() == 0) {
                    Notification.show("Missing prefix for the filename", 10000, Notification.Position.MIDDLE);
                } else {
                    try {
                        // Ensure lowercase due to kubernetes TODO keep casing but ensure kubernetes job to pass validation
                        textFieldPrefix.setValue(textFieldPrefix.getValue().toLowerCase());

                        if (!textFieldPrefix.getValue().matches("[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*")) {
                            Notification notification = new Notification();
                            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                            notification.setPosition(Notification.Position.MIDDLE);
                            Div text = new Div(new Text("Only alphanumeric characters and '-' allowed and not " + textFieldPrefix.getValue()));

                            Button closeButton = new Button(new IronIcon("lumo", "cross"));
                            closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
                            closeButton.getElement().setAttribute("aria-label", "Close");
                            closeButton.addClickListener(event -> notification.close());

                            HorizontalLayout layout = new HorizontalLayout(text, closeButton);
                            layout.setAlignItems(FlexComponent.Alignment.CENTER);

                            notification.add(layout);
                            notification.open();
                        } else {
                            IRODSAccount irodsAccount = credentials.getIrodsAccount();
                            IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount());

                            HashSet<String> folders = new HashSet<>();

                            // Obtain all hdt files
                            for (Biom selectedItem : grid.getSelectedItems()) {
                                folders.add(selectedItem.getFolder());
                            }

                            // Create input file for yaml job
                            File jobFile = new File(textFieldPrefix.getValue() + "_" + comboBox.getValue() + ".job");
                            PrintWriter pWriter = new PrintWriter(jobFile);

                            // Obtain investigations
                            HashSet<String> investigations = new HashSet<>();

                            for (String folder : folders) {
                                // Obtain files
                                pWriter.println(folder);
                                // Obtain TURTLE files from the investigation
                                if (folder.contains("/STU_"))
                                    investigations.add(folder.split("/STU_")[0]);
                                // Backwards compatibility for S_
                                if (folder.contains("/S_"))
                                    investigations.add(folder.split("/S_")[0]);
                            }

                            // List and add turtle files
                            for (String investigation : investigations) {
                                File[] irodsFiles = fileFactory.instanceIRODSFile(investigation).listFiles();
                                for (File irodsFile : irodsFiles) {
                                    if (irodsFile.getName().endsWith(".ttl")) {
                                        if (irodsFile.getName().startsWith(".")) continue;
                                        pWriter.println(irodsFile.getPath());
                                    }
                                }
                            }

                            pWriter.close();

                            WorkflowBiom workflowBiom = new WorkflowBiom();
                            workflowBiom.setJob("/" + irodsAccount.getZone() + "/home/" + irodsAccount.getUserName() + "/export/" + jobFile.getName());
                            workflowBiom.setDestination("/" + irodsAccount.getZone() + "/home/" + irodsAccount.getUserName() + "/export/");
                            workflowBiom.setProvenance(false);
                            workflowBiom.setIdentifier(textFieldPrefix.getValue());
                            // Memory set per files selected * 20mb per file memory
                            workflowBiom.memory = 20 * folders.size();

                            File yamlFile = new File(textFieldPrefix.getValue() + "_" + comboBox.getValue() + ".yaml");
                            YamlWriter writer = new YamlWriter(new FileWriter(yamlFile));
                            writer.write(workflowBiom);
                            writer.close();

                            Workflow.fixClazz(yamlFile.getName());
                            Workflow.fixComments(yamlFile.getName());

                            // Upload job file
                            File yamlPath = new File("/" + irodsAccount.getZone() + "/home/" + irodsAccount.getUserName() + "/export/" + yamlFile.getName());
                            File jobPath = new File("/" + irodsAccount.getZone() + "/home/" + irodsAccount.getUserName() + "/export/" + jobFile.getName());

                            Data.uploadIrodsFile(irodsAccount, yamlFile, yamlPath, true);
                            Data.uploadIrodsFile(irodsAccount, jobFile, jobPath, true);

                            // Delete uploaded files
                            while (yamlFile.exists())
                                yamlFile.delete();

                            while (jobFile.exists())
                                jobFile.delete();


                            // Create system object
                            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
                            IRODSAccessObjectFactory accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

                            // Ensure home directory is inheritance
                            CollectionAO collectionAO = accessObjectFactory.getCollectionAO(irodsAccount);
                            String homedir = "/" + irodsAccount.getZone() + "/home/" + irodsAccount.getUserName();
                            collectionAO.setAccessPermissionInherit(irodsAccount.getZone(), homedir, true);

                            // Give technicians access
                            collectionAO.setAccessPermissionOwn(irodsAccount.getZone(), homedir, "technicians", true);

                            // Add access rights to the technicians group
                            DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(irodsAccount);
                            dataObjectAO.setAccessPermissionOwn(irodsAccount.getZone(), yamlPath.getAbsolutePath(), "technicians");
                            dataObjectAO.setAccessPermissionOwn(irodsAccount.getZone(), jobPath.getAbsolutePath(), "technicians");

                            // Add metadata tag...
                            dataObjectAO = irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount());
                            AvuData avuMetaData = null;
                            if (comboBox.getValue().toLowerCase().matches("phyloseq")) {
                                avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/ngtax/ngtax_to_biom.cwl", "waiting");
                            } else if (comboBox.getValue().toLowerCase().matches("picrust2")) {
                                avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/picrust2/picrust_to_biom.cwl", "waiting");
                            } else {
                                // Default value
                                Notification.show("Job type " + comboBox.getValue().toLowerCase() + " not recognised");
                            }

                            if (avuMetaData != null) {
                                dataObjectAO.setAVUMetadata(yamlPath.getAbsolutePath(), avuMetaData);

                                try {
                                    V1Job v1job = Kubernetes.main(yamlPath.getAbsolutePath());
                                    if (v1job != null && v1job.getMetadata() != null && v1job.getMetadata().getName() != null) {
                                        Notification.show("Starting job  " + v1job.getMetadata().getName());
                                    } else {
                                        Notification.show("Starting job on kubernetes"); // + v1job);
                                    }
                                } catch (ApiException apiException) {
                                    Notification.show("Job submission failed, try a different prefix or contact the system admins", 10, Notification.Position.MIDDLE);
                                }

                            }
                            Notification.show("Job submitted, check your home folder in a few minutes for the biom file");
                            grid.deselectAll();
                        }
                    } catch (Exception jargonException) {
                        jargonException.printStackTrace();
                    }
                }
            });
        } else {
            add(new Label("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }
}
