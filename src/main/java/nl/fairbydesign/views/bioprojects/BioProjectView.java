package nl.fairbydesign.views.bioprojects;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.bioprojects.ncbi.NCBI;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.vaadin.flow.component.Unit.PERCENTAGE;
import static com.vaadin.flow.component.Unit.PIXELS;
import static com.vaadin.flow.data.value.ValueChangeMode.EAGER;

@Route(value = "bioproject", layout = MainView.class)
@PageTitle("BioProject Metadata Generator")
@CssImport("./styles/views/empty/empty-view.css")
public class BioProjectView extends Div {
    private String excelFilename;
    private static final Logger logger = Logger.getLogger(BioProjectView.class);
    private TextArea stacktraceArea;

    public BioProjectView() {

        // Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        int charLimit = 1000000;
        excelFilename = "bioprojects_xlsx/" + UUID.randomUUID() + ".xlsx";
        TextArea textArea = new TextArea();
        textArea.setValue("Place the text containing bioproject identifiers here. For example ");
        textArea.setLabel("Bioproject identifiers");
        textArea.setMaxLength(charLimit);
        textArea.setValueChangeMode(EAGER);
        textArea.setWidthFull();
        textArea.setHeightFull();
        textArea.setMaxWidth(50, PERCENTAGE);
        textArea.setMaxHeight(500, PIXELS);

        stacktraceArea = new TextArea("Info");
        stacktraceArea.setWidthFull();
        stacktraceArea.setHeightFull();
        stacktraceArea.setMaxWidth(100, PERCENTAGE);
        stacktraceArea.setMaxHeight(500, PIXELS);
        stacktraceArea.setVisible(false);
        stacktraceArea.setEnabled(false);

        HashSet<String> bioprojects = new HashSet<>();
        textArea.addValueChangeListener(e -> {
            bioprojects.clear();
            String regex = "(PRJ[DEN][A-Z]\\d+)";
            Pattern r = Pattern.compile(regex);
            Matcher m = r.matcher(textArea.getValue());
            while (m.find()) {
                bioprojects.add(m.group());
            }
            e.getSource().setHelperText("Detected " + bioprojects.size() + " projects");
        });

        Button generateExcelMetadataButton = new Button("Generate");
        Button downloadExcelMetadataButton = new Button("Download");

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        // Download
        FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(new StreamResource(new File(excelFilename).getName(), () -> createResource(new File(excelFilename))));
        horizontalLayout.add(generateExcelMetadataButton, downloadExcelMetadataButton, buttonWrapper);
        buttonWrapper.wrapComponent(downloadExcelMetadataButton);
        buttonWrapper.setVisible(false);
        // Adding the button wrapper to the interface
        // add(buttonWrapper);


        downloadExcelMetadataButton.setEnabled(false);
        downloadExcelMetadataButton.setVisible(true);
        HorizontalLayout topSection = new HorizontalLayout();
        VerticalLayout progressbarLayout = new VerticalLayout();
        topSection.add(textArea, progressbarLayout);
        add(topSection);
        add(horizontalLayout);

        ProgressBar progressBar = new ProgressBar();
        ProgressBar progressBar2 = new ProgressBar();

        progressBar.setVisible(false);
        progressBar2.setVisible(false);

        Div progressBarLabel = new Div();
        Div progressBar2Label = new Div();
        progressbarLayout.add(progressBarLabel, progressBar,progressBar2Label, progressBar2, stacktraceArea);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);

        generateExcelMetadataButton.addClickListener(clicked -> {
            // Forces a refresh which includes a new UUID needed for the excel file
            generateExcelMetadataButton.setEnabled(false);
            // Start the thread
            Thread t = new Thread(() -> {
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(true)));
                progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setVisible(true)));
                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setVisible(true)));
                progressBar2Label.getUI().ifPresent(ui -> ui.access(() -> progressBar2Label.setVisible(true)));
                progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setMin(0)));
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setMin(0)));
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setMax(bioprojects.size())));
                progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setMax(1)));
                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setText("Processing files (0/"+bioprojects.size()+")")));
                stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setEnabled(true)));
                stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setVisible(true)));
                stacktraceArea.setValue("");

                try {
                    NCBI ncbi = new NCBI();
                    // Loop for the progress bar to fetch all data
                    int progressBarValue = 0;
                    for (String bioproject : bioprojects) {
                        progressBarValue++;
                        int finalProgressBarValue = progressBarValue;
                        progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setValue(finalProgressBarValue)));
                        progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setText("Processing project " + bioproject + " ("+finalProgressBarValue+"/"+bioprojects.size()+")")));

                        String rootFolder = "./json/";
                        ArrayList<String> ids = ncbi.searchSamples(rootFolder, bioproject);
                        // Reset for EBI
                        if (ids.get(0).startsWith("ebi")) {
                            stacktraceArea.setValue("Project " + bioproject + " has " + ids.size() + " samples but metadata is not available via the NCBI\n" + stacktraceArea.getValue());
                            ids = new ArrayList<>();
                        } else {
                            stacktraceArea.setValue("Project " + bioproject + " has " + ids.size() + " samples\n" + stacktraceArea.getValue());
                        }
                        for (int i = 0; i < ids.size(); i++) {
                            double d = i * 1.0 / ids.size();
                            int finalI = i;
                            progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setValue(d)));
                            ArrayList<String> finalIds = ids;
                            progressBar2Label.getUI().ifPresent(ui -> ui.access(() -> progressBar2Label.setText("Processing samples ("+ finalI +"/"+ finalIds.size()+")")));
                            String id = ids.get(i);
                            // For validation purposes...
                            ncbi.processSamples(id, rootFolder);
                        }

                        ncbi.processProject(bioproject, rootFolder);

                        // bioproject
                        // https://www.ebi.ac.uk/ena/browser/api/xml/PRJDB10485
                        Thread.sleep(1);
                    }
                    // Do it again, but now it will generate the Excel file
                    new File(excelFilename).getParentFile().mkdirs();
                    ArrayList<String> bioprojectsList = new ArrayList<>(bioprojects);
                    Collections.sort(bioprojectsList);
                    // String md5 = DigestUtils.md5Hex(StringUtils.join(bioprojectsList, "")).trim().trim();
                    // setExcelFilename("bioprojects_xlsx/" + md5 + ".xlsx");
                    progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setValue(1.0)));
                    progressBar2Label.getUI().ifPresent(ui -> ui.access(() -> progressBar2Label.setText("Generating excel file... please wait")));
                    ncbi.fetch(bioprojects, excelFilename);
                    progressBar2Label.getUI().ifPresent(ui -> ui.access(() -> progressBar2Label.setText("Excel file generated...")));

                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                downloadExcelMetadataButton.getUI().ifPresent(ui -> ui.access(() -> downloadExcelMetadataButton.setEnabled(true)));
                // Progress information
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(false)));
                progressBar2.getUI().ifPresent(ui -> ui.access(() -> progressBar2.setVisible(false)));
                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setVisible(false)));
                progressBar2Label.getUI().ifPresent(ui -> ui.access(() -> progressBar2Label.setVisible(false)));
                // Download button
                buttonWrapper.getUI().ifPresent(ui -> ui.access(() -> buttonWrapper.setVisible(true)));
            });
            t.start();
        });
    }

    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setExcelFilename(String excelFilename) {
        this.excelFilename = excelFilename;
    }

    public void updateStackTrace(String message) {
        this.stacktraceArea.setValue(message.strip() + "\n" + this.stacktraceArea.getValue());
    }
}
