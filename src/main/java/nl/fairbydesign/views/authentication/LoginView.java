package nl.fairbydesign.views.authentication;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.ApplicationServiceInitListener;
import nl.fairbydesign.backend.Config;
import nl.fairbydesign.backend.credentials.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Login and authentication system based on iRODS
 */
@Route("login")
@PageTitle("Login | Vaadin CRM")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

    private final LoginForm login = new LoginForm();
    private static final Logger logger = LoggerFactory.getLogger(LoginView.class);

    public LoginView() {
        addClassName("login-view");
        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);

        login.setAction("login");

        // To cookie or not to cookie
//        Cookie[] cookies = VaadinRequest.getCurrent().getCookies();
//        for (Cookie cookie : cookies) {
//            logger.debug(">> Present cookies " + cookie.getName() + " " + cookie.getValue());
//        }


        // The vaadin default login form
        LoginForm component = new LoginForm();

        // Get config file content
        Config config = ApplicationServiceInitListener.getConfig();

        // irodsHost
        TextField hostField = new TextField();
        hostField.setLabel("Host");
        hostField.setValue(config.getIRODSHost());

        // irodsZone
        TextField zoneField = new TextField();
        zoneField.setLabel("Zone");
        zoneField.setValue(config.getIRODSZone());

        // irodsPort
        TextField portField = new TextField();
        portField.setPattern("[0-9]*");
        portField.setPreventInvalidInput(true);
        portField.setMaxLength(4);
        portField.setLabel("Port");
        portField.setValue(String.valueOf(config.getIRODSPort()));

        // irodsSSL
        Select<String> sslNegPolicy = new Select<>();
        sslNegPolicy.setLabel("SSL Negotiation Policy");
        sslNegPolicy.setItems("CS_NEG_REQUIRE", "CS_NEG_REFUSE");
        sslNegPolicy.setValue(config.getIRODSSSLNegotiationPolicy());

        // irodsAuthScheme
        Select<String> authScheme = new Select<>();
        authScheme.setLabel("Authentication schema");
        authScheme.setItems("STANDARD", "PASSWORD", "PAM");
        authScheme.setValue(config.getIRODSAuthScheme());

        // Dialog
        Dialog dialog = new Dialog();
        add(dialog);

        // Hides the forgot password button as it is not implemented
        component.setForgotPasswordButtonVisible(false);

        // Upon login the session is coupled to the credentials
        component.addLoginListener(event -> {
            // Credentials object
            Credentials credentials = new Credentials();
            // Store it in this session
            VaadinSession.getCurrent().setAttribute("credentials", credentials);

            // Set credentials
            credentials.setUsername(event.getUsername());
            credentials.setPassword(event.getPassword());
            // Standard credential settings
            credentials.setHost(hostField.getValue());
            credentials.setPort(Integer.parseInt(portField.getValue()));
            credentials.setZone(zoneField.getValue());
            // Negotiation policies
            credentials.setSslNegotiationPolicy(sslNegPolicy.getValue());
            // PAM / SSL authentication
            credentials.setAuthenticationScheme(authScheme.getValue());
            // Certificate when available
            if (config.getIRODScertificateTrustStore() != null) {
                credentials.setCertificateTrustStore(config.getIRODScertificateTrustStore());
                credentials.setCertificateTrustStorePassword(config.getIRODScertificateTrustStorePassword());
            }

            // Perform authentication
            boolean isAuthenticated = authenticate(credentials);

            // When authenticated navigate to frontpage
            if (isAuthenticated) {
                UI.getCurrent().navigate("");
            } else {
                component.setError(true);
            }
        });

        // Forgot password module, still in progress not sure if needed with LDAP implementation on the way
        component.addForgotPasswordListener(event -> {
            // Clear dialog
            dialog.removeAll();
            // Add fields
            TextField labelField = new TextField();
            labelField.setLabel("Username");
            Button newPassword = new Button("New password please");
            dialog.add(new Text("Please fill in your username"), labelField, newPassword);
            dialog.setWidth("300px");
            dialog.setHeight("200px");
            dialog.open();
            // Clicked on forgot password what now?
            newPassword.addClickListener(buttonClickEvent -> {
                boolean status = false;
                if (labelField.getValue().length() > 0) {
                    // status = sentEmail(labelField.getValue());
                } else {
                    labelField.setInvalid(true);
                }
                dialog.close();
                if (status)
                    Notification.show("A new password request has been sent to you", 5000, Notification.Position.MIDDLE);
                else
                    Notification.show("Failed to sent a new password request please contact us", 5000, Notification.Position.MIDDLE);
            });
        });

        AtomicBoolean disabled = new AtomicBoolean(false);
        UI.getCurrent().getPage().executeJs("return window.location.href").then(String.class, location -> {
            if (location.contains("localhost")) {
                // Login enabled
            } else if (location.contains("m-unlock.nl")) {
                // Login enabled
            } else {
                disabled.set(true);
            }
        });

        Button anonymousLogin = new Button("Anonymous login");
        anonymousLogin.addClickListener(click -> {
            Credentials credentials = new Credentials();
            VaadinSession.getCurrent().setAttribute("credentials", credentials);

            // Set credentials
            credentials.setUsername("anonymous");
            credentials.setPassword("");
            // Standard credential settings
            credentials.setHost(hostField.getValue());
            credentials.setPort(Integer.parseInt(portField.getValue()));
            credentials.setZone(zoneField.getValue());
            // Negotiation policies
            credentials.setSslNegotiationPolicy(sslNegPolicy.getValue());
            // PAM / SSL authentication
            credentials.setAuthenticationScheme(authScheme.getValue());
            // Certificate when available
            if (config.getIRODScertificateTrustStore() != null) {
                credentials.setCertificateTrustStore(config.getIRODScertificateTrustStore());
                credentials.setCertificateTrustStorePassword(config.getIRODScertificateTrustStorePassword());
            }

            boolean isAuthenticated = authenticate(credentials);

            if (isAuthenticated) {
                UI.getCurrent().navigate("");
            } else {
                component.setError(true);
            }
        });


        // Authentication is disabled when it is not localhost or not running from unlock domain
        if (disabled.get() && LoginView.class.getResource("LoginView.class").toString().contains("jar")) {
            add(new H1("FAIRDS Login disabled"));
        } else {
            // Add the login bells and whistles to the page
            add(component);
            Accordion accordion = new Accordion();
            VerticalLayout irodsSettings = new VerticalLayout();
            irodsSettings.add(hostField, zoneField, portField, sslNegPolicy, authScheme);
            // Anonymous login added
            add(anonymousLogin);
            // Accordion for settings
            accordion.add("iRODS Settings", irodsSettings);
            accordion.close();
            add(accordion);
        }
    }

    /**
     * Performs the simple authentication in the backend and shows error if not successful.
     * @param credentials object with authentication information
     * @return if authentication is successful
     */
    private boolean authenticate(Credentials credentials) {
        credentials.authenticate();
        if (credentials.isSuccess()) {
            logger.info("Successful login for " + credentials.getIrodsAccount().getUserName());
            return true;
        } else {
            Notification.show("Authentication failed " + credentials.getMessage(), 5000, Notification.Position.MIDDLE);
            logger.error("Failed attempt for " + credentials.getIrodsAccount().getUserName());
            return false;
        }
    }

    /**
     * add cookie implementation if needed
     * @param key for the cookie
     * @param value for the cookie
     */
    public static void addCookie(String key, String value) {
        Cookie myCookie = new Cookie(key, value);

        // Make cookie expire in 1 year
        myCookie.setMaxAge(60 * 60 * 24 * 7 * 52);

        // Set the cookie path.
        myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());

        // Save cookie
        VaadinService.getCurrentResponse().addCookie(myCookie);
    }

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            login.setError(true);
        }
    }
}