package nl.fairbydesign.views.authentication;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.views.main.MainView;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.UserGroupAO;
import org.irods.jargon.core.pub.domain.UserGroup;

@PageTitle("User overview")
@Route(value = "user", layout = MainView.class)
@CssImport("./styles/views/empty/empty-view.css")
public class UserView extends VerticalLayout implements BeforeEnterObserver {

    public UserView() {
        // User info, groups
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null) {
            // logout button
            Button logoutButton = new Button("Log out");
            logoutButton.addClickListener(click -> logoutButton.getUI().ifPresent(ui -> ui.navigate("logout")));
            add(logoutButton);

            VerticalLayout verticalLayout = new VerticalLayout();
            add(verticalLayout);
            String content = "<div><h1>User information</h1>";

            content += "<br>Username: " + credentials.getIrodsAccount().getUserName();
            content += "<br>Zone: " + credentials.getIrodsAccount().getZone();
            content += "<br>Connected at: " + credentials.getIrodsAccount().getHost();
            content += "<br>Authentication scheme at: " + credentials.getAuthenticationScheme();
            content += "<br>SSL negotiation policy: " + credentials.getSslNegotiationPolicy();
            content += "<br>Port: " + credentials.getPort();
            content += "<br>Certificate Trust Store: " + credentials.getCertificateTrustStore();


            // Obtain groups
            try {
                content += "<br><h2>Groups:</h2>";
                IRODSFileSystem irodsFileSystem = IRODSFileSystem.instance();
                IRODSAccessObjectFactory accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();
                UserGroupAO userGroupAO = accessObjectFactory.getUserGroupAO(credentials.getIrodsAccount());

                for (UserGroup userGroup : userGroupAO.findAll()) {
                    if (userGroupAO.isUserInGroup(credentials.getIrodsAccount().getUserName(), userGroup.getUserGroupName())) {
                        content += "<br>" + userGroup.getUserGroupName();
                    }
                }
            } catch (JargonException e) {
                e.printStackTrace();
            }
            content += "<br>";
            Html html = new Html(content);
            add(html);
        } else {
            add(new Label("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
    }
}