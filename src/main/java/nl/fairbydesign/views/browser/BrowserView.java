package nl.fairbydesign.views.browser;


import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.AVU;
import nl.fairbydesign.backend.data.objects.DiskUsage;
import nl.fairbydesign.backend.data.objects.Item;
import nl.fairbydesign.backend.irods.Browser;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.views.main.MainView;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.jboss.logging.Logger;
import org.vaadin.stefan.LazyDownloadButton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

import static com.vaadin.flow.component.grid.GridVariant.LUMO_ROW_STRIPES;
import static nl.fairbydesign.backend.WebGeneric.humanReadableByteCountBin;
import static nl.fairbydesign.backend.data.objects.Item.Type.FILE;
import static nl.fairbydesign.backend.data.objects.Item.Type.FOLDER;
import static nl.fairbydesign.backend.irods.Data.deleteFile;
import static nl.fairbydesign.backend.irods.Data.downloadFile;

@Route(value = "browser", layout = MainView.class)
@PageTitle("Browser")
@CssImport("./styles/views/empty/empty-view.css")
public class BrowserView extends Div implements BeforeLeaveObserver {
    private IRODSFileFactory fileFactory;
    private List<Item> filesAndFolders = new ArrayList<>();
    private TextField pathBar;
    private Grid<Item> browserGrid = new Grid<>(Item.class);
    private Grid<AVU> avuGrid;
    public static final Logger logger = Logger.getLogger(BrowserView.class);
    private HashSet<Checkbox> checkboxes = new HashSet<>();
    private boolean threadStop = false;

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        logger.error("Person leaves page!");
        threadStop = true;
    }
    public BrowserView() {
        setId("master-detail-view");

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        // Layout for the overall page
        VerticalLayout verticalLayout = new VerticalLayout();

        if (credentials != null && credentials.isSuccess()) {

            try {
                fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount());
            } catch (JargonException e) {
                e.printStackTrace();
            }

            // Top bar containing the current path
            pathBar = new TextField("Folder path");
            if (credentials.getIrodsAccount().getUserName().contains("anonymous")) {
                pathBar.setValue("/" + credentials.getIrodsAccount().getZone());
            } else {
                pathBar.setValue("/" + credentials.getIrodsAccount().getZone() + "/home/" + credentials.getIrodsAccount().getUserName());
            }
            pathBar.setWidthFull();

            // Top grid containing the files
            // Grid<Item> browserGrid = new Grid<>(Item.class);
            // diskUsageGrid.setItems(diskUsages);
            ListDataProvider<Item> dataProvider = new ListDataProvider<>(filesAndFolders);
            browserGrid.setDataProvider(dataProvider);
            browserGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            browserGrid.addThemeVariants(LUMO_ROW_STRIPES);

            // Bottom left grid containing the metadata grid in the metadata tab
            avuGrid = new Grid<>(AVU.class);

            // Set the columns for the file browser
            browserGrid.setColumns("name", "modified", "size");

            // Set the columns for the metadata
            avuGrid.setColumns("attribute", "value", "unit");
            avuGrid.setWidthFull();

            // Layouts
            HorizontalLayout metadataHorizontalLayout = new HorizontalLayout();
            VerticalLayout verticalLayoutMetaDataFields = new VerticalLayout();
            TextField attribute = new TextField("Attribute");
            TextField value = new TextField("Value");
            TextField unit = new TextField("Unit");
            // Creating the buttons for metadata modifications
            Button buttonDeleteMetadata = new Button("Delete");
            Button buttonModifyMetadata = new Button("Modify");
            Button buttonAddMetadata = new Button("Add");
            // Disabled by default as no element is selected
            buttonDeleteMetadata.setEnabled(false);
            buttonModifyMetadata.setEnabled(false);

            HorizontalLayout metadataButtons = new HorizontalLayout();
            metadataButtons.add(buttonAddMetadata, buttonModifyMetadata, buttonDeleteMetadata);
            verticalLayoutMetaDataFields.add(attribute, value, unit, metadataButtons);
            metadataHorizontalLayout.add(avuGrid, verticalLayoutMetaDataFields);
            // metadataHorizontalLayout.setVisible(true);
            metadataHorizontalLayout.setWidthFull();
            avuGrid.setMaxWidth("50%");

            // Upload functionalities
            MemoryBuffer buffer = new MemoryBuffer();
            Upload upload = new Upload(buffer);
            upload.setMaxFiles(25);
            upload.setDropLabel(new Label("Upload a datafile to drop in the current folder you are viewing"));

            Dialog uploadDialog = new Dialog();
            uploadDialog.add(upload);

            /**
             * LISTENER SECTION
             */

            // Listener for double-click on file browser
            browserGrid.addItemDoubleClickListener(folderItemDoubleClickEvent -> {
                if (folderItemDoubleClickEvent.getItem().getType().equals(FOLDER)) {
                    checkboxes = new HashSet<>();
                    pathBar.setValue(folderItemDoubleClickEvent.getItem().getPath());
                    getContent(credentials.getIrodsAccount(), pathBar.getValue());
                    browserGrid.setItems(filesAndFolders);
                } else {
                    Notification.show("FILE CLICKED... TODO", 1000, Notification.Position.BOTTOM_CENTER);
                }
            });

            // Add ENTER listener when typing a path and hit enter
            pathBar.addKeyDownListener(Key.ENTER, e -> {
                if (pathBar.getValue().length() == 0) {
                    pathBar.setValue("/");
                }
                getContent(credentials.getIrodsAccount(), pathBar.getValue());
                browserGrid.setItems(filesAndFolders);
            });

            // Add listener to the bar
            pathBar.addAttachListener(attachEvent -> {
                getContent(credentials.getIrodsAccount(), pathBar.getValue());
                browserGrid.setItems(filesAndFolders);
            });

            // Listener when selecting a metadata item and deselecting an item
            avuGrid.addItemClickListener(avuItemClickEvent -> {
                if (avuGrid.getSelectedItems().size() == 0) {
                    attribute.setValue("");
                    value.setValue("");
                    unit.setValue("");
                    buttonDeleteMetadata.setEnabled(false);
                    buttonModifyMetadata.setEnabled(false);
                } else {
                    AVU item = avuItemClickEvent.getItem();
                    attribute.setValue(item.getAttribute());
                    value.setValue(item.getValue());
                    unit.setValue(item.getUnit());
                    buttonDeleteMetadata.setEnabled(true);
                    buttonModifyMetadata.setEnabled(true);
                }
            });

            // Listener when the add button is clicked
            buttonAddMetadata.addClickListener(buttonClickEvent -> {
                Set<Item> items = new HashSet<>();
                if (browserGrid.getSelectedItems().size() == 0) {
                    // Check if the select column was used
                    for (Item item : browserGrid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())) {
                        if (item.isSelected()) {
                            items.add(item);
                        }
                    }
                    Notification.show("Multi select option used to add metadata");
                } else {
                    items = browserGrid.getSelectedItems();
                }
                for (Item item : items) {
                    Browser.addMetadata(credentials.getIrodsAccount(), item, attribute.getValue(), value.getValue(), unit.getValue());
                }
                avuGrid.getDataProvider().refreshAll();

                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            // Listener when the delete button is clicked
            buttonDeleteMetadata.addClickListener(buttonClickEvent -> {
                Item item = browserGrid.getSelectedItems().iterator().next();
                if (avuGrid.getSelectedItems().size() == 0) {
                    Notification.show("No AVU element selected");
                } else {
                    Browser.deleteMetadata(credentials.getIrodsAccount(), item, avuGrid.getSelectedItems().iterator().next());
                }

                avuGrid.setItems(item.getAvus());
                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            // Listener when the modify button is clicked
            buttonModifyMetadata.addClickListener(buttonClickEvent -> {
                // Delete then add
                Item item = browserGrid.getSelectedItems().iterator().next();
                item = Browser.deleteMetadata(credentials.getIrodsAccount(), item, avuGrid.getSelectedItems().iterator().next());
                item = Browser.addMetadata(credentials.getIrodsAccount(), item, attribute.getValue(), value.getValue(), unit.getValue());
                avuGrid.setItems(item.getAvus());
                // When the add button is clicked the AVU is added...
                // Then the grid needs to be cleared and reloaded
                avuGrid.getDataProvider().refreshAll();
                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            verticalLayout.setMaxWidth("100%");
            HorizontalLayout pathBarAndUpload = new HorizontalLayout();
            pathBarAndUpload.setWidthFull();
            Button uploadButton = new Button("", new Icon(VaadinIcon.UPLOAD));
            pathBarAndUpload.add(pathBar, uploadButton);
            verticalLayout.add(pathBarAndUpload, browserGrid, metadataHorizontalLayout); // , tabs, pages);
            add(verticalLayout);

            // Upload button listener
            uploadButton.addClickListener(click -> {
                uploadDialog.open();
            });

            // Upload listener
            upload.addSucceededListener(succeededEvent -> {
                // Check if directory is writable
                try {
                    boolean writable = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount()).instanceIRODSFile(pathBar.getValue()).canWrite();
                    if (writable) {
                        File localFile = new File("upload/" + UUID.randomUUID());
                        // Temp folder?
                        localFile.getParentFile().mkdirs();
                        // Stream to server to stream to irods but this should be done in one go?
                        Files.copy(buffer.getInputStream(), localFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        // To iRODS using current browser path
                        File targetFile = new File(pathBar.getValue() + "/" + succeededEvent.getFileName());
                        Data.uploadIrodsFile(credentials.getIrodsAccount(),localFile, targetFile);
                        IRODSFile irodsFile = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount()).instanceIRODSFile(targetFile.getAbsolutePath());
                        if (irodsFile.exists()) {
                            Notification.show("Upload successful of " + succeededEvent.getFileName());
                            localFile.delete();
                        }
                    } else {
                        Notification.show("You have no write access to this folder");
                    }
                } catch (JargonException | IOException e) {
                    Notification.show("Upload failed");
                    logger.error(e.getMessage());
                }
            });

            upload.addAllFinishedListener(listener -> {
                // listener.unregisterListener(); // is this needed?
                uploadDialog.close();
                getContent(credentials.getIrodsAccount(), pathBar.getValue());
                browserGrid.setItems(filesAndFolders);
            });

            // Add info, almost working
            browserGrid.addItemClickListener(click -> {
                Item item = Browser.getAVUs(credentials.getIrodsAccount(), click.getItem());
                avuGrid.setItems(item.getAvus());
                // metadataHorizontalLayout.setVisible(true);
            });

            // Add optional delete
            browserGrid.addColumn(new ComponentRenderer<>(item -> {
                Button button = new Button("", new Icon(VaadinIcon.TRASH));
                button.setEnabled(item.isTrash());
                button.setVisible(item.isTrash());
                if (item.isTrash()) {
                    button.addClickListener(click -> {
                        // Create and open dialog with yes no option
                        Dialog dialog = new Dialog();
                        dialog.add(new Text("Do you really want to delete"));
                        dialog.add(new Text(item.getPath()));
                        Button yes = new Button("yes");
                        Button no = new Button("no");
                        // Add buttons horizontally below the text.
                        dialog.add(new HorizontalLayout(yes, no));
                        no.addClickListener(clickEvent -> {
                            dialog.close();
                        });

                        yes.addClickListener(clickEvent -> {
                            // Delete file?
                            try {
                                // Delete given file
                                String uuid = item.getUuid();
                                deleteFile(item.getPath(), credentials.getIrodsAccount());
                                // Remove item from grid list
                                Item removeMe = null;
                                for (Item toDelete : filesAndFolders) {
                                    if (toDelete.getUuid().contains(uuid)) {
                                        removeMe = toDelete;
                                        break;
                                    }
                                }
                                filesAndFolders.remove(removeMe);
                                browserGrid.setItems(filesAndFolders);
                            } catch (JargonException e) {
                                Notification.show("Failed to delete " + item.getName());
                            }
                            dialog.close();
                        });
                        dialog.open();
                    });
                }
                return button;
            })).setHeader("Trash").setWidth("10px");

            // Add optional download
            browserGrid.addColumn(new ComponentRenderer<>(item -> {
                LazyDownloadButton button = new LazyDownloadButton("Download",VaadinIcon.DOWNLOAD.create(), () -> item.getName(), () -> {
                        try {
                            System.err.println("ITEM PATH AFTER CLICK " + item.getPath());
                            downloadFile(fileFactory, credentials.getIrodsAccount(), item.getPath());
                            return Files.newInputStream(Paths.get("." + item.getPath()));
                        } catch (JargonException | IOException e) {
                            throw new RuntimeException(e);
                        }
                    });

                button.setVisible(item.getType().equals(FILE));
                button.setEnabled(item.getType().equals(FILE));

                return button;
            })).setHeader("Download").setWidth("10px");

            // TODO, fix direct stream download, might need an irods update...
//            browserGrid.addColumn(new ComponentRenderer<>(item -> {
//                LazyDownloadButton button = new LazyDownloadButton(VaadinIcon.DOWNLOAD.create(), () -> item.getName(), () -> {
//                    try {
//                        System.err.println("ITEM PATH AFTER CLICK " + item.getPath());
//                        // IRODSFile irodsFile = fileFactory.instanceIRODSFile(item.getPath());
//                        IRODSFileInputStream irodsFileInputStream = fileFactory.instanceIRODSFileInputStreamWithRerouting(item.getPath());
//                        return irodsFileInputStream;
//                    } catch (JargonException e) {
//                        throw new RuntimeException(e);
//                    }
//                });
//
//                if (item.isDownload()) {
//                    button.setVisible(item.isDownload());
//                    button.setEnabled(item.isDownload());
//                }
//
//                return button;
//            })).setHeader("Download 2").setWidth("10px");

            // Add optional multi select
            browserGrid.addColumn(new ComponentRenderer<>(item -> {
                // create checkbox
                Checkbox checkbox = new Checkbox();
                checkboxes.add(checkbox);
                // invisible and not selected for folders
                if (item.getName().equals("...")) {
                    checkbox.setLabel("ALL");
                } else if (item.getType().equals(FOLDER)) {
                    checkbox.setEnabled(false);
                    checkbox.setVisible(false);
                }
                // Listener for value change
                checkbox.addValueChangeListener(checkValue -> {
//                    logger.info("Checkbox for " + item.getName() + " changed to " + checkValue.getValue());
//                    item.setSelected(checkValue.getValue());
//                    // If the ... checkbox is selected then all in there are selected
                    if (item.getName().equals("...")) {
                        checkboxes.forEach(checkbox1 -> checkbox1.setValue(checkValue.getValue()));
                    }
//                        // Change all items to true
//                        List<Item> items = browserGrid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
//                        for (Item item1 : items) {
//                            if (item1.getType().equals(FILE)) {
//                                item1.setSelected(checkValue.getValue());
//                                logger.info(item1.getName() + " changed to " + item1.isSelected());
//                            }
//                        }
//                        // logger.info("Checkbox for " + items.get(1).getName() + " changed to " + items.get(1).isSelected());
//                        browserGrid.setItems(items);
//                        browserGrid.getDataProvider().refreshAll();
//                        // items = browserGrid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
//                        // logger.info("Checkbox for " + items.get(1).getName() + " changed to " + items.get(1).isSelected());
//                    }
                });
                return checkbox;
            })).setHeader("Multi edit").setWidth("10px");

        } else {
            add(new Label("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }

    public void getContent(IRODSAccount irodsAccount, String path) {
        // Temp disable grid?
        browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.setEnabled(false)));

        // Reset files and folders
        setFilesAndFolders(new ArrayList<>());
        // Add go to parent option
        addFilesAndFolders(new Item("...Loading",0L, "", Item.Type.FOLDER, new File(path).getParent()));

        // List<Item> folderList = new ArrayList<>();
        // List<Item> fileList = new ArrayList<>();
        Thread t = new Thread(() -> {
            try {
                IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);

                IRODSFile irodsPath = fileFactory.instanceIRODSFile(path);

                // An issue when an illegal path is given most likely an empty path
                try {
                    irodsPath.isDirectory();
                } catch (IllegalArgumentException e) {
                    // When something goes wrong reset to root path
                    irodsPath = fileFactory.instanceIRODSFile("/");
                    logger.error("WARNING iRODS path changed to " + irodsPath.getAbsolutePath());
                }

                if (irodsPath.isDirectory()) {
                    File[] files = irodsPath.listFiles();
                    for (int i = 0; i < files.length; i++) {
                        // Only loop present when leaving we should "exit" the thread...
                        if (threadStop) continue;
                        File file = files[i];
                        IRODSFile irodsFile = fileFactory.instanceIRODSFile(file.getAbsolutePath());
                        if (irodsFile.isHidden()) {
                            // ignore hidden files... When is a file hidden?
                        } else if (irodsFile.isDirectory()) {
                            Item item = new Item(irodsFile.getName(), 0L, "-", Item.Type.FOLDER, irodsFile.getAbsolutePath());
                            item.setInfo(true);
                            filesAndFolders.add(item);
                        } else {
                            Item item = new Item(irodsFile.getName(), irodsFile.lastModified(), humanReadableByteCountBin(irodsFile.length()), Item.Type.FILE, irodsFile.getAbsolutePath());
                            item.setTrash(true);
                            item.setInfo(true);
                            item.setDownload(true);
                            filesAndFolders.add(item);
                        }
                        // Refresh grid ever nth file?
                        if (i % 10 == 0) {
                            browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.getDataProvider().refreshAll()));
                        }
                    }
                }
                // Final refresh...
                filesAndFolders.get(0).setName("...");
                browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.getDataProvider().refreshAll()));
                browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.setEnabled(true)));

            } catch (JargonException e) {
                e.printStackTrace();
            }
        });
        t.start();
//        folderList.add(new Item("Uh oh...", 0L, "-", Item.Type.UNKNOWN, ""));
//        return folderList;
    }


    public void setFilesAndFolders(List<Item> filesAndFolders) {
        this.filesAndFolders = filesAndFolders;
    }

    public void addFilesAndFolders(Item item) {
        this.filesAndFolders.add(item);
    }
}
