package nl.fairbydesign.views.validation;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.parsers.ExcelValidator;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;
import org.irods.jargon.core.connection.IRODSAccount;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.*;
import java.nio.file.StandardCopyOption;

import static com.vaadin.flow.component.notification.Notification.Position.MIDDLE;
import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;

@Route(value = "validate", layout = MainView.class)
@PageTitle("Ingestion engine")
@CssImport("./styles/views/empty/empty-view.css")
public class ValidationView extends Div {

    public ValidationView() {

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null && !credentials.isSuccess()) {
            Label label = new Label("If you can login your metadata will be automatically uploaded into the data storage system");
            add(label);
        }

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setMaxFiles(1);
        upload.setDropLabel(new Label("Upload a project excel file in .xlsx format"));

        TextArea logArea = new TextArea("Description");
        TextArea stacktraceArea = new TextArea("Stacktrace");
        Button stacktraceButton = new Button("Stacktrace");
        stacktraceButton.setVisible(false);
        stacktraceButton.setEnabled(false);
        // TODO find xlsx file type
        // upload.setAcceptedFileTypes("xlsx");
        // upload.setMaxFileSize(300);

        logArea.setWidthFull();
        logArea.setPlaceholder("Log will appear here ...");
        logArea.setVisible(false);

        stacktraceArea.setWidthFull();
        stacktraceArea.setVisible(false);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);

        add(upload);
        add(progressBar);
        add(logArea);
        add(stacktraceButton);
        add(stacktraceArea);

        stacktraceButton.addClickListener(clicked -> {
            if (stacktraceArea.isVisible()) {
                stacktraceArea.setVisible(false);
                stacktraceArea.setEnabled(false);
            } else {
                stacktraceArea.setVisible(true);
                stacktraceArea.setEnabled(true);
            }
        });

        upload.addFileRejectedListener(event -> Notification.show(event.getErrorMessage()));

        upload.addSucceededListener(succeededEvent -> {
            // Copy to close buffer
            try {
                java.nio.file.Files.copy(
                        buffer.getInputStream(),
                        new File("./fairds_storage/" + buffer.getFileName()).toPath(),
                        StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                System.err.println("Copy inputstream failed");
            }
            String excelFileName = buffer.getFileName();

            Thread t = new Thread(() -> {
                // When upload is successful start the Excel validator!
                if (!buffer.getFileName().endsWith("xlsx")) {
                    Notification notification = new Notification();
                    notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                    Div text = new Div(new Text("File format not accepted. Only excel files (.xlsx)"));
                    Button closeButton = new Button(new Icon("lumo", "cross"));
                    closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
                    closeButton.getElement().setAttribute("aria-label", "Close");
                    closeButton.addClickListener(event -> {
                        notification.close();
                        // Refresh current page
                        UI.getCurrent().getPage().reload();
                    });

                    HorizontalLayout layout = new HorizontalLayout(text, closeButton);
                    layout.setAlignItems(CENTER);
                    notification.setPosition(MIDDLE);
                    notification.add(layout);
                    notification.open();
                } else {
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setVisible(true)));
                    progressBar.getUI().ifPresent(ui -> ui.access(() -> {
                        progressBar.setVisible(true);
                        progressBar.setIndeterminate(true);
                    }));
                    ExcelValidator excelValidator = new ExcelValidator();
                    try {
                        IRODSAccount irodsAccount = null;
                        if (credentials != null) {
                            irodsAccount = credentials.getIrodsAccount();
                        }

                        InputStream inputStream = new FileInputStream("./fairds_storage/" + excelFileName);
                        String rdfPath = excelValidator.validate(excelFileName, inputStream, irodsAccount, logArea);
                        File rdfFile = new File(rdfPath);
                        if (excelValidator.getMessage().contains("Is this an excel file?")) {
                            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nInput file does not appear to be a valid XLSX file")));
                        } else {
                            if (credentials != null && credentials.isSuccess()) {
                                logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nValidation appeared to be successful please notify the data steward of this project")));
                            } else {
                                logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nValidation appeared to be successful.")));
                                if (rdfFile.exists()) {
                                    // Provide download button
                                    Button buttonDownload = new Button("DOWNLOAD RDF");
                                    this.getUI().ifPresent(ui -> ui.access(() -> {
                                        FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(new StreamResource(rdfFile.getName(), () -> createResource(rdfFile)));
                                        buttonWrapper.wrapComponent(buttonDownload);
                                        add(buttonWrapper);
                                        buttonDownload.setEnabled(true);
                                    }));
                                }
                            }
                        }
                    } catch (Exception e) {
                        logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(
                                "\n\n=================================================================\n\n" +
                                        "Your dataset did not pass the validation...\n\n" +
                                        "When it is unclear please provide the following message to your data steward\n\n" +
                                        excelValidator.getMessage() +
                                        "\n\nPlease validate your mappings in the excel file\n\n" +
                                        "\n\n=================================================================\n\n")));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setValue("Message & Stacktrace:\n\n" + e.getMessage() + "\n\n" + StringUtils.join(e.getStackTrace(), "\n"))));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setVisible(true)));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setEnabled(true)));
                        System.err.println(StringUtils.join(e.getStackTrace(), "\n"));
                    }
                }
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(false)));
            });
            t.start();
        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }

    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
