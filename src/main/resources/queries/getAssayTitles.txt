PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
PREFIX unlock: <http://m-unlock.nl/ontology/>
select ?desc ?title (COUNT(?title) AS ?count)
where {
    ?study unlock:assay ?assay .
    ?assay schema:description ?desc .
    ?assay schema:title ?title.
} GROUP BY ?title ?desc ?type