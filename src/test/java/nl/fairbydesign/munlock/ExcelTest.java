package nl.fairbydesign.munlock;

import nl.fairbydesign.backend.irods.Connection;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.MetadataParser;
import nl.fairbydesign.backend.parsers.ExcelValidator;
import org.apache.log4j.Logger;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

public class ExcelTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    @Test
    public void testSheetParsing() throws Exception {
        // If metadata file exists locally
        InputStream is;
        logger.info("Checking if " + new File("./fairds_storage/metadata.xlsx").getAbsolutePath() + " exists");
        if (new File("./fairds_storage/metadata.xlsx").exists()) {
            // Check modification date
            try {
                java.net.URL url = MetadataParser.class.getResource("/metadata.xlsx");
                System.err.println(url.getFile());
                System.out.println(new Date(url.openConnection().getLastModified()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            logger.info("Using local excel file");
            is = new FileInputStream(new File("./fairds_storage/metadata.xlsx"));
        } else {
            // else load xlsx from jar file
            logger.info("Using internal excel file");
            is = MetadataParser.class.getClassLoader().getResourceAsStream("metadata.xlsx");
        }

        Workbook workbook = WorkbookFactory.create(is);

        SortedMap<String, ArrayList<Metadata>> sheetObjects = new TreeMap<>();
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            logger.debug("Processing " + sheet.getSheetName() + " with " + sheet.getLastRowNum() + " entries");
            ArrayList<Metadata> metadataHashSet = MetadataParser.parser(sheet);
            sheetObjects.put(sheet.getSheetName(), metadataHashSet);
        }

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./fairds_storage/metadata.obj"));
        oos.writeObject(sheetObjects);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./fairds_storage/metadata.obj"));
        SortedMap<String, ArrayList<Metadata>> metadata = (SortedMap<String, ArrayList<Metadata>>) ois.readObject(); // cast is needed.
        ois.close();
        for (String s : metadata.keySet()) {
            System.err.println(s);
        }

    }

    @Test
    public void testProperty() throws Exception {
//        File excelFile = new File("PROJECT_ID-8.xlsx");
        File excelFile = new File("~/Downloads/mgnify_rhizosphere_selected_addition_modified.xlsx");
        if (excelFile.exists()) {
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
            POIXMLProperties props = workbook.getProperties(); // .getCustomProperties().getProperty("sample");
            POIXMLProperties.CustomProperties custProp = props.getCustomProperties();
            CTProperty ctProperty = custProp.getProperty("sample");
            System.err.println(ctProperty.getName());
            System.err.println(ctProperty.getLpwstr());
        }
    }

    @Test
    public void validate() throws Exception {
//        File excelFile = new File("/Users/jasperk/BO3B.xlsx");
        File excelFile = new File("./src/test/resources/BO3B.xlsx");
        if (excelFile.exists()) {
            FileInputStream inputStream = new FileInputStream(excelFile);
            ExcelValidator excelValidator = new ExcelValidator();
            Connection connection = new Connection();
            excelValidator.validate(excelFile.getName(), inputStream, connection.getIrodsAccount(), null);
        } else {
            logger.error("Test file does not exists");
        }
    }
}
