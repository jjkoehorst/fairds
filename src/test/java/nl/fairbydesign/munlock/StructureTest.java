package nl.fairbydesign.munlock;

import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.io.*;

public class StructureTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    @Test
    public void testStructure() throws Exception {
        File rdfFile = new File("BO3B.ttl");
        if (rdfFile.exists()) {
            nl.fairbydesign.backend.parsers.Structure.create(rdfFile.getAbsolutePath());
        }
    }
}
