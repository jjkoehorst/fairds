package nl.fairbydesign.munlock;

import com.google.gson.Gson;
import com.vaadin.flow.component.textfield.TextField;
import nl.fairbydesign.backend.Config;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;

public class ConfigTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    @Test
    public void testStructure() throws Exception {
        String userHome = System.getProperty("user.home");
        File configFile = new File(userHome + "/.fairds/config.json");
        // If file does not exists on dev machine
        if (!configFile.exists()) {
            InputStream inputStream = Util.getResourceFile("config.json");
            // Ensure that the folder is created
            configFile.getParentFile().mkdirs();
            // Copy to local config path
            Files.copy(inputStream, configFile.toPath());
            inputStream.close();
        }

        String content = new String(Files.readAllBytes(configFile.toPath()));
        Config config = new Gson().fromJson(content, Config.class);

        HashMap<String, ArrayList> configMap = new HashMap<>();
        try {
            for (PropertyDescriptor propertyDescriptor :
                    Introspector.getBeanInfo(Config.class, Object.class).getPropertyDescriptors()) {
                Method readMethod = propertyDescriptor.getReadMethod();
                Method writeMethod = propertyDescriptor.getWriteMethod();
                ArrayList<Method> methods = new ArrayList<>();
                methods.add(readMethod);
                methods.add(writeMethod);
                configMap.put(readMethod.getName(), methods);
                System.err.println(readMethod.getName().replaceAll("^get","") + ": " + readMethod.invoke(config, (Object[])null));
            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }

        ArrayList<TextField> fields = new ArrayList<>();
        for (String readMethod : configMap.keySet()) {
            TextField labelField = new TextField(readMethod);
            labelField.setId(readMethod);
            fields.add(labelField);
        }
    }
}
