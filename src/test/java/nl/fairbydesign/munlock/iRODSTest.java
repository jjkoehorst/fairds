package nl.fairbydesign.munlock;

import nl.fairbydesign.backend.data.objects.Process;
import nl.fairbydesign.backend.irods.Connection;
import org.irods.jargon.core.exception.JargonException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static nl.fairbydesign.backend.irods.Data.getPIS;
import static nl.fairbydesign.backend.irods.Data.logger;

public class iRODSTest {

    @Test
    public void pisTest()  {
        try {
            Connection connection = new Connection();
            System.err.println(connection.getIrodsAccount());
            ArrayList<Process> processes = getPIS(connection.getIrodsAccount());
            for (Process process : processes) {
                System.err.println(process.getPath());
            }
        } catch (JargonException e) {
            logger.error("Authentication failed, cant perform test");
        }
    }
}
