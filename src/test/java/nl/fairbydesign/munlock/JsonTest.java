package nl.fairbydesign.munlock;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.kubernetes.client.proto.Meta;
import nl.fairbydesign.backend.json.*;
import nl.fairbydesign.backend.json.ItemProperties;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.MetadataParser;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class JsonTest {
    public static final Logger logger = Logger.getLogger(JsonTest.class);

    @Test
    public void testSheetParsingToJson() throws Exception {
        // Sent Excel file to be converted to JSON
        String xlsxFile = "./src/main/resources/metadata.xlsx";

        MetadataParser.setXlsxFile(new File(xlsxFile));
        HashMap<String, ArrayList<Metadata>> metadataObjects = MetadataParser.main();

        // For each sheet process all packages
        for (String sheet : metadataObjects.keySet()) {
            // Create a placeholder for all of the packages for a given sheet
            HashMap<String, ArrayList<Metadata>> packages = new HashMap<>();
            // Loop through the memory excel sheet metadata objects
            for (Metadata metadata : metadataObjects.get(sheet)) {
                if (!packages.containsKey(metadata.getPackageIdentifier())) {
                    packages.put(metadata.getPackageIdentifier(), new ArrayList<>());
                }
                packages.get(metadata.getPackageIdentifier()).add(metadata);
            }
            // After sheet has been processed, default is to be added to all packages of this sheet
            ArrayList<Metadata> metadataDefault = packages.get("default");
            for (String packageName : packages.keySet()) {
                if (!packageName.equalsIgnoreCase("default")) {
                    packages.get(packageName).addAll(metadataDefault);
                }
            }

            for (String packageIdentifier : packages.keySet()) {
                ArrayList<Metadata> metadataArrayList = packages.get(packageIdentifier);

                Root root = new Root();
                root.set$async(true);
                root.setTitle(metadataArrayList.get(0).getPackageName());
                root.set$id(metadataArrayList.get(0).getPackageIdentifier());
                root.setVersion("1.0.0");
                root.set$schema("http://json-schema.org/draft-07/schema#");
                root.setAuthor("FAIR Data Station");
                root.setRequired(new ArrayList<>());
                root.getRequired().addAll(List.of("attributes"));
                Properties properties = new Properties();
                Attributes attributes = new Attributes();
                properties.setAttributes(attributes);
                attributes.setRequired(new ArrayList<>());

                HashMap<String, AttributeProperties> attributeProperties = new HashMap<>();
                attributes.setProperties(attributeProperties);

                for (Metadata metadata : metadataArrayList) {
                    if (metadata.getRequirement().equalsIgnoreCase("mandatory")) {
                        attributes.getRequired().add(metadata.getLabel());
                    }

                    attributeProperties.put(metadata.getLabel(), new AttributeProperties());
                    attributeProperties.get(metadata.getLabel()).setDescription(metadata.getDefinition());
                    // If there is no pattern or .*/.+
                    if (!metadata.getRegex().pattern().equalsIgnoreCase(".*")) {
                        Items items = new Items();
                        attributeProperties.get(metadata.getLabel()).setItems(items);
                        ArrayList<String> itemsRequired = new ArrayList<>();
                        // TODO check what this is?
                        itemsRequired.add("units");
                        items.setRequired(itemsRequired);
                        items.setProperties(new ItemProperties());
                        HashMap<String, String> itemValues = new HashMap<>();
                        itemValues.put("pattern", metadata.getRegex().pattern());
                        // Check for groups?
                        // checkForGroups(metadata.getRegex().pattern());
                        items.getProperties().setValue(itemValues);
                    }
                }
                root.setProperties(properties);
                root.setDescription("Automatic generation of " + sheet);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                objectMapper.writerWithDefaultPrettyPrinter();
                // Converting the Java object into a JSON string
                String jsonStr = objectMapper.writeValueAsString(root);
                // Manual fix for items null
                jsonStr = jsonStr.replaceAll("\"items\" : null", "\"items\" : {}");
                jsonStr = jsonStr.replaceAll("\"definitions\" : null", "\"definitions\" : {}");
                new File("json/validator/" + sheet).mkdirs();
                PrintWriter printWriter = new PrintWriter("json/validator/" + sheet + "/" + metadataArrayList.get(0).getPackageIdentifier() + ".json");
                printWriter.println(jsonStr);
                printWriter.close();
            }
        }
    }

    private void checkForGroups(String pattern) {
        pattern = pattern.replaceAll("[A-Za-z0-9 ]", "");
//        System.err.println(pattern);
//        System.err.println(pattern.matches("\\(\\|+\\)"));
    }
}