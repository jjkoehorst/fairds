package nl.fairbydesign.munlock;

import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static nl.fairbydesign.backend.metadata.MetadataParser.generateRegex;

public class RegexTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    @Test
    public void testRegex() {
        // Longer match
        String example = "example_with_more_than_10_chars";
        String syntax = "{text}{10,}";
        Pattern pattern = generateRegex(null, syntax, example);
        System.err.println(pattern.pattern());
        System.err.println(pattern.matcher(example).matches());
        Assert.assertTrue(pattern.matcher(example).matches());
//
//        if (pattern.pattern().matches(".*\\{[0-9]+,}.*")) {
//            System.err.println("A minimum length ("+pattern.pattern().replaceAll(".*?\\{([0-9]+),}.*", "$1")+") is set for " + metadata.getLabel()););
//        }

        // Exact match
        example = "example_10";
        syntax = "{text}{10}";
        pattern = generateRegex(null, syntax, example);
        System.err.println(pattern.pattern());
        System.err.println(pattern.matcher(example).matches());
        Assert.assertTrue(pattern.matcher(example).matches());

        // Short match
        example = "short_10";
        syntax = "{text}{1,10}";
        pattern = generateRegex(null, syntax, example);
        System.err.println(pattern.pattern());
        System.err.println(pattern.matcher(example).matches());
        Assert.assertTrue(pattern.matcher(example).matches());

        syntax = "{bla}";
        pattern = generateRegex(null, syntax, example);
        Assert.assertNull(pattern);
    }

    @Test
    public void simpleTest() {
        String syntax = "{text}{10}";
        syntax = syntax.replace("-", "\\-");
        boolean match = syntax.matches("\\{text\\}[0-9]+\\\\-");
        System.err.println(match);
    }

    @Test
    public void countTest() {
        String regex = "(PRJ[DEN][A-Z])";
        Pattern r = Pattern.compile(regex);
        Matcher m = r.matcher("PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299");
        while (m.find()) {
            System.out.println(m.group(0));
        }
        System.err.println("Detected " + m.groupCount() + " projects");
    }
}
